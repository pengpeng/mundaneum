 
 

 
 

 

618.11 GYNÉCOLOGIE. — PÉDIATRIE
618.11 Ovaires.

“12 Trompes de Fallope. F

19 Organes péri-utérins.

137.1 Ligaments péri-utérins.

SLT Ligaments larges.

Ter — ronds.

197,13 ee teisacrés.

197214 — tubo-ovariens: k

.137.2 Tissu cellulaire et péritoine. j

-137.3 Muscles. $

.137.4 Vaisseaux.

19720 Nerfs.

137 0e Cul-de-sac de Douglas.

.I4 Utérus et col utérin.

“477 Col de l'utérus.

.147.8 Corps de l'utérus.

147.9 Utérus gravide.

19 Vagin.

M

.16 Vulve. :

17 Troubles fonctionnels. Maladies de la menstrua- :
tons ; : net

LL Troubles de la période d'établissement de la mens-

truation. ne

“172 Troubles de la période d'activité de la menstruation.

179 Troubles de la ménopause.

-174  Ménorrhagie.

170 Dysménorrhée. Bis , :

.176 Aménorrhée. . 1

77 Stérilité. “4

.178 Leucorrhée.

.170 Autres troubles. .

.18 Affections du périnée chez la femme.

19 Glande mammaire, :

618.2 Obstétrique. Grossesse.

Classer à 618.2 (02) les Traités d'accouchement et d’obstétrique et tout ce qui a

trait à l'accouchement en général. ; Se Le
“21 Physiologie de la grossesse.
.22. Signes de la grossesse.
23 Durée dela grossesse.
.24 Hygiène de la grossesse.

 

F

É ÿ REA chene ht ne EP ES) ‘
DES es Re tr Ro E  o née hr EL ER

 
 

— - — RE 2

 

GYNÉCOLOGIE. — PÉDIATRIE 618.52
618.25 ! Grossesses multiples. Jumeaux.
.28 Grossesse chez des femmes ayant subi des opé-

rations gynécologiques.

618.3 Pathologie de la grossesse.

37 Grossesse extra-utérine.

On traitera la grossesse extra-utérine et chacune de ses subdivisions
comme une maladie. On aura, par conséquent, pour le traitement chirur-
gical de la grossesse abdominale, 618.318.0805 et on mettra à 68.31.07

2 l'anatomie pathologique de la grossesse extra-utérine:
IT Grossesse ovarienne.
.312 —  tubaire.
319 —  péri-utérine.
: : .314 — anormale.
5 "910 Le vaginale. y
.316 — intestinale.
.318 — , abdominale.
ik .32 Pathologie de l'œuf.
.33 ee du fœtus. Mort et rétention.
.34. 2 des annexes du fœtus.
+99 — de la caduque.
.36 — du placenta.
07 — de l'amnios. ;
O2 — du cordon ombilical.
.39 Avortements. Fausse-couches. Accouchements
prématurés.

618.4 Accouchement. Travail (Physiologie).

Voir aussi 173.4 Infanticide (Morale), 340.615 Avortement (Médecine légale)
343.621 Avortement (Droit pénal).

AT. Mécanisme du travail. =
.42 Présentations. Positions.

.43 Evolution clinique du travail.

.44. Direction du travail normal.

618.5 Pathologie du travail.

OI Anomalies du travail dépendant de l'impuissance :
des forces expulsives. 5
.52 Obstacles mécaniques.

 

 
 

 

 

618.53 GYNÉCOLOGIE-— PÉDIATRIE

 

618.53 Anomalies du fœtus.
.54 Hémorragies de l'accouchement. :
«55 Rupture et dilacération des voies génitales.
.06 Rétention du placenta.
07 Inversion de l'utérus.
.58 Prolapsus du cordon.
.59 Autres complications pathologiques.

618.6 Etat puerpéral (Physiologie, Hygiène).
Soins à donner, ycompris ceux intéressant l'enfant.
618.7 Pathologie de l’état puerpéral.

dyAE Maladies de la lactation. Fièvre de lait. :
.72 Fièvre puerpérale.

73 Métrite. Péritonite.

.74  Septicémie.

.75 Eclampsie puerpérale, |
.76 Manie puerpérale. ie . |
277 Phlébite. Thrombite. Phlegmatia. z
.78 Autres affections puerpérales.

.70 Mort subite après la délivrance. es

618.8 Opérations obstétricales.

.81 Levier. Forceps.
.82 Version.
.83 Embryotomie.

.84 Dilatation du col.

.85 Symphyséotomie. Se

.86 Opération césarienne.

.87 Ablation du placenta. |

.88 Provocation de l'accouchement.
_.89 Autres opérations obstétricales.

.89r - Antisepsie.

.892 Anesthésie obstétricale.

.893 Réduction de l’utérus gravide.

.895 Incision du col. s

.897 _ Opération de Porro.

 
 

 

 

 

 

 
 

 

 

 

 

 
 

ee

 

 

MÉDECINE COMPARÉE 619.19

 

619 Médecine comparée. Art vété-
rinaire.

On ne classe ici que la médecine comparée en général et tout ce qui concerne
- l'anatomie, la physiologie, la pathologie et la thérapeutique des animaux utiles
ou domestiques. Ces mêmes questions étudiées au point de vue des autres
animaux sont classées avec la Zoologie [59].
Voir aussi les divisions suivantes :
636 Zootechnie, élevage.
682.1 Maréchalerie,
614.9 Police sanitaire des animaux.
614.317 Inspection des viandes,

619 (o) Généralités. Ouvrages généraux.

L'art vétérinaire. et toutes ses one peuvent être combinées avec les
subdivisions de généralités, de forme, de lieu et de temps.
Voir les subdivisions FR As dont les principales ont été reproduites
sous 616 (0). Ex. :
619 (05) Revues de médecine vétérinaire.

619: Médecine comparée générale.

Ces questions sont classées ici à 610 : Suivi du nombre propre à châcune de
ces questions, conformément aux divisions de la médecine générale 611 à
618. Ex. : ; :

619:616.5 Les maladies du sang en médecine comparée.

619,1à.9 Art vétérinaire.

Les questions d’art vétérinaire propres à chaque espèce d’animaux domes-
… tiques sont classées de 619.1 à 610.9. Ex. :
610.4 Médecine vétérinaire du porc.

Ces divisions concordent avec celles de 636 Zootechnie. Elles peuvent être
subdivisées à leur tour comme la médecine comparée générale. Ex. :

619.4:616.5 Les maladies du sang chez le porc. :

Les maladies propres dux animaux et qui ne sont pas inscrites à la patho-
logie interne ni externe, à 616 et à 617, sont classées à 616.900 Autres maladies
générales. Ainsi on classera à

619.5 :616.009 Choléra des poules (qui n’a aucun FARAbre avec le
choléra humain). Fe
2 619.4 : 616.999 . Rouget du porc.

619.1 Les équidés domestiques.

‘IT Cheval.

12 Ane.

19 Mulet.

.19 Autres équidés domestiques.

 
 
 
