 

 

 

CONGRÈS MONDIAL
TEE . .
ASSOCIATIONS INTERNATIONALES
Deuxième sesgion : GAND-BRUXELLES, 19-19 juin 1913
Organisé par l'Union des Associations Internationales
Office central! : Bruxelles, 3bis, rue de in Régence

1913.04

Actes du Congrès. _— Documents préliminaires.

 

 

 

 

 

 

 

 

La deuxième session du Congrès mondial.

C'est du 15 au 18 juin prochain, qu'aura lieu à Gand-Bruxelles, la
deuxième session du Congrès Mondial des Associations Interna-
tionales. La lettre d'invitation, le programme du Congrès ainsi
que le questionnaire de l'enquête-referendum préalable aux rapports
généraux qui seront présentés sur les divers points de l'ordre du jour
précisent l'objet de la session.

Les adhésions des Associations Internationales arrivent nom-
breuses. Les associations qui n'ont pas encore désigné leurs délégnés
soné instamment priées de le faire.

À, -— Tutérét du Congrès pour les Associations.

Toutes les Associations Internationales ont un intérêt à prendre
part aux travaux du Congrès, pour les motifs suivants :

1. — D'abord les questions mêmes inscrites à l’ordre du jour ont
été choisies parmi celles qui directement intéressent tous les organismes
internationaux. Le Congrès est une vaste enquête, un échange de
vues sur des points que rencontrent dans leur gestion les dirigeants
de toutes les Associations.

2. — Le Congrès est un moyen offert à chaque Association defaïre
connaître à une élite internationale, les résultats généraux de l'œuvre
réalisée par elle et d'établir d’utiles relations. -

3. — Le principe de la coopération, si heureusement imis en lumière
par la première session du Congrès, trouvera l'occasion de se déve-
lopper lors de la deuxième session, C'est ainsi que le concouis de
l'Union Interparlementaire, qui dispose de groupes puissants dans
tous les parlements, a été promis pour appuver les mesures interna-
tionales qui auraient fait l'objet de conclusions étudiées avec soin et
largement appuyées. D’autres organismes internationaux ont aussi
fait l'offre de leur concours pour des questions qui leur sont communes
avec d'autres Associations.

 
4. — Enfin, il n'est pas indifférent, pour le succès des buts parti-
culiers poursuivis par chaque Association, que le mouvement inter-
national général apparaisse dans toute son ampleur, aux venx des
gouvernements et de l'opinion publique.

B. — Participation aux travaux du Congrès.

La participation aux travaux dn Congrès Mondial est rendue facile
par toutes les mesures qui ont été prises.

1.— La représentation peut se faire par tel nombre de délégués
que les Associations jugeront utile. Le Congrès pouvant intéresser
tous les membres des Comités des Associations, ils sont tous invités
à suivre les séances. La délégation peut être éventuellement exercée
par les délégnés belges des Associations. Ceux-ci étant sur place,
peuvent facilement et sans frais assister au Congrès et faire ensuite
rapport à leur Association.

2.— Ua questionnaire d'enquête très complet a été dressé pour
recucéllir l'opinion des Associations sur les questions à l’ordre du jour.
Une première forme de participation consiste à envoyer des réponses
à toutes où à quelques-unes de ces questions, ou à formuler des con-
clusions et motions les concernant.

3. — Le questionnaire servira de base aux discussions. Des ré-
ponses orales pourront alors être recueillies de la part des délégués,
mais il ést désirable qu'elles soient envoyées immédiatement par
écrit, pour faciliter l'élaboration du rapport général,

4. — Enbn il est demandé aux Associations un rapport sur l'inter-
nationalisation dans leur domaine propre et sur les travaux qu'elles
ont accomplis. Ce rapport peut laisser de côté les détails d’organisa-
tion (fondation, statuts, règlements personnels, etc.), qui ont déjà
figurés dans l'Annuaire de la Vie Internationale. Il est désirable
qu'elles exposent les résultats acquis et les problèmes qui restent à
résoudre.

L'exposé de l'état actuel de l'internationalisation, dans les divers
domaines des études et de l'activité pratique, devrait comprendre
notamment les points suivants :

a) Organismes internationaux existants, projetés ou sugpérés :

b) Coopération réalisée ou projetée entre organismes de la même
spécialité ou avec d'autres ;

c) Publications internationales existantes ou en projet (revues,
annuaires, encyclopédies, etc.) ;

4} Réglementation internationale officielle ou libre ;

e} Unification et systèmes d'unités :

{)} Langage : terminologie, classification, notations, usage des
langues naturelles, langues artificielles ;

 

 

— 3 —

g} Documentation : bibliothèque internationale et principaux
fonds internationaux dans les bibliothèques nationales, bibliographie,
archives, musées spéciaux et principales collections existantes.

Si chaque Association consent à établir un tel rapport, leur en-
semble constituera le bilan général du mouvement international et
permettra de dresser un tableau qui promet d'être tout à fait saisis-
sant. Les deux rapports qui ont été publiés déjà dans La Vie fnter-
tationale, l'un sur le Droit d'auteur et le Bureau International de
Berne (M. Rôthlisberger, t. If, p. 201}, l’autre sur les Poids et Mesures,
le Système métrique et le Bureau International du Mètre (M. Guil-
faume, t. II, p. 5), permettent de se rendre compte de l'intérêt puis-
sant qui s'attache à de tels travaux et les utiles enseignements qui
en ressortent.

5. — Une attention toute spéciale est appelée sur l'établissement
du budget dont chaque Association Internationale devrait pouvoir
disposer pour accomplir la tâche qu'elle s’est imposée. L'ensemble des
budgets ainsi réunis permettra de dresser le budget général de l'inter-
nationalisme et de montrer les sommes relativement faibles néces-
saires pour promouvoir des réformes d’un intérêt universel.

C. — Coopération des Associations laternationales.

La première session du Congrès a provoqué d’utiles mesures de
coopération entre les Associations. Il est désirable que la deuxième
session soit l’occasion de nouveaux rapprochements et que des rap-
ports sur des coopérations limitées à certains domaines soient pré-
sentés. L'étude parue dans La Vie Tniernationale (1912, t. I, p. 6x1),
sur la Coopération entre l'Union [nterparlementaire, l'Institut de
Droit International et le Bureau de la Paix et entre ces organismes
et la Conférence de la Paix, fournit un remarquable exemple de ce que
peut et pourrait la coopération. La Semaine sociale crganisée en 1972,
à Zurich et dont la première idée fut lancée au Congrès Mondial de
1910, en fournit un autre: La Vie Fnternatianale ui a également con-
sacré une étude (1912, t. IE, p. 61).

A l'ordre du jour de la Conférence de l'Union Interpartementaire,
qui aura lieu à La Haye, du 3 au 5 septembre 1913, a été inscrite la
question de la er Coopération de l'Union et de ses groupes aux œuvres
internationales x,

Pour faciliter les ententes, des séances particulières seront éven-
tuellement organisées au Congrès Mondial, à leur demande, entre les
délégués d'Associations poursuivant des buts connexes et désirant
rechercher en commun des terrains d'entente et de coopération.

 
D. — Concours des Assoctations officielles.

Les Associations Internationales officielles ont été invitées à prendre
part aux travaux du Congrès. Ces Associations se trouvent dans une
situation spéciale à raison du caractère de leur organisation. Cer-
taines se considèrent liées par leurs règlements organiques et empé-
chées de prendre une part active au Congrès.

Il est désirable qu’elles examinent à nonveau la question en tenant
compte des considérations suivantes : l'assistance au Congrès n'im-
plique pas nécessairement l’afiliation à Union ; cette assistance peut
se faire #4 audiendum et pour information ; alors même que certaines
Associations jugeraient qu'elles n'ont que peu à retirer de semblables
réunions —. ce qui est on ne peut plus contestable, — il leur est de-
mandé d'agir dans un esprit de solidarité et, par la présence de leurs
délégués à qui pourront être posées des questions d'ordre pratique,
de ne pas priver les autres Associations des indications et renscigne-
ments utiles qu'ils pourraient apporter aux débats. L'action libre des
Associations est souvent stérile, si eîles ne sont pas en contart avec les
Associations officielles similaires. D'autre part, celles-ci à défaut d’un
tel contact risquent souvent d'être retardées dans leur développement,
privées qu'elles sont du bénéfice d'utiles initiatives.

Une séance du prochain Congrès est spécialement réservée aux
délégués des Associations officielles, afin de leur permettre de déli-
bérer entr'eux sur l'attitude qu'il importe aux organismes officiels
d'avoir à l'égard des organismes libres et sur leur coopération à l'Union
des Associations Internationales,

E. -— Représentation des Gouvernements.

Les Gouvernements ont été invités à se faire représenter au pro-
chain Congrès. Cette invitation a été adressée sous une double forme :
directement par l'Union aux représentants des puissances à Bruxelles,
et par la voie diplomatique ordinaire, à l'intervention du Ministère
des Affaires étrangères de Belgique, à raison du patronage spécial
accordé à l'Union par le Gouvernement belge,

Plusieurs Gouvernements ont accepté cette invitation. 1ls seront
représentés notamment par leur ministre à Bruxelles,

L'intérêt des Gouvernements à se faire représenter au Congrès est
multiple, Ils sont généralement représentés auprès de tons les grands
congrès. Il est logique qu'ils le soient auprès d'un « Congrès des Con-
grès ». Des sections nationales ayant été créées au sein du Musée
International, à Bruxelles, il est naturel que ces sections soïent
placées sous le haut protectorat des divers Gouvernements. Ceux-ci

— 5 —

trouvent dans les travaux du Congrès Mondial une documentation
abondante sur l’action des diverses Associations, sur leur organisa-
tion, sur la valeur de leurs travaux. Ils peuvent aussi se rendre compte
du mouvement général auquel ils sont invités à chaque instant à
participer, de la corrélation qui existe entre les résolutions particu-
lières des Associations, des grandes œuvres d'ensemble que celles-ct
ont entreprises sur la base de la coapération et de l'impérieuse néces-
sité de voir les États s'y associer et harmoniser avec leurs desiderata
l'organisation de leurs propres services intérieurs.

Les délégués des Gouvernements assisteront aux séances du Con-
grès ad audiendum. En outre, une séance spéciale leur sera réservée
exclusivement, séance dans laquelle aura lieu un échange de vuc off-
cieux sur la coopération qu'il est désirable de voir apporter par les
Gouvernements aux Associations Internationales et à leur Union.

EF. — Publications du Consrès.

La publication des documents du Congrès se poursuit. À ce jour
elle se compose de : .

a) ACTES Du ConGRËs : publiés en documents séparés qui seront
réunis ultérieurcment en volume.

N9 1. Lettre d'invitation.

N°9 2. Programme du Congrès.

NS 3. Organisation et marche des travaux.

N0 4. Résumé, sous forme de thèses, des principes et des faits qui
sont à la base de l'Union des Associations Internationalés.

No 5. Enquête-référendum sur les questions à l’ordre du jour du
Congrès, avec références aux publications de l’Union.

b) DocumExTs pu CoxGrËs : Rapports et Communications, pu-
bliés dans la revuc La Vie Zuternationale.

c) PUBLICATIUSS CONNEXES :

1. Compte rendu de la réunion préparatoire du Congrès d'avril 1912
{Publication n° 33). Publié aussi dans la Vie Internationale, À, D. 148.

2. Annuaire de la Vie Internationale : monographie descriptive des
810 Associations Internationales existantes (Publications n°5 3 et 47).

3. L'Union des Associations Internationales : notice générale sur
l'Union et ses services (Publication n° 254).

Ces diverses publications conticnnent les matériaux préparatoires
du congrès et les sources à consulter. En voici l'indication à ce jour.
par questions.

 
— 6 —

Les abréviations emplovées sont les suivantes :

Congrès — I. Actes du Congrès de 1910.
IT. Documents préliminaires dun Congrès de 1913.

Code — Conclusions générales codifiées présentées au
Congrès de 1910 et incluses dans les Actes
de ce Congrès (p. 39).

Annuaire Annuaire de la Vie Internationale : 1 (rgo8-
1909); II {1g10-19r17).

Revue La Vie Internationale ; désignation par tomaison.

Union Publication n° 25: L'Union des Associations
Internationales.

Ire SECTION :

À. Coopération entre assactations internationales, — Code, 17° ques-
tion ; Congrès I, 30, 257, 823, 1155; Revue, I, 61.

B. Codification des vœux des Congrès. — Union, 104: Congrès I,
37-196.

C. Union des Associations iniernalionales.
a) Union : Union, 06-98, 112-162; b] Centre international,
Union, 20, 140: Revue, II, 123; c) Participation des États,
Revue, 11, 357, 111, 290.

IIe SECTION :

A. Régime juridique des 4 ssociations internationales. — Revue,
I, 488-408 ; Congrès I, 53, 317-333, 82$, 1051.

B. Droit international conventionnel. — Congrès 1, 320; Annuaire
{texte des principales conventions internationales),

C. Conception et rôle des Assocrations internationales. — Revue, 1, 1;
Congrès I, 273; Congrès II, n°9 4; Annuaire, I, 65, 11, 195.

ITIS SECTION :
Uni fication des syslèmes d'unités. — Système universel des poids et
mesures, Standardisation technique et industrielle. Congrès I,
59-73, 1091-1115 ; itevue, ILE, 5.

IVe SECTION :

À. Moyens divers d'accroître l'utililé et le rendement des Associations
internationales et des Congrès internationaux. — Annuaire,
1, 29-166 ; Congrès 1,75; Revue, IT, 397.

B. Réalisation des résolutions des Congrès. Revue, Il, 2071, IÉE, 5;
{Cas particuliers.) Annuaire, statuts et règlements des diver-
ses grandes associations.

C. Réglement-4ype des Congrès. — Annuaire, passim. (Cas parti-
culiers.) Congrès 1, 439-464.

 

 

— ? —

D. Participañion des Etais ef des diverses nationalités. Exemples
empruntés à des associations. — Annuaire, I, 7a, 280,
351, GE.

E. Budpet idéal. Exemples. — Annuaire, passion; Congrès I, 259;
Revue, Il, 355.

Ve SECTION :

À. Organisation des publications. — Congrès I, 100-137; Annuaire,
11, 623; Revue, IT, 307.

B. Organisation de la documentation. — Union, 139; Congrès I,
87-104, 157, 481, 1143, 1207; Code, 5° question.

C. Université et Enseienement inlernational. — Union, 144; An-
nuaire, 1, 949, II, 1731; Revue, Il, q3 ; Congrès I, 260.

D. ÜUüilisation des Exposilions universelles. — Revue, 1, 312 (n° 17),
314, 543, 627, 648.

VIE SECTION :

À. Terminologie, Nomenclature internationale. — Congrès I, 165,
204, 589-027, II15.

B. Emploi des langues dans les relations internationales, — Congrès Z,
1123-1142; Code, 6° question.

G. — Exposition des Associations Tnlernationales.

Cette Exposition aura lieu au Musée International. Les Associa-
tions, qui n'y auraicnt pas encore organisé leur stand, sont invitées
à se mettre immédiatement en relation à ce sujet avec l'Office Central.
L'Office leur facilitera de toutes manières cette participation. Des
types de stands complets ont été étudiés et exécutés déjà pour plu-
sieurs Associations. [Ils leur ont permis de réaliser rapidement et aisé-
ment le programme d'exposition dont le détail a été publié dans la
notice générale sur l'Enion {Publication n° 254, p. 112 à 119).

H. — Fôles du Congrès.

Le Congrès se tiendra au cours de la grande Exposition Universelle
organisée à Gand, exposition dont l'importance est aussi considérable
que celle qui a eu lieu à Bruxelles en 1910. L'exposition des fleurs
(Floralics) lui donne un intérêt exceptionnel.

Toute une série de Congrès internationaux y auront lieu, suivant
un plan aussi systématique que possible, tendant à grouper les Congrès
en semaines, en guintaines, de manière à réunir les personnes des
mêmes spécialités tout en laissant à chaque Congrès son existence

autonome et en lui permettant de constituer la suite des Congrès

 
—_ B—

antérieurs sur le même sujet (1), Un palais, construit en matériaux
définitifs, a été spécialement aménagé pour héberger les Congrès.
Une journée du Congrès est réservée à Bruxelles et à Ja visite des
institutions qui y sont établies.
De grandes fêtes auront lieu au moment du Congrès, dont la date
coïncide avec la sortie d'un grand cortège historique.

J. — Facilités de voyage et de logement.

Vovage, — Il est recommandé aux congressistes de se munir d'un
abonnement de cinq jours ou de quinze jours, qui leur permettront
de circuler sur tous les chemins de fer belges (2). Ce système est plus
avantageux qu'aucune autre combinaison et permettra aux congres-
sistes de se rendre à Bruxelles, Bruges, Ostende ou Blankenberghe,
ou à une ville voisine à leur convenance, si les hôtels de Gand étaient
encombrés.

Les cartes d'abonnement s’obtiennent dans toutes les stations et
haïtes des chemins de fer belges, sur demande verbale faite au moins
une heure d'avance et sans autre formalité que la remise du portrait-
de l'intéressé, photographié sur papier 6 X 4, la hauteur de la tête
étant d'au moins 1 centimètre, Les cartes peuvent être obtenues dès
la veille du premier jour de leur validité, à partir de 18 heures. Elles
ne peuvent être utilisées ce jour-là.

Ii suffit aux étrangers d'envoyer leur photographie à la premicre
station d'entrée, en indiquant la natureet la classe de l'abonnement,
le jour et l'heure de leur passage, pour que la carte désirée soit tenue
à leur disposition.

Les cartes doivent être restituées à une station quelconque des
chemins de fer belges, au plus tard le lendemain du jour de l'expira-: :
tion, avant midi,

Logement. —- Un Comité de logement a été institué à Gand, 36, rue
Digue de Brabant. Ce Comité publiera la liste des hôtels de Gand; il
aura pendant toute [a durée de l'Exposition, un burçau à la gare de
Gand-Sud, un bureau à la gare de Gand-Saint-Pierre, un burean à
l'Exposition. I] conservera également son bureau actuel où toutes les
correspondances devront être adressées. Les bureaux seront ouverts
de 8 heures du matin à 10 heures du soir,

Moyennant une légère rétribution {25 centimes}, des chasseurs
seront mis à la disposition des étrangers pour les conduire à leurs
appartements.

{1) Sur l'Exposition de Gand, voir La Vie Internationale, t. |, P. 543,
591, 627 et le Calendrier des Congrès.

(2) Abonnement dé 5 et 15 jours sur tous les chemins de fer belges :
5 jours, 176 classe, fr, 30.95; 2° classe, fr. 20,50; 3 classe, Îr, 11.76;
15 jours : 1'€ classe, fr. 61.50; 2° classe, 41 francs; 3° classe, Ér. 33,50.
Garantie, 5 francs.

#

 
