 

 

 

 

 

 

 

 
 

 
 

 
 
 
 
 

 
 

 

 

: = \ io
î |
| e
d ME |
; AE i OS | |
<

 
1 PHILOSOPHIE ET QUES-

  

TIONS MORALES

Divisions générales.

11 Métaphysique et Cosmologie.

13 Rapports de l’âme avec le corps.

14 Systèmes et doctrines philosophiques.
15 Psychologie.

16 Logique.

17 Morale. Questions morales.

Matières connexes. Références et concordances..

21 Théologie naturelle. Théodicée. :
301 Philosophie des sciences sociales. Sociologie.
34.017 Philosophie du Droit.

707 Philosophie de l'Art. Esthétique.

901 Philosophie de l'Histoire.

Observations générales.

 

A. Bases de la division. — Il y a lieu de distinguer : ,

Les diverses branches de la Philosophie. (Ex. : logique,
morale, psychologie, etc.);

Les questions PRIS PRAUEE en particulier. (Ex. : cause,
volonté, etc.);

Les systèmes phi es (Ex. : kantisme, monisme, etc.);

Les philosophes et les écoles philosophiques. (Ex. : Aristote,
Descartes, Cartésiens, etc.); *

L'histoire générale de la Philosophie ;

L'étude des questions philosophiques, limitée à un lieu et à
une époque. (Ex. : L'idée de cause chez les philosophes _.
çais du xviue siècle);

Les questions qui ne rentrent pas dans les cadres de la Philo-
sophie proprement dite, mais qui, tout en relevant d’autres
sciences, sont traitées au point de vue de la Fes (Ex
Philosophie du Droit.)

 

 
 

B. Branches et questions spéciales, — Les diverses branches
de la Philosophie et les questions particulières sont classées
sous les divisions 11 à 17 (division 14 exceptée). Ex. :

15 Psychologie.
159 Volonté.

Lorsqu'une étude est limitée à l'examen de la question dans
un système philosophique déterminé, on la classe en ordre
principal à la matière spéciale, suivie du nombre propre à ce
système, par division de relation. Ex. :

159 : 149.222 La volonté — dans le kantisme.
122 : 149.015 L'idée de cause — dans le monisme,
122 : 149,016 L'idée de cause — dans l’évolutionisme.

Lorsque la matière est limitée à l'examen de la question,
dans un pays où à une époque donnée, ou chez un philosophe
déterminé, on la divise par les subdivisions de lieu, de temps ou.
de nom propre. Ex. :

122 (51) L'idée de cause — chez les philosophes chinois.

122 «17» L'idée de cause — chez les philosophes du xvin° siècle,
122 (Aristote) L'idée de cause — chez Aristote,

C. Systèmes philosophiques. — Les systèmes philosophiques
généraux sont classés sous les divisions respectives de 14. L'on
réunit là toutcequiconcernel’exposé des systèmes, leur histoire,
leur critique, etc. Les systèmes d’idée d’un philosophe ou de
son école sont classés comme il est dit sous D.

Les subdivisions de lieu et de temps sont applicables aux
divisions par systèmes. Ex. :

147 «11» Le panthéisme au xn° siècle.

Facultativement et par duplicata, on peut classer sous chaque
système, par les divisions de relation : , l'application du système
à une branche de la Philosophie ou à une question déterminée.
Ex

147 : 122 Le panthéisme — et la causalité,

D. Philosophes et écoles, — Tout ce qui concerne un philo-
sophe déterminé, sa vie, ses œuvres, ses doctrines générales,
les écrits émanant de lui et ceux dont lui-même et ses œuvres
ont été l’objet, est classé sous r (A-Z), pourvu qu'il s'agisse
d’écrits traitant de l’ensemble de l’œuvre du philosophe ou de
l'une de ses œuvres qui traite de la Philosophie en général.
Lorsque ces écrits concernent une question spéciale de philo-
sophie, ils sont classés sous cette question, ainsi qu'il est dit
sous l'observation B. On classe sous chaque philosophe tout ce
qui concerne les écoles qui se rattachent directement à lui et
que l’on peut regarder comme continuant son œuvre. Ex. :

1 (Aristote) Les aristoteliciens ou l’école péripatéticienne.

Facultativement et par duplicata, on peut classer sous la division
propre à chaque philosophe leurs travaux personnels ou les

 

 
 

travaux qui les concernent relatifs à une question spéciale. On
se sert à cet effet des divisions de relation. Ex. :

1 (Aristote) : 122 Aristote — et la théorie de la cause.
x (Aristote) : 159 Aristote — et la théorie de la volonté.

Les écrits sur les philosophes d'un pays ou d'une époque
sont classés sous la division 1 suivie des subdivisions de lieu ou
de temps correspondantes. Ex. :

x (44) Les philosophes français.
I C17> Les philosophes du xvri° siècle.
x (44) «17 » Les philosophes français du xvim® siècle,

E. Histoire de la Philosophie, — L'histoire de la Philosophie,
d'une de ses branches ou de questions philosophiques parti-
culières étudiées en général ou limitées à un pays et à une
époque est classée à la division correspondante, subdivisée par
la subdivision commune correspondante. Ex. :

x (09) Histoire générale — de la Philosophie,

1 (44) Histoire de la Philosophie —en France.

1 (44) «16» Histoire de la Philosophie — en France — au xvri' siècle.

15 (09) Histoire générale — de la Psychologie,

15 & 14 La Psychologie au — x1v' siècle,

159 (00) Histoire générale — des doctrines sur la volonté,

150 (51) La théorie de la volonté — chez les philosophes de la Chine.
F. Matières étrangères à la Philosophie. — Les études à un

point de vue philosophique des diverses questions qui ne figu-
rent pas dans la classification de la Philosophie, mais qui
relèvent d’autres sciences, sont classées avec chaque matière
spéciale, soit sous un nombre spécial, soit sous le nombre
propre à l’une de ces sciences, suivi de la subdivision commune
(oz) Philosophie de... Ex. :

34.07 Philosophie du Droit,
5r(or) Philosophie des mathématiques.
7.01 Philosophie de l'Art et du Beau ou Esthétique.

Facullativement et par duplicata, et en se servant des subdivisions
par relation :, on pourra réunir sous les divisions du tr tous les
travaux sur les questions quelconques dont le sièse des ma-
tières est ailleurs, mais qui sont envisagées au point de vue des
doctrines et théories philosophiques.

G. Terminologie de la Philosophie. Synonymie, — La ter-
minologie de la Philosophie est loin d’être fixée. Un grand
nombre de termes ont une signification spéciale d'après les
écoles et suivant les époques. Pour faciliter le classement, le
contenu des rubriques a été précisé par l’indication des termes
les plus usités dans l’histoire de la Philosophie et chez les
philosophes contemporains. Les synonymes ont été distingués
les uns des autres, et de nombreuses références ont été faites

d’une division à l’autre lorsqu'une même question est traitée

sous divers chapitres et à des points de vue différents, par
exemple, en Psychologie, en Logique et en Morale.

 

 

 

 

 

 

 

 

 

 
 

 

TE

II

III

 

 

H. Résumé. — En résumé, par l'application des combinai-
sons de divisions qui viennent d'être exposées, on dispose du
moyen de classer, soit l’ensemble de la Philosophie, soit une
branche particulière de la Philosophie, soit une matière philo-
sophique spéciale, aux quatrè points de vue différents du lieu,
du temps, d’un système, d’un philosophe ou de son école.

La récapitulation des exemples cités montrera l’ordre de
succession des rubriques combinées comme il vient d’être dit :

1 (09) Histoire générale — de la Philosophie,

I (44) La Philosophie — en France,

1 (Aristote) La Philosophie — d’Aristote,

1 «16» La Philosophie — au xvu° siècle.

122 Cause.

122 : 140.015 l’idée de cause — dans le monisme,

122 (5x) L'idée de cause — chez les Chinois,

122 (Aristote) L'idée de cause — chez Aristote.

147 (44) Le Panthéisme — en France.

147 (44 «11» Le Panthéisme — en France — au xu' siècle,

15 (00) Histoire générale — de la Psychologie.

15 (44) Les études de Psychologie — en France.

15 (44) « 16 » La Psychologie — en France — au xvii' siècle, !
15 : 149.222 Ja Psychologie — au point de vue du kantisme. a
15 ‘Aristote) La Psychologie — d’Aristote. 1
159 (5t) La volonté — d'après les philosophes chinois,

159 « 16» Les idées sur la volonté — au xvu° siècle,

159 : 140.222 La volonté — dans le kantisme, s

159 : 149.222 «18» La théorie de la volonté — chez les philosophes kantiens
— du xix° siècle,
159 (Aristote) La théorie de la volonté — chez Aristote.

(), «», A-Z] Generalia.

Les subdivisons de relation, de forme, de temps, de nom de
personnes sont employées comme il a été dit aux observations
générales.

Les divisions de (o) sont empruntées à la table I, Subdivi-
sions communes de généralités et de forme. Elles peuvent être
combinées avec une division quelconque des tables de Philo-
sophie. Ex. :

1(02) Traité de Philosophie,
16 (02) Traité de Logique.
105) Revue de Philosophie,
16 (05) Revue de Logique.

2

Métaphysique.

Voir aussi 2r Théologie naturelle,

Ontologie.
“ Être. Possible. Réel. Existence. Essence.
2 Attributs transcendantaux de l'être. Unité. Vérité ontologique.
Bonté. Perfection. Mal physique. Beauté. Identité. Totalité.

Voir aussi 157.1 Psychologie du Beau.
7-01  Philosophiedes Beaux-Arts. Esthétique.

 

 
Frs

 

 

LS

Y

112

113

114

TI

116

117

118

119

Substance. Nature. Individu. Monade. Personne. Hypostase.
Principe d'individuation.
Voir aussi 113 Cosmologie.
Accident. Qualité. Faculté. Mode. Figure. Fonction.

Voir aussi r11,3 Substance.
111.5 Relation.
113  Cosmologie générale.

Relation. Rapport. Relativisme. Phénomène. Attributs. Appa-
rences. Hypothétique. Absolu. Noumène. Chose en soi:
Inconnaiïissable. Antinomie.

Voir aussi 111.3 Substance,
165 Certitude.

Acte et puissance.
Immatériel. Esprit pur.

Voir aussi 15r Ame,

Méthodologie générale. Classification philosophique des
sciences.

Voir aussi 025,4 Classification bibliographique.

Cosmologie générale.

Lois de la nature et monde physique au point de vue philo-
sophique. Transformation et corruption substantielle.

Voir aussi rr7 La matière et la forme.

Espace. Lieu. Dimension. Vide.

Voir aussi r19 Etendue, nombre.
152,8 Perception de l’espace.

Temps. Durée. Eternité.

Voir aussi 152.8 Perception du temps.

Mouvement. Mobile. Action transitive, immanente, à
distance. Devenir.

Voir aussi 152.8 Perception du mouvement.

Matière. Forme. Corps. Cause matérielle et formelle.
Entéléchie.

Force. Energie.

Quantité. Etendue. Continu. Nombre. Grandeur. Masse.

Voir aussi 152.8 Perception de l'étendue.

12 Autres matières de métaphysique.

ÊZ

Théorie de la connaissance. Origine et limite. ]

Classer à 157.2.

 

 

 
 

Oo

122 Causalité. Principes. Cause efficiente. Condition. Effet.
Occasion.

Voir aussi 117 Causes matérielle et formelle.
124 Causes finales.

123 Liberté et nécessité. Contingence. Futurs contingents.

Voir aussi 150.1 Liberté et déterminisme, en psychologie.
233.7 Liberté, en théologie.
234.9 Prédestination.

124 Téléologie. Finalité. Causes finales. Ordre. Norme. Ha-

sard. Destinée. Cause exemplaire. Idéal. Type. Arché-
type. Valeur.

125 Infini. Fini. Indéfini. Univers.

126 Conscience. Personnalité.

Classer à 153.7,

127 Inconscience. Automatisme.

Classer à 153.8.

1289 ame.

Classer à 151.4.

129 Origine de l’âmeindividuelle.

Classer à 151.5.

JL Créatianisme.
Classer à 151.57.

2 Traducianisme.
Classer à 151.52.

‘3 Préexistence.
Classer à 151.53.

4 Migration. Palingénésie.
Classer à 157.54.

;
29 Emanation.

Classer à 151,55
Voir aussi r47 Panthéisme philosophique.
212 Panthéisme théologique.

 

 

13 Rapports du corps et de l'esprit. Anthropo-
logie philosophique.
Voir aussi 15 Psychologie
57 Biologie.
576 Origine de la vie, en biologie.
612 Physiologie.

 

 

 

 

 
 

   

131 Physiologie et hygiène de l'esprit.

Voir 613,8 Hygiène du système nerveux.

132 Troubles de l’esprit.

Maladies mentales. Psychoses. Psycho-névroses. Délires.
Obsessions. Tics intellectuels. Manies. Psycho-pathologie.
Psychologie tératologique. Psychiâtrie. Aliénation mentale.

Voir aussi 343.063 Folie et criminalité.
616.8 Maladies mentales, en médecine, |

Folie. Insanité. Aliénation mentale.
|

TL

2 Imbécillité. Idiotie. |
53 Mélancolie.

4 Catalepsie. Epilepsie. Hystérie. |

Voir aussi 134 Hypnotisme. |

55 Extase. |

|

1

Voir aussi 133.2 Extase, en sciences occultes,

.6 Manies criminelles. Cleptomanie.

Voir aussi 843.9 Anthropologie criminelle.

Manies vicieuses. Dipsomanie.

Voir aussi 178 Alcoolisme.

ŸY

 

133 Etudes psychiques. Sciences occultes.

Esotérisme. Xénologie. T'élépathie. Médiumnité.

Un grand nombre de questions que l’on fait parfois rentrer
dans le cadre des sciences occultes sont classées au siège prin-
cipal de ces matières, dans les autres parties de la classification,
notamment avec la Philosophie et la Religion.

à Voiraussi 125 Fini etinfini.

24 140.3 Mysticisme philosophique.

149.018 Théosophie.

151.54 Métempsycose.

164 Symbolique en général,

246 Symbolique religieuse.

308 Croyances et traditions populaires,

612.8 Psycho-physiologie. .
613.26  Végétarianisme.

Les questions morales, religieuses, sociales et scientifiques,
envisagées au point de vue de l’occultisme, sont classées par
division de relation sous 133 :. On classera ainsi notamment :

133 : 51 L'Occultisme et les mathématiques.
Mystique des nombres, Kabbales. Quatrième dimension. Les
trois plans.
133 : 52 L'’Occultisme et l’Astronomie.

Astrologie. Corps astral.
Classé précédemment à 52.ox et 133.5 Astrologie.

 

 

 

 
 

 

 

 

19301

2

2)

4

 

 

133 : 53 L'Occultisme et la Physique.
Physique transcendantale ou occulte. Rayonsinconnus, Spectre
universel, Action à distance, Xénophysique cosmique,
133 : 54 L’Occultisme et la Chimie.
Chimie transcendantale, Alchimie, pierre philosophale.
Grand œuvre.
133: 57 L'Occultisme etla Biologie.

Generatio œquivoca. Elixir de longue vie et d'immortalité.
Vis formativa. Sixième sens. Monstruosités, Extériorisation de
la sensibilité et de la motricité.

133 : 59 L'Occultisme et la Zoologie.

Animaux fabuleux. Androïdes,

133 : 61 L'Occultisme etla Médecine.

Prières pour recouvrer la santé. Cures sympathiques. Méde-
cine spagyrique. Hermétisme,

133 : 02 Hommes curieux et étranges.

Apparitions. Esprits.
Etres surnaturels, spectres, esprits, revenants, fantômes, vampires, loups-
garous. Maisons hantées, lutins, farfadets, Iycanthropes.
Voir aussi 133.9 Spiritisme.
231.7 Apparitions.
235 Anges, diables, démons.
398.4 Fées. Elfes,

Hallucinations. Illusions. Visions et extases religieuses. Illumi-
nisme.

Voir aussi 132.5 Extase au point de vue psychiatrique.
308.3. Croyances et superstitions populaires.

Devination. Prédictions.

Art devinatoire, présages, signes célestes, prodiges, augures, horoscope. Oracle,
sorts, baguette devinatoire, Sibylles. Enigmes, Explication des songes. Double
vue. Somnambulisme extra-lucide. Inspiration, prophétisme, prédiction, pro-
nostic. Bonne aventure. Cartomancie, tireurs de cartes, interprètes du marc de
café, du verre d’eau, etc. Géomancie,

Voir aussi 003 Graphologie.
133.6 Chiromancie.
224 Prophéties bibliques.
228 Apocalypse.

231.7 Miracle et prodige.
308.7 Livres de songes.

Magie et sorcellerie.

Démonologie. Théurgie, sanatisme, nécromancie. Magie noire, magie blanche.
Fascination. Evocation. Enchantement, ensorcellement, Exorcisme. Mauvais
œil, envoûtement, maléfices. Talisman, charmes, amulettes, mauvais conseils,
Tarot, grimoires, livres noirs, bibliomancie. Animaux incubes et succubes.
Instruments de magie, métaux magiques, messes noires,

Voiraussi 235 Anges et démons.
140.918 Esotérisme,
263:t. Sabbat.
272.8  Persécutions.

 

 

 

 
pe . . L

1990 Astrologie.
A classer sous r33 : 52.

.6 Chiromancie.

Chirologie, Chirognomonie. Physiologie dela main. Art delire dans la main,

Charlatanisme.

Ÿ

Voir aussi 308.3 Croyances et superstitions populaires,
.0 Spiritisme. Recherches psychiques.

Tables tournantes. Bons et mauvais esprits. Forces occultes, Médiumnité. Exté-
riorisation de sensations.
Voir aussi 235 Démonologie,

280.4. Eglise swédenborgienne.
6r2.821.715 Spiritisme et sciences connexes, en physiologie,

134 Hypnotisme.
Suggestion. Magnétisme, Magnétisme animal. Mesmérisme, Léthargie, Faki-
risme. Clairvoyance. Lucidité, Transmission de la pensée. Télépathie.
Voir aussi 343.64 Hypnotisme, en droit pénal,
612.821.711 Hypnotisme, en physiologie.
621.821.714 Lucidité et télépathie, en physiologie.

 

135 Sommeil et veille.

Rêves. Somnambulisme.

Voir aussi 612.$21.7 Sommeil, en physiologie.

136 Caractères mentaux.

Psychologie génétique, individuelle et sociale.
Voir aussi 612.82r.303 Intelligence selon les races, les individus, les âges et
les professions,
qi Influence du sexe : hommes et femmes. Psychologie sexuelle.

Voir aussi 376.2 Capacité mentale des femmes.

 

2 Influence du milieu physique.
Voir aussi 573.4 Histoire naturelle de l'homme.
3 Influence de l’hérédité et de l’évolution. Influence ancestrale.

.

Voir aussi 233,3 Hérédité spirituelle.
575.1 Evolution.

4 Influence de la race. Psychologie ethnique. Psychologie des

peuples.
Voir aussi 572 Ethnologie.
+9 Influence de l’âge : adolescence et vieillesse.
Voir aussi 136.7 Influence de l'enfance.
.6 Influence de l'organisme. Tératologie.
7 Influence de l'enfance. Psychologie infantile. Pédologie.

Voir aussi 37 Éducation.

371 Pédagogie.
.8 Influence du milieu social, de la collectivité, des classes sociales.
Psychologie collective des professions, des foules, des groupes.

Voir aussi 343.95 Psychologie des criminels.
343.974 Crime des foules.

 

 

 
 

 

 

 

 

 

 

 

 

137 Tempérament. Caractère. Psychologie individuelle ou
différentielle. Idiosyncrasie.

Voir aussi 136.1 Psychologie sexuelle,
138 Physiognomonie.

138 Physiognomonie.

Mimique, siynes révélateurs du caractère, expression des
émotions.

Voir aussi 003 Graphologie.
157 Emotion, sympathie,

139 Phrénologie. Localisations cérébrales.

Voir aussi 152 Sens,
612.825.7 localisation cérébrale, en physiologie.

14 Systèmes et doctrines philosophiques.

Exposé, historique, critique, etc., des divers systèmes et
doctrines.

Voir observation sous 1 D.
Voir aussi sous 2 les doctrines religieuses,
141  Idéalisme.
Transcendantalisme. Subjectivisme. Phénoménisme. Philo-
sophie de l'identité.
142 Philosophie critique.

Criticisme. Critique du jugement. Critique de la raison pure.
Voir aussi 165.17 Vérité logique.

143  Intuitionalisme.
144 Embpirisme.
145 Sensualisme.

146 Matérialisme. Positivisme.
Naturalisme. Religion de l'Humanité.

Voir aussi 176.7 Naturalisme dans l'art.

147  Panthéisme. Panenthéisme.

Voir aussi 149.015 Monisme,
212 Panthéisme religieux, en théologie.

148  Eclectisme. Syncrétisme.

149 Autres systèmes philosophiques.

I Nominalisme. Conceptualisme.

2 Réalisme.
Voir aussi 146 Matérialisme.

 

 

 
 

 

 

 

149.3 Mysticisme philosophique.
A Associationalisme.
2) Optimisme. Méliorisme.
.6 Pessimisme.
Voir aussi 216 Le mal, en théologie,
7 Agnosticisme philosophique.
Voir aussi 273.8 Agnosticisme religieux, en théologie.
.8 Nihilisme.
.911 Rationalisme. Intellectualisme. Innatisme. Innéisme. Nativisme.
Voir aussi 2117 Rationalisme, en religion.
912 Scepticisme. Sophistique.
.013 Fatalisme.
Voir aussi 214 Providence, en théologie.
.014 Spiritualisme,
.915 Monisme et dualisme. Parallélisme. Harmonie préétablie. Ani-
misme.
.916 Evolutionisme. Evolution des espèces. Transformisme.
Voir aussi 575 Evolution, en biologie,
.917 Atomisme et dynamisme. Mécanicisme. Energétique.
.918 Théosophie. Philosophie. Esotérique.
Voir aussi 133 Sciences occultes.
204 Bouddhisme,
.019 Scolastique.
.921 Néo-scolastique.
.022 Kantisme.
.923 Néo-kantisme. Néo-criticisme
.924 Occasionalisme.
.025 Libéralisme.
.926 Dogmatisme.
.027 Traditionalisme. Fidéisme.
.928 Pragmatisme.
.929 Polyzoïisme.
.930 Volontarisme.

15 Psychologie. 5

Etude des facultés mentales.
La psychologie comparée des races, des groupes, des âges et =
des sexes est classée sous 136.
La psychologie physiologique, comme son nom l'indique, relève à
la fois de la Psychologie 15 et de la Physiologie 612. Elle a été
classée avec détail sous 612.82. On se reportera donc à cette
division, mais il sera loisible de classer ici, sous 15, par duplicata,
toute la psychologie physiologique, en y rattachant par le signe
de relation : , les divisions de 612.82. Ex. :

612.825.2 Localisation cérébrale.
15 : 612.825.2 Localisation cérébrale, en psychologie.

 
 

RS,

EESTI EEE

 

 

 

Les divisions ci-après sont plus spécialement consacrées aux
travaux d'ordre philosophique. En regard des divisions, on a
rappelé les divisions correspondantes propres à la Physiologie.

Voir aussi 343,95 Psychologie des criminels.
612.82r.303 Psychologie comparée des âges, des sexes, des indi-

vidus, des professions et des races.
612.S21.3r Psychologie comparée des animaux.

151 Ame. Connaissance.
Activité psychique. Espèce intentionnelle. Cognition.

Voir aussi 131.7 Esprit pur.
152 Connaissance sensible.
L Génie. Talent d'invention.

Voïr aussi 612.821.302, en physiologie,

.2 Théorie de la connaissance. Origines et limites de la connais-
sance. Idéologie.
Classé précédemment à 127.
Voir aussi 152 Sens.
153 Intelligence,
+3 Connaissance, âme, intelligence, instinct des animaux. Psycho-
logie animale ou comparée.

Voir aussi 612.821.3r Instinct des animaux.
612.821.32 Intelligence des animaux, en physiologie,
636.044 Dressage des animaux domestiques,

Nature et siège de l’âme. Unité, spiritualité, individualité
et personnalité, etc.
Classé précédemment à 128,

Voir aussi 151.6 Immortalité de l’âme.
153.7 Personnalité,
218 Immortalité, en théologie.
237 Etat futur, en théologie.

5 Origine de l’âme. Théories diverses.
Classé précédemment à 120.

.5I Créatianisme.

Voir aussi 231 Création.

52 Traducianisme. Transmission de l'âme par hérédité.

Voir aussi 136,3 Hérédité mentale, en psychologie. :
233.3 Hérédité spirituelle, en théologie.
575.1 Evolution.

:53 Préexistence de l'âme.
.54 Migrations. Transmigrations. Palingénésie. Réincarnation.
Métempsycose.
509 Emanation des âmes.
Voir aussi 147 Panthéisme.
212 Théologie naturelle.
.6 Vie future. Immortalité de l’âme. Survivance.

Voir aussi 218 Vie future, en théologie.

 

 

 

 
 

192 SENS.

Faculté passive ou réceptive. Perception et représentation
sensible. Sensation. Sensibilité. Impression. Connaissance sen-
sible. Psycho-physique. Psychologie physiologique. Synes-
thésie. Anesthésie. Hyperesthésie.

Voir observation sous 15.
Voir aussi 611,8 Anatomie du cerveau et du système nerveux,
612.8 Physiologie du système nerveux.
0 Vision. Sens visuel. Daltonisme. Images consécutives. Con-
traste.
Voir aussi 612,84.
2 Audition. Sens auditif. Audition colorée.

Voir aussi 612,85.

3 Olfaction. Sens olfactif. Odorat.
Voir aussi 612.86.

À Gustation. Goût.
Voir aussi 612,87.

dE) Toucher. Sensibilité tactile.
Voir aussi 612.88.

.6 Sens musculaire. Effort musculaire. Position. Station. Equilibre.
Vertige.

 

Voir aussi 612.885.

7 Psycho-physique. Psychométrie. Esthésimétrie. Quantité des
sensations. Lois de Weber et de Fechner,

.8 Objectivation, extériorisation, localisation des sensations. Signes
locaux. Perception de l'étendue, du mouvement, de l’espace,
du temps.

Voir aussi 133.9 Extériorisation, en sciences occultes.

153 Intelligence. Raison. Entendement.
Faculté active ou pensante. Connaissance intellectuelle. In-
tellect. Pensée. Intuition.
Voir aussi 16 Logique.
612.821.307.
ci Conception. Concept. Idée. Notion. Attention. Aperception.
Appréhension. Attente. Sélection.
Voir aussi 161.1 Concept, en logique:
124 Idéal.
612.821.2.
2 Association.

Voir aussi 612.821.22.

S Abstraction. Abstrait.

Voir aussi r6r.r Universaux.

 

 

 

 

 

4 Réflexion.
0 Jugement.

Voir aussi 162 Déduction et induction.

LES

 

 

 
 

 

 

 

 

153.6

109

Ee

197

158

Raisonnement.

Voir aussi 168 Raisonnement, en logique.

Conscience. Personnalité. Identité personnelle. Moi. Déper-
sonnalisation.
Classé précédemment à 126,
Voir aussi 151.4 Individualité et personnalité de l'âme,
Inconscience. Subconscience. Automatisme.

Classé précédemment à 127.

Mémoire.

Faculté reproductrice. Souvenir. Réminiscence. Amnésie.

Voir auss1\612.821.23

Mnémonique. Mnémotechnique. Méthodes pour développer la
mémoire.

Imagination. Images. Sens internes.
Faculté créatrice, cogitative, estimative. Sens commun.

Voir aussi 135 Rêves.
612.821.26.

Raison. ]

Classer à 153.

Sentiments. Emotions.

Affections. Passions. Etats émotifs. Plaisir. Douleur. Peine.
L’agréable et le désagréable. Amour. Désirapathique. Phobie.
Peur. Pitié. Sympathie. Antipathie. Concupiscence. Sensibilité
émotionnelle. Pathologie des sentiments.

Voir aussi 138 Expression des émotions,
612.521.32 Emotions.

Psychologie du: Beau. Emotion. Esthétique. Sublime.
Voir aussi 612,821.27: :
7,01 Esthétique. Philosophie des Beaux-Arts.

Rire. Comique. Humour.

Appétit sensible.

Instincts. Mouvements et actes reflexes, spontanés, automa-
tiques. Appétition. Inclination: Tic. Motricité. Volition sensible.
Tendance. Geste. Conation. Impulsions. Inhibition.

Voir aussi 132.6 Manies criminelles.
182.7 Manies vicieuses,
153.8 Automatisme,

Travail. Fatigue. Ergographie. Dynamogénése.
Imitation. Jeux. Orientation.
Parler. Langage intérieur. Aphasie. Alexie. Chant.

Voir aussi 4o1 Origine du langage.

 

 

 

 

 
 

158.4 Ecriture. Dessin. Agraphie. Graphologie.
. Marche. Locomotion. Ataxie.

159 Volonté. Volition. Efforts.
Velléité. Aboulie.

“L Liberté de la volonté. Choix. Alternative. Attente. Décision.
Déterminisme. Indéterminisme. Imputabilité.
2 Action humaïne. Exécution. Pratique.
Voir aussi 123 Liberté et nécessité, en métaphysique,
171.011 Responsabilité, en morale,
343,069.5 Responsabilité, en anthropologie criminelle,
343.222 Responsabilité, en droit pénal,
3 Habitude. Accommodation et adaptation. :
Voir aussi 111.4 Faculté et puissance, en métaphysique,
A Intentions. Mobiles. Motifs.

Voir aussi 171.4 Intention morale,

16 Logique. Dialectique.

Doctrine canonique (Epicure).
Consulter aussi les divisions de la Psychologie relatives aux
questions suivantes.

161 Analyse. Synthèse.

ÊT Concept. Terme. Prédicat. Prédicaments. Catégorie. Catégo-
rème. Genre. Univoque. Méthode d'immanence.

Voir aussi 153.1 Concept, en psychologie,
153.3 Abstraction.

.2 Jugement. Proposition. Affirmation. Négation. Equipollence.

162 Déduction. Induction. Méthode expérimentale.

Méthode des variations concomitantes, des différences.
Expérimentation. Observation,

163 Témoignage. Foi. Certitude morale, historique. Argument
d'autorité.

Voir aussi 165.1 Vérité logique.
234.2 Foi, en théologie,

164 Symbolique. Algébrique. Logique mathématique. Log:-
que algorithmique.

165 Critériologie. Certitude.
Critérium. Critique de la connaissance. Sources d’erreurs.
Sophismes. Préjugés. Epistémologie. Evidence. Paralo-
gisme. Antithèse. .

 

 
 

 

 

165.7 Vérité logique. Etats del’esprit (certitude, erreur, doute, opinion).
Voir aussi 163 Foi.
22 Certitude (objectivité) de l'ordre idéal. Objectivité des principes.
Axiomes. Notions premières. Principes. Principe de contra-
diction, d'identité.

3 Certitude (objectivité) du monde interne, du moi. Certitude
de conscience. Expérience interne.

4 Certitude (objectivité) du concept, du monde extérieur, de l’indi-
viduel. Expérience externe. Connaissance du concret. Trans-
cendant.

Voir aussi 152.8 Objectivation des sensations.
166 Syllogismes. Entymène.
167 Hypothèses. Postulats.

168 Raisonnement. Argumentation. Persuasion. Démonstra-
tion. Généralisation. Le Général.

Voir aussi 153.3 Abstraction.
153.6 Raisonnement, en psychologie.
161.1 Universaux.
162  Déductionet induction,
166  Syllogisme.
1 Définition.
2 Division. Classification.
3 Science. Système.

169 Analogie. Arguments probables. Exemples. Calcul des
probabilités et statistique au point de vue de la logique.
Paradoxe. ;

17 Morale. Ethique. Questions morales.

z On classera ici la morale théorique et la morale appliquée

aux diverses questions de la vie pratique, ainsi que la morale
naturelle ou scientifique, indépendante de toute métaphysique
et de tout dogme religieux.

La morale en général, ou une question particulière de la
morale, envisagée au point de vue d’un système philosophique
ou d’une doctrine religieuse, est classée ici sous la subdivision
de relation correspondant au système ou à la doctrine envi-
sagée. Ex. :

17 : 140.016 La morale de l’évolutionisme.

17222) Morale religieuse ou théologique en général.
17: 222,16 Morale du Décalogue.
17.: 282 Morale catholique. Commandements de l'Eglise catholique.

17: 284.2 Morale calviniste,

a
mn.

 

 

 

 

 

 

 

 
 

 

7e

4

 

 

 

 

 

171 Théories morales. Bases et philosophie de la morale.

+9
.OII

.012

013

Morale individuelle.

Autorité, loi, règle, norme morale. Maxime. Impératif catéso-
rique. Volonté divine, base de la morale. Morale religieuse,
morale chrétienne, morale naturelle, morale indépendante.
Autonomie. Anarchie morale. Hétéronomie.

Sentiment moral. Raison pratique. Obligation morale. Devoir.
Morale intuitive.

Perfection, idéal, bien et mal moral. L’honnête. Acte humain.
Action morale. Péché. Mérite et démérite. Sanction. Punition.

Bonheur. Béatitude. Hédonisme. Fin de l’homme. Destinée
humaine. Intention morale. Vie morale. Mœurs.

Voir aussi 150,4 Intention, en psychologie,

Utilitarisme. Eudémonisme.
Conscience morale. Probabilisme. Casuistique. Syndérèse.
Voir aussi 153.7 Conscience, en psychologie.
172.3 Täberté de conscience, en morale,
Morale basée sur l’éducation et morale basée sur l’évolution.
Education morale.
Altruisme. Solidarité. Charité.
Egoïsme. Intérêt. Individualisme.
Responsabilité morale. Imputabilité en morale.
Voir aussi 150.1 Liberté, en psychologie.
Devoirs de l'homme envers Dieu.
Voir aussi 217.
Devoirs de l’homme envers lui-même.

Devoirs de conservation, amour du travail, devoir de s'instruire. Dignité
personnelle.

172 Morale sociale. ;

Toutes les matières juridiques et sociales pouvantèêtre étudiées
au point de vue du droit naturel, on peut classer, facultativement
et par duplicata, les travaux de cette espèce à la morale sociale
par le signe de relation: , suivi du nombre classificateur propre
à chaque matière.

Rapports de l'individu et de l'Etat. È
Morale civique. Devoirs civiques. Devoirs des citoyens. Patriotisme. Cosmo-
politisme, Amour et défense de la patrie. Courage civique. Respect aux lois et
autorités. Humanitarisme,

Devoirs des autorités publiques et des gouvernants.
La religion et l'Etat.
Liberté de conscience. Tolérance, Intolérance, Persécution, suppression de la
religion. Respect des croyances.
Voir aussi 261.7 L'Egliseetl Etat, en théologie,
322. L'Eglise et l'Etat, en politique,
348.7 L'Eglise et l'Etat, en droit civil ecclésiastique.

 

 

 
 

 

 

 

 

 

 

 

 

172.4

173

©

174

D

ün À

©

©

 

Morale internationale. Paix et guerre.

Voir aussi 34r Droit international.

Morale familiale.

Voir aussi 343.55 Infractions contre l’ordre des familles, en droit pénal.
347.6 Mariage, en droit privé,
Mariage et divorce.
Polygamie et monogamie. Communauté des femmes,
Voir aussi 298 Mormonisme,
343.552 Polygamie, en droit pénal.
Amour libre.
Devoirs des époux.
Voir aussi 176.6 Adultère.
Infanticide.
Voir aussi r79.2 Cruauté envers les enfants.
343.622 Infanticide, en droit pénal.
Devoirs des parents envers les enfants.
Devoirs des enfants envers les parents. Devoirs des enfants les
uns envers les autres. Respect, obéissance, assistance.
Vie familiale. Solidarité de la famille.
Devoirs des maîtres et des serviteurs.

Voir aussi 331.78 Domestiques, en sociologie.
647 Domestiques, en économie ménagère.

Morale des diverses professions et occupations.

Déontologie. Vocation. Occupation. Emploi de la vie. Choix
d’une profession ou d’une carrière.

À subdiviser par division de relation, d’après la profession ou
l'occupation s'il n'existe une division spéciale. Voir aussi, sous
chaque sujet, subdivision commune (060) Déontologie de.
Clergé.

Médecins.
Avocats.

Voir aussi 347.965.3.
Morale en affaires.
Spéculation.
Jeu

Voir aussi 175.9 Paris,

343.56 Lejeu, en droit pénal...

Contrat. Promesse au point de vue moral.
Relations entre employeurs et employés, au point de vue moral.

Voir aussi 173.8 Devoirs des maîtres à l'égard des serviteurs,

175 La morale et les amusements et récréations.

8
9

À subdiviser par division de relation comme 79 Amusements
et récréations.

Lecture des romans.
Paris.

AE

 

 

 

 

 

 
 

 

176 La morale et les relations sexuelles.

Fe

177 La morale et les relations privées ou sociales. Morale

se

(Vita sexualis.)

Voir aussi 157 Amour, en psychologie.

Chasteté.
Célibat.

Voiraussi 254 Célibat du clergé.

Continence.

Perversions sexuelles. Actes contre nature. Inversion sexuelle.

Bestialité.
Voir aussi 343.543 Psychopathie sexuelle, en droit pénal.

Prostitution.

Ars amatoria. Erotica. Bacchanales. Mystères d'Eleusis, Licence des rues. Vie

galante. Proxénétisme. Truite des blanches. Moralité publique, etc.
Voir aussi 173.25 Amour libre.
343.344 Prostitution, en droit pénal.
351.764 Police des mœurs, en droit administratif.
Adultère.
Voir aussi 343.55r Adultère, en droit pénal.
La morale et l’art. Naturalisme dans l’art.
La morale et la littérature. Pornographie.
Voir aussi 343.542 Pornographie, en droit pénal.

sociale.

Devoirs envers les hommes en général.
Courtoisie. :
Voir aussi 305 Etiquette. Politesse. Savoir-vivre,
Conversation, bavardage, commérage.
Sincérité.

Mensonge. Flatterie. Médisance. Calomnie, Diffamation. Respect de l’hon-

neur et de la réputation d'autrui.

Luxe. Lois somptuaires. Toilette. Faste.

Inégalité sociale. Esprit de caste et de classe. Richesse et rang

social.
Amitié. Affection. Coquetterie.

Philanthropie. Humanité. Pitié. Dévouement et sacrifice.

Charité.
Moir aussi 146 Religion del’Humanité.
Misanthropie.
Devoirs de justice envers autrui.

Respect de la propriété, vol et fraude. Respect de la liberté individuelle.

178 La morale et la tempérance.

Stimulants et narcotiques. Alcoolisme. Intempérance. Ivresse.

Ivrognerie. Abus des boissons alcooliques.

 

 

 

 

GARE RE

 

 

 

 

 

 

 

 

 
 

 

il

 

 

 

 

 

 

DOTE

 

On classe ici, comme siège principal des matières, tout ce
qui concerne la tempérance en général et l'usage des stimulants
et narcotiques.

Voir aussi 343.57 Intempérance, en droit pénal.

613.3 Usage des boissons en hygiène,

613.8 Hygiène du système nerveux.
612.821.44 Alcool au point de vue physiologique.
663,7 Boissons alcooliques, fabrication,

Boissons enivrantes. Usage médical.
Abstinence totale ou tempérance.

Boissons prises en société, à table.
Commerce des boissons enivrantes. Licence.

A classer sous 38 : 663.7,
Voir aussi 335.26 Impôts.

Prohibition du commerce des boissons enivrantes,
A classer sous 351,765.
Réformatoires. Asiles pour buveurs, etc.
Voir aussi 362.13.
Tabac.
Stimulants et narcotiques divers.
Opium, hashich, chloral, éther, etc,

Gourmandises et intempérances diverses.

179 Questions diverses de morale.

I
-2

Moralité de la presse.
Cruauté envers les enfants. Enfants martyrs.
Voir aussi 173.4  Infanticide.
331.3 Travail des enfants.
362,74 Protection de l'enfance en général.
Cruauté envers les animaux. Œuvres en faveur des animaux.
Voir aussi 343.58 Mauvais traitement des animaux, en droit pénal.
351.765 Protection des animaux, en droit administratif.
Vivisection.
Voir aussi 612.012 Vivisection. (Physiologie)
614.22 Vivisection. (Médecine publique.)
Serment. Jurons.
Héroïsme, bravoure, courage, poltronerie.
Vie, duel, suicide. Homicide. Légitime défense. Respect de la
vie d'autrui.
Voir aussi 394.8 Coutumes.
343.613 Duel, en droit pénal.
343.614 Suicide, en droit pénal.
Vices et défauts divers.

Orgueil, cupidité, envie, colère, paresse, jalousie, haine, etc.

Vertus et qualités diverses.

Humilité, libéralité, douceur, prudence, patience, amour du travail, diligence,
modestie, ordre, économie, etc.

 

 

 

 

 

 

 
 

#

 

18 et 19 : Concordances.

 

I81

O HITS

 

© oo

QI oE &'D

à

œ

Pour mémoire, on a reproduit ci-après les divisions qui avaientété
attribuées en propre à un certain nombre de philosophes et d’écoles
philosophiques dans les éditions antérieures des tables de classifica-
tion. Voir observation sous r D.

18 Philosophes anciens.

Philosophes orientaux.

Chinois. (Confucius, Mencius.)
Egyptiens.
Juifs. (Kabale, Philo, Maimonides.)
Indiens. (Gymnosophistes, Nirvana.)
Voir aussi 218 Annihilation.
Persans. Suffisme.
Chaldéens.
Sabéistes.
Phéniciens.
Autres philosophes orientaux. Syriens, etc.

182 Philosophes grecs en général. Philosophes antésocra-

tiques.

Ioniens. (Thalès, Anaximandre, Anaximène. Ecole maté-
rialiste.)

Italiques ou Pythagoriciens. (Ecole mi-idéalistes.)

Eléates. Ecole idéaliste. (Xénophanes, Parménides. Zéno.
Mélissus.)

Héraclite.

Empédocle.

Atomisme.

Démocrite.

Anaxagore.

Autres philosophes grecs de la période ancienne.

183 Sophistes. Ecole socratique.

Sophistes. (Protagoras, Gorgias, Prodicus, Hippias.)
Socrate.

Ecole socratique.

Ecole cynique. (Antisthène, Diogène, Crates, etc.)
Ecole cyrénaïque. (Aristippe, Hegesias, etc.)

Ecole de Mégare. (Buclide, Eubulides, Diodore, etc.)
Eléens, Erétriens. (Phedon, Menedemus, etc.)

 

 

 

 
 

 

 

 

 

 

 

 

 

 

I
Se)
3

+ & NH

186

$ à D

187

188

HI D nE & D

1

 

184 L'ancienne académie. Ecole platonicienne.

185 Aristotéliciens. Le iycée. Péripatéticiens.

Théophraste.

Pyrrhoniens. Néo-Platoniciens.

Epicuriens. (Epicure, Lucrèce.)

Stoiciens.

.189 Premiers chrétiens et philosophes du moyen âge.

 

Platon.
Speusippe.
Xénocrate.

Aristote.

Eudème.
Straton.

Pyrrhoniens. Sceptiques. (Pyrrho. Timon.)

Nouvelle Académie. (Arcésilas, Carnéade, Plutarque.)

Eclectiques. (Cicéron.)

Ecole d'Alexandrie. Néo-Platoniciens. (Philon, Plotin, Pro-
clus, Porphyre, Jamblique.)

Zénon.
Cléanthe.
Chrysippe.
Panætius.
Posidonius.
Sénèque.
Epictète.
Marc-Aurèle.

Gnostiques et byzantins.
Voir aussi 273.1 Basilides, Marcion.
273,2 Manichéisme.

Pères de l'Eglise. Patristique. (Tertullien, Augustin, Clément,
Origène.)

Philosophes arabes et asiatiques. (Avicenne.)

Les scolastiques. (Duns Scott, saint Thomas d'Aquin,
saint Anselme, Abélard.)

Mystiques. (Reuchein, Cornélius, Agrippa, Paracelse, Servet,
Bôhm, etc.)

Théosophes.

Averroïstes.

Panthéistes et autres antiscolastiques.

Philosophes de la Renaissance.

 

 

 

 

 

 

 
 

 

IOI

O OI oO Es à D H D DOI Gin & h à O DOI UE &w D h

© DID ik wDh

 

19 Philosophes modernes.

Philosophes américains.

Jonathan Edwards.

O. A. Brownson.

Ralph Waldo Emerson.
Laurens P. Hickock.

James Mac Cosh.

Noah Porter.

Francis Bowen.

William T. Harris.

Autres philosophes américains.

102 Philosophes anglais.

Bacon.

Locke.

Berkeley.

Hume.

Reid.

Dugald Stewart.

John Stuart Mill.

Spencer.

Autres philosophes anglais.

103 Philosophes allemands.

Leibnitz,

Kant.

Fichte.

Schelling.

Hegel.

Schleiermacher.
Schopenhauer.

Lotze.

Autres philosophes allemands.

194 Philosophes français.

Descartes:

Malebranche.

Condillac.

Rousseau.

Diderot.

Lamennais.

Cousin.

Comte.

Autres philosophes français.

 

 

 

 

 

 
s CEE

195 Philosophesitaliens.

106 Philosophes espagnols.

197 _ Philosophes slaves.

198 Philosophes scandinaves.

 

 

109 Autres philosophes modernes.

 

 
 
