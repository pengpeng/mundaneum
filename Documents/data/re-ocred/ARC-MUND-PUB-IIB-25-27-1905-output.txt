 
 

 
 

 
 

 
 

 
 
 

  

 
6rr ANATOMIE.

Divisions principales.

61x (o)
6x1.01

6Irt1.012
611.013
GIL.O14
611.016
61.018
6I1.019

661.1à.9 |

GII.I
611.2
Grr.3
611.4
Gr1.6
611.7
611.8
611.9

Généralités. Ouvrages généraux.

Anatomie analytique.

_ Tératologie.

Ontogénie. Embryologie.
Anatomie anthropologique.
Anatomie paléontologique.

_ Histologie.

Anatomie des animaux inférieurs.

Anatomie systématique.

Angéiologie. .

Organes respiratoires.

Organes de la nutrition.

Système lymphatique.

Appareil génito-urinaire..

Organes locomoteurs. Peau.
Système nerveux. Organes des sens.
Anatomie topographique.

Observation générale.

Chacune des subdivisions de l'anatomie due Poe
être subdivisée par combinaison at moyen du signe de
relation : suivi des autres divisions de l'anatomie. Ex

6It.12
GII-12 :
6rt.12 :
6Tr.12 :

= Le cœur. ë ;
61r.013 Embryologie du cœur.
611.018 Histologie du cœur.

611.13 Circulation axtérielle du cœur.

 
: 617,42

: 611.46 ë

: 61r.72
: 611.73
: 6IT.74T
: 611.742
: 611.743
: 6r1.744
: 611.75
: 611.83

611.80

| Embryolo cn
! éloges: ;
= Tissu conjonctif.
Cartilage.
+ Epithélium:
_ Glandes.
_ Muqueuses.
_ Séreuses.
Structure microscopique des nerfs
Circulation artérielle.

. Circulation veineuse,

Circulation lymphatique.
Ganglions lymphatiques.

Aponévroses. ;
Gaines tendineuses, Re
Ligaments.
Gaines synoviales. Bourses Séreuses
Innervation.
Innervation +
Ganglions nerveux.

 
GT TOI. 0 Forme extérieure de l'embryon.
Fe AUnEKES de l'embryon.
Sac vitellin.
Allantoïde.
_Amnios. :
Liquide  .
j Ch ion

la grossesse.

| Embryologie expérimental.

| Anatomie anthropologique.
‘ Voir aussi 572 Ratronoiese :

.016 Anatomie paléontologique.
RATES Voir aussi576 Paléontologie.

6rr.o18 Histologie.

Hal e la structure microscopique d’un organe,
s'expriment en faisant suivre le nombre propre à er
organe, par nt de l'indice : Gr. O18.  . :

611.47 Rate. k
GIT.41 : GIROTS Des de là rate. RE

Ci ee
À classer à 576. 3 Cytologie. .

Système ‘conjonctif. Tissu conjoncti.  e. 2

‘Tissu muqueux. ;
_ Tissu adipeux. .
_ Tissu élastique.
‘Tissu fibreux.
Tissu cartilagineux.
_ Tissu osseux. “4 RES Rue
Cellules osseuses. È ne HÉGES
. Substance fondamentale. Res osseuses. Canaux de
_ Havers. Tissu spongieux, étc.
. Périoste. tee
& ie des os.

 
611.018.5 Sang.

Voir aussi 6r2,rr Sang, en ne Lee
Pour le sperme, or 6xr.013.11 et 612.617.
Globules rouges. 3
Hématoblastes. don Globulins. FHRGÈNeS, etc.
Leucocytes.
Granulations du leucocyte.
Lymphocytes.
Mononucléés.
Polynucléés.
Éosinophiles,
Mastzellen. Chlasmatocytes.
_ Plasmazellen.
Plasma sanguin. -
Salive.
Voir aussi 6r2.313.1 Composition dela ee ;
Chyle.

Voir aussi 612.332 Achon du suc port

1

Lymphe.
Voir aussi 612.42 LAS Habite.

Mucus.
< Moir aussi 612.495, Mucus, en pli,
bats

*Voir aussi 612. 664.7 1 (Composition du ait.

oi musculaire Se
Muscle lisse.
 Musclestrié.
Muscle cardiaque. U
Etude histologique de la Each musculaire.

Système épithélial.
Système glandulaire.
Muqueuses. è
Séreuses.

‘Tissu nerveux.
Cellule nerveuse en général.
Forme de la cellule. Types de cellules.
Rapports anatomiques des cellules nerveuses entre
elles. Réseaux extra-cellulaires. Théori ie e dur neurone.
Corps cellulaire. L
- Protoplasme. d
Corps de Nissl.
Structures Pehculaires :
Structures fibrillaires.
_ Structures canaliculaires.
Fe He

z

 
 

 
Gr1.136.4

4
+42
.43
44
01

.14
I4T
.142
.143
CH

8 .

_ Tronc cœliaque.
Artère hépatique. |
Artère. splénique.
Artère coronaire stomachique.
Artère omphalo-mésentérique.
Artère mésentérique supérieure.
| Artère petite mésentérique.
Artères capsulaires moyennes.
Artères 1 rénales.
 Artères spermatiques.
_ Artères utéro-ovariennes.

Artèresiliaques.

: Artères iliaques primitives.
Artère iliaque interne ou hypogastrique. Fa
Artère ilio- lombaire.
Artères sacrées latérales. É

Aïrtère fessière.

Artère obturatrice:

Artère ombilicale.

Artères vésicales.

Artère déférentielle.

Artère prostatique.

Artère utérine.

Artère vaginale.

Artère hémorrhoïdale moyenne.

Artère ischiatique.
/ Artère honteuse interne.
Artère allantoïdienne. “ ;
Artère iliaque externe.

Artère. épigastrique.

_ Artère cir nflexe iliaque.

5 Ar! tère fémorale.

_ Artère fémorale profonde.

. Artère fémorale superficielle.
Artère poplitée. te
Artère tibiale antérieure.

‘Tronc tibio-péronier. Fu
Artère tibiale postérieure.
 Artère péronière. ; ;
Artère pédieuse. -

 Artères plantaires.

Artères interosseuses du pied.

 Veines. Fe

£  Veines pulmonaires.
Veines coronaires.
Veine subintestinale.
 Veines cardinales.
_ Canal veineux d’Arantius.

 
. Veine cave Supérieure, Troncs veineux brachiocéphaliques
etleuts branches thoraciques.
Veine jugulaire interne.
+ | Voir aussi 611.218 Sinus crâniens.
_ Veiïnes diploïques.
 Veine jugulaire ‘externe.
Veines sous-clavière et axillaire.
Veines profondes du membre supérieur.
Veines superficielles du membre supérieur.
Veines jugulaire postérieure et vertébrale.
Veines azygos, lombaire ascendante, iléolombaire, sacrée.

Veine cave supérieure.
* Veines lombaires.
Veines rénales.
Veïnes Capsulaires moyennes.
Veines diaphragmatiques inférieures:
Veines diaphragmatiques supérieures:
Veines spermatiques.
Veines ovariennes.

Veines iliaques.
 Veines sous-cutanées oo ie
Veines profondes du membre inférieur.
Veiïnes superficielles du membre inférieur.
Veine saphène interne. :
Veine saphène externe.

Veine porte.
Grande veine mésentérique.
Petite veine mésentérique.
Veines sus-hépatiques.
Veines ombélicales.

.15 Vaisseaux capillaires.

611.2 Organes respiratoires.

12 Nez.

Nez externe.
Fosses nasales.
Cloison.
Plancher des fosses nasales.
Choanes.
Cornets.
Cornet inférieur.
Cornet moyen.
Cornet supérieur.

{

 
 

 
 

 

611.34 Intestin.
ï _  6rr.34: 611.383 Méso-intestn.

 

 

.341 Intestin grêle.
:342 Duodénum. à
: 611.342: 611.383 Mésoduodénum, fossettes duodénales.
.343 Jéjunum.
611.343: 611.383 Mésojéjunum, diverticule de Meckel, recessus para-
jéjunal.
.344 Iléon.
611.344: 611,383 Méso-iléon.
.346 Cæcum.
611.344: 611.383 Mésocæcum.
5 é 7 Valvule iléo-cæcale.
Er .2 Appendice iléo-cæcal.

611.346: 611,383 Méso-appendice.

 

   

.347 _ Gros intestin et cæcum.
611.347 : 611,383 Mésentère du gros intestin. C S NE
.348 Côlon. É

E 611:348: 611.383 Mésocôlon, fossette intersigmoïdess
Re .349 S. iliaque. ji
. 611.348 : 611.383 MésoS,. iliaque.

E ns .35 Rectum. Anus.

 

    
     
       
         
       
     

Lee .351 Rectum. 5 ;
ne .352 Anus.

36 Foie.

.361 Voies biliaires en général. Canalicules biliaires. Fe

.362 Canal hépatique.

.364 Canal cystique.

.366 Vésicule biliaire. F

.367 Canal cholédoque. Tubercule et ampoule de Vater.

368 Capsule du foie.

  

37 Pancréas.

373 Canal de Wirsung.

5 379 Canal de Santorini.
.376 Acini.
577 Ilots de Langerhans.

.38 Péritoine. Mésentère. Cœlome.

.381 Péritoine.
.382 Epiploon. :
.383 Mésentère. :

Les mesos s'expriment par combinaison. Ex. :
611.383 : 611.348 Mésocôlon.

 

.389 Cæœlome.
.39 Corps adipeux.

 

 
 

 

6II.4 | Système lymphatique.

VAL Rate.
.418 | Capsule de la rate.
42 Vaisseaux lymphatiques.

Pour les lymphatiques des organes et des tissus, voir à chacun de
organes et de ces tissus.

\

 

UE mbète:
é 12 Face. à

518 Cou.

A ‘Thorax.

.15 Abdomen.

; .16 Région pelvienne.

pe ; “ny Mémbre supérieur.
ï ‘172 Bras.
pe .174. Avant-bras.
be ; .176 Main.
[6 .18 Membre inférieur.
E 19277 Cuisse.

.184 à Jambe.

..188 hPied:

.19 Queue.

Citerne de Pecquet.

Canal thoracique.

Grande veine lympathique.
Vaisseaux chylifères.

D + &

 

:43 Thymus.

 

.435 Thymus accessoire.

.44 Thyroiïide..
.445 Thyroïdes accessoires ou aberrantes.
.447 Glandules parathyroïdes.

  
 

.45 Capsules surrénales.

.46 Ganglions lymphatiques.

46 Ganglions de la tête.
è .462 Ganglions de la face. '
463 Ganglions du cou. |
#L Ganglions sous-occipitaux.
. 2 _Ganglions mastoïdiens. |

3 Ganglions parotidiens.

4 . Ganglions sous-maxillaires.
D Ganglions sous-mentaux.
Lo Ganglions rétro-pharyngiens.
;
8

 

Chaînes cervicales et jugulaires.
Ganglions sus-claviculaires.

ces

 

   
  
   
   
   
   
   
   
   
   
   
   
   
 
 
611.66
604
66
666

.667
.668

.67
.671

ne

.672.
.673

AUtÉTUS:

 Cornes de l’utérus.
Corps. Parenchyme.
Col. NE des

‘ Annexes de l'utérus.
Placenta.

Vagin. Vulve.

Vagn.
Cul-de-sac du vagin.
Hymen. Vestibule du vagin.

 Vulve.
Mont de Vénus.
Grandes lèvres
Petites lèvres. | ;
\Drètré de la femme. : ù
Sphincter de Vurètre de la ee

Clitoris. :

Bulbes du vagin.
Glandes de Bartholin.

Mamelles.

Mas et aréole.

Acini. 3

Canaux et sinus galactophores.
Glandes mammaires accessoires.

7 on locomoteurs. Peau.

| Ostéotogie.

one Vébrae. se
_Vertèbres cervicales.
Atlas.
Axis. al
- Vertèbres thoraciques.
Vertèbres lombaires.
Vertèbres sacrées.
 Vertèbr es COR
5 Côtes.
- Cartilages costaux.

Sternum.
Os de la tête.

 
 
  
 
 
 
 
 
 
  
  
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  

G11.718.7 Os du tarse.

 

71 Astragale.

75 Calcanéum.

.75 Cuboïde.

.76 Scaphoïde.

+78 Cunéiformes.

.8 Métatarsiens. à
.9 Os des orteilles. 3
OI Phalanges.

.93 Phalangines.

.95 Phalangettes. =

.72  Syndesmologie. Arthrologie.

721 . Articulations des vertèbres entre elles et avec le crâne.

I Articulations occipito-atloïdienne et occipito-axoïdienne.
3 Articulations atloido-axoïdienne etatloïdo-odontoïdienne.
5 Articulations intervertébrales.

.6 Articulations sacro-vertébrales.

mi Articulations sacro-coccygiennes.

8 Articulations coccygiennes.

 

 

 
 
  

22 Articulations costo-vertébrales.
729 Articulations chondro-costales, chondro-sternales, inter-
chondrales et sternales.

.724 Articulation temporo-maxillaire. ;

727 Articulations du membre supérieur.
ÉE Articulations sterno-claviculaire et costo-claviculaire.
2 Articulations de l'épaule. ;
20 Articulation scapulo-humérale.
24.  Articulationacromio-claviculaireetcoraco-claviculaire.
3 Articulations du coude. s
SE Articulation huméro-cubitale.

RER .34 Articulation radio-cubitale supérieure.
A Articulations du poignet.
41 Aïrticulation radio-cubitale inférieure
Ë é : .44 Articulation radio-carpienne.
50 Articulation carpienne.
.54  Articulation médio-carpienne.
.6 Articulations carpo-métacarpiennes.
F 7 Articulations intermétacarpiennes.

.8 Articulations métacarpo-phalangiennes.
9 Articulations phalangiennes de la main.

 
       
    

ke .728 Articulations du membre inférieur. | :
.I Articulations sacro-iliaque et pubienne.
/ 2 Articulation coxo-fémorale.
.3 Articulations du genou.
ie Articulation fémoro-tibiale. ee

 
 

Aïrticulation tibio-péronière supérieure.

  

 
 

 

 

 

ü& & D

732

 

 

Articulations du cou-de-pied.
Articulation tibio-péronière inférieure.
Articulation tibio-tarsienne.
Articulations tarsiennes.
Articulations astragalo-calcanéennes, scaphoïdienne.
Articulation calcanéo-cuboïdienne.
Articulation médio-tarsienne.
Articulation cuboïdo-cunéiforme.
Articulations scaphoïdo-cuboïdienne, scaphoïdo-cunéi-
forme. :
Articulations intercunéiformes.

Articulations tarso-métatarsiennes.

Articulations intermétatarsiennes.
Articulations métatarso-phalangiennes.
Articulations phalangiennes du pied.

Myologie.

Musclès dorsaux.

Muscles spino-huméraux (grand dorsal, rhomboïde,
angulaire de lomoplate, levator scapulæ).

Muscles spino-costaux (dentelés postérieurs).

Muscles spino-dorsaux (trapèze, cucullaris).

Muscles splenius et complexus.

Muscles sacro-lombaires (sacrospinalis, iliocostalis), long
dorsal (longissimus dorsi), épi-épineux (spinalis).

Muscle transversaire épineux (transversospinalis, semi-
Spinalis, multifidus, rotatores), :

Muscles intertransversaires, interépineux, transversaire
du cou.

Muscles biais de la tête, droits postérieurs et droit
latéral de 1a tête.

Muscles de la tête.
_Peaucier du cou.

Muscles épicrânien, occipital, frontal, temporal super-
ficiel.

Muscles du nez (pyramidal, transverse du nez, myrti-
forme, constricteur des narines , dilatateur des narines).

Muscles de l'oreille.

Muscles orbiculaires de l'œil (sourcilier, orbiculaire des
paupières). :

Muscles buccaux (buccinateur, orbiculaire des loves
canin, élévateur commun de la lèvre supérieure, éléva-
teur propre de la lèvre supérieure, zygomatiques,
muscle de la houppe du menton, carré du menton,
triangulaire des lèvres, risorius, transverse du menton).

Muscles masticateurs (masséter, temporal, ptérygoï-
diens).

 

  

 
 

611.733 Muscles du cou.

#1 Sterno-cléido-mastoïdien.

2 Sterno-cléido-hyoïdien.
Omo-hyoïdien.
Sterno-thyroïdien.
Thyro-hyoïdien.
Long du cou.
Grand droit antérieur.
Petit droit antérieur.
Scalène.

Muscles hyoïdiens ( asie, stylo-hyoïdien, mylo-hyoi-
dien, génio-hyoïdien).

Muscles thoraciques.
Grand pectoral.
Petit pectoral.
Sous-claviculaire.
Surcostaux.
Grand dentelé.
Intercostaux externes.
Intercostaux internes.
Présternal.

. Muscles abdominaux et coccygiens.
Droit de l'abdomen.
Pyramidal de l'abdomen.

Obliques de l'abdomen.
Crémaster. :
‘Transverse de l’abdomen.
Carré des lombes.

Muscles coccygiens.

Muscles des membres supérieurs. :

Muscles de l'épaule (deltoide, sous-scapulaire, sus-

_  épineux, sous-épineux, petit rond, grand rond).
 Muscles du bras (biceps, brachial, coraco-brachial,
_ brachial antérieur, triceps)..
 Muscles de l’avant-bras.
Muscles pronateurs de l'avant-bras (rond pronateur,
carré pronateur).

Muscles fléchisseurs de l’avant-bras (grand palmaire,
petit palmaire, cubitalantérieur, fléchisseurs des doigts,
fléchisseur du pouce).

Muscles extenseurs et supinateurs de l’avant-bras (long

_ supinateur, court supinateur, radiaux, extenseurs des
doigts, extenseur du petit doigt, cubital postérieur,
anconé, long abducteur du pouce, court et long exten-
seurs du pouce, extenseur de l'index).

Muscles de la main (lombricaux de la main, interroseux
de la main, thénar, court abducteur du pouce, court
fléchisseur du pouce, opposant, adducteur du pouce,
hypothénar, adducteur du petit doigt, court fléchisseur
du petit doigt, opposant, palmaire cutané).

 

 
»
611.738 Muscles des membres inférieurs.
s GT Muscles de la hanche et de la fesse (psoas-iliaque, petit

_psoas, fessier, pyramidal de la hanche, jumeaux de la
hanche, obturateur carré, crural).

Muscles de la cuisse (couturiér, droit antérieur de la
cuisse, triceps fémoral, quadriceps fémoral, tenseurs
de la synoviale du genou, biceps fémoral, demi-tendi-
neux, demi-membraneux, tenseur du facia lata, vaste
externe, pectiné, adducteurs). \

Muscles de la jambe.

Muscles antérieurs de la jambe (jambier antérieur, tibial
antérieur, extenseur des orteils, extenseur des gros.
orteils, péroniers). :

Muscles postérieurs de la jambe (jumeaux cruraux,
gastrocnémiens, soléaire, plantaire grêle, poplité,
jambier postérieur, tibial postérieur, fléchisseur du
gros orteil).

Muscles du pied.

Muscles dorsaux du pied (pédieux).

Muscles plantaires (adducteur du gros orteil, court
fléchisseur du gros orteil, abducteur du petit orteil,
court fléchisseur du petit orteil, court fléchisseur plan-
taire, accessoire du long fléchisseur, lombricaux du
pied, interosseux du pied, abducteurs du gros orteil).

Téndons.
À diviser comme les muscles.

_Aponévroses,
Aponévroses dé la tête.
Aponévroses de la face.
Aponévroses du cou. -
Aponévroses du thorax. F
_ Aponévroses de l'abdomen.
Aponévroses de la région pelvienne et périnéale.
Aponévroses du membre supérieur.
Aponévroses de l'épaule.
Aponévroses du bras.
Aponévroses du coude.
Aponévroses de l’avant-bras.
Aponévroses du poignet.
Aponévroses dela main.
. Aponévroses des doigts.
st Aponé oses du membre inférieur. :
_ Aponévroses de la hanche et de la Fe
Aponévroses de la cuisse. .
Aponévroses du genou.
. Aponévroses de la jambe.
_ Aponévroses du cou-de-pied.
_ Aponévroses du pied.
.  Aponévroses des orteils.

N

 
.78
.78I

785€

.786
-787
.788

Gaines tendineuses ét fibreuses. Ligaments.
À diviser comme les OR

Gaïnes tendineuses.
À diviser comme les muscles.

Bourses séreuses.
À diviser comme les aponévroses.

Tissu conjonctif.

Voir aussi 6rr.078.2 Tissu conjonctif, en histologie.

Téguments. Glandes de la peau.

Epiderme.

Glandes de la peau.
Glandes sébacées.
Glandes sudoripares.
Glandes ciliaires.
Glandes anales.
Glandes à cérumen.
Derme. ’
611.778 : 6rr.73 Muscles du derme,
Pigment.

Hypoderme.

Poils, ongles, écailles, plumes,
Poils. ;
Ecailles.

Ongles.
Plumes.
Cornes.

611.8 Système nerveux. Organes des sens.

.8x

i

SIT

ZT
«2

Encéphale. Cerveau.

Structure génér ale du cerveau et de l’axe eo spinal.
Voies de conduction. Systèmes de fibres. \
Voies motrices.
Voie pyramidale.

Voies optiques.

Voies acoustiques.
Ruban de Reil latér al.

Voies olfactives.

Voies gustatives.

Voies sensitives.
Ruban de Reil Doi
Voies unissant le cerveau au cervelet et vice versa.
Voïes unissant l'écorce au corps strié et vice versa.
Voies runissant l'écorce au thalamus et vice versa.
Autres voies corticales..
Autres voies courtes intra-cérébrales.
Fibres d'association.

_ Fibres commissurales.

 
 

 

 

 

 

 

611.812
2813.
+
SEL
2
9
14

10
.16
17
.18
.I9
2

OL
22
:23
24.
+29
3

© Go O tn

.8T4

orTaouk

.815

Prosencéphale.
Télencéphale. Cerveau antérieur.

Écorce cérébrale. Lobes. Sillons. Circonvolutions.
Lobe frontal.

Lobe pariétal et sillon de Rolando.
Lobe temporo-sphénoïdal. Scissure de Sylvius.
Corne d'Ammon. Corps godronné. Corps bordant.
Noyau amygdaloïde.
Lobe occipital. Sillon pariéto-occipital.
Lobule paracentral.
Cuneus etscissure calcarine.
Lobe quadrilatère.
Insula de Reil.
Corps strié.
Noyau caudé.
Noyau lenticulaire.
Avant-mur.
Septum lucidum, cinquième ventricule.
Bandelette demi-circulaire.

Rhinencéphale, bulbe olfactif, bandelettes olfactives,

. substance perforée antérieure, etc.

Chiasma optique, bandelettes optiques; récessus optique,
lame terminale, commissures de Gudden et de Mey-
nert ec.

Hypophyse.

Corps mamillaires. Faisceau de Vicq-d'Azyr.

Centre ovale. Substance blanche cérébrale.

Ventricules latéraux.

Corps calleux.

Diencéphale. Cerveau intermédiaire.

Hypothalamus. Corps de Luys. Anse pédonculaire.
Anse lenticulaire. Ganglion basal de Meynert. Com-
missure postérieure.

Couche optique. Commissure grise.

Epiphyse. Glande pinéale. Habenula.

Corps genouillés.

Capsule interne.

Troisième ventricule. Trous de Monro.

Mésencéphale. Cerveau moyen.

Tubercules quadrijumeaux (bijumeaux) et bras de ces
tubercules. =

Pédoncules cérébraux.
Substance noire de Sæmmering.
Pied du pédoncule.
Calotte.
Noyau rouge.

Système de l'oculo-moteur commun et des oculo-moteurs
en général.

Aqueduc de Sylvius.

 
611.816 Isthme de l’encéphale.
3 Valvule de Vieussens & :

4 Pédoncules cérébelleux supérieurs.

5 Système du pathétique.

6 Ganglion interpédonculaire.

.817 Mésencéphale. Cerveau postérieur.

Ge Cervelet.
.II ….  Lobes, sillons, valvule ce Fe etc.
215 _Noyaux gris centraux, olives cérébelleuses, noyaux
eee _ du toit, noyaux dentelés; etc. à
Substance blanche du cervelet.
* Protubérance annulaire. Pont de Varole.
Calotte. Formation réticulaire.
Noyaux du pont. Pédoncules cérébelleux moyens.
Fibres unissantla protubérance au ele etrécipro-
quement.
Système du trijumeau.
Système de l’oculomoteur externe. :
Système du facial et du nerf intermédiaire de Wrisberg.
Système de l'acoustique. ge eus Co tra
ee

>. Ar Re Hitbe: Moelle .

és inférieures et accessoires et systèmes de fibres
qui en dépendent.

Système du glosso- pharyngien et du pere -gastrique.

Système du spinal. =

Système de l’hypoglosse. ë

Pédoncules cérébelleux inférieurs. Corps restiformes. .
Novaux de Goll et de Burdach. Voies
_leuses et cérébello-spinales.

Substance réticulaire et autres parties du bulbe.

a etméninges cérébrales en particulier.

Be ee ER ne Pere vs

Toile orodenne. Piexus Choroides.

Arachnoïde. È

Cavité sous-arachnoïdienne. Confluents sous-arachnoï-
diens. : FE

Dure-mère. Tente du cervelet. Faux du cerveau.

Liquide encéphalo-rachidien.

Trous de Magendie et de Luschka.

Granulations de Pacchioni.

 
‘611:82° : Moelle épinière.

.821I Renflements, sillons, structure générale, etc.
‘ Moelle cervicale:

Moelle dorsale.
Moelle lombaire.
Moelle sacrée.
Cône terminal.
Queue de cheval.

Substance grise.

._ Cornes antérieures.
Cornes postérieures.
Colonne de Clarke. Doatr de Sülling.
Autres cellules.
Névrolgie.

Substance blanche.

Cordon antérolatéral.

Faisceau pyramidal.

Faisceau cérébelleux direct.

Faisceau de Gowers.

Faisceau sulco-marginal.

Faisceau intermédiaire du cordon latéral.

Fibres courtes.

Commissure antérieure.

Pour les fibres cérébello- spinales ctspino-cérébelleuses, voir nous 818. 7: :

Cordons postérieurs. s

Fibres ascendantes.

Fibres descendantes.

n ibres radiculaires. :

Racines des nerfs
Racines pi
Racines antérieures.
Ganglions spinaux.

Canal rachidien. Ependyme. Sinus rhomboïdal. ee de
Reissner.

Méninges spinales.

A subdiviser comme 6zr.810.
Nerfs périphériques.

Nerfs crâniens et leurs ganglions.
Nerfolfactif.
 Nerfoptique.
Nerf oculomoteur commun.
Nerf pathétique.
Nerftrijumeau.
Ganglion de Gasser. .
"Nerf. ophtalmique de Willis.
. Ganglion ophtalmique.

 
 

 

 

G11.831.54
.541
542
.56
.561
.37I
072
073

OI
.OII
.012
.92
.02I
022
:923
.924
.925
.926
.027
.928
929
.93
97
.832
Æ
Fi

.833

 

Nerf maxillaire supérieur.
Ganglion de Meckel.
Nerf sus-orbitaire.
Nerf maxillaire inférieur.
Ganglion otique.
Nerf dentaire inférieur.
Nerf buccal.
Nerf lingual.
Nerf auriculo-temporal.
Nerf masticateur.
Nerf oculomoteur externe.
Nerf facial.
Nerfs pétreux supérieurs.
Corde du tympan.
Autres branches collatérales.
Branches terminales.
Nerf intermédiaire de Wrisberg.
Ganglion géniculé.
Nerf auditif acoustique.
Nerf cochléaire.
Ganglion spiral de Corti.
Nerf vestibulaire.
Ganglion vestibulaire de Scarpa.
Nerf glosso-pharyngien.
Ganglions d'Andersch et d'Ehrenritter.
Nerf de Jacobson. Nerfs pétreux profonds.
Nerf pneumogastrique. : ë
Ganglions jugulaire et plexiforme.
Nerfs et plexus pharyngiens.
Nerfs cardiaques et nerf de Cyon.
Nerf laryngé supérieur.
Nerf récurrent.
Branches pulmonaires.
Branches œsophagiennes.
Branches péricardiques.
Branches abdominales.
Nerf spinal, accessoire de Willis.
Nerf hypoglosse.

Nerfs rachidiens.

Branches postérieures.
Branches antérieures.

Nerfs cervicaux.

Branches postérieures des nerfs cervicaux. Petit et grand
nerfs sOus-occipitaux.

Branches antérieures des nerfs cervicaux.

Plexus cervical.
Plexus cervical superficiel.
Plexus cervical profond.
Branches musculaires du plexus cervical profond.
Branchedescendanteinterne. Nerf cervical descendant.
Nerf phrénique.
Branches anastomotiques.

 

 

 

 

 

 
 

STE &

   

 

 

 

611.833.4 Plexus brachial.
AIT Nerf de l’angulaire et du rhomboïde. Nerf dorsal de
l'omoplate.
.412 Nerf du grand dentelé. Nerf long thoracique.
.413 Nerf sous-scapulaire.
414 Nerf du grand rond.
415 Nerf du grand dorsal.
.42 Nerf axillaire, circonflexe.
431 Nerf sus-scapulaire.
.432 Nerf du sous-clavier. :
.433 Nerf du grand pectoral.
.435 Nerf du petit pectoral.
.44. Nerf musculo-cutané.
.45 Nerf médian.
.46 Nerf cubital. ;
47 Nerf radial.
.48 Nerf brachial cutané interne. : ;
49 Nerf accessoire du brachial cutané interne.
è .834. Nerfs thoraciques.
116 FT Branches postérieures.
2 Branches antérieures. Nerfs intercostaux.
.835 Nerfs lombaires et sacrés.
I Branches postérieures.
2 Branches antérieures.
e 3 Plexus lombo-sacré. : à
4 Plexus lombaire. ? u
AT Grand nerf abdomino-sénital, ilio-inguinal.
3 42 Petit nerf abdomino-génital, ilio-inguinal.
.43 Génito-crural, génito-fémoral.
.44 Fémoro-cutané.
0 Nerf crural, fémoral.
ou Nerf musculo (fémoro)-cutané externe.
a re 02 Nerf musculo (fémoro)-cutané interne.
- 09 Nerf du quadriceps.
2:04 Nerf saphène interne.
.6 Nerf obturateur.
.61 Nerf obturateur accessoire.
.68 Nerf lombo-sacré.
ee 7 Plexus sacré.
TL Nerf fessier supérieur.
72 Nerf fessier inférieur.
AS Nerf cutané postérieur de la cuisse.
.74. Autres nerfs.
.8 Nerf sciatique.
Ù -84 Nerf sciatique poplité externe, péronier commun.
.85 Nerf musculo-cutané, péronier superñciel.
86 Nerf tibial antérieur, péronier profond.
£ .87 Nerf sciatique poplité interne, tibial.
.88 Nerf saphène externe, cutané interne de la jambe.

 

 
611.835.9  Plexus génital, honteux.
.93 Branches collatérales.
99 Nerf honteux interne.

Plexus sacro-coccygien. Nerfs coccygiens.

Système du grand sympathique.
Sympathique cervical.
Ganglion cervical supérieur.
Nerf carotidien interne.
Gañglion cervical moyen.
_ Ganglion cervical inférieur.
Nerfs cardiaques. Plexus cardiaque.
Sympathique thoracique.
Nerfs œsophagiens.
Nerfs trachéens, b SR ee pulmonaires,
Nerfs vertébraux.
Sympathique abdominal.
Plexus solaire.
Ganglions semi-lunaires.
Nerf grand splanchnique.
Nerf petit splanchnique.
Plexus et nerfs diaphragmatiques.
Plexus et nerfs hépatiques.
Plexus et nerfs (cholé) cystiques.
Plexus et nerfs pyloriques.

- Plexus et nerfs gastro-épiploïques.
Plexus et nerfs spléniques. :
Plexus et nerfs coronaires stomachiques.
Plexus et nerfs mésentériques.

_ Plexus et nerfs surrénaux.
Plexus et nerfs rénaux. 5
Plexus et nerfs spermatiques.
lente et nerfs utéro-ovariques.

Plexus et nerfs hémorroïdaux : moyens.
Plexus et nerfs vésicaux. -

Plexus et nerfs prostatiques.

Plexus et nerfs vaginaux.

Plexus et nerfs utérins.

SIÉRRES de la vue.

Tuniques fibreuses de l'œil,
Conjonctive. e :

Sclérotique. -

 
Iris. PAU
Choroïde.
Corps ciliaire. Muscle ciliaire. Procès ciliaire. one de
Zinn.
_ Canaux de Petit, de Fontana, de Schlemm, d'Hovius.
. Chambre. antérieure. Humeur _° Re
Chambre postérieure.

Rétine. Nerf optique.

Nerf optique.
Rétine. 1

Appareil dioptrique =. né œil.
Cristallin.
Capsule cristalline.
Humeur vitrée.

Organes accessoires.
Muscles extrinsèques de l'œil.
Appareil lacrymal. ;
Glande lacrymale.
Points et conduits lacrymaux.

_ Sac lacrymal et canal nasal.

_ Paupières.

Sourcils.
Orbite. .
ere ‘ee Ténon. ; -

Orancs de Vouie.

Vestibule.
Saccule.
Utricule.
Canaux semi-circulaires.
Nerf acoustique.
Endolymphe.
Labyrinthe ( osseux.
Oreille moyenne. :
_Attique. ;

. d'Eustache. : .
Osselets de. Joreille:

 
Organes de l’olfaction.
Organes du goût.
Organes tactiles.

Ganglions.

Ganglions des nerfs crâniens. :
Pour chacun de ces ganglions, voir les nerfs auxquels ils se rapportent
NHisous 611.381. : ;
| Ganglions des nerfs rachidiens.
Classer plutôt à 61r.827.3.
Ganglions sympathiques.
Classer plutot à Grr.830.

Anatomie topographique.

‘hète.
Face.
Cou.

.94 Thorax.

.95 Abdomen.
.054 Ligne blanche.
Ombilic.

_ Région pelvienne et périné. e chez l'homme.
Région pelvienne et périnéale chez la femme.

Membre supérieur.
Epaule. Aisselle.

SraS a
Coude.
Avant-bras.
Poignet.
Mains.
Doigts.

 
lembre

inf

98 _

inal.

.981I

D.
20.
RNA

A

Hanche.
 Fesse.
PL

de
221

inguinal.

Canal
Canal crural.

inal

ingu

Cuisse.

ité.

eux pop

e-pied.

Jambe.
U-

Genou. Cr
qd

Co

1e
85

983.
98

“

 
 
 
 

 
