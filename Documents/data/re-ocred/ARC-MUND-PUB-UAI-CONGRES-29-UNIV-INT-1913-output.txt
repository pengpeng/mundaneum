 

CONGRÈS MONDIAL

ASSOCIATIONS INTERNATIONALES N° 29
Deuxième session : GAND-BRUXELLES, 19-48 juin 1913
Organisé par l'Union des Associations Internationales

Office central : Bruxelles, 3bis, rue de la Régence

 

5° Section

 

Actes du Congrès. — Documents préliminaires.

 

 

Sur le projet de Création d'une Université
Internationale

PAR

EMILE CORRA

Président de la Société Positiviste Internationale

78 (> )]

Au nom de la Société Positiviste internationale, j'adhère au
projet de la création d’une université internationale, parce
qu'une pareille institution accélérerait, à mon sens, l’évolution
de l'élite du genre humain vers une similitude et une unité
mentales dont l'avènement est l'un des résultats les plus certains
que la continuité du développement général de la civilisation
engendrera.

Cette évolution est, en effet, un phénomène constant et pro-
gressif; la philosophie de l'histoire le démontre.

Son origine se confond avec celle de la civilisation occidentale,
et sa tendance s'est de plus en plus accusée avec l'hellénisme,
avec la civilisation romaine, avec le christianisme et l’islamisme,
avec la Renaissance et l'humanisme. La philosophie moderne,
la science moderne, l’industrie et la politique modernes lui ont
donné une impulsion irrésistible et, maintenant, elle est dans son
plein essor. °

Mais, jusqu'ici, cette évolution s'est accomplie, spontané-
ment, aveuglément, en raison de l'ignorance dans laquelle les

 
— 2 —

hommes étaient plongés, à l'égard de sa nature et de son orien-
tation.

Or, cette ignorance est désormais inexcusable et tous les
savants, tous les philosophes, tous les éducateurs, au niveau des
connaissances du XXe siècle, travaillent plus ou moins sciemment,
plus ou moins clairement, à la dissiper.

Néanmoins, il importe de coordonner, de systématiser, un
mouvement aussi profond, aussi grandiose, et la création d’une
université internationale contribuerait certainement à lui donner
plus de rapidité et plus d'efficacité.

Le Positivisme, dont la Société Positiviste internationale est,
actuellement, le centre, pourrait prêter à cette université un con-

cours des plus utiles, parce qu'il a précisément pour but de :

synthétiser les connaissances universelles qui se dégagent de
toutes les manifestations de l'esprit humain et de démontrer
que celui-ci, d’abord inévitablement dominé par des concep-
tions théologico-métaphysiques, évolue fatalement vers une
conception purement naturelle du monde, de la vie, de la société,
de la morale, en un mot vers une philosophie positive qui finira
par s'imposer à la masse entière des hommes éclairés.  /
C'est pourquoi la Société Positiviste internationale se ferait
un devoir de contribuer, immédiatement, au fonctionnement
d'une université internationale, en y exposant la philosophie
positive, sinon dans son intégralité, du moins sous la forme

d'un certain nombre de conférences, destinées à faire connaître,

tout d'abord, l'ensemble de cette doctrine.

Le premier programme de ces conférences pourrait être, par
exemple, celui que fa Société d'Enseignement populaire positi-
viste, filiale française de la Société Positiviste internationale,
a développé, l'hiver dernier encore, à Paris, devant un audi-
toire cosmopolite.

Ce programme était ainsi conçu :

I. — Histoire générale de la philosophie positive.

IT. — Aperçu général de la constitution de la philosophie
positive.

TIT. — La philosophie mathématique.

IV. — La philosophie astronomique.

V. — La philosophie physique.

VI. — La philosophie chimique.

VII. — La philosophie biologique.

VIII — La psychologie positive.

IX. — Histoire de la sociologie.

— 3 —

X. — Fondation de la science sociale par Auguste Comte.
XI. — Conditions générales de l'existence des sociétés.
XII. — Conditions générales de l’évolution des sociétés.
XIII — Histoire générale de la morale.

XIV. — Bases de la morale positive.

XV. -- Principales règles de la morale positive.

XVI. — L'avenir du Positivisme.

. D'autres séries de conférences seraient ensuite plus spéciale-
ment consacrées à la théorie positive de l’ordre social, du pro-
grès social, de la religion, de la morale, de l'éducation.

Ces conférences, objets successifs de l’enseignement annuel
que la Société Positiviste internationale professe déjà, pour-
raient être faites, en Europe, par des positivistes français
où anglais, parlant chacun dans leur langue nationale.

 
