 

 

 

CON re < EC 8

St ANSE ES |

SALE ENTREE
Sn tt re US io

 

 

 

 

 

_ SECTIO Z00LOGICA

Conspectus Methodieus
TA LIT

EU ANNE

YANESUE

 
 

 
Conspectus Methodicus.

+.

56 PALAEONTOLOGIA
(56.29 eodem modo dividuntur ac 59.29)

(11) Stratigraphia

(111) Archaicum

(112) Palaeozoïicum
: (1121) Cambricum

(113) Siluricum

(114) Devonicum

(115)  Carbonicum, Permic.
(116) Mesozoicum

(1161) Triassicum

(1162) Jurassicum
(117)  Cretacicum
(118) Tertiaericum
(1181) Eocaenicum
(1182) Miocaenicum
(1 183) Pliocaenicum
(119) Quataericum
(1191) Recentium

 

575  Evolutio
.l Hereditas
Variatio (cf. 11.5)
Effectus ,mediorum
ambientium“
Selectio naturalis
Selectio sexualis
Degeneratio
Substantia animata
Vita
Conditiones vitae
Differentia inter plan-
tas et animalia
Vis vitalis
Mors
Sexualitas (v. 11.6)
578 Microscopium
.l :‘Varietates
2. Partes opticae
3 ——  mechanicae
A "Accessoriae

578.49 Microphotographia
:5 Partes illuminatores
.6 Subsidia technica
.61 Conservat. histologica
cf. 579.2
65 Coloratio
:07 Microtomia
.68 Tractatio sectionum
.69 Reconstructio
579  Collectio; conservatio
; Contéctio sceletorum
Fluida conservantia,
durantia
Injectio vasorum
Paxidermia
Expositio rerum prae-
paratarum
Collectio, Cultura
Dispositio in museis
‘Putela collectionum

 

 

 

 
CONSPECTUS METHODICUS

 

59 ZOOLOGIA

0 Generalia stationes, aquaria,

O1 Philosophia, Classifi- | musea  — cf. 9579.
catio a 08 Scripta collecta, miscel-

02 Compendia | lanea

03 Lexica | 09 Historia

05 Scripta periodica | 091 Bibliographia

06 Scripta societatum

| 092 Biographia
07 Subsidia studii; horti, | j

 

 

 

11. Physiologia 11.45 Venena
IL1 Circulatio A9 Urina
.12 Actio cordis 11.5 Variatio (cf. 575.2)
.13 Vasa .5l - polymorphica
11.2 Respiratio .52 - geographica
22 — cutanea 53 - heterophagica
.23 — per vasa aquifera .55 - mimetica
.25 — per branchias .56 - sexualis
26 -— per tracheas .57 - colorativa
.27 — per pulmones .58 Formae hybridae
28 Calor animalis .59 ; monstrosae |
11.3 Nutritio (v. 12)
81 Acquisitio nutrimenti | 11.6 Generatio
32  Digestio nutrimenti .6l Abiogenesis
.33 Assimilatio nutrimenti 62 Parthenogenesis
.34 Incrementum .63 . Paedogenesis,nutrices
35 Evolutio 64 Kissura
.36 Restitutio consumpti 65 Gemmatio
.37 Productio materiae .66 Fecundatio (cf. 13.13)
organicae .67 Hermaphroditisnius
.38 Conditionesnutritionis .68 Viviparitas
39 Longaevitas .69 Regeneratio
11.4 Secretio et excretio 11.7  Motus f
4l - cutanea 71 Itus Ü
43 - ad digestionem .12 Reptatio i
spectans .73 Natatio
431 Saliva 74 Volatus
432 Succus gastricus 11.8 Functiones systematis
433 Succus intestinalis . nervosi — cf. 15.1
434 Secretio hepatica .85 Sensus
435 — pancreatica 852 Tactus

 

 

   
 

CONSPECTUS  METHODICUS

 

11.853 Gustatus 1422 Larynx

 

.854 Odoratus
855 Auditus
.856 Visus è
11.9 Chemia physiologica
12.  Pathologia et Terata-
logia eodum modo
ac 14. dividuntur

- 13. Embryologia (cf. 14.63.1)

.| Ovum, Segmentatio

.11 Maturatio :
heal OOl

13 Fecundatio ovi

.15 Segmentatio

.16 Morula

.17 Blastula

.2 Laminae germinis,

Gastrula
.3 : Embryo, primordia
.31 Primordia ectoder-

malia
33 Prim.entodermalia
35 — mesodermalia

39 Adnexa embryonis
4 Metamorphosis
41 Larvae

45 Metagenesis

.5 Juvenes

6 Productio sexuum

7 Hybridisatio (v. 11.58)

.8 Incestus

:9" Embryologia experi-
mentalis

14. Anatomia
.[  Organa circulationis
.11 Pericardium

122 Cor:
.13 Vasa, arteriae
.14 Venae
.15 Capillares
ct 185

142 Organa respirationis
21 Nasus

23 Trachea (Vert).;bronchi
24 Pulmo
.25 Pleura
.26 Diaphragma
.28 Branchiae
29 Organa alia, Tracheae
(ins.), Vesica nata-
toria
14.3  Organa nutritionis
31270) d
.31.3 Lingua
.31.4 Dentes
31.6 Glandulae
.32 Pharynx, oesophagus
.33 Ventriculus
.34 Intestinum
. .35 Rectum, cloaca
.36 FHepar
: 37 Pancreas
.38 Peritoneum, coeloma
.39 Corpora adiposa, etc.
144 Systema lymphatica
41 Lien
42  Vasa lymphatica
43  Thymus
44 Glandula thyreoidea
A5 GI. suprarenalis
46 Gl. lymphaticae
14.6 Organa urogenitalia
.61 Ren, ureter
.62 Vesica, urethra
.63 Testis, vas deferens
.63.1 Sperma, spermatogen.
.64' Organa copulationis
65 Ovarium, oviductus
.65.1 Ovum, oogenesis
.66 Uterus
67 Vagina
69 Mammae
147 Organa motus
.11- Sceleton —cf 18:3,18.4

 

72 Articulationes

 

 
  

CONSPECTUS

 

 

METHOPICUS

 

 
 
 
 
  
 
 
 
 
 
 
 
 
 
  
  
   
 
  
  
  
  
  
  
 
  
 
 
 
 

1473 Musculi — ci. 18.6

.13.9 Organa electrica

.714 Tendines, Fasciae

.16 Tela conjunctiva
Crelo2

17 Integumentum cf. 18.7

.18 Pili, ungues, plumae ete.

.18.1 Pil

.18.5 Squamae, Exosceleton

48.6 Ungues

.78.7 Plumae

.718.8 Cornua

 

  

148 System nervosum
cf. 18.8
81 — centrale, Encephal.
82 Medulla spinalis
83 Syst. nerv. peripheric,
84 Organa visus s
.85 . — auditus
86: — olfactus
.87 — gustus
.88 -— sensus, tactus
.889 — lateralia
89 Ganglia sparsa
149 Somatologia
.91 Personae cormorum
92 Antimera, metamera
93 Caput
94 Cephalothorax, collum
.95 Thorax
.96 Abdomen
97 Cauda, telson
98 Extremitates
99 Appendices corporis
15. .Mores; vitae ratio

| Instmetuse-1ct 11.8
.2  Locus; migratio

 

15.

ONOG BU

:8
:9
:9

How © No wN—

 

© I © O1 PR & D

|
|
À
|

Alimentum
Anni tempora;
Hibernatio
Socialitas
Neomelia
Jutamenta
Vitae consuetudines
aliae
Zoologia ad res dome-
sticas, rusticas etc.
se referens
Usus
in \natura
— pro alimento et me-
dicamento
—— in artibus
Noxae
Bestiae rapaces
Animalia morbum
efficientia
— materias inanimas
corrodentia
Parasita
Di a Vium
— mammalium
.32 -— rodentium
éICr ec:
Histologia
Cellula
1463:1
Pela conjunctiva
Cartilago
Os
Sanguis, Lympha
Musculus
Epithelium

Systema nervosum

2hÈe

131

 

  

19(21) Terrae continentes
(212) Regiones temperatae
(213) — intertropicae
(22)  Insulae

 
 
  
 

 

| 19(23)
L. (24)
(25)

|
4

19  DISTRIBUTIO GEOGRAPHICA (cf. 15.2)

Montes
Cavernae
Plana: deserta

  

 

 
 

 

CONSPECTUS METHODICEUS

 

(26) Maria, oceani

(26.01) Plancton

(26.02) Fauna pelagica
(26.03) — abyssalis
(26.1) Atlanticum
(26.12) Gerranicum

(26.13) Balticum

(26.2) Mediterraneum
(26.23) Adria

(26.25) Pontus Euxinus
(26.3) Sinus mexicanus .
(26.35) Mare caraibicum
(26.4) Atlanticum australe
(26.5) Pacificum

(26.6) Pacificum orientale
(26.7) Indicum; sinus ben-

g'alensis

(26.75) . Mare rubrum
(26.8) Oceanus arcticus.

| (v. C8)

(26.9) Oceanus antarcticus
[v. (09)]

(28) Aquae dulces

(2801) Limnoplancton
(281) Flumina

(285) Lacus
(29)  Fontes; putei; aqua

solo contenta

(4) EUROPA
(403) regio palaearctica
(405) -— mediterranea

(404) — europaea
(41) SCOTLAND — cf. (42)
(41.1) Northern Scotland

.[f Shetland

.12 Orkney

.13 Caithness

.14 Sutherland

15 Cromarty

.16 Ross

.17 Hebrides
(41.2) North Central Scotland

.21 Inverness

 

qi

(41:22) Nairn
23 Elgin
24 Banff
25 Aberdeen
26 Kincardime

(41.3) South Central Scotland
.31 Forfar
.32 Perth
.33 Fife
.34 Kinross
.35 Clackmannan
.36 Sterling
.37 Dunbarton
.38 Argyle
.39 Bute
(414) Southern Scotland
41 Renfrew
42 Avr
43 Lanark
44 Linlithgow, Edinburgh
45 Haddington, Berwick
46 Peebles, Selkirk
47 Roxburgh
48 Dumfries
A9 Kirkcudbright, Wigton
(41.5) IRELAND -— cf. (42)
(41.6) Ulster
.61 Antrim
.62 Londonderry
.63 Donegal
.64 Tyrone
.65 Down
.66 Armagh
-67 Monaghan
.68 Fermanagh
69 Cavan
(41.7) Connaught
.71 Leiïtrim
.12 Sligo
13 Mayo
74 Galway
.75 Roscommon

 

 

 

 

 
 

 

CONSPECTUS METHODICUS

 

(41.8) Leinster
81 Longford, West Meath
82 Meath, Louth
.83 Dublin
.84 Wicklow
.85 Kildare
-86 King's Co.
87 Queen's Co.
.88 Carlow, Wexford
.89 Kilkenny
(41.9) Munster
.91 Waterford
.92 Tipperary
93 Clare
.94 Limerick
95 Cork
.96 Kerry

(42) ENGLAND, Gr. Britain
(42.1) Middiesex. London
(42.2) Southeastern
21 Surrey
.23 Kent
.25 Sussex
.27 Hants
.28 Isle of Wight
29 Berks
(42.3) Southwestern
31 Wüälts
.33 Dorset
.34 Channel Islands
.35 Devon
.37 Cornwall
.38 Somerset
(42.4) West Midland
41 Gloucester
A3 Monmouth
44 Hereford
45 Salop
46 Staflord
47 Worcester
48 Warwick
a 5) North and South Midland
u Derby

 

(42.52) Notts
.53 Lincoln
.54 Leïcester, Rutland
.55 Northampton
.56 Huntington, Bedford
57 Oxford, Buckingham
.58 Hertford
.59 Cambridge
(42.6) Eastern
61 Norfolk
.64 Suftolk
.67 Essex
(42.7) Northwestern and York-
shire
.71 Cheshire
.72 Lancashire
.74 Yorkshire
(42.8) Northern
81 Durham
.82 Northumberland
.85 Cumberland
.88 Westmoreland
.89 Isle of Man
(42.9) Wales
.91 Anglesey
.92 en Merioneth
.93 Denbigh, Flint
94 Montgomery
95 Cardigan
.96 Radnor, Brecknock
97 Glamorgan
.98 Carmarthen
99 Pembroke

(43) DEUTSCHLAND
(43.1) Nordostdeutschland,
Preussen
.11 Ost-Preussen
.12 West-Preussen
.13 Posen
.14 Schlesien
.15 Brandenburg
.16 Pommern
17 Mecklenburg, Lübeck

 

 

 

 

 

 
 

 

CONSPECTUS METHODICUS

 

(43.18) Provinz Sachsen
.19 Anhalt
(43.2) Mitteldeutschland
21 Sachsen
22 Thüringen
.23 Sachsen- Weimar
24 Sachsen-Altenburg
.25 Sachsen-Coburg-Gotha
.26 Sachsen-Memingen
.27 Schwarzburg
28 Reuss
Ge 3) Bayern
.31 Ober-Franken
.32 Mittel-Franken
.33 Unter-Franken
.34 Ober-Pfalz
.35 Unter-Bayern
.36 Ober-Bayern
37 Schwaben
(43.4) Süd-Deutschland
.4| Hessen-Darmstadt
42 Rhein-Preussen
43 Pfalz
44 Elsass
45 Lothringen
46 Baden
A7 Württemberg
48 Schwarzwald
49 Hohenzollern
(43.5) Nordwest-Deutschland
.51 Schleswig-Holstein
Hamburg
: 52 Oldenburg, Bremen
.53 Hannover
54 Braunschweig
55 Lippe
.56 Westfalen
.57 Waldeck
.58 Hessen-Nassau
(43.59) LUXEMBURG

(43.6) OESTERREICH-UNGARN
.61 Unter-Oesterreich
.62 Ober-Oesterreich

 

Di

(43.63) Salzburg - .
.64 Tyrol, Ÿ orarlberg
.65 Se
.66 Kärnthen
.67 Krain
.68 Istrien
.69 Dalmazien
(43.7) Bühmen etc.
.7| Bôühmen
.12 Mähren
.43 Schlesien
.74 Galizien
.15 Bukowina
(43.9) Ungarn, Hongrie etc.
91 Ungarn
92 Transylvanien, Sieben-
bürgen
93 Slavonien
94 Kroatien, Fiume
95 Bosnien
96 Herzogowina

(44) FRANCE
(44.1) Bretagne,

.11 Finistère

.12 Côtes-du-Nord

.13 Morbihan

.14 Loire-Inférieure

.15 Ille-et-Vilaine

.16 Maine, Mayenne

.17 Sarthe

.18 Anjou, Maine-et-Loire
(44.2) Normandie, Picardie

21 Manche

.22 Calvados

23 Orne

.24 Eure

.25 Seine-Inférieure

.26 Picardie, Somme

.27 Artois, Pas-de-Calais.

28 Nord
(44.3) Champagne, Isle de

France, Lorraine
.31 Ardennes

Maine, Anjou

 
 

 

 

CONSPECTUS METHODICUS

  

 

(44.32) Marne

Aube, Haute-Marne

.33
.34
.35
-36

.37
: .88
.39

Isle-de-France:
Oïse

Aisne

Seine-et-Oise, Paris,

Seine
Seine-et-Marne
Lorraine, Meuse

Meurthe-et-Moselle,

Vosges

(44.4) Bourgogne, Franche

41
42
43
44
A5

46
A7
48

Comté, Savoie
Yonne

Côte d'Or
Saône-et-Loire
Ain

Franche Comté, Haute

Saône, Belfort
Doubs

Jura

(44.7) Guienne, Gascogne

{|
12
13
.74
#19
.16
AC
.18

.19

Gironde

Dordogne
Lot

Aveyron
Tarn-et-Garonne
Lot-et-Garonne
Gascogne, Landes, Gers
Hautes-Pyrénées

Béarn, Basses- Pyrénées

(44. 4) Languedoc

2
-83
.84
-85
-86
-87
.88
-89

{ Haute-Loire, Lozère

Ardèche

Gard

Hérault

‘arn
Haute-Garonne

Aude

Foix, Ariège
Pyrénées- Olientales

 
  
 
 
 
 
 
 
 
 
  
  
 
 
 
 
 
 
 

Savoie

   
   
  
 
   
 
  

 

49 Haute-Savoie

(44.5) Orléans, etc.,
.51 Eure-et- Poe
.52 Loiret
.53 Loir-et-Cher
.54 Touraine, Indre-et-Loir
.55 Berry, Indre, Cher
.56 Nivernais, Nièvre
.57 Bourbonnais, Allier
.58 Lyonnais, Loire, Rhône
59 Auvergne, Puy-de-

Dôme, Cantale

(44.6) Poitou, Limousin
61 Vendée
.62 Deux-Sèvres
.63 Vienne
.64 Charente-Inférieure
.65 Charente
.66 Limousin, Haute-Vienne
.67 Corrèze
.68 Marche, Creuse

Auvergne

 

 

(44.9) Provence, Dauphiné
91 Bouches- due Rhône
92 Vaucluse
.93 Var
94 Alpes-Maritimes ‘
.95 Basses-Alpes
96 Dauphiné
97 Hautes-Alpes
.98 Drôme
99 Isère
Corse v. (45.99)

(45) ITALIA

(45.1) Piemonte, Liguria
2 Lombardia
.3 Veneto
4 Emilia
.5 Toscana
.6/Marche, Umbria, Roma
.7 Napoli etc.

.71 Abruzzi Molise
.12 Campania

.13 Napoli

.74 Salerno

 

 
14

 

 

CONSPECTUS METHODICUS

 

(45.75) Puglie
.76 Lecce, Terre di Otranto
.77 Basilicata, Potenza
.78 Calabria
.49 Reggio di Calabria
.8 Sicilia, Malta
3 Sardegna .99 Corse
(46) ESPANA
(46.1) Galicia, Asturias
.2 Leon, Estremadura
.3 Castilla la vieja
4 Castilla la nueva
5 Arragon
.6 Provincias Bascas
.{ Cataluna, Islas Balear.
Valencia, Murcia
.8 Andalucia, Islas Canarias
(469) PORTUGAL $
(469.8 Madeira
(469.9 Acores
(47) RUSSIE
(47.1) Finland, Lapland
-2 Grande Russie septentr.
.3 Grande Russie mérid.
4 Provinces Baltiques
St. Pétersbourg
.5 Pologne russe :
.6 Russie occidentale
./ Petite Russie; Russie
mérid.
.8 Russie orientale
.9 Caucase
(48) NORGE, SVERIGE, DAN-
MARK
(48.1) Norge (Norwegen)
.2 Kristiania
3 Kristiansand, Bergen
4 Hamar, Trondhjem,
"lromsô ‘
(48.5) Sverige (Schweden)
6 Gôütarike
.7 Svearike
8 Norrland

 

(48.9) Danmark

(49) DIVISIONES MINORES

(491) Island, Faroe

(492) Nederland

(493) Belgique, Belgie

Luxemburg v. (43.59)

(494) Suisse, Schweiz

(495) Grèce

(496) Turquie en Europe

(497) Serbie, Bulgarie, Mon-
tenegro

(498) Roumanie,
Moldavie

(499) Archipel grec

(5) ASIA — cf. (403)
(502) regio orientalis
(503) — indo-sinica

Valachie,

(504) — indo-malavica
(505) — indica
(506) — ceylonica

(51) Chine
(51.1) Nord-Est :
.2 Sud-Est, Canton
.3 Sud-Ouest
4 Nord-Ouest
.5 Tibet
.6 Turkestan chinois
.4 Mongolie
8 Mandchourie
9 Corée
(53) Japon .
.{ Nippon
4 Jezo
.7 Kurilles
8 Liu-Kiu
9. Formosa (ante 51.2)
(53) Arabie
(53.1) Sinaï, Najd.
.2 Hedjaz .
.3 Yémen <
4 Hadramaut, Mahrah
5 Oman

 

tele

Va
Ei
14

M:
 

 

 

 

 

CONSPECTUS METHODICUS

(53.6) Hasa (Lahsa)
.7 Dahna, Roba el Chali
8 Nejd
3 Désert syrien
(54) India
(54.1) Bengal, Orissa, Assam
Bhotan
2 Northwest Provinces
Ardh Nepal
.3 Central Provinces
Æ Rajputana
5 Punjab, Delhi, Lahore
.6 Cashmere
.7 Bombay, Sind, Baroda
8 Madras, Mysore, Travan-
core, Ceylon (22: 54.8)
3 Hyderabad
(55) Perse
(56) Turquie en Asie
(56.1) Khodavendikyar
.-2 Aïdin, Smyrne
Kastamuni, Angora
Konieh, Adana, Cyprus
TFrebizond, Sivas
Erzerum Diarbekr, Kar-
poot, Arménie
Mesopotamia
Aleppo, Syrie
Palestine
(57) Russie en Asie
.| Sibérie orientale
4 —— occidentale
.6 Asie centrale (Pamir,
Kirgkiz etc.)
9 Transcaspia
(58) Afghanistan &c.
(58.4) Buchara, Chiva
(58.8) Beloutschistan
(59) Farther India. Indochine
(59.1) Burmah
.3 Siam
4 Laos, Shan
5 Malay Peninsula

Do ou

 

 

(59.6) Cambodge
.1 Cochinchine française
8 Annam
9 Tonkin
(6) AFRICA
(61) Afrique septentrionale
(61.1) Tunis — cf. (403)
(61.2) Tripoli, Barca

(62) Egypt

(63) Abissinia, Eritrea

(64) Moroc

(65) Algérie |

(66) Afrique centrale,  Nord-
Ouest

(66.1) Sahara

2 Soudan

3 Sénégambie, Guinée

franc.

4 Sierra Léone

.5 Guinée septentrionale

.6 Liberia, Côte d'ivoire

1 Achanti, Togo

8 Dahomey

9 Niger Territories
(67) Afrique centrale, Sud
(67.1) Kamerun

-2 Laongo, Congo franc.

.3 Angola, Benguela

.5 Congo

.6 British East Africa

.7 Somali

.8 Sansibar, Deutsch - Ost:

afrika

«9 Mozambique
(68) Afrique méridionale
(68.1) Sofala

2 Transvaal

.3 Zululand

4 Natal

5 Oranje-Freistaat

.6 Kañffraria

.7 Cape Colony

 
Lxs
@

Ost

 

 

 

CONSPECTUS METHODICUS

 

(68.8) Deutsch Sud-West-
Afrika
.9 British Zambesia, Rho-
desia
(69) Madagascar, Mauritius
(69.5) Iles Mascarènes
(7) NORTH AMERICA
(701) regio nearctica
(71) Canada, British America
GE 1) Bo Columbia
2 North West Territory
Ontario
Quebec
New Brunswick
Nova Scotia
Prince Edward Island
Newfoundland
Labrador
(72) Mexico
.[ Norte
.2 Baja California, islas
.3 Estados centrales paci-
ficos
4 Interior
5 Mejico
.6 Estados meridionales dél
Golfo
.{ Estados pacificos del sul
(728) America centrale
(729) West Indies, Antilles
(729.1) Cuba
2 Jamaica
-3 Santo Domingo
4 Hayti
.5 Porto Rico
6 Bahamas
.7 Leeward Islands
8 Windward Islands,  Bar-
bados
9 Bermudas

Rise

(73) UNITED STATES & TERRI-

TORIES

(74) North Eastern (New Eng.)

 

(44. 1) Maine
New ae
Vermont
Massachusetts
Rhode Island
Connecticut
New York
Pennsylvania
New Jersey
(75) South Eastern
(25.1) Delaware
-2 Maryland
.3 District of: Columbia
4 West Virginia
.5 Virginia
.74 North Carolina
-8 Georgia
9 Florida
(26) South Central or Gulf
(76.1) Alabama
-2 Mississippi
.3 Louisiana
4 Texas
-6 Indian Territory, Okla-
homa :
.{ Arkansas
.8 Tenessee
3 Kentucky
(27) North Central or Lake
(77.1) Ohio
.2 Indiana
.3 Illinois
4 Michigan
.5 Wisconsin
.6 Minnesota
.7-lowa
.8 Missouri
(78) Western or Mountain
(78.1) Kansas
2 Nebraska
.3 South Dakota
4 North Nakota
.6 Montana

ERNDAaREN

 

 
 

 

 

 

 

 

CONSPECTUS METHODICUS

 

(78.7) Wyoming

8 Colorado

9 New Mexico
(79) Pacific
(79.1) Arizona

2 Utah

3 Nevada

4 California

5 Oregon

.6 Idaho

4 Washington

8 Alaska.
(8) America australis
(801) regio neotropica
(81) Brasilia
(82) Argentinia
(82.9) Patagonia

(83 Chili
(84) Bolivia
(85) Peru

(86) Columbia

(86.6) Ecuador

(87) Venezuela

(88) Guiana

(89) Paraguay

(89.6) Uruguay

(9) Oceania cf. (502)—(504)
(902) regio austro-malayica
(91) Malaysia

(91.1) Borneo

(91.2) Celebes

(91.3) Moluccas

(91.4) Filipinas

(92) Sunda

(921) Sumatra

(922) Java

 

(93) Australasia

(931) New Zealand

(932) Nouvelle Calédonie
(933) Loyalty Islands

(934) N. Hebrides

(935) Salomon Inseln

(936) N. Pommern (N. Britain)
(937) Admiralitäts- inseln

: (94) Australia

(O4, 1) Western Australia
2 South Australia
.3 Queensland
4 New South Wales
.5 Victoria
.6 Fasmania

| (95) N. Guinea, Papua

(96) Polynésie
(96.1) Füi, Friendly (Tonga),
Navigator’s (Samoa)
.2 Society (Taiti), Austral
(Foubouaï), Cook’s.
3 les Marquises (Mendana)
Low Archipelago
(Fouamotou)
.4 Insulae minores
.5 Micronésie
.6 Islas Carolinas, Palaos
(Pelew)
.7 Ladrone (Mariannes)
.8 Marshall, Gilbert
9 Hawaii (Sandwich)
(97) Insulae sparsae
(98) Regiones arcticae
é. (26.8)
(99) Regiones antarcticae
ci. (26.0)

 

2 INVERTEBRATA

31.4  Radiolaria
.5. Infusoria
.6 Flagellata
7 Ciliata

31. Protozoa
.! Rhizopoda
2  Foraminifera
.3  Heliozoa

 

 
 

in) à

 

CONSPECTUS METHODICUS

 

 

 

31.75 Suctoria
-9 Sporozoa
91 Gregarinidae
.92  Cocciduidae
93  Sarcosporidia
94  Myxosporidia
95  Microsporidia
32 RADIATA
33 Coelenterata
34 Spongiae

|  Myxospongiae
-2 Ceraospongiae
.3  Halichondriae
4  Lithospongiae
-5  Hyalospongiae
6  Calcispongiae

35  Cnidaria

36 Actinozoa

Rugosa

Alcyonaria

Zoantharia
Antipatharia
Actniaria
Madreporaria

DnpwN—

37  Hydrozoa

Hydromedusae
Siphonophora
Acalephae
Calycozoa
Rhizostomidae
Marsupialida
Discophora
38  Ctenophora
39  Echinoderma

NO 1 © N —

.[  Crinoidea

.2 ‘ Asteroidea

9 Stellerida

4 Ophiuroidea
.5 Echinoidea

.6 Holothurioidea
7  Pedata

 

 

39.8 Apoda

9 Enteropneusta
& Mollusca
Lamellibranchia
Scaphopoda
Gastropoda
.31 Amphineura
.32 Prosobranchia
.34 Heteropoda

69 ND —

.35 Opisthobranchia

.36 Nudibranchia
.37 Tectibranchia
.38  Pulmonata

4 Pteropoda

.-5 Cephalopoda
51 Tetrabranchia
.52 Nautiloidea
.53 Ammonitae
55 Dibranchia

.56 Octopoda

-58 Decapoda

46  Molluscoidea (Brachio-

Stoma)
47 Bryozoa

| Gymnolaemata
[1 Cyclostoma
.12  Ctenostoma
.13  Chilostoma
-2 Phylactolaemata
.3 Pterobranchia
4  Entoprocta

Phoronis v. 51.76

48 Brachiopoda
49  Tunicata

.|  Ascidiae

-2 Copelatae
.3  Monascidiae
4  Synascidiae
5  Pyrosoma

-6 : Salpae

.7  Doliolum

 

 

 
 
  
 
 
 
  
 
 
 
   
 
 
 
 
  
  
  
 
 
 
   
  
  
 
 
   
 
 
 
  
  
 
 
 
  
  
 
   

 

5
5l

.78
51.8
.85
.88
51.9
.95

52
53

CONSPECTUS

ARTICULAT A
Vermes
Helminthes. Parasiti
Platyhelminthes
Cestodes
Mrematodes
Turbellart
Nemertini
Nematodes
Gordiacei
Acanthocephali
Chaetognathi
Annelida
Hirudinea
Oligochaeta
Polychaeta
Gephyrea
Phoronis
Myzostomum
Rotifera
Echinoderes
Gastrotricha
Orthonectida
Dicyemida
ARTHROPODA
Crustacea
Entomostraca
Pantopoda
Phyllopoda
Branchiopoda
Cladocera
Ostracoda
Copepoda
Parasita
Cirripedia
Malacostraca
Arthrostraca
Amphipoda
Isopoda
Thoracostraca
Cumacea
Stomapoda
Schizopoda

METHODICUS

53.84  Decapoda
81 Macrura
842 Brachiura
9 . Gigantostraca
91. Merostomata
.92  Xiphosura
93 Trilobita
54 Arachnida
ÿ e Linguatulida
Tardigrada
Acarina
Phalangida
Araneae
Pedipalpi
Scorpiones
Pseudoscorpiones
Solifugae
55  Onychophora
56  Myriopoda

: Chilognatha

© 1 © Ü1 À & ND = —
N

l

2 Chilopoda

3 Symphyla

4  Pauropoda

57 INSECTA

.! Thysanura
.Îl Campodeidae
13 Poduridae
15 Lepismatidae

Orthoptera
2! Dermaptera
Cursoria
.23 Gressoria
24 Phasmidae
25 Mantidae
26 Saltatoria:
27 Acrididae
28 Locustidae
29 Gryllidae
57.3 Pseudo-Neuroptera
31 ‘lhysanoptera.
32 Corrodentia

 

33 Odonata

 

 

Sense

sinistres

 
 

Ps

CONSPECTUS

METHODICUS

 

57.34 Ephemeridae
.35 Perlidae

57.4 Neuroptera
41  Planipennia
42 Megaloptera
43  Sialidae
44,  Panorpidae
45 Trichoptera
46 Strepsiptera

57.5 Hemiptera
-5l: Aptera
512  Pediculidae
514  Mallophaga
-52 Phytophthires
-53 Homoptera
-54  FHeteroptera

57.6 Coleoptera

61 Pentamera

62 Adephaga
.63  Clavicornia
.64  [Lamellicornia
65  Sternoxia
.66  Malacodermata
.67 Héteromera

- 68 Tetramera

 

57.69 Trimera
57.7 Diptera
.11 Nematocera
.12 Brachycera
14 Pupipara
15 Aphaniptera
57.8 Lepidoptera
.81 Heterocera
82 Microlepidoptera
-83 Macrolepidoptera
.85  Geometrina
.86  Noctuina
87  Bombycina
-88  Sphingina
89 Rhopalocera
57.9 Hymenoptera
91 Terebrantia
-92  Entomophaga
-93  Phytophaga
.94 Aculeata
95 Chrysididae
.96  Formicidae
.97  Fossoria
-98 Vespidae
-99 Apidae

 

oo

7 Pisces
| Pharyngobranchii

2  Marsipobranchii
.3  Elasmobranchii.
.31 Selachoidei
-35 Rajae
-38 Holocephali
4  Ganoideï
42  Acanthodidei
.43  Placoderma
44 : Chondrostei
45  Pycnodontidei
46  Crossopterygii
47 Euganoïdei
48 Dipnoi

VERTEBRATA

 

.5  Teleostei
53 Lophobranchi
-54  Plectognathi
-55  Physostomi
.56  Anacanthini
57  Pharyngognathi
-58  Acanthopteri
76  Amphibia
77 Ophiomorpha
18 Anura
19 Ürodela
79.5  Stegocephala
81 Reptilia
î Sauria
.[l  Amphisbaenoidae

Re

£ 4%

 

 
 

 

CONSPECTUS

METHODICUS

 

Lacertilia
Chamaeleontidae
Ascalabotae
Rhynehocephaha
Ophidia
Serpentes innocui
Serpentes venenosi
Chelonia
Crocodilia
Ichthyopterygia
Sauropterygia
Anomodontia
Pterosauria
Dinosauria
Ornithoscelides

AVES

Grallatores
Fulicariae (Rallides)
Alectorides
Limicolae
Ciconiae
Natatores
Lamellirostres
Longipennes
Steganopodes
Impennes
Odontornithes

Ratitae
Struthiones
Rheae
Casuarü
Apteryges
Archaeopteryx

Rasores
Columbae

cansores
Psittaci
Picariae scansores
Frogones

82
83

85

86

[

2

3

4

5

5

87 S
[
2
3

 

874 Coccyges
Coluidae
Insessores
.| Acromyodi (Oscines)
.6 Mesomyodi
9 Picariaé (Scansores
Coccyges, Trogones 4
sub 87)
89 Raptores
| Falcones
7 Striges
9. MAMMALIA
Monotremata
Marsupialia
Placentalia
Edentata
Rodentia
Insectivora |
Chiroptera ï
.5 Cetacea ñ
Mysticete
Denticete
Sirenia
Proboscidea
Hyracoidea
71 Ungulata
.72 Perissodactyla
.725 Solipedes
13 Artiodactyla
735 Ruminantia
.74 Carnivora
.743 Creodonta
.745 Pimnipedia
75 Mammalia domestical
8  Quadrumana

88.9
88

 

 

 

81  Prosimui

.82 Pitheci

.88 Anthropomorpha
9 Bimana

an

EAST

 
  

 

 

 

FES? £ s Ha IDE

 

1C4
 
Le SE

 
