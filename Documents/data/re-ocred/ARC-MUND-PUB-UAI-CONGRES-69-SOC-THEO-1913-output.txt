 

 

CONGRÈS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES N° 69
Deuxième session : GAND-BAUXELLES, 1518 juin 1913
Organisé par l'Union des Associations Internationales

Office central : Bruxelles, 3bis, rue de la Régence

1913.06

 

 

Actes du Congrès. — Documents préliminaires, |

 

 

 

La Société Théosophique

—

RAPPORT

PRÉSENTÉ PAR

M.F. WITTEMANS
Au nom de la Section Belge de la S.T.

[149.918 (062) (w)]

La Société Théosophique, par les buts qu'elle poursuit, est
une société internationale par excellence et est de la plus haute
utilité pour l'humanité. Voici quels sont ces buts :

19 Former le noyau d'une fraternité universelle dans l’huma-
nité sans distinction de race, de crayance, de sexe et de couleur ;

29 Encourager l'étude des religions comparées, de la philoso-
phie et des sciences ;

32 Étudier les pouvoirs latents dans l’homme et les lois
inexpliquées de la nature.

La Société Théosophique fut fondée en 1875, à New-York:
depuis 1882, son quartier général est établi à Adyar près de
Madras. Elle compte à ce jour au delà de 20,000 membres,
répartis entre 22 pays ayant des sections régulièrement orga-
nisées, sans tenir compte des pays dans lesquels les membres
ne sont pas réunis en section. Ces membres sont groupés en

 
— 2 —

950 loges ou branches. Les diverses sections sont indépendantes
dans leur organisation ainsi que dans leur travail, tout en se
soumettant au règlement général de fa société et en contribuant
aux dépenses du quartier général.

La Société n’a pas de dogmes et ne connaît ainsi pas d’héré-
tiques; elle a cependant des enseignements appclés théoso-
phiques (le mot grec Théosophia signifie connaissance divine),
qui constituent la quintessence des vérités enseignées dans les
diverses religions; elle considère celles-ci comme des vérités
partielles d'une Vérité unique, donnée aux hommes par les
Grands {nstructeurs de l'Humanité et appropriée à leur avan-
cement, leur race, leur milieu et aux autres circonstances qui
leur sont propres.

Comme société d'étude des pouvoirs latents dans l’homme,
elle forme un milieu initiatique et ésotérique; rompant avec
les habitudes des sociétés analogues existant avant elle, elle a
estimé que les vérités dites occultes devaient être l'apanage de
l'humanité tout entière et non le dépôt de sociétés secrètes
provoquant la défiance et les attaques des exclus. Ces vérités
peuvent se résumer dans les points suivants : l'évolution infinie
de la vie à travers les divers règnes de la nature, l'état septuple
du monde et de la matière, la pluralité des existences en rapport
avec la loi de Karma ou de causalité, l'existence des Maitres de
la Sagesse, c'est-à-dire des êtres qui ont achevé leur évolution
humaine et qui restent attachés à cette terre ignorés de la masse
pour aider ceux qui sont prêts à atteindre Ja libération humaine,
l'existence d’un plan divin préconçu se réalisant dans le monde
et prévoyant pour tous le bonheur et la toute-connaissance
finale... |

TRAVAUX ACCOMPLIS

L’exposé des travaux accomplis par la Société Théosophique
au point de vue international depuis sa fondation prendrait
le développement d'un volume. Nous devons donc indiquer
sommairement les résultats suivants :

Le résultat principal des idées théosophiques s'est manifesté
au point de vue du rapprochement entre les diverses religions ;
l'organisation de Congrès internationaux des religions, tels que

 

— 3 —

ceux de Chicago et de Bâic, est certainement une conséquence
de la large tolérance prêchée en matière religieuse par les théoso-
phes dans tous les pays. Certaines idées comme celles de la
réincarnation et de la loi-de Karma sc sont répandues jusque
dans la presse, sur la scène et dans la chaire de vérité; c'est
surtout dans la littérature et dans l'art que les enseignements
théosophiques se sont le plus infiltrés.

Dans l'Inde, la Société Théosophique a relevé le niveau moral,
intellectuel et religieux des Hindous. Constatant que la domina-

tion anglaise et l'influence des missionnaires avaient refoulé les

enseignements brahmanistes et bouddhistes ,quis’étaient d’ailleurs
fortement altérés, avaient déprimé le sentiment de dignité
national et religieux des habitants et entraîné la population dans
un état de dépression dangereuse pour les individus autant que
pour l'état, la Société Théosophique réenseigna aux Hindous,
dans toute leur pureté, les vérités religieuses contenues dans
leurs écrits sacrés et qui, au point de vue de la profondeur philo-
sophique, sont de loin à la tête des écritures dites révélées, de
l'avis de tous les orientalistes; elle créa des écoles hindoues,
des collèges, des écoles pour parias ; elle organisa des comités
d'éducation morale,et d’une façon générale, obtint le relèvement
de l'Inde, cette contrée immense où des populations diverses
sont mêlées, y formant un réservoir de forces qui, bien dirigées,
forment réellement le joyau de la couronne de l'Empire britan-
nique. L'excellence de l'œuvre réalisée par la Société Théoso-
phique a été reconnue fréquemment par le Gouvernement de
l'Inde; grâce à cette œuvre celle-ci est traitée actuellement
avec plus de douceur et de sympathie; le couronnement de
l'Empereur et de l'Impératrice des Indes à Delhi, où celle-ci parut
la robe ornée, en dehors des fleurs symboliques du Royaume-Uni,
de l'étoile des Indes, et l'organisation administrative des Indes
ne sont certes pas choses étrangères à l’action de la Société
Théosophique, qui a toujours compris dans son scin plusieurs
des plus -hauts fonctionnaires de l'Inde.

D'autre part, l’Église bouddhiste du Nord et l'Église bouddhiste
du Sud de l'Inde, qui étaient séparées depuis des siècles se sont
rapprochées grâce aux tentatives faites dans ce but par la Société
Théosophique. Elles reconnaissent, depuis l’année 1891, un code
commun de 14 articles constituant les principes fondamentaux

du Bouddhisme, rédigé par M. Olcott, le président fondateur dela

 
 

— 4 —

Société, Ce code fut, dans la suite, approuvé également par les

grands prêtres du Japon, — dans lequel neuf fractions bouddhistes
différentes se disputaient précédemment, — de la Birmanie et
de Ceylan. |

La présidente actuelle, Mme Besant, a poursuivi avec un
labeur admirable la tâche si élevée commencée par son prédé-
cesseur. C'est surtout l'unification du Christianisme qui est
l'objet de ses efforts ; elle dit aux Chrétiens : « Le lien qui vous
unit est trop sacré pour que vous vous disputiez entre vous;
Celui qui est le Seigneur d'Amour ne peut être un objet de
discorde. » Elle a écrit des ouvrages en matière religieuse que
tout chrétien devrait lire, tels que le Christianisme ésotérique,
Les Religions pratiquées actuellement dans l'Inde, Les Maïtres et
l'Œuvre théosophique, La Sagesse antique, etc. Aussi, les cffets du
travail de Mm€ Besant sont-ils manifestes dans nombre de com-
munautés chrétiennes, animées actuellement d'un véritable
souffle de fraternité entre elles.

Mme Besant s'est attachée dans les derniers temps à faire un
rapprochement entre les textes sacrés des différentes religions,

pour montrer le fond qui leur est commun; elle a publié ur .

Précis de Religion et de Morale en trois volumes (le troisième est
en cours de publication}, qui est une étape sur le chemin d’uni-
fication des religions que l'avenir nous réserve et dont de norn-
breux signes avant-coureurs sont visibles. Mmé Besant, une
mystique et une femme de science peu ordinaire, affirme d’ailleurs
que le Christ est en réalité l'Instructeur Suprême du mende
entier et qu’il a annoncé son retour pour unir tous les hommes en
une grande fraternité nouvelle, réunissant toutes les religions
en unc seule. ‘

. Ce court exposé démontre toute l'importance de la Société
Théosophique comme association internationale et justifie ce
qu'elle prétend être, à savoir le berceau de la grande religion
de l'humanité future.

ÉTAT ACTUEL DE L’INTERNATIONALISATION
DE LA SOCIÉTÉ THÉOSOPHIQUE

a) La Société Théosophique a créé de nombreux organismes
internationaux distincts relevant de l'Ordre de Service de la ST.
Cet ordre de service constitue un essai d'application de la

#

a
er

   

"

7 9

  
 

Théosophie en vue de pourvoir aux besoins de toutes les classes
de l'Humanité, ce au moyen de diverses ligues qui peuvent être
groupées comme suit : To

Éducation. — Le Relèvement des classes déprimées, Allepy.

Ligue de l'éducation, Rangon.

Éducation théosophique, Amsterdam.

Éducation morale, Paris.

Éducation harmonieuse, La Haye.

Éducation nationale, Muzaffurpur.

Tables rondes pour la jeunesse, à Londres, en Écosse, en Aus-
tralie, en Italie, en Hongrie, en Nouvelle-Zélande, aux Pays-Bas,
en Amérique, à Paris ct à Bruxelles.

Liguc pour l'Éducation des jeunes filles, Bénarès.

Liguc pour l'Éducation, à Bruxelles.

Réforme des maux sociaux. — L’Abolition de la vivisec-
tion, de la vaccination et de l'incculation, à Londres, Manchester -
et Bournemouth.

Antivivisection, à New-York.

Medical, Londres.

La Sociologie et le Problème social, Manchester.

Développement de la Pureté sociale, Chicago.

Développement de la Tempérance et de la Moralité, Surat.

Idéals élevés, Spokane.

Travaux d'hôpitaux et de prisons, Seattle.

Abolition des mariages entre enfants, Indes.

Protection des Animaux, Adyar. : :

Les Sept M, Buitenzorg. (Main, jeu ; Madon, commerce sexuel
illégal ; Minum, alcoolisme ; Madat, opium ; Malin, vol; Mada,
calomnie ; Mangani, gloutonnerie).

Ligue mentale internationaic de la paix, Rio-de Janeiro.

Ligue de l'union mentale pour la paix, Cuba.

Wereldvrede (Paix Universelle), La Haye.

Ligue Théosophique belge pour ta Paix Universelle, Bruxelles.

Propagation de la Théosophie. — Traduction d'ouvrages
sur la Sagesse ct l'Islam, Muzañffurpur. :

Ligue Brailice, Londres, Boston.

Université Théosophique, Chicago.

4

  

 
6 —

L'Oasis pour répandre la théosophie parmi les ouvriers de
l'arsenal, Toulon.
L'Union fraterneile, pour répandre la théosophie parmi les
_ classes laboricuses, Paris.
Science, Religion et Art, Brooklyn.
Le Bodhalaya, Bombay.
La Mission Théosophique, New-York,
Ligue de la pensée moderne, Adyar.
Ligue Théosophique Esperanto, Londres.
Ligue de la méditation journalière, Londres. :

Buts divers. — Esculapius, Bénarès et Manchester.

Fraternité des guérisseurs, Leyde.

Ordres des Aides, Melbourne.

Ligue de l'Unité, Paris.

Diminution de la souffrance, Paris.

Association de la pensée, Cape-Town, pour préparer le monde
à l'avènement du Maître.

L'Ordre du Soleil Levant, Bénarès, transformé en Ordre
indépendant de l'Étoile d'Orient, pour grouper tous ceux qui,
tant dans le sein de la Société Théosophique qu’en dehors, croient
à la venue de l'Instructeur Suprême du monde, comprenant
11,000 membres. °

Ligue Saint-Christophe pour aider ceux qui ont un lourd karma
physique, Londres.

Redemption League, Londres, pour la protection de la femme
et de la jeune fille.

À côté de l'Ordre de Service, des collèges et écoles diverses dont
il a été question ci-dessus, il existe une Ligue européenne pour
l'organisation de congrès théosophiques.

b) La Société Théosophique constituant un organisme unique,
iln'y a pas de coopération entre elle et d’autres organismes.

c) Les publications internationales de la Société Théosophique
en dehors des très nombreuses publications théosophiques
faites sous les auspices des sections nationales sont le Bulletin
& Adyar, le Theosophist ct le Herald of the Star, tous publiés à
Adyar, ce dernier étant l'organe de l'Ordre de l'Étoile d'Orient.

 

 

— f —

4) Ainsi qu'il résulte du présent exposé, la réglementation
internationale de la Société Théosophique n’a aucun caractère
officiel et est libre.

e) Iln'y a d'autre unification ou système d'unités que de viser
à une fraternité absolue entre les hommes et à découvrir une
unité fondamentale dans les religions ; l'application de ces prin-
cipes est laissée à l'interprétation individuelle.

}) La terminologie théosophique comprend assez bien de
mots sanscrits employés par la littérature des autres langues, :
aucune de celles-ci n'ayant la richesse du sanserit quant aux divers
concepts philosophiques. Il y a cependant ici un motif qui
éloigne un certain nombre de personnes de l'étude de la Théoso-
phie; aussi est-il recommandé d'employer autant que possible
les mots correspondants des diverses langues naturelles.

g) La documentation théosophique est un point capital pour
la Société Théosophique. La mission umficatrice des religions,
qui est la sienne, oblige ses membres à une étude approfondie
de toutes les religions ; les diverses sections ont aïnsi des biblio-
thèques en général richement fournies d'ouvrages philosophiques
et religieux; ces bibliothèques sont aussi accessibles aux non
membres sur la recommandation d'un membre. D'autre part,
la littérature théosophique est répandue de plus en plus dans les
bibliothèques nationales, bien qu'il reste encore beaucoup à faire
à cet effet.

T1 convient de faire une mention spéciale pour la bibliothèque
centrale d'Adyar, qui constitue en même temps un musée de
documentation religieuse fort intéressant. Des milliers d'ouvrages,
de périodiques, de manuscrits, dont un grand nombre remonte
à unc grande antiquité, y sont conservés sous la direction du
savant orientaliste le D" Otto Schrader. Si l’on peut dire que
grâce à l’Institut International de Bibliographie de Bruxelles,
le cerveau du monde contemporain est dans la capitale de la
Belgique, ainsi que le déclarait M. Le Foyer, au premier Congrès
national de la Paix, tenu à Bruxelles, les 8 et 9 juin, grâce à la
bibliothèque théosophique ct orientaliste réunie à Adyar, à

 
_ 8 —

l'instar des grandes bibliothèques anciennes d'Alexandrie, de
Ptolémais ou de Babylonc, les archives du passé philosophique
et religieux de l'humanité sc trouvent concentrées à Adyar (1).

(1) Afin que l'on puisse se rendre compte de l'importance du rôle que
la Société Théosophique joue dans l'évolution de la pensée contempo-
raine, nous extrayons les passages suivants du rapport que la doctoresse
Mme Schultz adressa en 1908, à M. le Ministre de l'Instruction publique en
France, à la suite d'une mission qui lui fut confée pour étudier aux Indes
la valeur et l'enseignement de la philosophie hindoue, et pour laquelle
Mme Schultz visita le quartier général de la Société Théosophique à
Adyar :

« C’est dans ce milieu de Vérité (l'Inde), que nos sciences et savants
d'Occident doivent aller se retremper, afin de donner un nouvel essor à
notre culture scientifique et pour arriver à la solution des grands problèrnes
qui tourmentent l'humanité de nos jours.

» La Société Thécsophique a fait beaucoup pour la diffusion de la
philosophie hindoue et les ouvrages de ses membres ont jeté une lumière
éclatante sur cette science.

» Il est à souhaiter que la Société française de théosophie, qui est
branche de la Société Théosophique totale, obtienne du Gouvernement
français une large protection et la déclaration d'utilité publique, afin de
favoriser son développement, dont dépend l'avenir intellectuel et moral
de notre pays. »

(Voyez La Philosophie hkindoue, par la Doctoresse Mme ScHuLTz. Paris,
Publications théosophiques, 10, rue Saint-Lazare ; p. 109.)

 
