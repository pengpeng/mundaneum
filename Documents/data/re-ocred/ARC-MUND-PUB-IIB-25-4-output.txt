 
Institut International de Bibliographie
PUBLICATION N° 25 “e [o25:4]

 

GLASSICATION BIBLIOGRAPHIQUE

DÉCIMALE

—— —_—

TABLES GÉNÉRALES REFONDUES
établies en vue de la publication du
Répertoire Bibliographique Universel
ÉDITION FRANCGAISE

publiée avec le concours du
BUREAU BIBLIOGRAPHIQUE DE PARIS

FASCICUl:E-N° 2.
Tables de la division [77]

PHOTOGRAPHIE

 

 

 

 

 

INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE

BRUXELLES, 1, rue du Musée. PARIS, 44, rue de Rennes.
ZURICH, 39, Eidmadstrasse.

1200

C MONS

 

 

 

 
 

 
 

 

PRÉFACE F

 

PRÉFACE

Le système de numération des documents bibliographiques,
désigné, par abréviation, sous le nom de Classification décimale a été
proposé en 1873, par M. Melvil Dewey, alors directeur de la Biblio-
thèque universitaire de l'Etat de New-York(r).

Il a été appliqué d’abord en Amérique pour le classement des

fiches des catalogues, ainsi que pour le classement des ouvrages

eux-mêmes sur les rayons des Bibliothèques.

Les fondateurs de l’Institut international de Bibliographie de
Bruxelles ont entrepris d'en fairé l'application à la création d’un
Répertoire bibliographique universel, comprenant non seulement les titres
des ouvrages eux-mêmes, mais aussi ceux des articles parus dans les
publications de toute nature (2).

Ce répertoire auquelils ont donné le nom de Brbliograplia univer-
salis et pour la publication duquel ils ont fait appel à la coopération
scientifique internationale, doit être constitué par une série de réper-

t'ires spéciaux chacun à une des branches multiples des connais-

sances humaines et qui pourront être établis séparément, mais d’après
un plan uniforme, par des spécialistes différents.

Le répertoire des sciences photographiques doit faire partie de
celte collection, sous le nom de Brblographia photographica.

Pour s'appliquer au classement des documents énumérés dans un
répertoire aussi complet que celui qui constituera la Bibliographia
universalis, le système de classification décimale créé par M. Melvil
Dewey a dû déjà être développé dans plusieurs de ses parties etila
fallu le compléter par un ensemble de règles établies de façon à per-
mettre d’en assurer encore l'extension sur un type uniforme, toutes
les fois qu’il pourra être utile d'y apporter de nouveaux développe-

{x} Consulter : Décimal Classification and Relatio Index, par Melvil Dewey, 1804: Library
Bureau, Boston, (5° édition).
(2) Consulter : Bwel/in de l'Inslilut international de Bibliographie. Bruxelles, 1805.

fe D 20

 

 
 

 

5 & PRÉFACE

 

ments en vué de l’appliquer à de nouveaux sujets ou à de nouvelles
extensions des sciences existantes.

On a reproduit ci-après les développements qui ont dû être apportés
à la partie consacrée par Melvil Dewey :à la photographie et on y
a jointun extrait des tables générales abrégées et certaines autres
tables publiées déjà par l’Institut de bibliographie pour les parties
auxquelles il peut être utile de se référer dans le classement biblio-
graphique de documents photographiques.

Ce travail contient donc tous les renseignements nécessaires pour
permettre à toute personne s ’occupänt de recueillir des documents
bibliographiques concernant la photographié d'attribuer à ces docu-
ments les numéros classificateurs sous lesquels ils devront figurer dans
le Répertoire bibliographique universel établi d’après la classification
bibliographique décimale.

-Pour pouvoir faire usage de ces tables, qui sont ainsi établies spé-
cialement en vue de l'application à la photographie, -il suffit d’ avoir
connaissance dés principes généraux du système. Ces principes ont
été résumés dans l'introduction de l'édition française des tables
générales développées qui est EAP par l’Institut international de
Bibliographie.

Nous avons reproduit ci-après cette introduction, à laquelle nous

“ne pouvons que renvoyÿer.le lecteur et nous pourrons, par suite, nous
‘contenter de ne donner ici que quelques explications co nplémen-
‘taires touchant spécialement la photographie et relatives aux dévelop-
: péments qui ont dû être introduits dans les tables; en'ce qui concérne

les applications de cette science si nouvelle.

La photographie, dans la classification adoptée par Melvil Dewey,
formé la 7e subdivision de la élasse des Beaux-Arts qui elle-même
porte le numéro 7 dans la série des divisions des connaissances
humaines ; elle est donc désignée par le nombre 77.

Afin de ne pas disséminer, dans plusieurs séries, lés documents
concernant spécialement la photographie, on a admis ‘que tous cés
documents seraient groupés sous ce numéro 77, dont les subdivisions
ont été developpées en conséquence.

On trouvera donc à 77, bien que ce nombre fasse partie de la
classe 7, Beaux-Arts, tous les documents concernant spécialement Ja
photographie, alors même qu'ils ne présenteraient aucun caractère
artistique.

À la réflexion, on reconnaïîtra qu'il n'y a là rien de véritablement
anormal ou qui doive paraître choquant, cat la classification biblio-

 
PRÉFACE t

 

:graphique décimale n’a pas la prétention de créer un classement
méthodique ét rationnel qui serait d’ailleurs, de fait, irréalisable ét
-elle se propose seulement d'attribuer à chaque sujet ou à chaque
rubrique de classement un numérotage toujours le même et d'assurer,
dans chaque branche de nos connaissances, un groupement uniforme
-etinvariable. Î
Il importe évidemment peu, dans ces conditions, que les sciences
«photographiques soient représentées par le nombre 77 ou par tout
-autre nombre.

On classera donc sont ce numéro 77 et sous ses divisions tout ce
qui se rattache directement à la Photographie et tout ce qui fait partie
des théories spéciales se rattachant à cette science. Toutefois les
questions qui se rattachent aux autres branches de nos connaissances
et où la Photographie n'intervient que d’une façon accessoire, comme
application ou comme élément complémentaire, pourront être clas-
sées sous des numéros rattachés à d’autres classes par l'application du
mode de formation des nombres classificateurs composés et l'emploi
des subdivisions communes de forme, de temps, de lieu, etc. (x).

L’exposé sommaire des règles de la Classification bibliographique
décimale qui est reproduit ci-après rendra facile la formation de
ces numéros classificateurs à l’aide des tables qui sont contenues dans
ce fascicule.

Les tables développées des sciences photographiques forment
naturellement la partie essentielle de ce travail, mais on yÿ a joint la
reproduction des tables générales abrégées embrassant l’ensemble des
connaissances humaines dont on peut avoir à faire usage, pour la
bibliographie photographique, par suite des relations que l’on peut
avoir à établir entre les sciencès photographiques et l'une quelconque
-des autres branches des sciences. l DS

On a placé, à la suite, les tables des subdivisions communes dont
‘on peut avoir à faire usage pour la formation des nombres composés.

Enfin, on a fait suivre ces tables méthodiques d’un Index destiné à
en faciliter l'usage. Il réunit en une seule série alphabétique les mots
correspondants à toutes les divisions de ces diverses tables.

. Le volume comprend encore un Exposé des règles pour la rédaction
-des notices bibliographiques et des Règles pour la publication des
recueils bibliographiques et la formation des Répertoires bibliogra-
phiques sur fiches. 5

{r) Ces règles ont été adoptées, sur la proposition de l'Institut International de Bibliographie, par
Ja Conférence bibliographique tenue à Bruxelles en 1807,

SL  —

 

 
 

É PRÉFACE

 

Il-constitue ainsi un manuel complet pour guider tous ceux qui
voudront, soit se servir de documents entrant dans la composition
de répertoires bibliographiques de photographie, basés sur la. classi-
fication décimale, soit procéder eux-mêmes à la préparation ou au
classement de répertoires de ce genre.

Si l’on désire plus de détails sur l'emploi de la Classitiee tion biblio-
graphique décimale, on pourra consulter la collection du Bulletin de
l'Institut International de Bibliographie qui paraît depuis 1805, ou les
ouvrages divers successivement publiés sur ce sujet et dont on trou-
vera l’'énumération dans l'Annuaire de cet Institut: (r)

Nous nous contenterons de donner seulement ici encore quelques
indications sur-la façon dont il a été procédé pour le développement
de la partie des tables spécialement consacrées à la Photographie.

Dans les tables primitives de Melvil Dewey, les subdivisions
prévues étaient limitées à dix, représentées par des nombres de
3 chiffres et correspondant aux titres dont nous donnons ici la tra-
duction abrégée.

770 Photographie. Généralités.

771 Matériel et chimie photographiques.

772 Procédés à l'argent et au collodion.

773 Procédés à la gélatine et aux poudres.

774 Impressions photographiques sur gélatine.
775 Photolithographie.

776 Photozincographie.

777 Photogravure.

778 Applications spéciales de la photographie.
779 Collections d'épreuves photographiques.

Malgré les difficultés que le problème présentait dans ces condi-
tions, et grâce aux multiples ressources que présentent les notations
et les règles adoptées pour l'emploi de la classification décimale.et
notamment en faisant usage des tables des subdivisions communes
et en créant des subdivisions analytiques spéciales, on à pu donner
plus d'extension aux rubriques comprises sous ces divers numéros et
réussir à grouper ainsi, de la même façon, les matières nouvelles qu’il
a fallu introduire dans les tables développées pour en faire disparaître

(x) Annuaire de l'Institut International de Bibliographie pour l’année 1890, Bruxelles, t, rue
du Musée,

On peut se procurer ces publications au Bureau Bibliogfaph'que de Paris, 44, rie de Rénnes,

SG

 

 
 

S

 

 

PRÉFACE 1

 

les lacunes et pour y comprendre les nouveautés les plus récentes
telles que la Radiographie, la Cinématographie et la Photographie
des couleurs.

On a réalisé ainsi le nouveau groupement général suivant :

77.0 Technique générale de la photographie. Opé-
rations. Genres et espèces de photographies.

77.1 Matériel photographique. Locaux, appareils,
ustensiles et produits.

Tee Procédés photographiques. Procédés à base
d'argent et d’autres métaux.

LS Procédés aux poudres et mixtions colorées et
procédés par imbibition et teinture.

77.4 à 77.7 Phototirages. Impressions photographiques.

77.8 Applications propres ou spéciales de la photogra-
phie. (Reproductions industrielles, Projections,
Photographies scientifiques, Stereoscopie, Ci-
nématographie, Photographie des couleurs,
Récréations et illusions photographiques, etc.)

77.9 Documents photographiques, Collections de
photographies.

On a pu d’ailleurs conserver, pour le classement des épreuves pho-
tographiques, la classification iconographique préparée par les soins
de M. Joseph Vellot pour le classement des collections du Musée des
photographies documentaires de Paris et qui avait, du reste, elle-même
été établie conformément aux règles adoptées pour l'emploi de la
Classification bibliographique décimale. Sous réserve des développe-
ments nouveaux apportés à la division 77 dans les nouvelles tables
qui remplacent le projet primitif établi, à titre provisoire, en 1896,
toutes les épreuves photographiques classées sous le numéro principal
77.9 recevront donc, par l'application des tables développées données
ci-après, des numéros concordants avec ceux déjà prévus par les tables
publiées par les soins du Musée des photographies documentaires.

 

 

 
 
Chai

bi
DIM

 
  
 
  
 
     
 
 

lar

NO

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   

 

 

 

 

 

  

  
  

 

 

 

 

 
 
 

 

 

 

 

 
 

 

 
©

 
 

 £

 

 

M

 

 

 

 

 

   

    

 

 

 

 

 

 
 
   

eloppem.

 

 

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

  

 

 

   

 

   

 

      

 

 

et pellicul:

 

 

 

 

 

  

 

     

   

 

  

 

     

   

 

 

 

   

   

      

 

  

 

 

 
 
de

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 
 

 

 

D

 

 

 
 

FA
 

 

 

 

  

 

        

   

   

    

   

   

 

 

       

 

 

     

 

 
 
 

L

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   

 

 

 

  

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 
 

 

 

 

 

  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   

 

   

       

     

 

  

 

 

 

 

 

 

AT r
| | ( | " 1!
( JE L
LU Ti Eu] IUTJU d
|
| r'ÉC hot C A:
ellulc
S N ‘es 5
( C -

 

 
   

 

 

e

 

 

 

 

 

 

 

 

 

 
 

 

   

      

 

     

         

 

      

 

      

   

 
        

       

 

      

 

   

 

 

    

  

        

  

 

 

 

 

 

   

 

   

     

 

   

   

   

 

 

& # x g , J
Ë % | J / ï
€ 7 ZA |
Or;pro Ÿ 4
= r | ) CRTC SIN € ÿ
k Pe j 1
Mac SULIT . É c Je <=
| À e L1eS ( 301] 7
We )D .
] ions colorées C.au
Mon. es D eraphiqu I 7 L EURE

 
 
 
 
 
