 

 

 

CONGRÈS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES N° 18
Deuxième session : GANO-BRUXELLES, 15-18 juin 1913

Organisé par l'Union des Associations Internationales

Office central : Bruxelles, Suis, rue de la Régence

 

1913.05

 

 

 

 

 

Actes du Congrès. — Documents préliminaires.

 

 

Réduction de la Taxe Postale Universelle

[383]

La Conférence internationale de l'Union Postale Universelle
doit se réunir à Madrid en 1914. Parmi les vœux dont elle est
saisie, figure l'introduction de la taxe postale internationale à
dix centimes. Des gouvernements, des administrations et des
associations ont demandé que le port actuel de 23 centimes,
pour les lettres dont le poids n'excède pas 20 grammes, soient
réduit à ce taux minime. Cette réforme aurait évidemment une
influence considérable sur les destinées de l’internationalisme,
Une telle réforme est du reste dans la logique historique et il est
utile à cet égard de rappeler quelques rétroactes.

La taxe axtuelle ne date que de 1875. Voici, en effet, la dis-
position que contenait la Convention de Berne {constitutive de
l'Union Postale} :

ART. 3. — La taxe générale de l'Union est fixée à 25 centimes pour la
lettre simple affranchie.

Toutefois, comme mesure de transition, il est réservé à chaäque pays,
pour tenir compte de ses convenances monétaires ou autres, a faculté
de percevoir une taxe supérieure ou intérieure à ce chiffre, moyennant

qu'elle ne dépasse pas 32 centimes et ne descende pas au-dessous de
20 centimes.

Sera considérée comme lettre simple toute lettre dont le poids ne dé-
passe pas 15 grammes. La taxe des lettres dépassant ce poids sera d'un
port simple par 13 grammes ou fraction de 13 grammes.

Le port des lettres non affranchies sera le double de la taxe du pays de
destination pour les lettres affranchies,

 
— 2 —

La Convention de Rome de 1906 modifiait la disposition pri-
mitive et la libellait comme suit :

ART. 5, — 1, — Les taxes pour le transport des envois postaux dans
toute l'étendue de FÜnion, y compris leur remise au domicile des destina-
taires dans les pays de l'Union où le service de distribution est ou sera
organisé, sont fixées comme suit :

19 Pour les lettres, à 25 centimes en cas d'affranchissement, et au
double dans le cas contraire, par chaque lettre ne dépassant pas le poids
de 20 grammes, et à 13 centimes en cas d'affranchissement, et au double
dans le cas contraire, pour chaque poids de 20 grammes ou fraction de
20 grammes au-dessus du premier poids de 20 grammes.

L'amélioration a donc essentiellement consisté en ce que -le
poids de la lettre a été porté de 13 à 20 grammes et la progres-
sion de la taxe d'affranchissement au delà du premier port de
20 grammes abaissé de 20 à 15 centimes, tout en portant la pro-
gression du poids de 15 à 20 grammes. Cette réforme est-elle
suffisante en présence des énormes changements qui se sont
produits dans les communications postales depuis 40 ans?
On l'a contesté avec raison : l’internationalisation commerciale
et industrielle, la coopération mondiale dans tous les domaines
de la science et de l’action, eh un mot tout ce vaste mouvement
qui tend au rapprochement des peuples, a centuplé les relations
entre les hommes et les pays. La diminution des frais postaux
ne pourrait qu'activer ces relations indispensables et fructueuses.
Il n'est pas douteux que le fait d'avoir maintenu à 25 centimes
la taxe simple des lettres et à 10 centimes le port des cartes

postales en régime international constitue, pour un grand nombre.

d'entreprises et d'œuvres, une charge excessive. Ce n’est pas le
poids de la lettre qu’il fallait augmenter, c'est le taux d’affranchis-
sement qui constitue en réalité l'obstacle qu'il y a lieu de ren-
verser, la nuisance qu'il faudrait supprimer. Les perfectionne-
ments apportés dans la fabrication des papiers légers ont permis
d'augmenter l'utilisation au maximum du poids normal des
missives, mais le prix relativement élevé du transport est resté
invariable : or, l'immense majorité des lettres ne dépasse cer-
tainement pas 13 grammes et la situation pour elles est dès lors
restée onéreuse.

Un tel état de choses a fini par émouvoir jusqu'aux dirigeants
des services postaux des pays les plus progressifs, C'est ainsi

 

 

 

— 3 —

qu’au Congrès postal de Rome, une proposition fut faite par Îcs
Pays-Bas, visant à la réduction à 20 centimes de la taxe pour la
lettre de simple poids : cette proposition ne fut rejetée que par
34 non contre 17 oui et 8 abstentions.

En attendant que l'heure sonne où un accord universel
deviendra possible, une cinquantaine d’ententes postales res-
treintes ont été conclues. L'Union postale sud-américaine, aux
termes de l'article 3 du traité de Montévidéo de 191T, a adopté
la taxe à 20 centimes. La Belgique et la Hollande ont conclu
un arrangement identique et dans les régions voisines de leurs
frontières communes ont abaissé le port à 10 centimes. D'autres
conventions particulières, d'un caractère plus radical, ont
abouti à ce résultats qu'actuellement 40 p. c. des lettres, échan-
gées entre les pays du monde entier, bénéficient d’une taxe
réduite à ïo centimes. L'Allemagne ct l'Autriche-Hongrie,
notamment, se sont entendues pour continuer l'application de la
taxe à 10 centimes en vigueur entr’elles depuis 1850 ; les États
scandinaves ont complété leur Union monétaire par une Union
postale appliquant une taxe identique ; l'Espagne et le Portugal,
la Bulgarie et la Roumanie, se sont mutuellement concédé la
taxe à 10 centimes: de même l'Allemagne et la Grande-Bre-
tagne ont obtenu des ports réduits pour la correspondance avec
les États-Unis ; le Luxembourg a conclu des accords de faveur
avec l'Allemagne, la France, la Belgique et le Grande-Bretagne ;
celle-ci à introduit depuis 1898, le «penny-postage» dans son
empire colonial et son exemple a été suivi par d'autres pays
colonisateurs (1).

Ce sont là des faits positifs qui mettent en relief la fragilité de
l'accord universel intervenu à Rome. D’autres faits sent du reste
tout aussi significatifs de la tendance générale vers une réduc-
tions nouvelle de la taxe postale. C’est ainsi que l'adoption de
la taxe à ro centimes fait depuis plusieurs années l’objet de
négociations entre l'Allemagne et la Suisse. M. Chaumet, sous-
secrétaire des postes et télégraphes en France, a déclaré au

{+} On sait que c'est à M. Henniker Heaton, soutenu par M. WT.
Stead que cette importante réfonre est due. Mais il ne s'est pas déclaré
satisfait et il est reparti en campagne pour conquérir le « penny-postage »
universel.

 
— 4 —

Sénat, le 23 juin 1911, qu'il préférait abaisser à 10 centimes Ia
taxe des lettres pour les pays limitrophes (la Grande-Bretagne,
la Suisse, la Belgique, l'Italie et l'Espagne), que d’abaisser à
5 centimes l'affranchissement de la carte postale dans le service
intérieur. Or, si un accord franco-anglais intervenait, l'Alle-
magne, en vertu du traité de Francfort, demanderait à en
bénéficier.

D'autre part, M. Samuel, ministre des postes et télégraphes
britanniques, dans un discours prononcé en 1911 à Paris, à la
Chambre de commerce, a déclaré que s’il n’était pas possible
pour le moment d'établir pour la correspondance entre la
France et la Grande-Bretagne la taxe à 10 centimes, cette
réforme n'était qu'ajournée.

Que dire des anomalies qui existent actuellement par suite du
fait que la taxe à 10 centimes a été établie entre la Grandc-Bre-
tagne, l'Allemagne, la France et leur colonies respectives? C'est
ainsi qu'une letire va de France en Nouvelle Calédonie et d’Alle-
magne en Nouvelle Guinée, à douze mille milles, pour 10 cen-
times et qu’une lettre exige 25 centimes pour venir de Douvres à
Calais. Ce qui est plus bizarre encore, c'est que vingt millions de
lettres envoyées de la Grande-Bretagne en Asie et en Australie
traversent la France et l'Italie, bien qu'elles ne portent qu'un
timbre d’un penny! Ces dernières anomalies semblent décisives,

Il semble donc que la prochaine Conférence de Madrid devrait
généraliser la situation créée par les accords bilatéraux rappelés
plus haut. Certains États pourront évidemment objecter que
l'application internationale de la taxe à 10 centimes leur cau-
serait des pertes momentanées de quelques millions de francs,
mais il est certain que cette application facilitera singulièrement
la correspondance mondiale, et par suite aidera dans une large
mesure au développement de la prospérité économique et merale
de chaque pays. Seuls des administrateurs à courte vue et imbus
à tort de cette idée que les recettes postales ont un caractère
fiscal, peuvent songer à entraver un essor aussi légitime qu'utile
et bienfaisant. C’est le moment, semble-t-il, pour les différents
organes de l’internationalisme, à intervenir avec toute l'autorité
.morale qui leur est propre. De multiples manifestations de leur
part se sont du reste déjà produites.

C’est ainsi que le Conseil international des Femmes vota, lors

— 5 —

du Congrès de Toronto en 1go9, la décision suivante : « Que le
Conseil international des Femmes soit prié de faire une demande
au Bureau de FUnion Postale Universelle, pour obtenir une
réduction sur le prix de la correspondance et pour l'introduction
d'une méthode simple de retours payés pour les réponses, et que
chaque Conseil national soit également prié d'employer son
influence dans ce même but auprès de son Gouvernement res-
pectif. » Le récent Congrès international des Chambres de com-
merce à Boston, recommanda énergiquement dans ses résolu-
tions la taxe à 10 centimes. L'Union interparlementaire, saisie en
1910 de la question par une motion de M. Fiore (Italie), a modifié
ses statuts dans le but de lui permettre de discuter de telles
réformes et Fa réduction de la taxe postale a été mise à l’ordre
du jour de sa prochaine session. Le troisième Congrès de la
Presse périodique, réuni à Paris en 1912, appuya également
l'abaissement de la taxe des lettres dans les relations interna-
tionales. Il chargea son Comité permanent de recommander à
l'attention de l'Union postale universelle, les propositions
suivantes :

1, La taxe fixée par la Convention Postale Universelle pour le trans-
port des lettres sera abaissée de 25 centimes à 10 centimes, soit à la taxe
d’affranchissement du service intérieur, Cette taxe d’affranchissement
sera perçue par poids ou fraction de poids de 20 grammes dans toute
l'étendue de l'Union postale, le poids étant calculé d'après le système
métrique.

2. Les États signataires de la Convention postale universelle de 1906
qui, au bénéfice du chiffre IIT du protocole final de cette Convention,
ont conservé les limites de poids et les taxes de la Convention postale
précédente, renonceront à ce régime d'exception.

3. Dans ie trafic international, la surtaxe perçue par la poste pour

absence ou insuffisance d’affranchissement des objets de la poste aux
lettres séra fixée uniformément à 5 centimes.

Le prochain Congrès mondial des Associations Internationales
est, lui aussi, saisi de la question et il n’est pas douteux qu'il
adoptera des résolutions similaires.

Ces groupements divers, qui représentent des collectivités
particulièrement puissantes par le nombre de leurs adhérents
et par l'influence dont elles disposent, pourront exercer une
action prédominante sur leurs gouvernements et les convaincre
de la nécessité de la réforme préconisée.

Si des considérations morales de haute culture et de diffusion

 
_6—

des idées sont invoquées en faveur de cette réforme par ceux
qui dirigent les organismes internationaux, des exigences d'ordre
éminemment pratique les préoccupent également. Les frais pos-
taux constituent pour la plupart des institutions internationales
une Charge parfois écrasante qui, en présence des faibles res-
sources dont elle disposent le plus souvent, paralyse leur bonne
volonté, Toutes regrettent les sommes ainsi dépensées sans pro-
fit direct pour des œuvres d'intérêt général et de pur dévoue-
ment qu'elles pousuivent. Aussi leurs désirs vont-ils au delà de la
question ici soulevée et déjà lors du premier Congrès mondial
des Associations Internationales, a-t-il été voté un vœu en faveur
de la franchise postale (1). Mais il faut envisager la situation
telle qu'elle est et se rendre compte que la réduction universelle
de la taxe des lettres à 10 centimes constituerait déjà, pour tous
ceux dont les relations avec les pays étrangers forment un élé-
ment essentiel de leur activité, sinon une économie effective, du
moins un moyen puissant de faciliter et de multiplier leurs
efforts. ….

Les gouvernements doivent avoir cette persuasion que le
développement des relations internationales par la télégraphie,
par la téléphonie et surtout par la poste constitue une des con-
ditions essentielles de la prospérité matérielle et de l'influence
morale de leurs pays respectifs. Ils devraient être les premiers à
préconiser l’abaissement des taxes postales à un taux qui leur
assure sans plus le recouvrement des frais de transport des
lettres et des imprimés, en attendant l'heure où la franchise
postale sera accordée à tous ou tout au moins à ceux qui pour-
suivent l'amélioration des relations entre les citoyens de toutes
les nationalités.

(1) Établir, selon des modes pratiques, un système de franchise pos-
tale internationale ou d'abonnement forfaitaire, tant pour la correspon-
dance que pour les imprimés, permettant à tous les organismes interna-
tionaux d'entretenir dés relations permanentes entre eux et avec les
grands organismes nationaux ; considérer à cette fin que tous les motifs
qui ont déterminé l'établissement des franchises postales nationales
peuvent étre invoqués en faveur de la franchise postale internationale,
et que la compensation des sacrifices à faire de ce chef par les États peut
être largement retrouvée dans l'accroissement du trafic international
actuel par suite du développement apporté aux relations internationales
par les Associations elles-mêmes.

 

 

 

 
