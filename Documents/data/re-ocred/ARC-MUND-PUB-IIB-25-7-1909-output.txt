 
 

MANUEL DU RÉPERTOIRE BIBLIOGRAPHIQUE
UNIVERSEL

(Publication hi 63 de l'Institut International de Bibliographie).

 

Cétte publication, aujourd’hui entièrement terminée, résume et condense
en un seul volume mis à jour les diverses publications fragmentaires que
l'Institut International de Bibliographie a fait paraître jusqu'à ce jour sur
son organisation, ses méthodes et ses travaux. Elle contient les matières
détaillées ci-après. :

L'ouvrage comporte un volume de plus de 2,200 pages, relié en pleine
percaline, avec disposition spéciale d’onglets facilitant sa consultation. Le
a prix de souscription est fixé à 50 francs par exemplaire, port en plus. Les
Res tables de classification reproduisent #7 exfeuso les tables publiées séparé-
Mn ment dans la publication n° 25. Elles comprennent plus de 32,000 divisions
L dans l’Index alphabétique général.

ORGANISATION, TRAVAUX, MÉTHODES.
Organisation.

1. Le Répertoire bibliographique universel de l’Institut Interna-
tional de Bibliographie.

II, Statistique générale des services et des travaux.

pin III. Actes officiels, statuts, etc.

| ne : Catalogue des travaux.

IV. Catalogue du Répertoire bibliographique universel. (Prototype
du manuscrit.) . k
V. Catalogue de la « Bibliographia Universalis ». (Contributions
ani imprimées.)
a VI. Catalogue des publications diverses.

 

Règles et méthodes.

VII. Etablissement des répertoires sur fiches : Répertoires des ma-
tières, répertoires des auteurs, autres répertoires.
VIII. Rédaction des notices bibliographiques.
: IX. Publication des notices bibliographiques en recueils et sur
À : fiches.
; X. Modes divers de coopération au Répertoire bibliographique
universel. ; ;
XI. Organisation des diverses collections de documents.
XII. Matériel et accessoires à l'usage des répertoires.

TABLES DE CLASSIFICATION BIBLIOGRAPHIQUE.

 

 

{
ES j Exposé et règles de la Classification décimale.
Ê è Table principale.
Ê : je : Tables auxiliaires,
Index alphabétique général!

 

 

Manuel abrégé
du Répertoire bibliographique universel.
(Publication n° 65 de l'Institut International de Bibliographie).

 

Ru Ce Manuel abrégé comprend les mêmes matières que le Manuel complet
És (publication n° 63) décrit ci-dessus, sauf pour les Tables de Classification bibho-
E graphique, qui sont limitées aux divisions principales (environ mille rubriques).
En souscription. 3 francs.

Les souscripteurs au Manuel abrégé pourront éventuellement compléter
cet ouvrage par l'acquisition des fascicules des Tables de Classification
rélatifs aux branches qui les intéressent. Pour la vente de ces fascicules, voir
ci-après Classification bibliographique décimale (publication n° 25).

 

 

 

 

 
 

 

 

 

SPORTS 797.6

 

70 Sports. Jeux et amusements.

79.0

.07
.09

791

77 Divertissements publics, Fêtes et spectacles forains.
792 Représentations théâtrales.
793 Jeux d'intérieur.
794 Jeux de calcul et de précision,
795 Jeux de combinaison et de hasard.
796 Jeuxetsportsde plein air. Sports athlétiques, Tourisme
Alpinisme, Cyclisme, Automobilisme.
797 Navigation de plaisance et aérostation.
798 Equitation, Courses de chevaux. Attelages.
799 Pêche. Chasse. Tir à la cible,
Les tables des subdivisions communes de forme, de lieu, de langue, de temps, etc.,
(tables T, TT, IT, IV et V) sont applicables dans tonte l'étendue de la division 79.
Lorsque des divisions spéciales n'auront pas été expressément prévues pour spéci-
fier les particularités relatives à la pratique des différents sports et exercices ou les
divers points de vue sous lesquels le sujet peut être traité, on pourra faire usage
aussi, dans toute l'étendue de cette division 70, des subdivisions analytiques dont le
tableau est donné ci-après 79.0 et qui sont caractérisées par le zéro sans parenthèse,
Les sports et amusements spéciaux à des pays déterminés, comme aussi les excur-
sions faites en différentes régions, pourront être localisés par l'emploi de l'indice
géographique des lieux considérés, c'est-à-dire l'indice placé entre parenthèses. Ex.:
796.64.09 (494) Excursion en bicyclette en Suisse.

Subdivisions analytiques spéciales. Con-
ditions accessoires applicables aux sports ou
exercices.

Hygiène spéciale.

Nourriture et aliments appropriés.
Vêtements spéciaux.

Equipement et outillage.

Abris

Organisation générale.
Apprentissage. Instr uction spéciale.
Excursions.

Divertissements pou Fêtes et spectacles

forains.
Pour les luttes et jeux athlétiques, voir 706.8.

Représentations foraines. Saltimbanques, Danseurs
de corde, Jongleurs, Bateleurs.

Concerts publics. Concours de + et de poésie,
Jeux floraux.

Cirques. Hippodromes, Rene tire équestres,
Jeux de cirque.

Pour les exercices équestres des gymnasiarques, voir 796.48.

Panoramas. Lanternes magiques.

Musées de cire. Marionnettes, Ombres chinoises.

Fêtes publiques. Cortèges historiques, Défilés, Caval-
cades, Iluminations, Feux d'artifices.

 

 

 

 
 

 

 

 

| 501.7 SPORTS

Ë 791.7 Jeux publics. Joutes, Mâts de Cocagne, Balançoires,
ë =: Bascules, Jeux de bagues, Montagnes russes.

Îl .8 Ménageries. Combats d'animaux entre eux etcombats

contre les animaux, Sports d'animaux, Combats de
coqs, Combats de chats et rats, Ours lutteurs, Com-
bats de taureaux, Courses de chiens.

Pour les exercices équestres, voir 706.48 ; pour les courses de chevaux,

 

voir 708,4.

.9 Autres divertissements.

792 Représentations théâtrales.

à ë
E Pour les théâtres de société, voir 793. Pour la littérature théâtrale, voir sous $
Ê : les divisions propres à la littérature de chaque langue.

 

: de Tragédies et drames.

22 Comédies et vaudevilles.
.3 Farces et pantomines. %
.4 Opéras et drames lyriques.
0 : Opéras comiques. :
.6 Opérettes et bouffonneries musicales. >
7 Cafés-Concerts et Music-halls.
8 Féeries et ballets. Chorégraphie.

Pour les danses de salon, voir 703.3.
.9 Autres pièces.

793 Jeux d’intérieur: Salons ow pelouses.
, JL Théâtre de société.

Voir aussi 702 Représentations théâtrales,
2 Jeux préparés. Tableaux vivants. Charades. Proverbes

en action.
:3 Danses de salon.

Pour la chorégraphie et les ballets, voir 702.8 ; pour les danses enfantinées,

voir 706,13.
Feb Danses populaires et danses nationales.
152 . Danses de caractères. Danses françaises anciennes.
Pavane, Menuet, Gavotte.

239 Danses de salon pour deux personnes. Ë

Polka, Valse, Mazurka, Redowa, Scottisch, Pas

: de deux, etc.
.34 Danses de salon pour plus de de personnes sans
accessoires.
Quadrilles, Pas de quatre, Lancier, étc.

 

 

 

 

 

 
 

 

 

 

SPORTS 705.43

 

703.35

ÿ ©

794

© © © in À & D h

799

HA

.42

.43

Danses avec accessoires.
Cotillon, etc.

Jeux d'action.
Colin-maillard, Main chaude, Furet. :

Jeux de gages et d’attrappe. Jeux innocents.

Jeux de pénitence.

_ Jeux d'esprit et de mémoire. Rebus, Charades,

Devinettes.

Récréations Scientifiques. Physique amusante. Esca-
motage.

Autres jeux d'intérieur.

Jeux de calcul et de précision avec appa-

reils spéciaux.
Pour les jeux de cartes, voir 795.4.
Jeux d'échecs. Jeu de Palamède.
Jeux de dames. Gobang, Agon.
Jeux de trictrac. Jacquet, Revertier.
Jeux de marelle.
Jeux de solitaire.
\ Jeux de dominos. Matador
Jeux de billard.
Jeu de bagatelle.
Autres jeux de calcul et de précision.

Jeux de combinaison et de hasard.

Jeux de dés et d’osselets. Jeu d'oie.
Jeux avec roues. Toton, Roulettes, Steeple-chase
ou petits chevaux.
Jeux à tirage de numéros. Loto, Biribi.
Jeux de cartes.
Jeux de cartes de combinaison. Piquet, Impériale,
Écarté, Triomphe, Mistigri, Whist, Hombre,
Reversi, Cribbage, Boston, Manille.

Jeux de cartes de hasard. Brelan, Trente et un.

Rams, Trente et Quarante, Nain Jaune, Lans-
quenet, Baccara.
Jeux de cartes de position et d'adresse. Patiences,

Tours de cartés.
Pour la Cartomancie, voir 133.3.

 

 

 

 

 

 

 

 
 

 

706 SPORTS

 

796 Jeux et Sports de plein air et sports athlé-
tiques. Gymnastique. Tourisme. Escrime.

 

 

Des Jeux d'Enfants. Récréations de plein air,
SLT *  Petts jeux paisibles et de la première enfance.
12 Récréations enfantines sur le sable et la plage.
19 Rondes et danses enfantines.
.14 Jeux d’agilité. Cheval fondu, Saute-mouton, Cache-
cache. Quatre-coins, Barres.

19 Cerf-volant. : -
5) Jeux d’action et d'adresse avec instruments.

Pour les Jeux de balles et ballon, voir 706.3.
21 Corde à sauter, Cerceau, Escarpolette. :
22 Toupies.
29 Billes:
BYE Palets, Bouchon, l'onneau, Bâtonnets. : :
.25 Bilboquet, Diable. 4 :
.26 Volant. L:
127 Crocket. x
.28 Boules, Cochonnet, Quilles. A
.29 Autres jeux d'action.
5) Jeux de balle et balion. Jeux de jet.
O1 Balle lancée à la main sans instrument. Balle au

mur, Balle au camp, Baïlle indienne, Balle empoi-
sonnée, Balle cavalière.

Sp) Ballon lance à la main.
33 Ballon lancé au pied. Foot-ball.
; .34 Jeux de balle avec tamis, tambourin ou raquettes. .
Paume, Longue paume, Lawn Tennis.

+139 Jeux de balle avec crosse ou maillet. Base Ball, Jeu
de Mail, Cricket, Gulf, Polo.

! .37 Jeux avec objets lancés. Choule, etc.
.38 Jeux de jet. Javelot, Sagaie, Boomerang.
.4 Gymnastique et exercices corporels.

Pour les sports athlétiques et l'escrime, voir 798; pour les combats d’ani-
maux, Voir 701.5; pour le patinage et la natation, voir 796.9; pour la
+ gymnastique scolaire, voir 371.73.

AI Exercices d'assouplissement et dé force. Appareils de
chambre, Haltères.

42 Marches, Courses, Echasses. :

43 Sauts et voltiges. Perches, Tremplin, Cheval de bois,

A4 Barres horizontales, Barres parallèles,

149 Portique;

 
 

 

 

h

796.46 Trapèze et anneaux.

À 47 Acrobatie, Dislocation, Gymnasiarques. !
.48 Exercices équestres. |

Pour l'équitation, voir 798.2 £ :

.49 Exercices avec animaux. Clowneries, Exercices divers. Ë

ds) Tourisme. Alpinisme. Marches, Pédestrianisme,

Excursions à pied en plaine et en montagne.

On fera usage des divisions géographiques pour spécifier les localités et
des divisions analytiques de 79, qui sont rappelées ci-dessous.

RER SSSR

! 251 Pédestrianisme. Tourisme. Marches. Excursions en
plaine. Il
:52 Alpinisme, Excursions en montagne. L
DIVISIONS ANALYTIQUES (f

.or Hygiène spéciale.

.o2 Nourriture, Boisson, Kola.

.03 Vêtements, Chaussures, Voiles, Lunettes.
.04 Outillage, Piolet, Corde, Crampon. 5 |
.05 Abris, Cabines, Refuges.

L \ il
{ | .521 Guides, Généralités, Règlements et tarifs. |
x Pour les assurances des guides, voir Assurances sur la vie 368.6. |
x 022 Dangers et accidents. Précautions et règles à observer. |
Pour les accidents qui ne sont pas spéciaux à l’alpinisme, voir 1
Eboulements 551.43, Avalanches 551.31, Débordements 551.57. À
Pour les maladies et indispositions, voir à la Pathologie et à la È
Physiologie : Mal de montagne 612.275:1 ; Troubles du cœur 4
616.12 ; Hémorragie 616.13 ; Vertiges 616.841. Ü
.523 Appels et signaux. À
.6 Cyclisme. Appareils de locomotion mus par l’homme. à
Au point de vue sportifseulement ; pour la théorie des cycles, voir 537 : 796.6; à
pour la construction, voir 629.118; pour les appareils avec moteurs, voir {
796.7. Æ î
Or Premiers appareils de vélocipédie, Célérifères, Drai- à
siennes, Hobby-horse. À
= .62 Monocycles. l
u h
.63 Bicycles. À
.64 Bicyclettes. ô
.65 Tandems. 2
.66 Triplettes, Quadruplettes. |
ë 2 b
.67 Appareils d'entraînement. À
.69 Autres appareils de locomotion. , #
7 Automobilisme.Appareiïlsde locomotionavecmoteurs.

Au point de vue sportif seulement ; pour la construction des véhicules et
l'industrie de la locomotion, voir 620.17 ; pour la construction et la théorie
dés moteurs, voir 621 et 536.8. À subdiviser par — comme 629.113.

71 Appareils à deux roues avec moteurs. Bicyclettes à
pétrole, =

 

 
 

 

+ RSR ATEN pee Rs ET

SPORTS

 

706.72
796.72 Tricycles à pétrole.
79 Quadricycles et voiturettes à pétrole.
.7A Tricycles, quadricyeles et voiturettes électriques.
79 Voiturettes automobiles légères pour deux ou quatre
personnes.
77 Voitures de courses.
.8 Sports athlétiques. Sports de combat. Boxeetescrime.
Pour les combats d'animaux, voir 707.8.
ataen Luttes à main plate
.82 Luttes olympiques.
.83 Boxe.
.84 Savate, Chausson.
09; Canne, Bâton.
.86 Escrime.
Pour les règles du duel, voir 304.8, et pour la législation du duel, 343.613.
.863 Fleuret et épée.
.865 Sabre.
.89 Autres sports clerdues.
.9 Exercices sur la glace, la neige et dans l’eau: Pati-
nage. Natation.
.9I Exercices sur la glace et la neige.
.013 Patinage sur la glace.
.015 Patinage sur la neige. Ski.
.016 Traîneaux.
.95 Exercices dans l’eau en général.
.051 - Natation sans appareils.
.992 Natation avec appareils.
2707 Navigation de plaisance et aérostation.
Pour les jeux de balle et de ballon précédemment classés sous ce même numéro,
voir 796.3.
797.1 Navigation de plaisance.
Au point de vue sportifseulement ; pour la navigation commerciale, voir 656.2;
pour la théorie, voir 527 ; pour la construction des navires, voir 600.
12 Canotage en rivière à rame et à voile.
DT Navigation cotière à voile. Vachting. RÉE ES
.15 Navigation à vapeur. È
GE) Vélocipèdes aquatiques. Podoscaphes et autres appa-
4 reils de navigation.
.5 Aérostation.
Au point de vue sportif seulement ; pour la construction des appareils, voir
629.13.
52 Baïlons libres.
253 Ballons captifs.

Ballons dirigeables.

 
æ

=“

 

SPORTS 799.32

 

707290
.56

07
.58

798

‘22

.23
.24

.45

.62

.64
.66

799

IT
12
19
.I4
19
16

PT
.22
23

.24.
29

Appareils plus lourds que l'air.
Appareils d'aviation.
Aéroplanes.

Cerfs-volants.

Equitation. Couïses de chevaux. Attelages. :

Aupoint de vue sportif seulement ; pour l'élevage des chevaux en général, voir
636.7.

Equitation.
Voir aussi 796.48 Exercices équestres.
Dressage du cheval de sélle. Harnachement.
Voir aussi 636,11.044 Dressage du cheval en général.

Equitation.
Manèges. Carrousels. ; :

Courses de chevaux. Champs de courses. Hippo-
dromes. Chevaux de courses. Elevage. Entraînement,
Couises d'obstacles. Steeple-chases.

Attelages.

Dressage du cheval d’attélage.
Attelage et conduite des voitures. Coatching.
Courses d’attelages. Concours hippiques.

Pêche. Chasse. Tir à la cible.

Au point de vue sportif seulement ; pour l’industrie de la chasse et l'exploitation
du gibier, voir 630.1; pour l’industrie de la pêche, voir 639.2 à 630.8.

Pêche en général.
Pêche en eau douce.
Pêche à la ligne.
Pêche au filet en rivière.
Pêches diverses. …
Pêche côtière. Pêche aux crevettes, aux crabes.
Pêche en mer.
Chasse en général.
Chasse à tir.
Chasse au fusil en plaine et en battue.
Chasse au chien d'arrêt.
Chaësé au marais.
Chasse à courre. Chasse au faucon. Chasse aux flam-
beaux.

Chasse aux animaux féroces ou sauvages.
Pour la chasse aux animaux à fourrures, voir 630.7.

,

Tir.
Tir à la cible en général. Tir desarmes à feu. Pistolet.
Fusil. Carabine.
Tirs à l'arc, à l’arbalète à la sarbacanne, etc.

 

 
 

 

 

CLASSIFICATION BIBLIOGRAPHIQUE DÉCIMALE

(Publication n° 25 de l'Institut International de Bibliographie).

NOTICE.— La Classification bibliographique décimale est universelle,
internationale, encyclopédique, à la fois particulière et générale: elle est
documentaire, elle s'exprime en une notation concise, elle est fort étendue
(environ 32,000 divisions) et indéfiniment extensible. Les bases en ont été
adoptées parle Congrès international de Bibliographie en 1805 et en 1807 à
Bruxelles et en 1900 à Paris, et elle a déjà reçu une large application, tant
en Europe que dans le Nouveau Monde.

Cette classification consiste en une vaste table systématique des matières,
dans laquelle tous les sujets de connaissances sont répartis par classes, sous-
classes et divisions, en passant du général au particulier, du tout à la partie,
du genre à l'espèce. 5

Chacune des rubriques de cette table est représentée par un nombre classi-
ficateur composé d’un ou de plusieurs chiffres, suivant le degré de généralité.
Ces nombres sont décimaux. en ce sens que chaque chiffre vers la droite du
nombre ne modifie pas la valeur ordinale des chiffres précédents, mais
correspond à une subdivision de la matière représentée par les chiffres
précédents. L'ordre dans lequel les nombres se suivent est aussi l’ordre
décimal. :

La table ‘systématique est complétée par un index alphabétique des
matières, dans lequeltoutes les rubriquesde la première table sont rangées en
un seul ordre alphabétique etsont suivies du nombre classificateur correspon-
dant. Exemples : ;

1 Philosophie. Il Economie financière 332
2 Religion. $ Economie politique 33
53 Sciences sociales. = Monnaie 332.4
8 31 Statistique. 2 Philosophie NE
#8 32 Politique. = Politique, question du travail 33x
È 33 Economie politique. = Religion 2
SSL Questions du travail. $ Sciences sociales 3
= 332 Economie financière. E Statistique 31
332.4 Monnaie. Travail 8x

L'indexation suivant ces tables {c'est-à-dire l'inscription sur les documents
à classer du numéro classificateur correspondant à la matière principale dont
ils traitent) permet donc de former des collections de documents rangées dans
un ordre méthodique parfait et susceptibles d’accroisséments et d’intercala-
tions continus.

D'une manière générale, comme classification des matières uniforme et
internationale, la classification bibliographique universelle est susceptible
d'être appliquée au classement des diverses espèces de documents et maté:
riaux dont les travailleurs intellectuels ont à se servir, et elle fournit, à cet
effet, des cadres tout prêts, tracés d’avance : classement des répertoires biblio-
graphiques et des catalogues; classement des ouvrages eux-mêmes dans les
bibliothèques; classement des notes, observations, extraits et documents
divers destinés à des études et à des travaux personnels; classement des tables
de matière des recueils périodiques; classement de documents graphiques,
illustrations ‘et photographies, de clichés, de brevets, de spécimens, de cata-
logues industriels, de circulaires commerciales et toutes autres applications
à la documentation, prise dans le sens le plus large.

Le jour où la classification documentaire universelle se sera répandue, où
son application aura été généralisée, au lieu d’avoir à se familiariser avec
vingt clés différentes variant d’après les institutions qui conservent et qui
classent les documents, le public des chercheurs pourra, à l’aide d’une seule
clé, c'est-a-dire d'une même table de classification des matières, se faire
ouvrir les trésors de tous les dépôts de documents. Une économie considé-
rable de temps pourra être réalisée ainsi, et le chercheur bénéficiera des avan-
tages de la connexion étroite établie entre toutes les sources documentaires
de nos connaissances. La classification bibliographique universelle permet-
tra enfin de créer l'entente et la coopération dans les travaux. Au point de
vue des collaborations internationales, elle pourra jouer un rôle similaire à
celui qu’on attend de la langue internationale, qui ne cherche pas à contra
rier les langues particulières ni à s'ysubstituer, mais bien à servir d’auxiliaire
et de complément pour les relations extérieures.

a EE TR SE EC RL

 

 

 

 
 

 

 

 

 

à FASCICULES. — Cette édition de la Classification bibliographique décimale réunit les diverses éditions fragmen-
taires parues jusqu'à ce jour. Elle'a été publiée par fascicules/ comme Z74/icafion y 65 de l'Institut. La souscription à
l’ensemble de cette publication est close, mais les personnes désireuses de recevoir les fascicules relatifs à nne branche
déterminée peuvent souscrire aux fascicules aux prix indiqués au tableau ci-après.

Dorénavant, pour recevoir en entier les 7a/es développées, il y'aura lieu de souscrire au J/anrnel di Répertoire biblio
£raphique universel (voir publication n°63), dans léquel elles ont été incorporées.

 

 

 

 

 

 

 

 

 

 

 

 

 

Tableau des fascicules composant le Manuel de l’Institut International de Bibliographie
permettant de constituer à volonté des Manuels spéciaux pour les diverses sciences.
< à
£ 8 MATIÈRES SE PRIX
6 8 e À 9 EN
TD ü «4 As
> ë de è DÉNOMINATION 5 ‘a FRANCS. à
35 » Organisation, travaux, méthodes de l’Institut Inter-
national de Bibliographie (exposé général) : . | 176 2)
| 14 » Exposé et règles de la Classification décimale . .| 32 1 }»
k 16 [o/o| Résumé des tables. Tables générales méthodiques à
: abrégées. Index alphabétique général abrégé .| 44 I » ‘
| 17 [o] (rénéralités. Bibliographie. Bibliothéconomie. So- 4
l ciétés savantes, etc. : 1.1. ARR ONE A EEE 215 1
| 19 [x] Philosophie. Questions morales ain is I » :ÿ
| 21 ï [2] Sciences religieuses . È : 68 2 4»
! 18 [31 Sciences sociales. Statistique. Economie politique. '
l Enseignement. Assistance. Folklore. : . . | 68 220»
| 8 [34] DÉOLE SE PTS NS A A A NE EN re ARS ER ER
EUX I4 [35] ACTDTIS CA CTO NE UE RNA PME ER ee En GS I »
Fe 23 [3554623] Séiences militaires + ee Rs Ut res DARD)
le 22 [48] Philolopieet litterature esters 2 10)
b 24 [517452] Mathématiques et Astronomie . 18 HE)
ï 3 [53] Sciences physiques HÉAaRS rationnelle et Phy-
Ë sique) . :. 46 TD
l 32 [54466] Sciences chimiques : Chimie ‘pure. Industries
| chimiques. Métallurgie . . nee 228 4
ÉDE 25 [548-549-4551 | Minéralogie. Cristallographie. Géologie Re 22 I »
| 26 |[56--57458450]| Sciences “biologiques. Paléontologie. Anthropo- ù
logie.. Botanique. Zoologie. . 74 2 D
3x [662] Sciences de l'ingénieur : Mécanique. Electricité à
fa l industrielle. Mines. Ponts et chaussées. Che- «0
RUE mins de fer et tramways. Travaux maritimes et à
| hydrauliques. Technologie sanitaire. Locomo-
TON: ENOÉNÉTA LEE RATS SES Ne RS TES 3. »
Eu : 2 [6xx| ATOME A AA NS M SEE Rene NA (Lee UCI EC GE
ke 12 [612] Physiologie : SCT ME RD PU et ON 08 I 0»
28 . [613-614] Hygiène privée et Hyg giène publique + . .° . |, 22 I »
9 1615] LHRÉTAPDEUTIQUER EME NNRANEANE RE  e Er ee L226 119 :
10 ei Patholorie intérne trier NRA Re Eee Tee)
II Pathologie exlenner ss NAT te | Een) ù
13 Re Gynécologie. Pédiatrie. Médecine comparée SR ER)
3x [62] Voir [662].
6 [620.1] Industries de la Locomotion (Locomotion par
terre et par eau, Aérostation). :.° . Se 8 LD ë
20 [63 Agriculture. Agronomie. Sciences agricoles Rd O VAE) 5
30 [64-65] Sciences appliquées diverses : Economie domes- à
5 tique. Sténographie. Imprimerie et édition. Trans- 3
à DOS Comptabilité de res PA NEre nn RES S T0) 1
ER 32 [66] Voir [54 - 66]. :
on 33 [6746860] Industries diverses. Professions et métiers divers. 1
RS Ale Construction. : SÉSrT SE) à
29 (7] Beaux-Arts : Architecture. Sculpture. Peinture. Æ
Photographie. Gravure. Musique ,. ::. : | 96,2 » 1208
4 F7 Sciences photographiques. . 2480 10) ‘4
7 [zol Sports (Tourisme, Cyclisme, Automobilisme) . ASEO NE | CAT É
22 [81 Voir [4 ES |
19 ] Histoire. Géographie. Biographie. Généalogie .| 100 D
24 |[(.), =, «5»; ;, A-7Z]| Table des subdivisions communes +: . : . | 84 2 1»
34 » Indes alphabétique générale meme Eten" #20 39
NOMBRE TOTAL DES PAGES .! . «| 2240

 

 

 

 

 

 

 

 

 

 

 
