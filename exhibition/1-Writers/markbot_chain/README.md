# Markbot Chain 
Markbot Chain is a social experiment in which the public has a direct influence on the result. The intention is to integrate responses in a text-generation process without applying any filter.

All the questions in the digital files provided by the Mundaneum were automatically extracted. These questions are randomly put to the public via a terminal. By answering them, people contribute to another database. Each entry generates a series of sentences using a Markov chain configuration, an algorithm that is widely used in spam generation. The sentences generated in this way are displayed in the window, and a new question is asked. 


## Installation notes
```
if [ $(tty) == /dev/tty1 ]; then 
  bash /home/pi/Documents/mundaneum/exhibition/markbot_chain/markbot_chain.sh
fi
```

## Author
Florian Van de Weyer

## License
Copyright (C) Florian Van de Weyer, Mons, 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.