# Data Workers Podcast

During our monthly Algolit meetings, we study manuals and experiment with machine learning tools for text processing. And we also share many, many stories. During the exhibition you could download these stories on your phone using a local network.

For outsiders, algorithms only become visible in the media when they achieve an outstanding performance, like Alpha Go, or when they break down in fantastically terrifying ways. Humans working in the field though, create their own culture on and offline. They share the best stories and experiences during live meetings, research conferences and annual competitions like Kaggle. These stories that contextualize the tools and practises can be funny, sad, shocking, interesting.

A lot of them are experiential learning cases. The implementations of algorithms in society generate new conditions of labour, storage, exchange, behaviour, copy and paste. In that sense, the contextual stories capture a momentum in a larger anthropo-machinic story that is being written at full speed and by many voices. The stories are also published in the [publication of the exhibition Data Workers](https://www.algolit.net/index.php/Data_Workers_Publication). 


## Authors

### Voices
David Stampfli, Cristina Cochior, An Mertens, Gijs de Heij, Karin Ulmer, Guillaume Slizewicz, Elodie Mugrefya, Michel Cleempoel, Géraldine Renauld, An Mertens, Donatella Portoghese, Peter Westenberg

### Editing
Javier Lloret

### Recording
David Stampfli

### Texts
Cristina Cochior, An Mertens 


## License
Algolit, Data Workers Podcast, 2019. Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License [http://artlibre.org/licence/lal/en/](http://artlibre.org/licence/lal/en/)

