from flask import Flask, jsonify, request
from classification_utils import db
from classification_utils.settings import CLASSES

baseurl = '/the-annotator/api'

app = Flask(__name__)

@app.route("{}/sentence".format(baseurl), methods=['GET'])
def get_sentence ():
  conn = db.connect()
  row = db.get_sentence_to_annotate(conn)

  sentence_id = row['rowid']
  document = row['document']
  sentence = row['sentence']
  line = row['line']

  return jsonify({
    'after': [row['sentence'] for row in db.get_next_lines(conn, document, line, 2)],
    'sentence': sentence,
    'before': [row['sentence'] for row in db.get_prev_lines(conn, document, line, 2)][::-1],
    'sentence_id': sentence_id,
  })

@app.route("{}/classify".format(baseurl), methods=['POST'])
def classify_sentence ():
  conn = db.connect()
  sentence_id = int(request.form['sentence_id'])
  classification = min(len(CLASSES), max(0, int(request.form['classification'])))
  db.classify_sentence(conn, sentence_id, classification)
  return jsonify("DONE")

@app.route("{}/classes".format(baseurl), methods=['GET'])
def get_classes ():
  return jsonify(CLASSES)

@app.route("{}/mark_dirty".format(baseurl), methods=['POST'])
def mark_dirty ():
  conn = db.connect()
  sentence_id = int(request.form['sentence_id'])
  db.mark_sentence_dirty(conn, sentence_id)
  return jsonify("DONE")

@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response