import { 
  SENTENCE_CLASS_SELECT,
  SENTENCE_LOAD_FAIL,
  SENTENCE_LOAD_SUCCESS
} from '../actions/sentence';

import { extractUpdate, extractProps } from '../utils';

const initialState = {
  sentence_id: null,
  sentence: '',
  before: [],
  after: [],
  selectedClass: null
}

export default function sentence (state = initialState, action) {
  switch (action.type) {
    case SENTENCE_CLASS_SELECT:
      return extractUpdate(state, action, ['selectedClass']);
    case SENTENCE_LOAD_SUCCESS:
      return {
        ...state,
        ...extractProps(action, ['sentence_id', 'sentence', 'before', 'after']),
        selectedClass: initialState.selectedClass
      }
    default:
      return state;
  }
}