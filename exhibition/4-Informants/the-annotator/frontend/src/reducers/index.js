import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import application from './application';
import sentence from './sentence';
import classes from './classes';
// Import reducers

export default function createRootReducer(history: {}) {
  const routerReducer = connectRouter(history);
  // Perhaps remove this last call / fat arrow

  return connectRouter(history)(
    combineReducers({
      router: routerReducer,
      application,
      sentence,
      classes
      /* , other reducers */ 
    })
  );
}
