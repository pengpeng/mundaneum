import {
  CLASSES_LOAD_SUCCESS
} from '../actions/classes';

import { extractUpdate } from '../utils';

const initialState = [];

export default function classes (state = initialState, action) {
  switch (action.type) {
    case CLASSES_LOAD_SUCCESS:
      return [].concat(action.classes);
    default:
      return state;
  }
}