// const API_URL = "http://localhost:5000/the-annotator/api/";
const API_URL = "/the-annotator/api/";

// import texts from './texts.json';

// const API_URL = "/api/";

export const api_url = (tail) => {
  return `${API_URL}${tail}`;
}

const get = (url) => {
  const promise = new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
          response.json()
          .then(data => resolve(data))
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}


const _post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    const postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (Array.isArray(data[key])) {
          data[key].forEach((v) => postData.append(key, v));
        } else {
          postData.append(key, data[key]);
        }
      }
    }

    fetch(api_url(url), {
      method: "POST",
      body: postData
    }).then(resolve).catch(reject);
  });

  return promise;
}

// TODO generalize API call function
const post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    _post(url, data).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

export const sentenceGet = () => get('sentence')

export const sentencePost = (sentence_id, classification) => post('classify', { sentence_id, classification })

export const classesGet = () => get('classes')

export const sentenceMarkDirty = (sentence_id) => post('mark_dirty', { sentence_id })

// export const replaceToken = (token, pos) => post('token/replace', { token, pos })

// export const proposeReplacements = (token, pos) => post('token/replace', { token, pos })

// export const flattenToken = (token, pos) => post('token/flatten', { token, pos })

// export const sourceTextsGet = () => get('source_texts')

// export const sourceTextAdd = (filename, content) => post('source_text/add', { filename, content })

// export const storedTextsGet = () => get('stored_texts')

// export const storedTextAdd = (text) => post('stored_text/add', { text })

// export const storedTextUpdate = (filenum, text) => post('stored_text/update', { filenum, text }) // perhaps filename as url parameter ?


export default { sentenceGet, sentencePost, classesGet }