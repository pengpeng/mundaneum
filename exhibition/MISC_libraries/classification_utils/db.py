import sqlite3
from . import settings
from . import clean_states
from .utils import error

def connect():
  conn = sqlite3.connect(settings.DATABASE)
  conn.row_factory = sqlite3.Row
  return conn

def init_table(conn):
  cur = conn.cursor()
  try:
    cur.execute("""CREATE TABLE IF NOT EXISTS sentences(
      document TEXT,
      line INTEGER,
      sentence TEXT,
      clean BOOLEAN,
      clean_annotation TEXT,
      checked BOOLEAN,
      classification INTEGER,
      detected_language CHARACTER,
      misspelled TEXT
    )""")
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error("Could not create sentences table {}".format(e))
    return False

def add_sentence(conn, document='', line=-1, sentence='', clean=False, clean_annotation='', checked=False, classification=-1, detected_language='fr', misspelled=[]):
  cur = conn.cursor()
  try:
    cur.execute("""INSERT INTO sentences(
      document,
      line,
      sentence,
      clean,
      clean_annotation,
      checked,
      classification,
      detected_language,
      misspelled)
    VALUES(
      :document,
      :line,
      :sentence,
      :clean,
      :clean_annotation,
      :checked,
      :classification,
      :detected_language,
      :misspelled
    )
    """, {
      'document': document,
      'line': line,
      'sentence': sentence,
      'clean': clean,
      'clean_annotation': clean_annotation,
      'checked': checked,
      'classification': classification,
      'detected_language': detected_language,
      'misspelled': ','.join(misspelled)
    })
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error('Could not insert sentence {}'.format(e))
    return False

def add_sentences(conn, sentences=[]):
  cur = conn.cursor()
  try:
    cur.executemany("""INSERT INTO sentences(
      document,
      line,
      sentence,
      clean,
      clean_annotation,
      checked,
      classification,
      detected_language,
      misspelled)
    VALUES(
      :document,
      :line,
      :sentence,
      :clean,
      :clean_annotation,
      :checked,
      :classification,
      :detected_language,
      :misspelled
    )
    """, sentences)
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error('Could not insert sentence {}'.format(e))
    return False

def classify_sentence(conn, rowid, classification):
  cur = conn.cursor()
  try:
    cur.execute(
      "UPDATE sentences SET classification=:classification WHERE rowid=:rowid",
      { 'rowid': rowid, 'classification': classification })
    conn.commit()
  except Exception as e:
    conn.rollback()
    error('Could not update sentence classification {}'.format(e))
    return False

def mark_sentence_dirty(conn, rowid):
  cur = conn.cursor()
  try:
    cur.execute(
      "UPDATE sentences SET clean=:clean, clean_annotation=:clean_annotation WHERE rowid=:rowid",
      { 'rowid': rowid, 'clean': False, 'clean_annotation': clean_states.MARKED_BY_USER })
    conn.commit()
  except Exception as e:
    conn.rollback()
    error('Could not update sentence cleanliness {}'.format(e))
    return False

def get_sentence_to_annotate(conn):
  cur = conn.cursor()
  try:
    cur.execute(
      "SELECT rowid, document, line, sentence FROM sentences WHERE clean = :clean AND classification = :classification ORDER BY RANDOM() LIMIT 1",
      { 'clean': True, 'classification': -1 })
    return cur.fetchone()
  except Exception as e:
    conn.rollback()
    error('Could not select row {}'.format(e))
    return False

def get_poem_sentence(conn):
  cur = conn.cursor()
  try:
    cur.execute(
      "SELECT document, line, sentence FROM sentences WHERE clean = :clean ORDER BY RANDOM() LIMIT 1",
      { 'clean': False })
    return cur.fetchone()
  except Exception as e:
    conn.rollback()
    error('Could not select row {}'.format(e))
    return False

def get_sentence_at_line(conn, line):
  cur = conn.cursor()
  try:
    cur.execute(
      "SELECT sentence, clean_annotation FROM sentences WHERE clean = :clean AND line = :line ORDER BY RANDOM() LIMIT 1",
      { 'clean': False, 'line': line })
    return cur.fetchone()
  except Exception as e:
    conn.rollback()
    error('Could not select row {}'.format(e))
    return False


def get_prev_lines (conn, document, line, limit = 5):
  cur = conn.cursor()
  try:
    cur.execute(
      "SELECT sentence FROM sentences WHERE document = :document AND line < :line ORDER BY line DESC LIMIT :limit",
      { 'document': document, 'line': line, 'limit': limit })
    return cur.fetchall()
  except Exception as e:
    conn.rollback()
    error('Could not select rows {}'.format(e))
    return False

def get_next_lines (conn, document, line, limit = 5):
  cur = conn.cursor()
  try:
    cur.execute(
      "SELECT sentence FROM sentences WHERE document = :document AND line > :line ORDER BY line ASC LIMIT :limit",
      { 'document': document, 'line': line, 'limit': limit })
    return cur.fetchall()
  except Exception as e:
    conn.rollback()
    error('Could not select rows {}'.format(e))
    return False


def get_document(conn):
  doc_cur = conn.cursor()
  try:
    doc_cur.execute("SELECT DISTINCT document FROM sentences ORDER BY RANDOM() LIMIT 1")
    row = doc_cur.fetchone()
    sent_cur = conn.cursor()
    sent_cur.execute(
      """SELECT line, document, sentence, clean, clean_annotation, checked
         FROM sentences WHERE document = :document
         ORDER BY line ASC""", { 'document': row['document'] })
    return sent_cur.fetchall()
  except Exception as e:
    conn.rollback()
    error('Could not select document {}'.format(e))
    return False

def update_document(conn, document, lines):
  cur = conn.cursor()
  try:
    # don't forget checked field
    cur.executemany("""
      UPDATE sentences
      SET sentence = :sentence,
      clean = :clean,
      clean_annotation = :clean_annotation,
      checked = :checked
      WHERE document = :document AND line = :line
    """, [{
      'document': document,
      'line': l['line'],
      'sentence': l['sentence'],
      'clean': l['clean'],
      'clean_annotation': l['clean_annotation'],
      'checked': True
    } for l in lines])
    return True
  except Exception as e:
    conn.rollback()
    error('Could not update document {}'.format(e))
    return False

def update_sentence(conn, documentTitle, line, sentence, clean):
  cur = conn.cursor()
  try:
    cur.execute("""
      UPDATE sentences
      SET sentence = :sentence,
      clean = :clean,
      clean_annotation = :clean_annotation
      checked = :checked
      WHERE document = :document AND line = :line
    """, {
      'document': document,
      'line': line,
      'sentence': sentence,
      'clean': clean,
      'clean_annotation': clean_states.MARKED_BY_USER,
      'checked': True
    })
    conn.commit()
  except Exception as e:
    conn.rollback()
    error('Could not update sentence {}'.format(e))
    return False

# cur = conn.cursor()
# try:
#   cur.execute(
#     "UPDATE sentences SET clean=:clean, clean_annotation=:clean_annotation WHERE rowid=:rowid",
#     { 'rowid': rowid, 'clean': False, 'clean_annotation': clean_states.MARKED_BY_USER })
#   conn.commit()
# except:
#   conn.rollback()
#   return False