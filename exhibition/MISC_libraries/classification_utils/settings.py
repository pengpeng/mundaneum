DATABASE = '/home/gijs/Documents/Bedrijf/Projecten/Algolit/mons/git/exhibition/classification_utils/mundaneum.db'

RAW_DATA_PATH = '/home/gijs/Documents/Bedrijf/Projecten/Algolit/mons/git/data/re-ocred'

DEBUG = False

CLASSES = [
  "0 - Science and Knowledge. Organization. Computer Science. Information Science. Documentation. Librarianship. Institutions. Publications",
  "1 - Philosophy. Psychology",
  "2 - Religion. Theology",
  "3 - Social Sciences",
  "4 - vacant",
  "5 - Mathematics. Natural Sciences",
  "6 - Applied Sciences. Medicine, Technology",
  "7 - The Arts. Entertainment. Sport",
  "8 - Linguistics. Literature",
  "9 - Geography. History"
]
