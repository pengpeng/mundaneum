$(document).ready(function(){

	var startDelay = 5000;
	var explanationsDelay = 5000;
	var splitSentencesDelay = 1000;
	var splitWordsDelay = 20;
	var lowercaseDelay = 50;
	var stopwordsDelay = 50;
	var tfDelay = 200;
	var idfDelay = 200;
	var tfidfDelay = 100;
	
	// var startDelay = 1000; 
	// var explanationsDelay = 1000;
	// var splitSentencesDelay = 100;
	// var splitWordsDelay = 10;
	// var lowercaseDelay = 10;
	// var stopwordsDelay = 10;
	// var tfDelay = 10;
	// var idfDelay = 10;
	// var tfidfDelay = 10;


	if(window.location.hash == "#analyse") {
 		console.log("Analyse en cours");
 		var dots = 0;
	  $('html, body').animate({
	    scrollTop: $(".text-content").offset().top
	  }, 1000);
 		$('form').hide();
 		$('.inprogress').show();
 		var dotsAnimation = setInterval (function(){
 			if(dots < 3) {
        $('.inprogress h3').append('<span class="dots">.</span>');
        $('.inprogress h3').prepend('<span class="dots">.</span>');
        dots++;
    	}
    	else{
    		$('.inprogress h3 .dots').remove();
    		dots = 0;
    	}
 		}, 500);
	

		// I'm spliting the text by sentences
		setTimeout(function(){
			$('.step-title')
			.html("| I'm spliting the text by sentences |")
			.css('border-color', 'yellow')
			.addClass('active')
			;

			$('.sentence').each(function(i){
				$(this)
				.delay(splitSentencesDelay * i)
				.queue(function (next) { 
					$(this).css('background-color', 'yellow'); 
					next(); 
				});
			});

		},startDelay);

		// Calculate how much time it takes to spacing sentences
		var timer = $('.sentence').length * splitSentencesDelay;

		// I'm spliting the text by words
		setTimeout(function(){
			$('.step-title')
			.html("|I|'m| |spliting| |the| |text| |by| |words|")
			.css('border-color', '#00FF00')
			;

			$('.word').each(function(i){
				$(this)
				.delay(splitWordsDelay * i)
				.queue(function (next) { 
					$(this).css('background-color', '#00FF00'); 
					next(); 
				});
			});
		},timer + startDelay);

		// Calculate how much time it takes to spacing sentences
		var timer2 = ($('.word').length * splitWordsDelay) + timer + startDelay;

			// I'm lowercasing the text
		setTimeout(function(){
			$('.step-title')
			.html("i'm lowercasing the text")
			.css('border-color', '#000000')
			;
			$('.regular-word').each(function(i){
				$(this)
				.delay(lowercaseDelay * i)
				.queue(function (next) { 
					$(this).css('opacity', '0'); 
					next(); 
				});
			});
			$('.lower-word').each(function(i){
				$(this)
				.delay((lowercaseDelay + 5) * i)
				.queue(function (next) { 
					$(this).css('opacity', '1'); 
					next(); 
				});
			});
		},timer2);

		var timer3 = ($('.regular-word').length * (lowercaseDelay+10)) + timer2;

		// I'm removing the stopwords
		setTimeout(function(){
			$('.step-title')
			.html("I'm removing &nbsp;&nbsp;&nbsp; stopwords")
			.css('color', '#000')
			;

			$('.lower-word').each(function(i){
				$(this)
				.delay(stopwordsDelay * i)
				.queue(function (next) { 
					// $(this).css('background-color', 'yellow');
					if($(this).hasClass('stopword')){
						$(this).css({
							'opacity':'0' 
						});
					} 
					next(); 
				});
			});
		},timer3);


		var timer4 = ($('.lower-word').length * stopwordsDelay) + timer3;
		var timer4bis = timer4 + 5000;

		// Starting to transform words in numbers
		setTimeout(function(){
			$('.step-title').html("% This is the clean text %");
			$('.sentence').hide();
			$('.tf-transform-wrapper').fadeIn(200).css('display', 'inline-block');
			$('html, body').animate({
		    scrollTop: $(".step-title").offset().top
		  }, 1000);
		},timer4);

		setTimeout(function(){
			$('.step-title').html("% Now I'm going to turn words into numbers %");


		},timer4bis);

		var timer5 = 5000 + timer4bis;
		var timer5bis = 5000 + timer5;

		// Tranforming words into number to calculate the Term Frequency Number
		setTimeout(function(){
			$('.step-title').html("/ I'm calcutating the Term Frequency (TF) for each word / ");
			$('.formula').html('<div class="eq-c">TF = <div class="fraction"><span class="fup">Number of times the word appears in a sentence</span><span class="bar">/</span><span class="fdn">Total number of words in that sentence</span></div></div><div class="calculating"></div>');
		},timer5);
		
		setTimeout(function(){
			$('.tf-transform-word').each(function(i){
				$(this)
				.delay(tfDelay * i)
				.queue(function (next) { 
					$(this).css('display', 'none');
					var formula = $(this).attr('data-formula');
					var word = $(this).html();
					$('.formula .calculating').html('Processing: '+word+' = '+formula)
					next(); 
				});
			});
		},timer5bis);

		setTimeout(function(){
			$('.tf-transform').each(function(i){
				$(this)
				.delay((tfDelay+10) * i)
				.queue(function (next) { 
					$(this).css('display', 'inline-block'); 
					next(); 
				});
			});
		},timer5bis);

		var timer6 = ($('.tf-transform-word').length * tfDelay) + timer5bis;

		// Display the words again
		setTimeout(function(){
			$('.step-title').html("/ I'm calcultaing the Inverse Documents Frequency (IDF) for each word / ");
			$('.formula').html('<div class="eq-c">IDF = <div class="fraction"><span class="fup">Total number of sentences</span><span class="bar">/</span><span class="fdn">Number of times the word appears in a sentence</span></div></div><div class="calculating"></div>');
			$('.tf-transform-wrapper').hide();
			$('.idf-transform-wrapper').fadeIn(400).css('display', 'inline-block');

		},timer6);

		var timer7 = 2000 + timer6;

		// Tranforming words into number to calculate the IDF Number
		setTimeout(function(){
			$('.idf-transform-word').each(function(i){
				$(this)
				.delay(idfDelay * i)
				.queue(function (next) { 
					$(this).css('display', 'none');
					var formula = $(this).attr('data-formula');
					var word = $(this).html();
					$('.formula .calculating').html('Processing: '+word+' = '+formula)
					// $('.formula').html("<p>IDF = Total number of documents / Number of times the word appears in a document </p><p>Processing:"+formula+"</p>")
					next(); 
				});
			});
		},timer7);

		setTimeout(function(){
			$('.idf-transform').each(function(i){
				$(this)
				.delay((idfDelay+10) * i)
				.queue(function (next) { 
					$(this).css('display', 'inline-block'); 
					next(); 
				});
			});
		},timer7);

		var timer8 = ($('.idf-transform-word').length * idfDelay) + timer7 + 2000;

		// Display the words again
		setTimeout(function(){
			$('.step-title').html("* I'm multiplying the TF number by the IDF number to find the most relevant words *");
			$('.formula').html('<div class="eq-c">TF-IDF = TF x IDF </div><div class="calculating"></div>');
			$('.idf-transform-wrapper').hide();
			$('.tfidf-transform-wrapper').fadeIn(400).css('display', 'inline-block');

		},timer8);

		var timer9 = 2000 + timer8;

		// Tranforming words into number to calculate the TF-IDF Number
		setTimeout(function(){
			$('.tfidf-transform-word').each(function(i){
				$(this)
				.delay(tfidfDelay * i)
				.queue(function (next) { 
					$(this).css('display', 'none');
					var formula = $(this).attr('data-formula'); 
					$('.formula .calculating').html('Processing:' +formula)
					next(); 
				});
			});
		},timer9);

		setTimeout(function(){
			$('.tfidf-transform').each(function(i){
				var tfidfNumber = $(this).html();
				$(this)
				.delay((tfidfDelay+10) * i)
				.queue(function (next) { 
					$(this).css({
						'display': 'inline-block', 
					}); 
					$(this).parents('.tfidf-transform-wrapper').css({
						'font-size' : tfidfNumber * 200 + 'px'
					});
					next(); 
				});
			});
		},timer9);

		// display words
		var timer10 = ($('.tfidf-transform-word').length * tfidfDelay) + timer9 + 2000;
		
		// remove the TF-IDF number
		setTimeout(function(){
			$('.tfidf-transform').each(function(i){
				$(this)
				.delay((tfidfDelay) * i)
				.queue(function (next) { 
					$(this).css({
						'display': 'none', 
					}); 
					next(); 
				});
			});
		},timer10);

		var timer11 = 2000 + timer10;

		// display the words 
		setTimeout(function(){
			$('.tfidf-transform-word').each(function(i){
				$(this)
				.delay(tfidfDelay * i)
				.queue(function (next) { 
					$(this).css({
						'display': 'inline-block',
					});
					next(); 
				});
			});
		},timer11);

		var timer12 = 15000 + timer11;

		// go back button
		setTimeout(function(){
			$('.inprogress h3').html('I have finished, <br>but…');
			$('.inprogress h3 .dots').remove();
			clearInterval(dotsAnimation);
			$(".step-title").hide();
			$(".formula").hide();
			$('.again').fadeIn(1000);
			$('.original-text').fadeIn(400);
			$(".result-text h2").fadeIn(400);
			$('.result-text').addClass('move');

		}, timer12);


		$('.again').on('click',function(){
			 window.location = '/';  
		});
	} 
});



function blinker() {
	$('.blinking').fadeOut(500);
	$('.blinking').fadeIn(500);
}