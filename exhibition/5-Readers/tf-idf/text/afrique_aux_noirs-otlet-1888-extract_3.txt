Nous, Européens, qui sommes allés coloniser le sol africain, nous,
surtout Belges, qui avons pris une part directe dans l’œuvre
civilisatrice du Congo, devons-nous, pouvons-nous assister les bras
croisés au grand mouvement de rapatriement qui se dessine au delà de
l’Atlantique?

L’œuvre du Congo est avant tout une œuvre humanitaire et chrétienne.
Ce sont des hommes et des frères qu’il s’agit de relever d’une trop
longue déchéance morale et intellectuelle. C’est aussi toute une
fraction de l’humanité qu’il faut appeler au progrès matériel et au
développement économique.

Cependant, en allant importer d’une pièce sur le territoire africain
notre civilisation si complexe, n’allons-nous pas créer là-bas un
formidable antagonisme entre deux états sociaux trop disparates pour
se fusionner? En mettant directement en contact le blanc raffiné et le
noir encore sauvage, n’allons-nous pas nuire plutôt qu’être utiles au
récent et glorieux avénement du continent noir?

