Ainsi donc les nègres d’Amérique se réveillent. Un Moïse noir s’est
élevé au milieu d’eux, prêchant non la guerre sainte, mais un
patriotique retour vers le sol natal.

Pourquoi ce grand mouvement au delà de l’Océan?

Nous savons ce que sont les noirs d’Amérique: Enlevés au foyer
des ancêtres par la force et la ruse des marchands d’esclaves,
courbés pendant plus de trois siècles sous le joug des colons du
Nouveau-Monde, des millions de représentants de la race noire ont
enfin été appelés à l’émancipation en 1865, après la grande guerre
esclavagiste.

Mais l’émancipation ne leur a pas rendu de patrie. Libres aujourd’hui
sous le gouvernement de ceux qui furent leurs maîtres, ils ne peuvent
jouir pleinement de leur liberté; riches, ils ne disposent à leur gré
de leurs richesses; égaux en droit devant la constitution américaine,
ils ne le seront jamais en fait, devant les orgueilleux Yankees.—Les
emplois du pays, où ils se comptent par millions, ne sont jamais pour
eux; les rangs de la société ne s’ouvrent pas pour les recevoir:
ils ne rencontrent partout que dédain, répugnance et froissements
d’amour-propre.

Cette situation nous a été révélée vingt fois par des voyageurs et des
journalistes impartiaux.