Si toutes les solutions d’une même question étaient méthodiquement rangées en un tableau semblable à celui de Mendelieff, l’on
apercevrait aisément celles non tentées ; et il apparaît déjà actuellement queles étapes successives de tout problème pourraient être représentées géométriquement par une série de points qu’une courbe,
mathématiquement déterminable, doit réunir entre eux; les lacunes
comme aussi les prolongations de cette courbe, suivant sa formule,
indiqueraient les solutions à essayer. L'idée de J. Delbœuf déclarant
que l'étude de toute opinion opposée à l'opinion reçue pourrait conduire à quelque opinion nouvelle, revient à dire qu'il suffit de prolonger la courbe représentative des opinions admises en-dessous de
l'axe des abcisses pour trouver des opinions originales.

« Toute synthèse nouvelle, a dit E. Le Roy (12), sort d’une
analyse critique préliminaire : une phase de démolition la précède
et la prépare. »

Si je trouve dans les fiches relatives au mode de soutien d'une
pièce animée d’un mouvement de rotation rapide, que dans une
machine on a réalisé des progrès réels en suspendant à un arbre
flexible une pièce qui primitivement était supportée à l'extrémité
supérieure d’un arbre rigide, ne devrais-je pas dans tous les cas
analogues étudier si cette transformation n’est pas utile. Si je trouve
dans ces fiches que dans une machine une fonction a été successivement réalisée par un certain nombre de combinaisons mécaniques,
décrites dans d'autres fiches permettant d'obtenir le résultat auquel
il est destiné, ne devrais-je pas essayer dans cette même machine les
autres combinaisons possibles. Deux systèmes viennent récemment
d'être essayés pour la transmission des images à grande distance.
Tous les deux utilisent pour la reproduction des images un courant
électrique dont l'intensité varie proportionnellement à la luminosité
de la partie à transmettre. Dans l’un d'eux cette variation est produite par les différences de conductibilité électrique de l'ancre réalisant l'image sous l'influence de différence d'épaisseur ; dans l'autre
elle est produite par les différences de conductibilité d’une plaque de
sélénium sous des différences d’éclairement. Si tous les moyens
possibles de réaliser des variations d'intensité électrique dans les
conditions du problème étaient connus, ne serait-il pas facile d'imaginer aussitôt d’autres solutions ?