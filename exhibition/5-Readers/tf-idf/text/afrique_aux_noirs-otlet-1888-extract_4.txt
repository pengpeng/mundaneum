Aujourd’hui, s’il faut en croire les dernières nouvelles, les
circonstances viendraient faciliter au plus haut point la réalisation
d’un tel programme.

Il existe dans les États de l’Amérique des millions de nègres déjà
christianisés, habitués au travail régulier et faits à toutes les
exigences d’une civilisation avancée. Ces nègres pour la plupart sont
instruits; beaucoup sont dans une enviable aisance ou même disposent
de forts capitaux dus à leur travail intelligent. Ils ont vécu au sein
d’une nation leur donnant tous les jours les plus rares leçons de la
liberté politique et de l’industrialisme moderne.

Et voilà qu’au lendemain de leur émancipation, ces nègres veulent
achever d’obtenir dans les rangs de l’humanité la place qui leur
revient de droit. Libres par le fait d’autrui, d’eux-mêmes ils
aspirent maintenant à se fixer dans un territoire qui soit à eux et
ils redemandent leur ancienne patrie.

A nous de favoriser ces légitimes aspirations.

Que le vaste État indépendant du Congo ouvre ses portes à ces
citoyens américains qui sont ses enfants: ils constituent le
meilleur élément moyen de fusion entre la barbarie africaine et la
civilisation européenne; qu’il leur demande d’aller achever, sous
la tutelle du souverain que l’Europe a choisi, l’œuvre considérable
de l’organisation politique, sociale et matérielle de ces immenses
contrées.