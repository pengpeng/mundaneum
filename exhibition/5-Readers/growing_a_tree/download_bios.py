from trees import trees
from growing_a_tree_sketch import bio
import wikipedia
import re
import os.path
import time

for gardener in trees.keys():
  print('Downloading bio for {}'.format(gardener))
  cut = " in "
  if cut in gardener:
    index = re.search(cut, gardener).start()
    gardener = gardener[:index]

  try:
    bio = wikipedia.page(gardener).summary
    filename = re.sub(r'\W', '', gardener)
    
    with open(os.path.join('bios', filename), 'w') as h:
      h.write(bio)
  except:
    pass

  time.sleep(.5)