import time
import random
import os
import progressbar

from colorama import init

# prevent sigint?
import signal
def handler(_, __):
    pass

signal.signal(signal.SIGINT, handler)


init()

#############################################################################################################################################################
     
#couleur

bleu = '\33[0;44;20m'
clignotte = '\33[0;5;20m'
rouge =  '\33[0;31;20m'
blancF = ' \33[0;37;20m'
grisitalique = '\33[3;20;20m'
arc = '\33[0;41;20m'  
italique = '\33[3;20;20m'
violet = '\33[0;34;20m'
turquoise = '\33[0;36;20m'
     
     
#############################################################################################################################################################
     
#variable


mots = [ ['belge', 'livre','otlet','image','monde'], [ 'oeuvre','savoir','humain', 'guerre', 'acquis', 'publie' ], [ 'juriste','travaux','passion', 'vigueur', 'support' ], [ 'utopiste','sommaire','objectif', 'archives', 'comprend', 'ensemble', 'standard' ], [ 'pacifiste','universel','mundaneum', 'bruxelles', 'faciliter', 'intuition' ], [ 'socialiste','discipline','sociologue','catalogues','sentiments' ], [ 'information','disposition','construction','rassemblant','microfiches' ], [ 'bibliographe','connaissance','scientifique','instigateur', 'gouvernement' ], [ 'documentation','philosophique','planification', 'prolongements', 'perfectionner', 'collaboration' ], [ 'mondialisation','classification','internationale','multiplication','collaborateurs' ] ] #,'litterature','otlet','markov')


mot = ''
niveau = 0
letter_differentes = []
letter_guess = []
lettre_mauvaise = []
store_letter = ''


def remise_zero ():
    
    print ("remise à zéro" + blancF)
    time.sleep (2)
    for i in progressbar.progressbar(range(100)):
        time.sleep(0.05)
    
    

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

#sort
   #effacement
def pendu_affichage_mot( m, ls ):
    
    out = ''
    
    for i in range(0,len(m)):
        
        lm = m[i]

        if i == 0 or i == len(m) - 1:
            out += lm
            continue

        found = False
        for ll in ls:
            if ll == lm:
                found = True
                out += ll
                break
        if not found:
            out += '_'

    print( out )

   #affichage du mot
def lettre_retenue( m ,ls): 
   
    nbrlettre = 0
    
    for i in range(0,len(m)):
        
        lm = m[i]
        for ll in ls:
            if ll == lm:
                nbrlettre += 1
    
    if nbrlettre == len(m):
       
        return True
        
    else: 
        return False
     

    
   #retiens la lettre
def ecran_demarrage():
    
    global niveau
    
    clear ()
    if niveau == 0:
        print( grisitalique + '''   
    Bienvenue sur le jeu du pendu ! 
          '''  + blancF + '''
    Les explications du jeu: 

            - Vous allez recevoir la première et la dernière lettre du mot.
            - Chaque lettre se trouve plusieurs fois 
              dans le mot sera indiquée.
            - Vous devez inscrire une lettre et appuyez sur enter.
            - Si vous avez deviné le mot, vous pouvez l'indiquez 
              en entier et appuyez sur enter.
            - Vous avez plusieurs tentatives pour deviner le mot.
            - Pour chaque bon mot trouvé, le niveau augmente. '''
                                                                    + grisitalique + '''
            - Le thème est sur Paul Otlet.

                                    '''   
 )
        time.sleep(1)
        print ("""
        

        
        """)
        time.sleep(10)


        
        for i in range(0, 9) :
            clear()
            if i % 2 == 0:
                print ('''


                    d8888b.  .d88b.  d8b   db d8b   db d88888b 
                    88  `8D .8P  Y8. 888o  88 888o  88 88'     
                    88oooY' 88    88 88V8o 88 88V8o 88 88ooooo 
                    88~~~b. 88    88 88 V8o88 88 V8o88 88~~~~~ 
                    88   8D `8b  d8' 88  V888 88  V888 88.     
                    Y8888P'  `Y88P'  VP   V8P VP   V8P Y88888P 


                   .o88b. db   db  .d8b.  d8b   db  .o88b. d88888b     db 
                  d8P  Y8 88   88 d8' `8b 888o  88 d8P  Y8 88'         88 
                  8P      88ooo88 88ooo88 88V8o 88 8P      88ooooo     YP 
                  8b      88~~~88 88~~~88 88 V8o88 8b      88~~~~~      
                  Y8b  d8 88   88 88   88 88  V888 Y8b  d8 88.         db 
                   `Y88P' YP   YP YP   YP VP   V8P  `Y88P' Y88888P     YP  
  
''' + blancF )
            else:
                print('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')      #à la ligne curseur   
            time.sleep(0.5)  #temps de clignottement
    
        time.sleep(1)
        print( "Appuyez sur enter pour démarrer" )
        i = input()
        clear()
        bienvenue_niveau()
        ###
    
    else:
        bienvenue_niveau()
    
def bravo():
    print (clignotte + '''

 ____                       
|  _ \                      
| |_) |_ __ __ ___   _____  
|  _ <| '__/ _` \ \ / / _ \ 
| |_) | | | (_| |\ V / (_) |
|____/|_|  \__,_| \_/ \___/ 
                           ''' +blancF)
    
    time.sleep(2)
    
def bienvenue_niveau():
    
    global niveau
    if niveau == 0:
        print( "        Niveau " + str( niveau + 1)        )
        print ("    ")
        print ("    ")
        print (italique + """      
        
                    Le saviez vous: 
                    
                    Paul Otlet est né le 23 août 1868 et 
                    il est mort le 10 décembre 1944 en Belgique        
                    
                    """ + blancF)
        print ("    ")
        print ("    ")
        
    elif niveau == 1:
        print( "Bienvenue au niveau 2!  " )
        print ("    ")
        print ("    ")
        print (violet + """       
        
                    Le saviez vous: 
                    
                    Paul Otlet est connu pour ses travaux 
                    en matière de bibliographie.        
                    
                    """)
        print ("    ")
        print ("    ")
    elif niveau == 2:
        print( "Niveau 3" )
        print ("    ")
        print ("    ")
        print (turquoise +
                '''   
                    Le saviez vous:
                    
                    Il crée, avec Henri La Fontaine, 
                    en 1895 l’Office international de bibliographie et met en place 
                    un « répertoire bibliographique universel », rassemblant 
                    tous les ouvrages publiés dans le monde, 
                    quels que soient le sujet et l'époque. 
                    
                    Cet Office vise également à faire reconnaître 
                    l'information comme discipline scientifique.     
                    
                ''' )
        print ("    ")
        print ("    ")
    elif niveau == 3:
        print( bleu + "Bienvenue au niveau 4" )
        print ("    ")
        print ("    ")
        print ('''     
                    Le saviez vous:
                    
                    En 1910, il met au point avec son collègue 
                    Robert Goldschmidt « la Bibliophoto », sorte de bibliothèque portable de microfiches.
                    Durant l'entre-deux-guerres, il caresse le projet de construction d'une Cité mondiale,
                    entièrement dédiée à la connaissance, en collaboration avec Le Corbusier.
                    
                    Il envisage plusieurs endroits: Genève, Bruxelles, Anvers. Le projet ne verra jamais le jour.          
                    
                    ''')
        print ("    ")
        print ("    ")
    elif niveau == 4:
        print( "Bienvenue au niveau 5" )
        print ("    ")
        print ("    ")
        print (violet+ '''        
                    
                    Le saviez vous: 
                    
                    Entre 1919 et 1920, 
                    L'ouverture, avec l’assistance du gouvernement belge,
                    du Palais Mondial ou Mundaneum, centre scientifique documentaire,
                    éducatif et social. Celui-ci héberge notamment l’OIB.
                    
                    L’idée naît de créer une Cité mondiale pour laquelle Le Corbusier élabore des plans
                    et des maquettes étonnantes (en particulier, la Tour du Progrès).
                    
                    Mais cette Cité, à laquelle l’architecte Jeanneret et le sculpteur
                    Hendrik Andersen se sont aussi intéressés, ne verra jamais le jour !  
                    
                    ''')
        print ("    ")
        print ("    ")
    elif niveau == 5:
        print( "Bienvenue au niveau 6!  " )
        print ("    ")
        print ("    ")
        print (arc + '''
        
                     Le saviez vous: 
                     
                     En 1922, Otlet connaît les premiers revers dans ses actions : 
                     il assiste, lâché par le gouvernement belge, à la fermeture du Mundaneum, 
                     à la dispersion de ses collections.
                     Mais il continue quand même à travailler.          
                     
                     ''')
        print ("    ")
        print ("    ")
    elif niveau == 6:
        print( "Bienvenue au niveau 7" )
        print ("    ")
        print ("    ")
        print (turquoise + '''
        
                      Le saviez vous: 
                      
                      Otlet prolonge les rêves grandioses des encyclopédistes des XVIIIe et XIXe siècles.
                      Face à l’abondance des documents, le besoin s’impose de les résumer et de les coordonner
                      en une Encyclopédie universelle.
                      
                      Une telle encyclopédie, monument élevé à la pensée humaine et graphique de toutes les sciences
                      et de tous les arts est le résultat du travail et de l’échange
                      de tous les penseurs de tous les temps et de tous les pays.
                      
                      Elle est la somme totale de l’effort intellectuel des travailleurs intellectuels du monde entier.
                      En ce sens, certains voient en Otlet l’ancêtre de l’encyclopédie Wikipédia
                      ou de mouvements de travail collaboratif tels, qu’aujourd’hui,
                      les folksonomies, web communautaire ou web social...         
                      
                      ''')
        print ("    ")
        print ("    ")
    elif niveau == 7:
        print( grisitalique + "Bienvenue au niveau 8" )
        print ("    ")
        print ("    ")
        print ('''        
        
                      Le saviez vous:
                      
                      L’internet a été inventé en 1895. 
                      
                      Otlet a eu l’intuition du réseau Internet.
                      Il décrit un « Réseau universel d’information et de documentation,
                      capable de mettre en relation tous les organismes particuliers de documentation.
                      
                      Sous nos yeux, écrit-il, est en voie de se constituer une immense machinerie pour le travail intellectuel.
                      
                      Elle se constitue par la combinaison des différentes machines existantes,
                      dont les liaisons nécessaires s’entrevoient.
                      
                      Cette machinerie constituerait un véritable cerveau mécanique et collectif… ».
                      « On prétend que l’internet serait né en 1969… Ne trouvez-vous pas cette hypothèse,
                      deux pacifistes ont inventé le web avant les militaires, séduisantes ?
                      
                      Les acteurs de l’internet non marchands, convaincus que la mise en réseau d’informations
                      peut permettre de faire avancer le monde réel,
                      se réclament aux aussi d’une vison utopique communautaire et pacifiste.
                      Ce sont tous les héritiers du Mundaneum ».         
                      
                      ''')
        print ("    ")
        print ("    ")
    elif niveau == 8:
        print( "Bienvenue au niveau 9" )
        print ("    ")
        print ("    ")
        print ('''        
        
                       Le saviez vous: 
                       
                       En 1931, L’IIB devient l’Institut international de documentation.
                       
                       C’est la deuxième grande date de la documentation où le terme apparaît clairement
                       comme objectif dans l’intitulé d’une organisation internationale.
                       Otlet sort définitivement de la bibliographie pour entrer dans la documentation.          
                       
                       ''')
        print ("    ")
        print ("    ")
    elif niveau == 9:
        print( "Bienvenue au niveau 10" )
        print ("    ")
        print ("    ")
        print (bleu + '''        
        
                        Le saviez vous: 
                        
                        En 1944, Otlet devient aveugle mais continue sa passion.
                        Il mourra dans la même année.         
                        
                        ''')
        print ("    ")
        print ("    ")
        
        
    else:
        print( "Oups ceci est un problème qui ne devrait pas être là... Faut me redémarrer, je pense bien ! / Oups, i think there is a problem... I need to be restarted ! " )
    
def continuons():
    
    global niveau
    
    if niveau == len(mots)-1:
        print( "Fin du jeu !" )
        time.sleep(5)
        return False
    
    print(arc + '''                         Voulez-vous continuer ? Y/N                           ''' + blancF)
    
    i = input()
    if i == 'n':
        #break
        time.sleep(2)
        print( 'Au revoir ! Revenez quand vous voulez ! / See you later ! ' )
        time.sleep(5)
        return False
    else:
        niveau += 1
        time.sleep(1)
        return True
    
def perdu():
    print( rouge + clignotte + '''
      ___           ___           ___           ___     
     /\  \         /\  \         /\__\         /\  \    
    /::\  \       /::\  \       /::|  |       /::\  \   
   /:/\:\  \     /:/\:\  \     /:|:|  |      /:/\:\  \  
  /:/  \:\  \   /::\~\:\  \   /:/|:|__|__   /::\~\:\  \ 
 /:/__/_\:\__\ /:/\:\ \:\__\ /:/ |::::\__\ /:/\:\ \:\__\ 
 \:\  /\ \/__/ \/__\:\/:/  / \/__/~~/:/  / \:\~\:\ \/__/
  \:\ \:\__\        \::/  /        /:/  /   \:\ \:\__\  
   \:\/:/  /        /:/  /        /:/  /     \:\ \/__/  
    \::/  /        /:/  /        /:/  /       \:\__\    
     \/__/         \/__/         \/__/         \/__/    

      ___           ___           ___           ___     
     /\  \         /\__\         /\  \         /\  \    
    /::\  \       /:/  /        /::\  \       /::\  \   
   /:/\:\  \     /:/  /        /:/\:\  \     /:/\:\  \  
  /:/  \:\  \   /:/__/  ___   /::\~\:\  \   /::\~\:\  \ 
 /:/__/ \:\__\  |:|  | /\__\ /:/\:\ \:\__\ /:/\:\ \:\__\ 
 \:\  \ /:/  /  |:|  |/:/  / \:\~\:\ \/__/ \/_|::\/:/  /
  \:\  /:/  /   |:|__/:/  /   \:\ \:\__\      |:|::/  / 
   \:\/:/  /     \::::/__/     \:\ \/__/      |:|\/__/  
    \::/  /       ~~~~          \:\__\        |:|  |    
     \/__/                       \/__/         \|__|    

    ''' + blancF )

def dernier_essai():
    for i in range( 0,10 ):
        if i % 2 == 0:
            print( "ATTENTION, C'EST VOTRE DERNIER ESSAI!" )
        else:
            clear()
        time.sleep( 0.1 )

def mot_devine():
    for i in range( 0,10 ):
        if i % 2 == 0:
            print( "SUPER, VOUS AVEZ TROUVÉ LE MOT!!!" )
        else:
            clear()
        time.sleep( 0.1 )
    time.sleep( 0.5 )
    print( "Le mot à deviner était bien: '" + mot + "'" )
    time.sleep( 1.0 )

limit = 10 #nombre d'essaie
#while True:

def partie ():
    
    global mot
    
    ecran_demarrage()

    mot = random.choice(mots[niveau])
    letter_differentes = []
    for l in mot:
        if not l in letter_differentes:
            letter_differentes.append(l)
    letter_differentes.sort()
    
    limit = 10
    if limit < len( letter_differentes ):
        limit = len( letter_differentes )
    if limit > 20:
        limit = 20
    
    letter_guess = []
    lettre_mauvaise = []
    l = mot[0]
    letter_guess.append(l)
    l = mot[len(mot)-1]
    if not l in letter_guess:
        letter_guess.append(l)
    letter_guess.sort()
    count = 0
    
   # clue = mot[0] + mot[(len(mot)-1):(len(mot))]
    
    while count < limit: #compte le nombre de lettre jouée
        
        clear()
        bienvenue_niveau()
        pendu_affichage_mot(mot, letter_guess)
        
        if count == limit - 1:
            dernier_essai()
        else:
            print( "nombre d'essais restant: " + str( limit - count + 1 ) )
            time.sleep( 0.5 )
        
        #i = input()
        inpu = input("Devinez une lettre ou tapez le mot en entier  " )
        
        if inpu == mot:
            return True
        
        letter = ''
        if len( inpu ) > 0:
            letter = inpu[0]

        if letter != '' and letter in mot:
            if not letter in letter_guess:
                letter_guess.append(letter)
                letter_guess.sort()
            if len( letter_guess ) == len( letter_differentes ):
                return True
            else:
                print('yes !')
                time.sleep(2)
            count += 1
        else:
            lettre_mauvaise.append(letter)
            print('no !')
            print (lettre_mauvaise)
            count += 1
            time.sleep(2)
            
            
        #pendu_affichage_mot(mot, letter_guess)
        
   
    print('\n')
    print('Il est maintenant temps de me donner ta réponse / Now its time to guess. You have guessed',len(letter_guess),'letters correctly.')
    print('Les lettres que tu as trouvé sont /These letters are: ', letter_guess)

    iii = input('Devine le mot / Guess the whole word: ')
    if iii.lower() == mot:
        return True
    else:
        return False 
  

while True:
    if partie():
        time.sleep(2)
        pendu_affichage_mot(mot, letter_guess)
        mot_devine()
        bravo()
        
        time.sleep(4)
        #print(mot)
        if not continuons():
            niveau = 0
            remise_zero()
    else:
        niveau = 0
        time.sleep(0.5)
        perdu()
        
        time.sleep(5)
        remise_zero()
        time.sleep(3)