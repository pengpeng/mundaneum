**An algorythmic interpretation of Bertillon's Portrait parlé.**

Sketch based on p5.js, works with chrome and chromium.
See the editable version at :https://editor.p5js.org/guillaume_slizewicz/sketches/8UQHdKfee

To run locally, it needs a node local server as explained here:
https://github.com/processing/p5.js/wiki/Local-server