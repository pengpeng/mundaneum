export const messages = {
  'document_store': 'Saving corrections..',
  'document_store_fail': 'Could not save corrections!',
  'document_success': 'Saved corrections',
  'document_load': 'Loading a document...',
  'document_load_fail': 'Could not load a document',
  'print': 'Print a cleaning poem',
  'print_active': 'Printing poem',
  'print_fail': 'Could not print the poem',
  'line_mark_clean': 'Mark clean',
  'line_mark_dirty': 'Mark dirty / noise',
  'line_edit': 'Edit sentence',
  'sentence_edit_save': 'save',
  'sentence_edit_cancel': 'cancel',
  'load_document': 'Load a new document',
  'store_document': 'Save corrections',
  'cleaning': 'Cleaning', // Cleaninge name-of-the-document.txt
  'close': 'close'
}

export default messages;