import { extractUpdate, simpleUpdate } from '../utils';
import { DOCUMENT_LOAD_SUCCESS } from '../actions/document';

import {
  LINE_EDIT_START,
  LINE_EDIT_DONE,
  LINE_EDIT_CANCEL,
  LINE_EDIT_SENTENCE,
  LINE_MARK_CLEAN,
  LINE_MARK_DIRTY
} from '../actions/lines'

const initialState = {
  'lines': [],
  'activeLineIdx': null,
  'activeLineText': ''
};

export default function document (state = initialState, action) {
  switch (action.type) {
    case DOCUMENT_LOAD_SUCCESS:
      return extractUpdate(state, action, ['lines']);
      
    case LINE_EDIT_START:
      return simpleUpdate(state, { activeLineIdx: action.lineIdx, activeLineText: state.lines[action.lineIdx].sentence })
    case LINE_EDIT_DONE:
      return simpleUpdate(state, {
        activeLineIdx: null,
        lines: state.lines.map((line, lineIdx) => {
          if (lineIdx == state.activeLineIdx) {
            return {
              ...line,
              sentence: state.activeLineText
            }
          }
          return line
        }), 
        activeLineText: ''
      })
    case LINE_EDIT_CANCEL:
      return simpleUpdate(state, { activeLineIdx: null })
    case LINE_EDIT_SENTENCE:
      return simpleUpdate(state, { activeLineText: action.activeLineText })

    case LINE_MARK_CLEAN:
      return simpleUpdate(state, {
        lines: state.lines.map((line, lineIdx) => {
          if (lineIdx == action.lineIdx) {
            return { ...line, clean: true, clean_annotation: 'MARKED_BY_USER' }
          }

          return line
        })
      })

    case LINE_MARK_DIRTY:
      return {
        ...state,
        lines: state.lines.map((line, lineIdx) => {
          if (lineIdx == action.lineIdx) {
            return { ...line, clean: false, clean_annotation: 'MARKED_BY_USER' }
          }

          return line
        })
      }

    default:
      return state;
  }
}