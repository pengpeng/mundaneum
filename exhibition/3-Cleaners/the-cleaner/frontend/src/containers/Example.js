import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as exampleActions from '../actions/example';
import Example from '../components/Example'
// Import main component
// Import actions


function mapStateToProps ({ example }) {
  // Make a selection of the state
  return  { ...example }
}

// alternative for more complex mapper
// function mapStateToProps (state) {
//   // Make a selection of the state
//   return  { 
//     greeting: state.greeting
//   }
// }


function mapDispatchToProps (dispatch) {
  // Select action creators to be bound with dispatch
  return bindActionCreators(exampleActions, dispatch);
}

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Component);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Example);