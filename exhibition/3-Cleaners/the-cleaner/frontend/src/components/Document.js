import React from 'react';

import { t } from '../utils';

export const LineEditor = ({ activeLineText, lineEditDone, lineEditCancel, lineEditSentence }) => (
  <section class="document--line document--line--editor">
    <section class="document--line--left">
    </section>
    <section class="document--line--middle">
      <p>{ activeLineText }</p>
      <textarea onChange={(e) => lineEditSentence(e.target.value)} value={activeLineText} autoFocus />
    </section>
    <section class="document--line--right">
      <section class="document--line--editor--buttons">
        <button onClick={ lineEditDone }>{ t('sentence_edit_save') }</button>
        <button onClick={ lineEditCancel }>{ t('sentence_edit_cancel') }</button>
      </section>
    </section>
  </section>
)

export const Line = ({ sentence, clean, clean_annotation, lineMarkClean, lineMarkDirty, lineEditStart }) => (
  <section class="document--line" data-clean={ (clean) ? true : false }>
    <section class="document--line--left">
    </section>
    <section class="document--line--middle">
      <p>{ sentence }</p>
    </section>
    <section class="document--line--right">
      <section class="document--line--actions">
        { (clean) ? <button onClick={ lineMarkDirty }>{ t('line_mark_dirty') }</button> : <button onClick={ lineMarkClean }>{ t('line_mark_clean') }</button> }
        <button onClick={ lineEditStart }>{ t('line_edit') }</button>
      </section>
    </section>
  </section>
)

export const Document = ({ 
  document,
  activeLineIdx,
  activeLineText,
  lines,
  hasUnsavedChanges,
  documentLoading,
  documentStoring,
  documentLoad,
  documentStore,
  canPrint,
  lineEditCancel,
  lineEditDone,
  lineEditSentence,
  lineEditStart,
  lineMarkClean,
  lineMarkDirty,
  printPoem
}) => (
  <section class="document">
    <section class="document--actions">
      <button onClick={ documentLoad } disabled={ documentLoading }>{ t('load_document') }</button>
      <button onClick={ documentStore } disabled={ (!hasUnsavedChanges) || documentStoring }>{ t('store_document') }</button>
      <button onClick={ printPoem } disabled={ !canPrint }>{ t('print') }</button>
    </section>
    <section class="document--title">
      <header>{ t('cleaning') }: { document }</header>
    </section>
    { lines.map((line, lineIdx) => {
      if (lineIdx === activeLineIdx) {
        return <LineEditor 
          activeLineText={ activeLineText }
          lineEditDone={ lineEditDone }
          lineEditCancel={ lineEditCancel }
          lineEditSentence={ lineEditSentence }
          />
      } else {
        return <Line
          key={ lineIdx }
          sentence={ line.sentence }
          clean={ line.clean }
          clean_annotation={ line.clean_annotation }
          lineMarkClean={ () => lineMarkClean(lineIdx) }
          lineMarkDirty={ () => lineMarkDirty(lineIdx) }
          lineEditStart={ () => lineEditStart(lineIdx) }
          />
      }
    })}
  </section>
)

export default Document