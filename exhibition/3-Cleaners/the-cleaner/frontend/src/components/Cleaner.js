import React, { Component } from 'react';
import Document from './Document';
// import SentenceActions from './SentenceActions';
// import SentenceClassList from './SentenceClassList';

import { t } from '../utils'

export const Cleaner = class extends Component {
  componentDidMount = () => {
    this.props.documentLoad();
  }
  
  render = () => {
    const {
      document,
      activeLineIdx,
      activeLineText,
      lines,
      message,
      canPrint,
      hasUnsavedChanges,
      documentLoading,
      documentStoring,
      documentLoad,
      documentStore,
      lineEditCancel,
      lineEditDone,
      lineEditSentence,
      lineEditStart,
      lineMarkClean,
      lineMarkDirty,
      messageClose,
      printPoem
    } = this.props;

    return <section id="cleaner">
      { (message) ? <section id="application--message">{ message } <button onClick={ messageClose }>{ t('close') }</button></section> : '' }
      <Document 
        document={ document }
        activeLineIdx={ activeLineIdx }
        activeLineText={ activeLineText }
        lines={ lines }
        canPrint={ canPrint }
        hasUnsavedChanges={ hasUnsavedChanges }
        documentLoading={ documentLoading }
        documentStoring={ documentStoring }
        documentLoad={ documentLoad }
        documentStore={ documentStore }
        printPoem={ printPoem }
        lineEditCancel={ lineEditCancel }
        lineEditDone={ lineEditDone }
        lineEditSentence={ lineEditSentence }
        lineEditStart={ lineEditStart }
        lineMarkClean={ lineMarkClean }
        lineMarkDirty={ lineMarkDirty }
      />
      {/* <Sentence sentence={ sentence } before={ before } after={ after }/> */}
      {/* <SentenceActions
        sentenceLoading={ sentenceLoading }
        sentenceClassSelected={ selectedClass }
        sentenceLoad={ sentenceLoad } 
        sentencePost={ sentencePost } 
        sentenceMarkDirty={ sentenceMarkDirty } />
      <section className="interface--hint">{ t('select_class_explanation') }</section>
      <SentenceClassList
        classesLoading={ classesLoading } 
        sentenceClasses={ sentenceClasses } 
        sentenceClassSelected={ selectedClass } 
        sentenceClassSelect={ sentenceClassSelect } /> */}
    </section>
  }
}

export default Cleaner;