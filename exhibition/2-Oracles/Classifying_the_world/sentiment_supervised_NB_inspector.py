#!/Users/ben/anaconda/bin/python
# -*- coding: utf-8 -*-

#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

# WARNING: This script is a basic version/baseline. It has to be optimized for use with real data. See below.

'''
On Naive Bayes: Scikit Learn has 3 types of Naive Bayes classifiers:
Bernoulli, multinomial and Gaussian. The only difference is about the probability distribution adopted.
The first one is a binary algorithm particularly useful when a feature can be present or not.
Multinomial naive Bayes assumes to have feature vector where each element represents the number of times it appears
(or, very often, its frequency). This technique is very efficient in natural language processing or whenever the samples
are composed starting from a common dictionary. The Gaussian Naive Bayes, instead, is based on a continuous distribution
and it’s suitable for more generic classification tasks.
'''

import nltk
from nltk.corpus import movie_reviews
import random
import re
import numpy as np

# Load functions from machine learning library scikit-learn
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.externals import joblib


# We first define a few helper functions to be used later.
# You can also save these in a separate script and import them.
def seed():
	"""
	Returns the seed to be used in a random function
	"""
	nr = random.uniform(0, 1)
	return nr


def wrb(distribution):
	"""
	Calculate weighted random baseline of a class distribution.
	The variable 'distribution' is a list containing the relative frequency
	(proportion, thus float between 0 and 1) of each class.
	"""
	sum = 0
	if isinstance(distribution, float):
		elem2 = 1 - distribution
		distribution = [distribution, elem2]
	for prop in distribution:
		sum += prop**2
	return sum


def retreive_most_informative_features(vectorizer, clf, n=20):
	feature_names = vectorizer.get_feature_names()
	fns_with_coef = sorted(zip(feature_names, clf.coef_[0]), key=lambda x: x[1])
	return np.vstack((fns_with_coef[:n], fns_with_coef[:-(n + 1):-1]))


# LOAD LABELED DATA

# import movie reviews and their category (pos/neg label), word tokenization is already included
# In each category (we have pos or neg), take all of the file IDs (each review has its own ID),
# then store the word_tokenized version (a list of words) for the file ID,
# followed by the positive or negative label in one big list.
documents = [(list(movie_reviews.words(fileid)), category)
			 for category in movie_reviews.categories()
			 for fileid in movie_reviews.fileids(category)]

# Shuffle using the seed because we're going to be training and testing.
# If we left them in order, chances are we'd train on all of the negatives, some positives,
# and then test only against positives. We don't want that.
random.shuffle(documents, seed)

# CREATE LISTS OF REVIEWS & LABELS
reviews = [' '.join(document[0]) for document in documents]
labels = [document[1] for document in documents]
# print(reviews[0])

# EXTRACT WORD FEATURES USING FREQUENCY COUNT
vectorizer = CountVectorizer()
# this object is the same as the Counter() function finetuned with all options
'''
CountVectorizer(analyzer='word', binary=False, decode_error='strict',
		dtype=<class 'numpy.int64'>, encoding='utf-8', input='content',
		lowercase=True, max_df=1.0, max_features=None, min_df=1,
			ngram_range=(1, 1), preprocessor=None, stop_words=None,
		strip_accents=None, token_pattern='(?u)\\b\\w\\w+\\b',
		tokenizer=None, vocabulary=None)

max_df is used for removing terms that appear too frequently, also known as "corpus-specific stop words". For example:
	max_df = 0.50 means "ignore terms that appear in more than 50% of the documents".
	max_df = 25 means "ignore terms that appear in more than 25 documents".
The default max_df is 1.0, which means "ignore terms that appear in more than 100% of the documents". Thus, the default setting does not ignore any terms.
'''
X = vectorizer.fit_transform(reviews)

# features = vectorizer.get_feature_names()
# row = 10
# dense = X.toarray()
# sums = dense.sum(axis=0)
# print(reviews[row])
# for idx, count in enumerate(X.toarray()[row]):
# 	if count > 0:
# 		print(idx, features[idx], count, sums[idx])


'''
X is now a 'scipy.sparse.csr.csr_matrix', a Compressed Sparse Row matrix
https://en.wikipedia.org/wiki/Sparse_matrix
'''
# human readable print format
# Z = X.tocoo()
# overview = {k:v for k,v in zip(Z.col, Z.data)}
# print(overview)

# look at first word of vocabulary
# print(vectorizer.get_feature_names())
# print(vectorizer.get_feature_names()[0])
# this is how scikit learn prints vector as list
# print(X.toarray())

# Encode the class label
enc = LabelEncoder()
Y = enc.fit_transform(labels)

# print("encoded labels:", Y)
# look back at the labels
get_label = list(enc.inverse_transform(Y))
# print(get_label)
# 0 → Negative
# 1 → Positive
# Is the label encododing alphabetically ordered?
print(Y[0], labels[0])


# ------------------------------------------------------

# ### TRAIN & TEST DATA

# Try importing the trained model
# If this fails, we set a value that forces our script to train the model.

# quit()

def lookup (word, vectorizer):
	return vectorizer.get_feature_names().index(word)

try:
	model = joblib.load('sentiment_movie_reviews.pkl')
	training = False
except:
	training = True

# If the prepared dataset couldn't be loaded, we calculate the vectors here
if training:

	# Do this step once, after this you find the file sentiment_movie_reviews.pkl in the same folder
	# Divide the data in train (80%) and test set (20%)
	X_train, X_test, y_train, y_test = train_test_split(
	    X, Y, test_size=0.20, shuffle=False)
	documents_train = documents[:X_train.shape[0]]
	documents_test = documents[X_train.shape[0]:]

	# CALCULATE BASELINES
	distr = labels.count('pos')/len(labels)
	print('Majority baseline', max(1-distr, distr))
	print('WRB', wrb(distr))

	# TRAIN
	model = MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
	model.fit(X_train, y_train)

	# TEST
	y_pred = model.predict(X_test)

	# EVALUATION METHODS
	print('classification report:\n', classification_report(
	    enc.inverse_transform(y_test), enc.inverse_transform(y_pred)))
	cnf_matrix = confusion_matrix(y_test, y_pred)
	print('confusion matrix:\n', cnf_matrix)

	most_informative = retreive_most_informative_features(vectorizer, model, 50)
	coef_idx = { fn: coef for fn, coef in zip(vectorizer.get_feature_names(), model.coef_[0]) }

	print(model.coef_.shape, model.feature_log_prob_.shape)

	# # Lookup the log probabilities for features per class, sort on difference
	# # print out most 'similar' (neutral?) words
	# coefs = sorted([(fn, neg, pos, abs(neg-pos), coef) for fn, neg, coef, pos in zip(vectorizer.get_feature_names(), model.feature_log_prob_[0], model.feature_log_prob_[1], model.coef_[0])], key=lambda x: x[3], reverse=True)
	# print(coefs[:10])
	# print(coefs[-10:])

	# key_shrek = lookup('shrek', vectorizer)
	# doc_shrek = [[1 if i == key_shrek else 0 for i in range(len(coefs))]]

	# key_nbsp = lookup('nbsp', vectorizer)
	# doc_nbsp = [[1 if i == key_nbsp else 0 for i in range(len(coefs))]]

	# print("shrek", model.predict_proba(doc_shrek), "nbsp", model.predict_proba(doc_nbsp))
	# print("shrek", model.predict(doc_shrek), "nbsp", model.predict(doc_nbsp))
	# quit()

	fns = vectorizer.get_feature_names()

	# Loop through predictions, compare prediction against label, on wrong prediction display
	# the most informative feature...
	for prediction, label, doc, X in zip(y_pred, y_test, documents_test, X_test):
		if prediction != label:

			with open('output.html', 'w') as h:
				h.write('<style>.neg { color: red; } .pos { color: blue }</style>');
				print(doc[0])
				proba = model.predict_proba(X)
				h.write('<p>Proba: neg: {}, pos: {}</p>'.format(proba[0][0], proba[0][1]))
				for token in doc[0]:
					try:
						idx = lookup(token, vectorizer)
						neg = model.feature_log_prob_[0][idx]
						pos = model.feature_log_prob_[1][idx]
						strength = abs(neg-pos)
						label = 'neg' if (neg > pos) else 'pos'
						print(idx, token, strength)

						h.write('<span class="{label}" style="opacity:{strength}">{token}</span> '.format(token=token, label=label, strength=strength))
					except ValueError:
						continue
			h.close()

			print('	Prediction:', prediction, 'Label:', label)
			print(' Prediction probabilites', model.predict_proba(X))
			print()
			quit()
			# all_coef = sorted([(token, coef_idx[token]) for token in doc[0] if token in coef_idx], key=lambda x: x[1])

			# print("First 20")
			# for token, coef in all_coef[:100]:
			# 	print(token, coef)

			# print("Last 20")
			# for token, coef in all_coef[-100:]:
			# 	print(token, coef)
				
	quit()

	# weights = model.coef_ 
	print(model.coef_, type(model.coef_), model.coef_.shape)
	# feature_counts = model.feature_count_
	# print(feature_counts[0])

	quit()

	def show_most_informative_features(vectorizer, clf, n=20):
	    feature_names = vectorizer.get_feature_names()
	    coefs_with_fns = sorted(zip(clf.coef_[0], feature_names))
	    top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
	    for (coef_1, fn_1), (coef_2, fn_2) in top:
	        print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))

	show_most_informative_features(vectorizer, model, n=20)

	# ### SAVE MODEL
	joblib.dump(model, 'sentiment_movie_reviews.pkl') 

# ------------------------


# weights = model.coef_ 
print(model.coef_, type(model.coef_), model.coef_.shape)
# feature_counts = model.feature_count_
# print(feature_counts[0])

n = 100

features = vectorizer.get_feature_names()
counts = X.toarray().sum(axis=0)

for featurename, coef, count in sorted(zip(vectorizer.get_feature_names(), model.coef_[0], counts), key=lambda x: x[1])[:n]:
	print(featurename, coef, count)

quit()

# PREDICT CLASS of NEW SENTENCE
while True:

	# get sentence
	sentence = input("\n\t\tType Your sentence: ")

	# The film was beautiful. I really enjoyed the acting. The sceneray made me dream of going home. I would recommend this film to anyone.

	sentence = [sentence]
	X_new = vectorizer.transform(sentence)
	Y_new = model.predict(X_new)
	prediction = ''.join((enc.inverse_transform(Y_new)))
	if prediction == 'pos': 
		print("Your sentence is:", prediction+'itive.')
	else:
		print("Your sentence is:", prediction+'ative.')

# --------------------------


# To optimize the performance try the following:

# # Test using tf-idf Vectorizer instead of CountVectorizer

# # Checks weights & best performing features

# # Grid search for parameter optimisation

# # Scaling using StandardScaler or MinMaxScaler recomputes features to fall within range of [min,max]

# # Normalization (independently scale each instance to unit length)

# # Binary features

# # PCA

# # 10-fold crossvalidation

# # Test with different classifiers
