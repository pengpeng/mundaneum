#!/Users/ben/anaconda/bin/python
# -*- coding: utf-8 -*-

#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>. 

# WARNING: This script is a basic version/baseline. It has to be optimized for use with real data. See below.

'''
On Naive Bayes: Scikit Learn has 3 types of Naive Bayes classifiers:
Bernoulli, multinomial and Gaussian. The only difference is about the probability distribution adopted. 
The first one is a binary algorithm particularly useful when a feature can be present or not. 
Multinomial naive Bayes assumes to have feature vector where each element represents the number of times it appears 
(or, very often, its frequency). This technique is very efficient in natural language processing or whenever the samples 
are composed starting from a common dictionary. The Gaussian Naive Bayes, instead, is based on a continuous distribution 
and it’s suitable for more generic classification tasks.
'''

import nltk
from nltk.corpus import movie_reviews
import random 
import re
import numpy as np

# Load functions from machine learning library scikit-learn
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.externals import joblib


# We first define a few helper functions to be used later.
# You can also save these in a separate script and import them.
	
def seed():
	"""
	Returns the seed to be used in a random function
	"""
	nr = random.uniform(0, 1)
	return nr


def wrb(distribution):
	"""
	Calculate weighted random baseline of a class distribution.
	The variable 'distribution' is a list containing the relative frequency 
	(proportion, thus float between 0 and 1) of each class.  
	"""
	sum = 0
	if isinstance(distribution,float):
		elem2 = 1 - distribution
		distribution = [distribution,elem2]
	for prop in distribution:
		sum += prop**2
	return sum


### LOAD LABELED DATA

# import movie reviews and their category (pos/neg label), word tokenization is already included
# In each category (we have pos or neg), take all of the file IDs (each review has its own ID), 
# then store the word_tokenized version (a list of words) for the file ID, 
# followed by the positive or negative label in one big list. 
documents = [(list(movie_reviews.words(fileid)), category)
			 for category in movie_reviews.categories()
			 for fileid in movie_reviews.fileids(category)]

# Shuffle using the seed because we're going to be training and testing. 
# If we left them in order, chances are we'd train on all of the negatives, some positives, 
# and then test only against positives. We don't want that.
random.shuffle(documents, seed)

### CREATE LISTS OF REVIEWS & LABELS
reviews = [' '.join(document[0]) for document in documents]
labels = [document[1] for document in documents]
#print(reviews[0])

### EXTRACT WORD FEATURES USING FREQUENCY COUNT
vectorizer = CountVectorizer()
# this object is the same as the Counter() function finetuned with all options
'''
CountVectorizer(analyzer='word', binary=False, decode_error='strict',
		dtype=<class 'numpy.int64'>, encoding='utf-8', input='content',
		lowercase=True, max_df=1.0, max_features=None, min_df=1,
		ngram_range=(1, 1), preprocessor=None, stop_words=None,
		strip_accents=None, token_pattern='(?u)\\b\\w\\w+\\b',
		tokenizer=None, vocabulary=None)

max_df is used for removing terms that appear too frequently, also known as "corpus-specific stop words". For example:
	max_df = 0.50 means "ignore terms that appear in more than 50% of the documents".
	max_df = 25 means "ignore terms that appear in more than 25 documents".
The default max_df is 1.0, which means "ignore terms that appear in more than 100% of the documents". Thus, the default setting does not ignore any terms.
'''
X = vectorizer.fit_transform(reviews)
# print(X.shape) #(2000, 39659)
# print(X.vocabulary_)

features = vectorizer.get_feature_names()
len_colums = 39659
row = 10
text = reviews[row]
new_text = []

vectorizer_data = {}
for idx, count in enumerate(X[row].toarray()[0]):
	if count > 0:
		vectorizer_data[idx] = features[idx], count
print(vectorizer_data)

destination = open("CountVectorizer.txt", "w+")
for nr in range(len_colums):
	if nr in vectorizer_data:
		values = vectorizer_data[nr]
		value = values[0].upper()
		new_text.append(value)
	else:
		new_text.append(str(0))
for element in new_text:
	element = element+' '
	destination.write(element)


'''
X is now a 'scipy.sparse.csr.csr_matrix', a Compressed Sparse Row matrix 
https://en.wikipedia.org/wiki/Sparse_matrix
'''

