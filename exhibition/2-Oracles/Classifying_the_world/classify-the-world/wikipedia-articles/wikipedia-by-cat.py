import wikipediaapi, csv


wiki_wiki = wikipediaapi.Wikipedia('fr')
cat = wiki_wiki.page("Category:France")
print("Category members: Category:France")
csvData = []
for p in cat.categorymembers.values():
  if p.namespace == wikipediaapi.Namespace.CATEGORY:
    # it is category, so you have to make decision
    # if you want to fetch also text from pages that belong
    # to this category
    print(p)
  elif p.namespace == wikipediaapi.Namespace.MAIN:
    # it is page => we can get text
    # print(p)
    print('---------')
    print(p.title)
    print('---------')
    print(p.summary)
    # with open("Informatique"+p.title + ".txt", "w") as text_file:
    #   text_file.write(p.summary)

    csvData.append(['9', p.summary])


with open('wikipedia.csv', 'a') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerows(csvData)

csvFile.close()
    
