Three.js est une bibliothèque JavaScript pour créer des scènes 3D dans un navigateur web. Elle peut être utilisée avec la balise canvas du HTML5 sans avoir besoin d'un plugin. Le code source est hébergé sur le GitHub de son créateur mrDoob.
Son principe est d'être accessible à tout le monde, elle permet des rendus en webGL, CSS3D et SVG.
La bibliothèque contient par exemple les fonctionnalités suivantes :

Animation par squelette
LOD (niveau de détails pour les objets)
Chargement de fichiers au formats .OBJ, .JSON, .FBX
Système de particules (pour par exemple simuler la neige, le feu, etc.)