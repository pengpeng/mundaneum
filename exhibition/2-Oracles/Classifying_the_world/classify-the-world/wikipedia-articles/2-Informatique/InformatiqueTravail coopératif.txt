Un travail coopératif ou plus simplement la coopération est, au sein d'une organisation, la volonté collective et la capacité de faire les choses ensemble, en interagissant dans un but commun, en se partagent les tâches et en tentant d'éviter les conflits. 
Il faut bien distinguer un travail « coopératif » d'un travail « collaboratif » :

Un travail coopératif est formel, dépendant de l'application de procédures, généralement informatisées.
un travail collaboratif se fait en collaboration du début à la fin sans division fixe des tâches. Il est informel, dépendant de la bonne volonté des participants et proche d'une autopoiesis.C'est un mode de travail propre aux équipes virtuelles et aux équipes transverses.