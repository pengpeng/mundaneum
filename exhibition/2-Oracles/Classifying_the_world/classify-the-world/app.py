from flask import Flask, jsonify, request, render_template, send_from_directory

from classifier import initClassifier, annotatedPrediction

baseurl = '/classifying-the-world'

(model, vectorizer) = initClassifier()
tokenizer = vectorizer.build_tokenizer()
vector_labels = vectorizer.get_feature_names()

app = Flask(__name__, static_url_path='')

@app.route(baseurl, methods=['GET', 'POST'])
def post_sentence ():
  if request.method == 'POST':
    text = request.form['text'].lower()

    return jsonify(annotatedPrediction(text, model, vectorizer, tokenizer, vector_labels))
  else:
    return render_template('index.html')

def send_static(path):
    return send_from_directory('static', path)


@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response