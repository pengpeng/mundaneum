def dynamicCoinChange( T, L ):

	Opt = [0 for i in range(0, L+1)]
	
	n = len(T)
	for i in range(1, L+1):
		smallest = float("inf")
		for j in range(0, n):
			if (T[j] <= i):
				smallest = min(smallest, Opt[i - T[j]]) 
		Opt[i] = 1 + smallest 
	return Opt[L]
