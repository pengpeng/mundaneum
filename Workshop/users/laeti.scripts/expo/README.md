# Laeti script for exhibition

laetitia trozzi hangman script for exhibition

## notes

* 02/03/2019 *

- LT: J'ai modifié le code mais je n'arrive pas à indiquer le mot à la fin du jeu qu'on aille trouver le mot ou pas. J'aimerais avoir le résultat, tu verras je l'ai indiqué en commentaire dans le while true, j'ai essayé de le mettre un peu partout dans le code mais ça ne fonctionne pas donc si tu sais y jeter un oeil ! Merci
- FZ: code fixé: voir fonction 'mot_devine()' et son appel à la ligne 542 -> avant d'afficher 'bravo', la fonction mot_devine affiche le mot - le trick était de faire passer la variable 'mot' en global (voir ligne 455) - si tu veux rendre le jeu plus facile, on pourrait afficher les lettres mauvaises avant l'entrée texte, plutôt que après...
