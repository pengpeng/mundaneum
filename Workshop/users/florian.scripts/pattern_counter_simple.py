from pattern.fr import parsetree
#from pattern.en import parsetree
from collections import Counter
import re

counter = Counter()

with open('input/boulanger.txt', 'r') as h:
  raw_text = h.read()
  parsed_text = parsetree(raw_text, lemmata=True)

  for sentence in parsed_text.sentences:
    for word in sentence.words:
      if word.string not in counter:
        counter[word.string] = 0
      else:
        counter[word.string] += 1

print('*********')
print('3 Most Common:')
print(counter.most_common(3), '\n')
print(counter)
