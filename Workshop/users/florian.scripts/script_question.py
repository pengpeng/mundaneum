f = open('sentence.txt', 'r')
lines = f.readlines()

for s in lines:
    s = s.strip()
    if '#' in s:
        continue
    if not s[0].istitle():
        continue
    if not s[-1:] == '?':
        continue
    words = s.split(' ')
    if len(words) < 3:
        continue
    motdelongueurun = 0
    for word in words:
        if len(word) == 1:
            motdelongueurun+=1
    if motdelongueurun >= len(words)/3.0:
        continue
    print('@@@@@@@ ########',s)
