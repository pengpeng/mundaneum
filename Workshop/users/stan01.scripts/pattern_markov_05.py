# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import namedtuple
import random
import re

MarkovModel = namedtuple('MarkovModel', ['keys', 'data', 'history'])

def as_key (str):
  return str.lower()

def make_key (inp):
  return tuple([as_key(w) for w in inp])

def make_markov_model (corpus, history=3):
  model = MarkovModel(keys = [], history=history, data = {})

  for i in range(history, len(corpus)):
    key = make_key(corpus[(i-history):i])
    if not key in model.keys:
      model.keys.append(key)
      model.data[key] = []
    
    model.data[key].append(corpus[i])

  return model

def generate (model, text_length = 50, seed = None):
  if seed is None:
    key = random.choice(model.keys)
  else:
    key = make_key(seed)

  text = list(key)

  while (len(text) < text_length):
    if (key in model.keys):
      text.append(random.choice(model.data[key]))
      new_key = list(key[1:])
      new_key.append(text[-1])
      key = make_key(new_key)
    else:
      return text

  return text

def get_model_statistics (model):
  statistics = {
    'size': len(model.data),
    'alternatives': { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '5+': 0 }
  }

  for key, entries in model.data.items():
    l = len(entries)

    if l == 1:
      statistics['alternatives']['1'] += 1
    elif l == 2:
      statistics['alternatives']['2'] += 1
    elif l == 3:
      statistics['alternatives']['3'] += 1
    elif l == 4:
      statistics['alternatives']['4'] += 1
    elif l == 5:
      statistics['alternatives']['5'] += 1
    else:
      statistics['alternatives']['5+'] += 1

  return statistics

def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text

with open('input/shakespeare/selection.txt', 'r') as h:
  print("Reading text file", flush=True)
  # Read text file
  raw_text = h.read()

  print("Tokenizing", flush=True)
  parsed_text = parsetree(raw_text)
  # tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
  tokens = []

  for sentence in parsed_text.sentences:
    tokens.extend([word.string for word in sentence.words])

  print("\n*****")
  print("Making model (bigram)", flush=True)
  model = make_markov_model(tokens, history = 2)
  print(get_model_statistics(model))
  
  print("Generating text", flush=True)
  for i in range(5):
    print(make_text(generate(model, 100)), '\n')
  
 
  