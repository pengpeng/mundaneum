import re
import pprint
import json
import time

from spellchecker import SpellChecker


spell = SpellChecker( language ='fr')


pp = pprint.PrettyPrinter(indent=2)

#strikethrough
def strike(text):
    result = ''
    for c in text:
        result = result + c + '\u0336'
    return result


def regex_replace (pattern_to_use,reg_to_replace,reg_replacing,text):
	list= re.findall(pattern_to_use,text)
		# print (list)
	for item in list:
		item_2= re.sub(reg_to_replace,reg_replacing,item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, text) 


#open the file
with open("output_third_ocr.txt", "r") as h: 
	raw = h.read()

	# raw= re.sub(r'[\n]', ' ',raw)
	# print(raw)
	# print(raw)
	pattern = r'[0-9]+\,+[0-9]+\n+[a-z]+' #newline
	pattern_2=r'[0-9]+\,+[0-9]+\n+\ +[a-z]' #newline space
	pattern_3=r'[0-9]+\,+[0-9]+\ +\n+[a-z]' #space newline

	pattern_4=r'[0-9]+\,+[0-9]+\n+\ +\(+[a-z]' # newline parenthesis

	pattern_5=r'[a-z]+\.+\ +[0-9]+\,+[0-9]' # two number in same line
	pattern_6=r'[a-z]+\ +[0-9]+\,+[0-9]' 
	pattern_7=r'\w+\ +[0-9]+\,+[0-9]' 
	pattern_8=r'\w+[0-9]+\,+[0-9]' 


	pattern_9=r'[0-9]+\.+[0-9]'
	pattern_10=r'[[a-z]+[\)]+\.+\ +[0-9]+\,+[0-9]'
	pattern_11=r'\.+\ +[0-9]'
	pattern_12=r'\n+\n'


#### The following part is inelegant, Needs rewrite

	list12= re.findall(pattern_12,raw)
	for item in list12:
		item_2= re.sub(r"\n","",item)
		print(item)
		print(item_2)
		raw= re.sub(item, item_2, raw)

	list0= re.findall(pattern,raw)
	# print (list)
	for item in list0:
		item_2= re.sub(r"\n"," ",item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, raw)  

	list2= re.findall(pattern_2,raw)
	# print (list)
	for item in list2:
		item_2= re.sub(r"\n","",item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, raw) 

	list3= re.findall(pattern_3,raw)
	# print (list)
	for item in list3:
		item_2= re.sub(r"\n","",item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, raw) 

	list4= re.findall(pattern_4,raw)
	for item in list4:
		item_2= re.sub(r" ","\n",item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, raw)

	list5= re.findall(pattern_5,raw)
	for item in list5:
		item_2= re.sub(r" ","\n",item)
		# print(item)
		# print(item_2)
		raw= re.sub(item, item_2, raw)

	list6= re.findall(pattern_6,raw)
	for item in list6:
		item_2= re.sub(r" ","\n",item)
		print(item)
		print(item_2)
		raw= re.sub(item, item_2, raw)
	
	list7= re.findall(pattern_7,raw)
	for item in list7:
		item_2= re.sub(r" ","\n",item)
		print(item)
		print(item_2)
		raw= re.sub(item, item_2, raw)

	list8= re.findall(pattern_8,raw)
	for item in list8:
		item_2= re.sub(r" ","\n",item)
		print(item)
		print(item_2)
		raw= re.sub(item, item_2, raw)
		list8= re.findall(pattern_8,raw)

	list9= re.findall(pattern_9,raw)
	for item in list9:
		item_2= re.sub(r"\.",",",item)
		print(item)
		print(item_2)
		raw= re.sub(item, item_2, raw)
	raw = re.sub(r'([0-9]+\,+[0-9])', r'\n\1', raw)





	with open('output_third_ocr+regex.txt', 'w') as out_file:
		out_file.write(raw)
	# print(raw)





