#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>. 

from pattern.fr import parsetree
# https://www.clips.uantwerpen.be/pages/pattern-en#tree

#   Pattern is trained on this dictionary for French tagging
#   http://pauillac.inria.fr/~sagot/index.html#lefff

#   tokenize = True,         # Split punctuation marks from words?
#       tags = True,         # Parse part-of-speech tags? (NN, JJ, ...)
#     chunks = True,         # Parse chunks? (NP, VP, PNP, ...)
#  relations = False,        # Parse chunk relations? (-SBJ, -OBJ, ...)
#    lemmata = False,        # Parse lemmata? (ate => eat)
#   encoding = 'utf-8'       # Input string encoding.
#     tagset = None         # Penn Treebank II (default) or UNIVERSAL.

with open('input/lelivre_extrait.txt', 'r') as h:
  raw_text = h.read()
  parsed_text = parsetree(raw_text)

  for sentence in parsed_text.sentences:
  # parsed_text is in fact a Text object
    #print(sentence)
    ## A sentence object is actually holds more information
    # print(repr(sentence), '\n')

    ## We can easily loop through the individual words
    for word in sentence.words:
    print(word)

