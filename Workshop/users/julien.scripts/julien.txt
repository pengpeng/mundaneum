La loi de Poisson est une distribution de donn�es qui repr�sente le nombre d�occurrences d'un �v�nement dans un espace d�observation d�fini.

Le nombre d�appels quotidiens vers un centre d�appels, par exemple, suit une loi de Poisson.

Etant donn� qu�un �v�nement ne peut pas se produire un nombre de fois n�gatif, ou un nombre de fois non-entier, des donn�es de Poisson doivent �tre des nombres entiers non-n�gatifs sans borne sup�rieure.

Le nombre de d�fauts de peinture sur un capot de voiture constitue des donn�es de Poisson, en raison de l�absence de borne sup�rieure pour le d�compte.

De plus, l�espace d�observation doit �tre d�fini : le nombre de d�fauts de peinture sur un capot de voiture; le nombre de clients pour un jour ouvrable, etc.