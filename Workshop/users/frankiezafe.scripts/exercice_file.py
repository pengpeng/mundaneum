﻿import random

# occurence des caracteres dans le francais - source: http://pedroiy.free.fr/alphabets/index.php?pg=http://pedroiy.free.fr/alphabets/frequence.htm
letters = {
    ' ': 17.4,    
    'e': 17.16,
    's': 8.23,
    'a': 7.68,
    'n': 7.61,
    't': 7.3,
    'i': 7.23,
    'r': 6.81,
    'u': 6.05,
    'l': 5.89,
    'o': 5.34,
    'd': 3.60,
    'c': 3.32,
    'p': 3.24,
    'm': 2.72,
    'q': 1.34,
    'v': 1.27,
    'g': 1.10,
    'f': 1.06,
    'b': 0.80,
    'h': 0.64,
    'x': 0.54,
    'y': 0.21,
    'j': 0.19,
    'z': 0.07,
    'k': 0.001,
    'w': 0.001
}

# recalcul des pourcentages
total_pc = 0
for l in letters:
    total_pc += letters[l]

# repartition des lettres entre 0 et 1
letter_keys = letters.keys()
frequency = {}
for i in range( 0, len(letter_keys) ):
    l = letter_keys[ i ]
    letters[ l ] = letters[ l ] / total_pc
    frequency[ l ] = 0
    if i > 0:
        letters[ l ] += letters[ letter_keys[i-1] ]
    print( l, letters[ l ] )

# fonction 
def grab_random_letter():
    r = random.random()
    for l in letter_keys:
        if letters[ l ] >= r:
            print( r, l )
            return l


tirs = 20000
for i in range( 0, tirs ):
    l = grab_random_letter()
    frequency[l] += 1.0

for l in frequency:
    print( l, frequency[l] / tirs )

words = []

for i in range( 0, 1000 ):
    word = ''
    for j in range( 0, 8 ):
        word += chr( 97 + int( random.random() * ( 122 - 96 ) ) )
    #print( word )
