#!/Users/ben/anaconda/bin/python
# -*- coding: utf-8 -*-

#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>. 

# WARNING: This script is a basic version/baseline. It has to be optimized for use with real data. See below.

'''
On Naive Bayes: Scikit Learn has 3 types of Naive Bayes classifiers:
Bernoulli, multinomial and Gaussian. The only difference is about the probability distribution adopted. 
The first one is a binary algorithm particularly useful when a feature can be present or not. 
Multinomial naive Bayes assumes to have feature vector where each element represents the number of times it appears 
(or, very often, its frequency). This technique is very efficient in natural language processing or whenever the samples 
are composed starting from a common dictionary. The Gaussian Naive Bayes, instead, is based on a continuous distribution 
and it’s suitable for more generic classification tasks.
'''
 
import os
import time
import nltk
from nltk.corpus import movie_reviews
import random 
import re
import numpy as np

# Load functions from machine learning library scikit-learn
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

# classifiers
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC, NuSVC, LinearSVC # other evaluation methos, see doc

# evaluation methods
from sklearn.metrics import classification_report, confusion_matrix, f1_score

# save model
from sklearn.externals import joblib

######## CONFIGURATION ########

training_file = 'sentiment_movie_reviews.pkl'
txt_file = 'L-CONGRES-PROSTITUTION-1910-C.txt'


# We first define a few helper functions to be used later.
# You can also save these in a separate script and import them.
    
def seed():
    """
    Returns the seed to be used in a random function
    """
    return 0.47231099848


def wrb(distribution):
    """
    Calculate weighted random baseline of a class distribution.
    The variable 'distribution' is a list containing the relative frequency 
    (proportion, thus float between 0 and 1) of each class.  
    """
    sum = 0
    if isinstance(distribution,float):
        elem2 = 1 - distribution
        distribution = [distribution,elem2]
    for prop in distribution:
        sum += prop**2
    return sum


### LOAD LABELED DATA

# import movie reviews and their category (pos/neg label), word tokenization is already included
# In each category (we have pos or neg), take all of the file IDs (each review has its own ID), 
# then store the word_tokenized version (a list of words) for the file ID, 
# followed by the positive or negative label in one big list. 
documents = [(list(movie_reviews.words(fileid)), category)
             for category in movie_reviews.categories()
             for fileid in movie_reviews.fileids(category)]
'''
# http://www.nltk.org/book/ch02.html 
# print( movie_reviews.fileids() )
print( movie_reviews.categories() )
for d in documents:
    print( d )
quit()
'''

# Shuffle using the seed because we're going to be training and testing. 
# If we left them in order, chances are we'd train on all of the negatives, some positives, 
# and then test only against positives. We don't want that.
random.shuffle(documents, seed)

### CREATE LISTS OF REVIEWS & LABELS
reviews = [' '.join(document[0]) for document in documents]
labels = [document[1] for document in documents]
#print(reviews[0])

### EXTRACT WORD FEATURES USING FREQUENCY COUNT
vectorizer = CountVectorizer()
# this object is the same as the Counter() function finetuned with all options
'''
CountVectorizer(analyzer='word', binary=False, decode_error='strict',
        dtype=<class 'numpy.int64'>, encoding='utf-8', input='content',
        lowercase=True, max_df=1.0, max_features=None, min_df=1,
        ngram_range=(1, 1), preprocessor=None, stop_words=None,
        strip_accents=None, token_pattern='(?u)\\b\\w\\w+\\b',
        tokenizer=None, vocabulary=None)

max_df is used for removing terms that appear too frequently, also known as "corpus-specific stop words". For example:
    max_df = 0.50 means "ignore terms that appear in more than 50% of the documents".
    max_df = 25 means "ignore terms that appear in more than 25 documents".
The default max_df is 1.0, which means "ignore terms that appear in more than 100% of the documents". Thus, the default setting does not ignore any terms.
'''
X = vectorizer.fit_transform(reviews)
# print(X)
'''
X is now a 'scipy.sparse.csr.csr_matrix', a Compressed Sparse Row matrix 
https://en.wikipedia.org/wiki/Sparse_matrix
'''
# human readable print format
# Z = X.tocoo()
# overview = {k:v for k,v in zip(Z.col, Z.data)}
# print(overview)

# look at first word of vocabulary
#print(vectorizer.get_feature_names())
#print(vectorizer.get_feature_names()[0])
# this is how scikit learn prints vector as list
#print(X.toarray())  

# Encode the class label
enc = LabelEncoder()
Y = enc.fit_transform(labels)
# print("encoded labels:", Y)
# look back at the labels
# get_label = list(enc.inverse_transform(Y))
# print(get_label)

#------------------------------------------------------

# ### TRAIN & TEST DATA

# Try importing the trained model
# If this fails, we set a value that forces our script to train the model.
#training = True

training_file = 'sentiment_movie_reviews.pkl'

if not os.path.isfile( training_file ):

    # If the prepared dataset couldn't be loaded, we calculate the vectors here

    #previous_score = 0
    MNB_alpha = 1
    #if training:
    #while training:

    print( 'MNB_alpha', MNB_alpha )

    ### Do this step once, after this you find the file sentiment_movie_reviews.pkl in the same folder
    # Divide the data in train (80%) and test set (20%)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.20, random_state=0)


    ### CALCULATE BASELINES
    distr = labels.count('pos')/len(labels)
    print('Majority baseline', max(1-distr,distr))
    print('WRB', wrb(distr))

    ### TRAIN
    model = MultinomialNB( alpha = MNB_alpha, class_prior=None, fit_prior=True )
    model.fit(X_train, y_train)
    #model = SVC(gamma='scale')

    ### TEST
    y_pred = model.predict(X_test)

    ### EVALUATION METHODS
    print('classification report:\n', classification_report(enc.inverse_transform(y_test),enc.inverse_transform(y_pred)))

    cnf_matrix = confusion_matrix(y_test, y_pred)
    print('confusion matrix:\n', cnf_matrix)

    # weights = model.coef_ 
    # print(type(weights))
    # feature_counts = model.feature_count_
    # print(feature_counts[0])


    def show_most_informative_features(vectorizer, clf, n=20):
        feature_names = vectorizer.get_feature_names()
        coefs_with_fns = sorted(zip(clf.coef_[0], feature_names))
        top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
        for (coef_1, fn_1), (coef_2, fn_2) in top:
            print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))
    
    weights = show_most_informative_features(vectorizer, model, n=20)
    print(weights)

    print( classification_report )
    
    
    '''
    a = f1_score( y_test, y_pred, average='weighted' )
    print( a )
    if previous_score >= a:
        print( "OK!" )
        training = False
    else:
        MNB_alpha += 0.1
    previous_score = a
    '''

    # ### SAVE MODEL
    joblib.dump( model, training_file ) 

# ------------------------

loaded_model = joblib.load( training_file )

#while True:
f = open( txt_file )
#for l in f:
#    l = l.strip()
#    if len( l ) == 0:
#        continue
sentence = [f]
X_new = vectorizer.transform(sentence)
Y_new = loaded_model.predict(X_new)
prediction = ''.join((enc.inverse_transform(Y_new)))
        #print( '\n\n##### start' )
if prediction == 'pos':
    print( '\033[32m' + f )
else:
    print( '\033[31m' + f )
        #time.sleep(1)
        #print( '\n\n##### end' )
    

### PREDICT CLASS of NEW SENTENCE
#while True:
#
#    # get sentence
#    sentence = input("\n\t\tType Your sentence: ")
#
#    #The film was beautiful. I really enjoyed the acting. The sceneray made me dream of going home. I would recommend this film to anyone.
#
#    sentence = [sentence]
#    X_new = vectorizer.transform(sentence)
#    Y_new = loaded_model.predict(X_new)
#    prediction = ''.join((enc.inverse_transform(Y_new)))
#    if prediction == 'pos': 
#        print("Your sentence is:", prediction+'itive.')
#    else:
#        print("Your sentence is:", prediction+'ative.')

