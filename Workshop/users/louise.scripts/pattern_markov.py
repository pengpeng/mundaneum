# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import namedtuple
import random
import re



CEND      = '\33[0m'
CBOLD     = '\33[1m'
CITALIC   = '\33[3m'
CURL      = '\33[4m'
CBLINK    = '\33[5m'
CBLINK2   = '\33[6m'
CSELECTED = '\33[7m'

CBLACK  = '\33[30m'
CRED    = '\33[31m'
CGREEN  = '\33[32m'
CYELLOW = '\33[33m'
CBLUE   = '\33[34m'
CVIOLET = '\33[35m'
CBEIGE  = '\33[36m'
CWHITE  = '\33[37m'

CBLACKBG  = '\33[40m'
CREDBG    = '\33[41m'
CGREENBG  = '\33[42m'
CYELLOWBG = '\33[43m'
CBLUEBG   = '\33[44m'
CVIOLETBG = '\33[45m'
CBEIGEBG  = '\33[46m'
CWHITEBG  = '\33[47m'

CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CBEIGE2  = '\33[96m'
CWHITE2  = '\33[97m'

CGREYBG    = 'background:'
CREDBG2    = 'bgred '
CGREENBG2  = '\33[102m'
CYELLOWBG2 = '#ffff00'
CBLUEBG2   = '\33[104m'
CVIOLETBG2 = '\33[105m'
CBEIGEBG2  = '\33[106m'
CWHITEBG2  = '\33[107m'

MarkovModel = namedtuple('MarkovModel', ['keys', 'data', 'history'])


def as_key (str):
  return str.lower()

def make_key (inp):
  return tuple([as_key(w) for w in inp])

def make_markov_model (corpus, history=3):
  model = MarkovModel(keys = [], history=history, data = {})

  for i in range(history, len(corpus)):
    key = make_key(corpus[(i-history):i])
    if not key in model.keys:
      model.keys.append(key)
      model.data[key] = []
    
    model.data[key].append(corpus[i])

  return model

def generate (model, text_length = 50, seed = None):
  if seed is None:
    key = random.choice(model.keys)
  else:
    key = make_key(seed)

  text = list(key)

  while (len(text) < text_length):
    if (key in model.keys):
      text.append(random.choice(model.data[key]))
      new_key = list(key[1:])
      new_key.append(text[-1])
      key = make_key(new_key)
    else:
      return text

  return text

def get_model_statistics (model):
  statistics = {
    'size': len(model.data),
    'alternatives': { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '5+': 0 }
  }

  for key, entries in model.data.items():
    l = len(entries)

    if l == 1:
      statistics['alternatives']['1'] += 1
    elif l == 2:
      statistics['alternatives']['2'] += 1
    elif l == 3:
      statistics['alternatives']['3'] += 1
    elif l == 4:
      statistics['alternatives']['4'] += 1
    elif l == 5:
      statistics['alternatives']['5'] += 1
    else:
      statistics['alternatives']['5+'] += 1

  return statistics

def make_text (words):
  text = ''
  first_word = True
  captilize = True

  for word in words:
    if re.match(r'\W', word) is not None:
      text += word

      if re.match(r'[!?\.]+$', word) is not None:
        captilize = True

      if re.match(r'\n+$', word) is not None:
        first_word = True

    else:
      if not first_word:
        text += ' '

      text += word.capitalize() if captilize else word
      captilize = False
      first_word = False

  return text

fichierhtml = open ("testcolor.html",'w')
fichierhtml.write( '<html><head><meta charset="UTF-8">\n' )
fichierhtml.write( '<style>\n' )
#fichierhtml.write( 'table, td { vertical-align: super; padding: 10px 10px 10px 10px; }\n' )
#fichierhtml.write( 'td { width: 50%; }\n' )
fichierhtml.write( '.bgred{background:#ff0000;}\n' )
fichierhtml.write( '.bgyellow{background:#ffff00;}\n' )
fichierhtml.write( '.fonty{font:courrier; color:white; font-size:18px; margin:auto}\n' )
fichierhtml.write( '</style>\n' )
fichierhtml.write( '</head><body>\n' )

with open('L-CONGRES-PROSTITUTION-1910-C.txt', 'r') as h:
  #print("Reading text file", flush=True)
  # Read text file
  raw_text = h.read()

  #print("Tokenizing", flush=True)
  parsed_text = parsetree(raw_text)
  # tokens = [[word.string for word in sentence.words] for sentence in parsed_text.sentences]
  tokens = []

  for sentence in parsed_text.sentences:
    tokens.extend([word.string for word in sentence.words])

  #print("\n*****")
  #print("Making model (bigram)", flush=True)
  model = make_markov_model(tokens, history = 2)
  #print(get_model_statistics(model))
  
  #print("Generating text", flush=True)
  for i in range(2):
    fichierhtml.write('<p style="bgyellow fonty">'+make_text(generate(model, 2))+'</p>')
    print(CYELLOWBG2+make_text(generate(model, 2))+CEND, '\n')
    
  for i in range(3):
    fichierhtml.write('<p style="bgred fonty">'+make_text(generate(model, 2))+'</p>')
    print(CREDBG2+make_text(generate(model, 3))+CEND, '\n')
        
  for i in range(4):
    print(CVIOLETBG2+make_text(generate(model, 4))+CEND, '\n')
        
  for i in range(5):
    print(CBLUEBG2+make_text(generate(model, 5))+CEND, '\n')
        
  for i in range(6):
    print(CBEIGEBG2+make_text(generate(model, 6))+CEND, '\n')
        
  for i in range(7):
    print(CGREENBG2+make_text(generate(model, 7))+CEND, '\n')
        
  for i in range(8):
    print(CYELLOWBG2+make_text(generate(model, 8))+CEND, '\n')
        
  for i in range(9):
    print(CREDBG2+make_text(generate(model, 9))+CEND, '\n')
  
  #print("\n*****")
  #print("Making model (trigram)", flush=True)
  model = make_markov_model(tokens, history = 3)
  #print(get_model_statistics(model))
  
  #print("Generating text", flush=True)
  for i in range(10):
    print(CVIOLETBG2+make_text(generate(model, 10))+CEND, '\n')
    
    
fichierhtml.write( '</body>\n' )
fichierhtml.write( '</html>\n' )
fichierhtml.close()