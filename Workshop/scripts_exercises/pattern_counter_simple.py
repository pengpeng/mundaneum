# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import Counter
import re

# word.sentence              # Sentence parent.
# word.index                 # Sentence index of word.
# word.string                # String (Unicode).
# word.lemma                 # String lemma, e.g. 'sat' => 'sit',
# word.type                  # Part-of-speech tag (NN, JJ, VBD, ...)
# word.chunk                 # Chunk parent, or None.
# word.pnp                   # PNPChunk parent, or None.

def notStopWord (word):
  stopWords = ['the',  'on', '.', 'in', 'a']

  return word not in stopWords

def count (key, counter, test=None):
  if test is None or test(key):
    if key not in counter:
      # Check whether the word is already in our counter
      # If not, add  it, set it to one
      counter[key] = 1
    else:
      # We've already seen the word, increase the count by one
      counter[key] += 1


counter = Counter()
lemmaCounter = Counter()
posCounter = Counter()
# Prepare a dictionary to store our counting

with open('input/cat.txt', 'r') as h:
# Open a file
  raw_text = h.read()
  # Read file, store it in raw_text
  parsed_text = parsetree(raw_text, lemmata=True)
  # Process the text, turn it into a list of sentences,
  # these are in fact lists of words

  for sentence in parsed_text.sentences:
    # Loop through the sentences
    # each sentence also holds a list of words

    for word in sentence.words:
      # Loop through the words in the sentence
      count(word.string, counter, notStopWord)
      count(word.lemma, lemmaCounter)
      count(word.type, posCounter)


print('*********')
print('3 Most Common:')
print(counter, '\n')

print('*********')
print('3 Most Common:')
print(lemmaCounter, '\n')

print('*********')
print('3 Most Common:')
print(posCounter, '\n')