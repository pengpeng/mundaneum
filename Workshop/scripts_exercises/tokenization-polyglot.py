#    Copyright (C) 2018 Constant, Algolit

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>. 

from polyglot.text import Text
import re
import treetaggerwrapper

tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr')

# Open a file handle to the text
with open("lelivre_extrait.txt", 'r') as h:
  text = Text(h.read())

  # for word in text.words:
  # print(text.sentences)

  # Split on sentences and loop through them
  for sentence in text.sentences:
    print(sentence)
    print(sentence.words)

    for row in treetaggerwrapper.make_tags(tagger.tag_text(list(sentence.words), tagonly=True)):
      print(row.lemma)
      print()

    # print(lemmas)

    # # Loop Through the 
    # for word, tag in sentence.pos_tags:
    #   # Use the format to produce a well aligned list
    #   print("{:<18}{:<8}{:<10}".format(word, tag, stemmer.stem(word)))

    # # Print a list of characters
    # print(list(str(sentence)))

    # # Print list of characters, a-z, A-Z, 0-9
    # print([letter for letter in list(str(sentence)) if re.match('\w', letter) is not None])
    
    # # Print list of characters which aren't whitespace
    # print([letter for letter in list(str(sentence)) if re.match('\S', letter) is not None]) 