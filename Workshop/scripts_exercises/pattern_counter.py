# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import Counter
import re

stringCounter = Counter()
lemmaCounter = Counter()
tagCounter = Counter()

def isWord (word):
  return re.match(r'\w', word) is not None

def count (key, counter, test = None):
  if test is None or test(key):
    if key not in counter:
      counter[key] = 1
    else:
      counter[key] += 1

def printCounter (counter, name, most_common=3, print_full=False):
  print('**{:*<18}'.format(name))
  print('{} Most Common:'.format(most_common), '\n')
  for key, count in counter.most_common(most_common):
    print('{:<12}{}'.format(key, count))
  
  if print_full:
    print('\n', counter, '\n\n')

with open('input/frankenstein.txt', 'r') as h:
  raw_text = h.read()
  parsed_text = parsetree(raw_text, lemmata=True)
  
  for sentence in parsed_text.sentences:
    for word in sentence.words:
      count(word.string, stringCounter)
      count(word.lemma, lemmaCounter, isWord)
      count(word.type, tagCounter, isWord)

printCounter(stringCounter, 'Token', 10, False)
printCounter(lemmaCounter, 'Lemma', 10, False)
printCounter(tagCounter, 'Tag', 10, False)