MINISTERE

L'INDUSTR[E ET

DE

ADMINISTRAl'lON CENTRALE iDES MINES.

ï¿½

OU

TRAVAIL

SERVICE

GEOLOGIQUE

LA

CLASSIFICATION DECIMAtE
DE MELVIL DEWEY

APPLIQUEE

AUX SCIENCES

GEOLOGIQUES

POUR

L'ELABORATHON

DE LA

BIBLIOGRAPH,IA GEOLOGICA
PAR

LE SERVICE

GEOLOGIQUE

DE

BELGIQUE

BRUXELLES
HAYEZ,

InIPRllIIEVR

DE

L'ACADElINE
1898

ROYALE

IlE

BELGIQUE

LA

CLASSIFICATION DEOIMALE
;

,

DE MEL VIL DEWEY
APPLIQUEE

AUX

SCIENCES

GEOLOGiQUES POUlt'L'ELABORATlON

BIBLIOGRAPHIA GEOLOGICA

Dll LA

ï¿½

PAil

LE

SERVICE

GEOLOGIQUE

DE

BELGIQUE

Nons croyons inutile de
rappeler ici les motifs pour lesÂ­
quels, a la suite de la Conference bibliographique internaÂ­
tionale tenue a Bruxelles en 189;), la classification
decimale
de Melvil Dewey a ete definitivement
I'Office

international de
La tache que

adoptee

Bibliographie auquel
nous

avons

'

par

nous sommes

affilies.

assumes et

qui consiste a dresÂ­
ser, d'apres cette classification, Ie Repertoire des travaux
concernant les sciences
geologiques est, certes, considerable,
mais nous avons l'espoir
qu'on nons saura gre de l'avoir
et
nous
trouverons
bien tOt dans chaque pays,
entreprise,
que
voire rneme peut-etre dans
chaque region, les concours
necessaires pour l'accomplir de la maniere Ia plus
complete.
possible.

TO{ltes les pu hlications dont nONS avons indexe les fiches,
jtlsUJu'a present, avec Ia collaboration de M. G. Simoens,
doeteur en' sciences minerales, attache plus specialement a

.1
,

I

( 4)
la section

classees
'10

geologique,

ont

ete

:

D'apres

l'aide

du Service

bihliographique

publication, a
Dewey qui lui
la presente
de
l'objet

la matiere fondamentale de la

du nombre

decimal

de la classification

correspond, dans les tableaux faisant
Note et qui constitue I 'index principal;
20 D'apres la forme au moyen de l'index (,oTmel, lequel est
toujours predede d'un 0 et suit Ie nomhre classificateur ordiÂ­
naire;
5" Et

enfin, s'il

y a

lieu, d'apres les rapports qu'elles preÂ­

sentent avec

les autres sciences,

dt!teTminants

places

entre

en

recourant a des nombres

parentheses.

I

.1
Gossclet

00 [63] 023.41

(J.).

1896. Sur- les

Lille, 1896,

t.

cartes

agronomiques

IA

nn.

de fa Soc.

geal.

du

XXIV, pp. 19-25.

o
i);;

l63]
Oï¿½3.41

=

=

=

geologie, represente ]'inde.'(; principal.
agriculture, represents le determinant.
cartes et

notices, representent l"indice [ormel.

Nord).

(

'0

)

Donnons, pour bien fixer les idees SUI' ces trois modes de
classernent, un exemple 11 I'aide de la fiche ci-contre du
modele

adopte

Comme Ie

pour la

B'ioliographia

montre cet

univeTsalis

exemple, qui

:

reunit les trois modes

de classement, il est facile de disposer les fiches suivant
fun ou I'autre des dits modes de classement, et cela 11

l'aide des nombres de la classification decimale de Melvil
Dewey, que no us avons parfois completes, mais en les modiÂ­
ilant aussi peu que possible.
Ces nombres, il est a peine besoin de Ie

rappeler,

eussent

d'imÂ­
pu etre tout 11 fait conventionnels et rendre encore
menses services, mais ce sera un titre de gloire pour l'emiÂ­
nent

bibliographe

americain de leur avoir donne

dination aussi rationnelle et aussi

l'etat actuel de

nos

scientifique

nne coorÂ­

que Ie

permet

connaissances.

MICHEL

MOhlRLON,

Directeur du Service

geologique
Belgique.
'

de

5"9.

549.1

MINERALOGIE.

Determination et earacteres

-

des

M9.H

Caraeteres

physiques
cristallographiques
Systemes cristallins

11 1.

.

1.1.1.1

.

111..3

â€¢

et accroissernent

111.6

Pseudomorphisme
1.1.1.7 Mesure des angles
11.1.8 Cristallograpnie physique
H1.9
optique

.

.

.

H1.ii Formation

.

â€¢

Clivages, isomorphisme, polymorphisme
Irregularites, inclusions

H1.4

.

â€¢

.

11.1.2 l\Iacles

.

.

mineraux,

des cristaux

.

â€¢

â€¢

.

.

.

.

.

Caracteres d'elasticite et de cohesion

11.2

.

1.1.3

.

114

.

.

.

.

â€¢

.

.

.

.

â€¢

.

.

Hii
116
117

U8

119
12,

de densite
r

de durete

.

thermiques
optiques
magnetiques
electriques
de fluorescence, couleur,
chimiques
.

.

..

.

.

-121.

de la voie seche

122

de 13. voie humide

13

organoleptiques

i3f

du toucher

132

du

.

happement.

.133

de lasaveur

134

de l'odeur.

.

.

.

.

.

.

.

eclat et autres caracteres

.

( 8 )

549.2.

Metaux natifs et miner-als

-

metalltques.

:i49.2l
.

.

.

J)ietaux

et mineraux

211 Metaux et minerals
212 1I1ineraux

.22
2;J

.

.

245
246

.

.

.

.

geologie appliquee
Beryllium
Magnesium
.

.

247 Zinc

.

251 Plomb

252 Thallium
2b6 Cuivre
257

Argent

.

.

.

.

.

.

.

.

2:58 Mercure

.

261 Yttrium

262 Cerium

.

.

.

263 Lanthane.

-

.264
.

.

Didymium

265 Erbiurri

.

.

.

.

.

.

.

266 Alluminium

.267 Indium
.

.

268 Gallium

.

269 Scandium
271

gites mineraux

Carrieres et autres industries

248 Cadmium

.

et

.

Manganese

272 Fer

.

.

273 Cobalt

et

metalliferes, industrie

.

tion.

.

.

.

'Mines, depots, filons
miniere

.

associes

.

.

274 Nickel.

.276 Chrome.

,277 Molybdene.
:278. Tungstene,

minerales, materiaux
travaux d'art, etc

aux

.

de construc-

( 9 )
549.279 Urane
281 Etain
.

.

.

282 Titane

.

.

283 Zirconium

.

284 Thorium

.

28!J Vanadium

.

286 Antimoine

.

287 Bismuth

.

288 Tantale

.

291 Or

.

294 Osmium

.

.

.

293 Iridium

.

.

.

.

292 Platine

.

.

.

289 Nobium

.

.

.

.

.

296 Rethenium

.

297 Rodhium

.

.

.

298 Palladium.

.

TABLEAU
SE RAPPORTANT A rOUTES LES SUBDIVISIONS UE
A PARTIR DU TERME 549.241>.

549.2

3

et minerais de
et du
metal.
Association des minerais de
entre eux
Association des minerais de
Combinaisons du
et des mineralisateurs

31

Genre soufre

-

-

.2 --l

.2
2

.

2

.

.

2
2

.

2

.

-

-

-

-

-

-

2

-

.2

-

2

-

.

.

-

2

32

-

-

-

.

-

.

,

.

Genre selenium

33

Ge n re tell me.

.

34

Genres arsenic et antimoine

4

Combinaisons du

41

Chlorures

42

Bromures.

-

.

et des corps

halogenes

.

.

1.

549.2

I

1
I

( to )
549.2

-

2

.

-

2

.

2

-

2

-

2

-

.

.

.

-

2

.

-

2

.

-

2

.

-

2
2

.

-

-

2

.

5

Oxydes

6

Silicates de

7

Autres

71

Tantalates et Niobates de

72

Phosphates

73

Borates de

-

2

.

2

.

â€¢

.

2

-

2

.

-

-

2

.

.

.

-

-

-

2

-

2

-

2

.

2

.

-

.

-

.

oxysels

-

.

de

-

.

-

.

-

.

-

.

Tellurates de

78

Carbonates de
Autres

-

.

-

.

-

.

-

.

de

oxysels

791 Nitrates de

-

2
2

â€¢

.

de

17

2 _. 79

.

.

Molybdates de
75 Sulfates anhydres de
76 Sulfates hydrates de

.

.

Iodures

Fluorures

74

-

2

.

43
44

-

.

-

.

792 Aluminates de __:_

.

793 Arseniates et antimoniates de

794 Vanadiates de
795 Chromates de
796 Tungstates de
797 Titanates de

.

.

-

.

-

â€¢

-

.

798 Niobates de
799 Autres oxysels de
-

.

-

-

-

.

8

Associations

\)

Autres combinaisons du

549.3.

-

des minerais de

et des corps

organiques

.

Mineralisateurs et miner-al isaÂ­

549.3f Genre soufre
32
selenium
33
tellure
.

.

.

.

.

34Â·

-

-.

teurs entre

.

-

-

arsenicet antimoine.

eux,

( 11 )

549.4

Chlorures

,')49.41

42 Bromures

.

43 Iodures

.

Sels Haloldes.

_-

.

.

.

44 Fluorures.

.

549.5

Oxydes

-

549.6

-

non

Silicates.

549.61 Silicates
.

.

62

63

anhydres d'alumine (silicates
Famill(de la silice
des feldspaths

.

.

.

.

.

hydrates d'alumine
66 Famille des gemmes
67 Silicates basiques

.

.

(silicates

de

rnetamorphisme)

.

.

.

68 Zeolithes
69 Silicates de
.

metamorphisme

et

72.Phosphates
73 Bora tes

.

.

.

65 Silicates

.)49.71 Tantalates

.

metamorphisms]

.

des micas

549.7.

.

de

.

64

.

metafliques.

.

7.4 Molybdates
75 Sulfates
76

niobates

.

anhydres
hydrates ..
.

_

exclusivement alumineux.

Autres

-

.

non

.

oxysels

.

( 12 )
,')49.77
78

Tellurates

791 Nitrates

.

.

Carbonates

.

792 Aluminates

.

793 Arseniates

.

794 Vanadates

.

796

.

antimoniates

.

.

Tungstates

797 Titanates

.

.

et

79ï¿½ Chrornates

.

.

.

.

.

.

798 Niobates,

.

Les subdioisions du 64,9.7

se

completent: a

les notations sltivanles

len?' ton?' par

:

31 !tl.etaux alcalins.
32 Potassium.

33 Sodium.
34 Lithium.
3ï¿½ Rubidium.
36 Ccesium.

4 Metaux alcalino-terreux,
41 Calcium.
42 Strontium.
43 Barium.

549.8

1)49.81

Combustibles-mineraux.

-

Diamant.

,

.82

.83
.

.

.

.

.

.

.

.

.

.

Graphite
Anthracite

84

Houille

.

.

85

Lignites

86

Jais

87

Tourbes

88

Hydrocarbures

.

.

.

881

.

gazeux

882 Petroles

.

.

883 Bitumes et schistes bitumineux
884

Asphaltes

89

Cires

et

.

.

resines fossiles et sels

organiques.

( 13 )

GEOLOGIE.

65.

BIBLIOGRAPHIE DES SCIENCES GEOLOGIQUES 016[tibJ.

5&1.

generate
(Morphologie. )

12

Croute terrestre.
Conductibilitc des roches.

13

14

551.2.

551.21
22
.

221
23
24

.

24'1

.

24L1

.

.241.2
242

.

.242.-1

.

.

.

interne.

phenomenes eruptifs.

Tremblements fie
Micros.eismes

terre

.

.

Geysers, solfatares, sources thermales
Tectonique, oscillations, dislocations, plissements,
.

Dislocations resultant de mouvements verticaux
Failles

structure

.

.

.

Flexures.

Dislocations resultant de mouvements horizontaux
Plis

.

.

synclinaux

.

242.12 Plis anticlinaux
242.3

Plis-Failles
Decrochements

211

Phenomenes Â·et

242.2

.

Geodynamique

-

Volcans et

242.11 Plis

.

Globe.

du

Interieur de la terre.
Chaleur interne, et temperature de la surface.

!1M. if

.

du Globe.

Structure

551.1.

.

Physique

-

.

.

.

changements

calorifiques d'origine

aux actions chimiques et
Metamorphisme, schistosite

dus

interne.

Â·t1

J

( 14 )

551.3.

t>5L31

Brosionlglaciaire. glaciers,

32 Â·MoraInes

.

Geodynamique

-

action de la

34

Icebergs

.

erratiques

.

.

31l Erosion aqueuse
36 Changements des

.

glace

.

33 Materiaux de transport, blocs

.

.

externe.

.

.

rivages, phenomenes

.

37 Erosion

.

.

..

38 Actions

atmospheriqna
chimiques superficielles

39 Actions

physiologiques.

littoraux

.

.

551.4.

.

Surface du Globe

-

(Morphologie, suite).
1l51.41

.

.

.

.

45

46
47

49
491

.

.

.

.

Courants marins

.

Lacs, marais, fleuves, rivieres, torrents, cataractes, regime

des

.

Hydrologie
Eaux souterraines, regime
.

Sources

493

Eaux minerales

494

Ports,

495.1 Eaux

aquiferes,

eaux

.

.

canaux, voies

Regime

des nappes

.

des

eaux

navigables (commerce. industrie).
villes, agglomerations, etc

dans les

potables
495.2 Eaux contaminees, egouts,
:.491l.3 Les cimetieres all point de
.

..

.

492

':491l

etc

.

siennes, etc
.

geographiques,

Montagnes, vallees, orologie (etude du relief)
Speleologie, excavations rpuits naturels)
Plaines, deserts
Oceanographis

eaux

.

Divisions

43

.48

.

(Repartition des).

Iles.

44

.

Continents

42

.

.

.

vue

hydrologique.

arte-

( 15 )

551.5.

Meteorologie.

-

(Voir Bibliographia Astron()m!ï¿½ca.)

551.6.

Metamorphisme.
(Voir 551.25.)

551.7.

Primaire,

.72

.

Siluro-cambrien

73

.

pre-cambrien.

Archeen et

:551.7-1

Cambrien

.731

Stratigraphie.

-

.

.

.

Silurien

732

.

74

.

.

.

.Devonien

.

741

inferieur

742

superieur

.

.742.1

.

.

.

.

.

.

.

â€¢

.

.

7511

751.2

Houiller ..

752

Permien

76

Secondaire

761

763

Triasique
J urassique
Cretacique

77

Paleocene

78

Tertiaire

78i

Paleogene

751

.

superieur

Permo-carboniferien
Carboniferien
Cal caire carbonifere

.75
.

et moyen

moyen.

742.2

.

.

762

.

.

.

.

.

.

.

.

.

.

.

( 16 )
551. 781.1
.

.

.

.

.

.

.

.

Eocene

.

781.11

Eocene inferieur

781.12

moyen et

781.121

moyen

781.122

superieur
Oligocene

781.21

Oligocene inferie ur

781.22

moyen et

781.221

moyen

Miocene

782.11

.

.

.

.

.

su

Neogene

782.1

.

.

'

perieur

moyen et

782.121

moyen

782.122

.

moyen

282.222

;152.1
.

.

.

.

.

.

Roches

.

Hl

Groupe

.

.

.

des Granites et des

des Felsophyres

112

Groupe

des Pechstein et

12

Roches neutres

121

Famille des

.

Syenites

.

des Andesites.

123

13

superieur

.

ignees,

Roches acides

.122
.

.

superieur.
Quaternaire

H

113

.

superieur.
Pliocene

782.221
79

superieur

.

moyen et

.

.

.

782.22

â€¢

.

.

Miocene inferieur

Pliocene inferieur

.

superieur

.

782.12

782.2

.

.

.782.21
.

.

.

782

.

superieur

.

781.2

781. 222

.

.

Eleolitique
Roches

basiques

.

.

Porphyres
Liparophyres
des Vitrophyres
.

et des

.

.

( t7 )

.

1.

Groupe

552.131
.131.1

l'Amphibole, des Pyroxenes et du Peridot.
Diorites, Diabases ophitiques, Ophites

Familles de

131.2

Familles des

132

Groupe

â€¢

.13-2.1'
'132.2

.

.133

.

.

.

Variolites, Porphyres basiques, Traps, Labradophyres, Spilites.

Families des
Roches

balsatiques

Groupe

III.

.

2

melaphyriques
Trachylytes, etc., et autrcs
Roches volcaniques

3

Roches

.133.2
.

.

.

Retinites

133.1

.

II

.

4

plutoniques
metamorphiques

5

sedimentaires
Alteration des roches

7

.8
,

81

.

82

.

.

Determination et caracteres des mineraux des roches
S tructnre des roches
.

.

.

.

â€¢

â€¢

.

.

Geolo51e

-

Ecosse.
Irlande.

3

Angleterre
Allernagne

31

Prusse

321

Saxe

33

Baviere

344
345
346

Alsace

2

.

.

.

.

.

.

.

Lorraine
Baden

.

.

;351

Wurtemberg
Hambourg.

.3t52

Breme,

347

.

.

.

.

Petrogra phie microscopique.

5 .. 4.

5M.1
.15

.

.

Meteorites. I

.6
.

roches vitreuses.

.

31)4

Brunswick

3t56

Westphalie

.

.

.

de

l'Europe.Â·

.

( 18 )
1>1)4.36

Autriche.

Â·

.37

Boheme.'

.38

Pologne
Hongrie

.

.

.

.

.

.

.

.

39

39.5 Bosnie

Herzegovine

4

France

5

Italie

58

Sicile

59

Sardaigne
Espagne.Â·
Portugal.

6

.69

.

.

.

.

Russie

Finlande

79

Caucase

Scandinavia

81

Norvege

.

.

.

Islande,

92

Hollande

93

Belgique

94.

Suisse

.

iles Feroe

.

.

.

.

Grece,

:96

.

.

.

8

95

Corse

.

9-1

.

et

.

71

Suede.

.

.

.

Danemark

.

.

.

85

.

.

.

.89

.

Â·

.

39:6

.7

Â·

.

.

97

Turquie d'Europe
Serhie, Bulgarie, Montenegl'o

98

Roumanie

99

Archipel

.

.

.

grec.

Geologie

1>5Â·5.-1

Chine

2

Japon

3

Arabie

4

Inde

.

.

.

.

.

.

.

.

.

5
6

.

.

Perse

.

Turcuie

d'Asie.

d eï¿½ I' "-!!ile.

( 19 )
555.7
.

.

â€¢

â€¢

Afghanistan

84

Turquestan

88

Balutchistan

93

Siam

.96
97

.

Siberie.

81

Cambodge.
Cochinchine
Annam

99

Tonkin.

Tripoli

2

Egypte

3

Abyssinie

4

Maroc

5

Algerie

Â·fH

Sahara.

62

Soudan

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

63

Senegambie

64

Sierra-Leone

65

ï¿½uinee

.

.

.

66

Liberia

67

Ashantee

68

Dahomey

69

Cote d'Or

72

Congo

.

.

.

.

.

78

Zanzibar

79

Mozambique

.

.82

Transvaal.

.84

Natal.

.

.

Geoloï¿½le

-

Tunisie.

12

.

francaise

.

006.

556.1-1

.

.

.98
.

.

.

85

Orange

86

Caffrerie

87

Colonie du

9

Madagascar.

.

.

.

Cap

.

Mauritius

.

de .'Al"rl

.. ue.

(

51i7.

Mexico

2

.28
.

.

.

.

GeoloA;le de I'.il.merlqlle d'i ï¿½o,"d.

-

Canada.

ii57.1
.

.

centrale

Amerique

281

Guatemala

2!i3

Honduras

284

San Salvador

285

Nicaragua.

.

.

286

Costa-Rica

Inde orientale

.291

Cuba

292

.

Santo

Hayti.

.

Porto Rico

291:1

Barbades

3

.

Domingo

29ii

.299

.

.

.

Bermudes.

Etats-Unis

5&8.

iiaB.!
.2

.

Jamaique.

.294
.

.

.

293

.

.

.

29

.

.

..

GeoloA;le de

-

Bresil.

Argentine.-

.3

Chili.

.4

Bolivie

ii

Pereu

6

Colombie, Equator

7

Â¥enezuela

B

Guiane

9

Paraguay, Uruguay.

.

.

.

.

.

20 )

.

.

.

.

.

I' .il.aneriqoe do Sod.

( 21 )

a1;9.

.Ii

12

.

Borneo

.

13

Celebes
Moluques.

14

Philippines

2

Sonde Illes de

.

.

.21
.

.

.

Sumatra

.

la)

Java

3

Australasie,

31
46

Nouvelle-Zelande
Australie
Tasmanie.

5

Nouvelle-Ouinee

.4
.

.

.6

.

.

Polynesia

.

.

Hawaii

8

Regions arctiques
Regions antarctiques.

9

.

.

69

.

.

.

.

22

.

de "Ocean'e.

Malaisie.

559.1

.

Geolo;;le

-

.

.

DETERl\'lINANTS [].
56

.

57.
571.
1

.

11

.

Paleontologic.
Biologic.
Archeologie prehistorique.
Age de la pierre
Epoque paleolithique
.

..

12

.

neolithique

..

.2

.3
4

.

.

5

Age
-

du bronze

.

du fer.

Autres debris

.

.6
.

.

.

63

arts primitifs
primitives, cavernes,

7

Ornements,

8

Habitations

9

Monuments et autres constructions

.

92[55]

Agriculture.
Biographies

.

relatives

aux

cites

sciences

lacustres, etc.,

.

geologiques,

etc

.

( 22 )

INDICES FORMELS.

.

01

Philosophie, role, progres et utilite de la t;eologic
Nomenclatures, notations, signes conventionels
cations, legendes
Theories, hypotheses, etc., sur la nature, I'origine,
causes, explications de :
.

.011

classifiÂ­

,

..

.012

.013
.

.

.

.

Notions de temps.

014

d'espace
d'investigation
.

01;;

Methodes

01:):1

d'observation

015.11

Observations

.01:U2
.

.

.

.

.

.

.

.

.

.

.

..

superficielles
profondeur,
.

en

015.2

Methodes

015.21

.

etc., de

experimentales

(Sondages, puits.)

.

analytiques
chimiques
microscopiques
synthetiques
.

015.211

.

01:).212
01:).22

.

.

02

Resumes

021

Resumes,

021.-1

el

sommaires, etc.,

et sommaires

021.2

etc

.

catalogues systematiques,

Tableaux

synoptiques
Catalogues systernatiques
Ouvrages de vulgarisation, poesies,

021.3

etc

.

.

.

.

022

etc

.

.022.1
.02'2.2

Romans

022.3

Poesies.

.

.

022.4

Dessins

.023
.

.

.

.

023.3
023.31

023.32

.0234
.

023.41

.023.4ï¿½

satyriques

Cravures, cartes,

023.1

.023.2

.

dessins et photographies
Diagramrnes,
Coupes
Notices, legendes, textes ex plicatifs etc
Eludes technologiques, couleurs, precedes graplliques
.

.

.

,

Cartes

.

.

.

.

Notices,

textes

cxplicatifs, Â·Jegendes,

ClbsenationsÂ·Lechnologiques,
graphiques.

.

.

etc.

03

Dictionnaires.

04

Essais, etc.,

etc

etc

.

.

observations,
gamma de couleurs, precedes

:

( 23 )
.

04l

Conferences, introductions, theses

.

.042
.

042.-1

.042.2
.

042.3
'

.

.

043
05

.06
.

.

.

.

.

.

.

circulaires, catalogues

Fetes et

anniversaires, discours prononces
Calendriers, annuaires.
Periodiques

aces solennites

.

.

Societes

.

06-1

Societes
Statuts

061.2

Rapports

061.3

Listes de membres

062

Societe industrielles et financieres.
Statuts

scientifiques

.

.

.

.

.

062.2
062.3

Rapports

07

Enseignement. collections, musees, etc., etc.
Appareils instruments d'enseignement et

.07-1

.

.

0611

,.062.-1
.

Prospectus, programmes,
Lines d'adresses

.

Liste de mernbres

.

,

de recherches

(sondes, etc.),
Materiel du voyageur geologue
Expositions, musees, etc
073.1
Expositions
.07311 Bapports, notices, descriptions, relations, comptes
critiques et debars relatifs aux :
.073.12 Guides, catalogues, manuels.
.

.

072

.

073

.

.

.

.

073.13

.073.2

Organisation

.

Musees, etablissements geologiques
services des leves

.073. 21
073.22
.073.23
073.3
;073.31
.

.

.

.

.

.

geologiques

et

miniers, academies,
miniers, commissions,

et

eccles speciales, etc.
Rapports, notices, descriptions, relations, comptes rendus,
critiques, debars relatifs aux :
Guides, catalogues, manuels
Organisation.
.

Collections ..

Rapports, notices. descriptions, relations, comptes rendus,

critiques, debats relatifs
catalogues

aux

:

073.32
073.33

Guides et

074

Voyages, explorations, excursions
Rapports, notices, descriptions, relations, comptcs rendus,

.074.1

074.2

.

Organisation

.

.

critiques
.

rendus,

et debats

Guides et manuels

relatifs

aux :

.

.

ï¿½
I

( ï¿½4 )
.

.

074.3
075

.076

.

Organisation

.

cours, discours d'ouverture

Leeons,

Manuels et

taires, enseignement technique..
,

.08

Travaux

09

Histoire

.

.

.

livres classiques, traites generaux traites elemenÂ­

collectifs, congres.

,

