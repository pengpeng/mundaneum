






















                            C O N G R U S  M O N D I A L
                                      DES
              ASSOCIATIONS           INTERNATIONALES
                             s
                                    G
                        Deuxième ession : AND-BRUXELLES,  15-18 uin  1913
                                                  j
                  Organisé  par l'Union  des  Associations  Internationales
                        Office  central  : Bruxelles ,  3bis,  ru e de l a Régence
              Actes  du  Congrès.  — Documents     préliminaires.





                     Comment        devrait    être  organisée une
                              Université     Internationale




                 Communication faite   par  l'Université Nouvelle  de  Bruxelles



                                           [ 3 8  ( « . ) ]
                                             7



                           A.  —  QUANT   A U X  P R O F E S S E U R S :
                   Étant  donné  qu'une  Université Internationale  doit  faire  appel
                 à  des  professeurs  venant de  toutes les  facultés  et  universités, et
                 que  le choix  qui  doit  en être  fait,  en vue  d'un  enseignement
                 déterminé  et  variable  chaque  année  suivant  l'état  des  connais-
                 sances  humaines et l'intérêt  que  certaines  matières  présentent
                 d'une  façon  toute  spéciale  à  chaque  époque  et dans  chaque
                 branche  de connaissances,  i l nous  apparaît  que  le personnel
                 enseignant  doit  comprendre :  i ° des  professeurs  appelés  pour  un
                 temps  déterminé,  d'une  façon  irrégulière,  suivant  les  besoins de
                 l'enseignement, pour donner  un cours ou une série de  conférences
                 sur  un sujet  choisi par l'Université  ; 2°  un  personnel  fixe  chargé
                 d'élaborer  pour  chaque  année ou chaque  semestre, le plan  des
                 cours  et  conférences  le plus  capable  d'intéresser  u n public
                 international,  de  coordonner  ces  cours  et  conférences,  d'en  fixer
                 le  programme, d'en tracer les limites et de recruter les professeurs
                 les  plus  qualifiés  pour  apporter, sur  ces données,  un  enseigne-























                                                 —   2  —                                                                             —  3  —
                       ment  original,  personnel dans certains cas,  et le  plus  d'actualité              rcnces  faits par les directeurs d'études pour préparer les  étudiants
                       possible.                                                                            à profiter  dans la plus grande mesure possible  des  cours  et  confé-
                                                                                                                                                                  l
                         Ce  personnel  serait  composé  d'un  certain  nombre  de  Direc-                  rences  faits  par  les  personnalités  appelées  de  l'étranger  à 'Uni-
                       teurs  d'études,  d'une  compétence  et  d'une  autorité  suffisantes               versité.
                       pour  remplir  ces  délicates  fonctions,  et  pour  diriger  en  même
                                                t
                       temps  les recherches  et les ravaux  des  étudiants  de  l'Université,                  D.  —  QUANT    A   LA DURÉE     D E S ÉTUDES    :
                       ou au besoin faire  des  cours ou des  conférences  complémentaires                    I l  va  de  soi  qu'avec  un  enseignement  ainsi  compris,  éminem-
                       ou  préparatoires  des  cours  et  conférences  faits  spécialement  par             ment  variable  et  changeant,  et  n'ayant  pas  pour  but  la  prépa-
                       les professeurs  étrangers.                                                          ration  à  des  examens  ou  des  concours  quelconques,  mais simple-
                         Ces directeurs d'études  auraient  à leur tête  un Directeur  général              ment  consacré  par  des  certificats  de  présence  délivrés  aux  étu-
                       chargé  de maintenir la coordination  des  différentes  directions,  de              diants  par les directeurs d'études,  i l 'y  a aucune durée  d'études
                                                                                                                                             n
                       veiller  à la  meilleure  répartition  des  cours  et  conférences,  et  de          à  prévoir.
                       surveiller  l'administration  de  l'Université  Internationale.
                                                                                                                       E.  —  QUANT    AUX   MÉTHODES      :
                                   B.  -  QUANT   A U X  ÉTUDIANTS     :                                      I l  résulte  encore  de  la  conception  même  d'une  Université

                         Le  but  d'une  Université  Internationale  étant  de  s'adresser  à               internationale  comme  un centre  de  hautes études  de perfection-
                       une  élite  intellectuelle  désireuse  de  rencontrer, groupées  en  un              nement  pour  des  jeunes  gens  déjà  pourvus  de  grades  universi-
                       même   centre,  les  personnalités  de  divers  pays  les  plus  compé-              taires,  que  les  cours  ne  doivent  pas  avoir  le  caractère  de  ceux
                       tentes  et les plus  remarquables dans chaque branche de  connais-                   des  universités  ordinaires,  dont  le  but  est  tout  autre,  et  que,
                       sances,  on  ne  devrait  admettre  que  des  étudiants  d'université                sans  se  placer systématiquement  au point  de  vue  comparatif  ou
                       ayant  obtenu leurs  grades  ou  en  cours  d'études  pour les  obtenir              international,  ils doivent  surtout  avoir  un  caractère  général  et
                       et  inscrits  dans  une université  quelconque.                                      permettre  de  faire  connaître  au public  scientifique  international
                                                                                                            les  idées  et  les  hommes  les  plus  représentatifs  à  chaque  époque
                                                                                                            d'une branche donnée  de  connaissances.
                                   C.  —  QUANT   A U X  BRANCHES     :

                          L'enseignement  nous  semble  ne  devoir comporter  ni  Facultés                              F.  —  QUANT   A U X  L A N G U E S  :
                       isolées,  ni  Facultés  groupées  en  université,  sur  le  modèle  des
                       universités  actuelles ordinaires.  Ce  doit  être  un centre de hautes                I l  paraît  bien  difficile  de  permettre  que  chaque  professeur
                       études, où  l'enseignement soit le plus  souple possible,  de  façon  à             s'exprime  dans  sa  langue nationale.  Ce  serait  aller  contre le  but
                       s'adapter  aux besoins  de  chaque  époque.  I l ne s'agit pas  de  faire           même   d'une Université  Internationale,  qui est  de  faire  connaître
                       une université pour distribuer  des  diplômes  à la suite  d'examens,               par  le plus  grand  nombre possible  d'hommes  de  science  les  idées
                       mais  un  institut  destiné  à  l'enseignement  le  plus  élevé,  le  plus          les  plus  importantes,  les  plus  intéressantes,  dans  tous  les  ordres
                       personnel,  le plus  actuel  possible, et  fait  par les personnalités  les          de  connaissances  humaines et  cela par les hommes  qui ont  lancé
                        plus  éminentes  de  chaque  pays  dans  chaque  ordre de  connais-                 ou  développé  le plus  ces  idées.  Si chaque  professeur parlait  dans
                                                                                                                      i
                       sances.                                                                             sa  langue, l en  résulterait  que  certains d'entre eux  ne seraient
                          Pas  de  cours  universitaires  fixes,  mais  des  cours  et  des  confé-         compris  que  par  les  étudiants  de  même  nationalité  qu'eux,
                        rences sous forme de cycles, sur tous les sujet les plus  intéressants             lesquels n'ont pas besoin  de sortir  de leur pays pour les entendre,
                        pour l'époque, en les complétant au besoin par des cours et confé-                  et  que  leur  enseignement  serait  inutile  pour  tous  les  autres

















                                                                                                                                                            1




                                                —  4  —                                                                              —  5  —

                      n'entendant  pas  leur  langue.  L'idéal  serait  évidemment  que                    se  signale l'activité  de  l'Université  Nouvelle. C'est  par  là  surtout  que  se
                      l'enseignement  fût  fait  dans  une  seule langue,  qui  serait  celle  du          manifeste  son  caTactère  international.
                      pays  où  siégerait  l'Université  internationale.  Mais  ce  désir  est               Qu'il  nous  suffise, pour le montrer,  de  dire  que  de  1902  à  1913,  c'est-
                      difficile  à  réaliser. Toutefois,  les  hommes cultivés  connaissent  en            à-dire pendant la dernière décade d'enseignement, et 263 professeurs  résidant
                                                                                                                       sont
                                                                                                                     y
                                                                                                             l'étranger
                                                                                                           à
                                                                                                                                                         le nombre
                                                                                                                                                                 de
                                                                                                                                                                    ces
                                                                                                                                                     que
                                                                                                                           venus donner
                                                                                                                                      916
                                                                                                                                          conférences
                      général  deux  ou  trois  des  langues suivantes  : français, anglais  ou            professeurs,  qui  n'était  les  premières  années  que  d'une  quinzaine,  a
                      allemand.  On  pourrait  donc  admettre,  en  principe,  que  l'ensei-               atteint  dans les dernières, la trentaine et a même été, cette année, de 44.
                      gnement  serait  facultatif  dans  une  de  ces  trois  langues  seulement.            Quant  aux  cours  universitaires  faits  par  le  corps  professoral belge, ils
                      Le  temps  et  les  circonstances  se  chargeraient  de  régler  cette               ont  comporté  en  1911-1912,  1,500  leçons,  et en  1912-1913,  1,570  leçons.
                      question  d'une  façon  plus  stricte  pour  le  plus grand  avantage  du            de Ces chiffres attestent mieux que tout  ce que l'on pourrait dire,  l'activité
                                                                                                                                                            international
                                                                                                                                       de
                                                                                                                                         vue
                                                                                                                                  point
                                                                                                                  en
                                                                                                                     plus
                                                                                                                                               l'enseignement
                                                                                                                         intense au
                                                                                                                                             de
                                                                                                              plus
                      public  international  scientifique,  dont  les  desiderata  sauraient               de  l'Institut  des  Hautes  Études  de  l'Université  Nouvelle,  et  l'estime
                      bien  se  faire  obéir.                                                              croissant dont l jouit  à  l'étranger.
                                                                                                                       i
                                                                                                             Il  ne lui  manque actuellement que  des ressources  matérielles suffisantes
                                                                                                           pour ui donner l'extension que  comporte  une  université  internationale,
                                                                                                                l
                      G.  —  QUANT    A U  SIÈGE  D E  C E T T E  I N S T I T U T I O N  :                 telle  que  la  propose  le  Congrès,  et  pour  réaliser  pleinement cette haute
                                                                                                           conception  qu'elle a  été,  elle  en  est  fïère,  la première  à  avoir  eue,  i l y  a
                        I l  semble  naturel  qu'elle  soit  installée  dans  un  pays  neutre             près de vingt  ans, et à avoir poursuivie depuis lors dans sa réalisation.
                      au point  de  vue  politique,  d'une part, qui  ne  soit  pas  déjà  pourvu
                      de  grandes  universités  et  établissements  d'enseignement  supé-
                      rieur,  d'autre  part,  et  enfin,  qui  soit  d'un  accès  aussi  facile  et
                      rapide  que  possible  aux  étudiants  de  tous  les  pays du  monde.
                                                                                                             I l  ne  nous  est  pas  possible,  ici, de  rappeler les  conférences  et  séries de
                        La  Belgique,  par  sa  situation  privilégiée  à  ces  divers  points              conférences qui ont été  organisées depuis le début  de  l'œuvre.  Nous  nous
                      de  vue,  paraît  tout  indiquée  pour  être  le  siège  d'une  pareille              bornerons  à citer,  parmi  les personnalités  étrangères  les plus marquantes
                      Université,  qui  y  est  du  reste  actuellement  représentée  par                   qui  ont donné  leur  concours  à l'Université  Nouvelle depuis  1894  :
                      l'Université  Nouvelle,  Ecole  internationale  d'enseignement                         MM.  Elisée  Reclus ;  —  Élie  Reclus ;  —  de  Roberty ; —  Enrico  Ferri,
                                                                                                                                                             membre du
                                                                                                                                            Maxime
                                                                                                                                                   Kovalevski,
                      supérieur,  et  qui  répond  entièrement  à  la  conception  proposée                professeur  à  l'Université  de  Rome  ;  — ; —  Max Nordau  ; —  Novicow ;  —
                                                                                                                                   Quillard
                                                                                                                              Pierre
                                                                                                           Conseil d'Empire ; —
                      aujourd'hui  par  le  Congrès  Mondial  des  Associations  Interna-                  Bernard  Lazare ;  —  Enriquès,  professeur  à  l'Université  de  Bologne ;  —
                      tionales.                                                                            Albert  Métin,  agrégé  à  l'Université  de  Paris,  député  à la Chambre  fran-
                                                                                                           çaise ; —  Niceforo, professeur aux Universités de Naples et de Lausanne ;
                                                                                                           —Médéric Dufour, professeur à l'Université de Lille ; —  Hubert  Lagardelle
                                               A N N E X E                                                 directeur  du .Mouvement  socialiste  ;  •—•  Loria,  professeur  à  l'Université
                                                                                                           de  Turin  ; —  Issaïeff,  professeur  à  l'Université  de  SaintPétersbourg  ;  —
                                                                                                           Edgard  Milhaud,  professeur à l'Université  de Genève ; —  Albert Thomas,
                                L'Université  Nouvelle  de  Bruxelles.                                     député  à la Chambre française  ; —  Ch. Seignobos,  professeur  à  l'Univer-
                                                                                                           sité  de  Paris ; —  Sylvain  Lévi,  professeur  au Collège  de  France ; —  Paul
                          Ecole  libre  et internationale  d'enseignement  supérieur.
                                                                                                            Sabatier ; —  Langevin,  membre du Collège  de  France ; —  Perrin,  profes-
                        L'Université  Nouvelle  de  Bruxelles,  —  École  libre  internationale            seur  à  l'Université  de  Paris ;  —  Scipio  Sighele ;  —  René  Worms ;  —
                      d'Enseignement  supérieur, —  a  été  fondée  à  Bruxelles,  en  1894,  époque       D.  Parodi  ;  —  Bernstein,  ancien  député  au  Reichstag ;  —  Pierre  Mille ;
                      depuis laquelle elle fonctionne sans interruption.  •—  Ses locaux se trouvent       —  Kohler  ;  —  Le  Dantec ;  —  Fontaine,  directeur  général  du  travail  à
                      67,  rue de  la Concorde,  à Bruxelles.  —  Elle  comprend en  ce  moment  : —       Paris  ;  —  le  général  Bazaine-Hayter  ;  —  D  r  Auguste Forel ;  —  Charles
                      une  Faculté  des  sciences  sociales,  économiques  et  financières  ;  —  une      Gide ;   Louis  Havet,  membre  de  l'Institut  de  France ;  —  Buisson,
                                         I
                      Faculté  de  droit  ; — un nstitut  géographique  ; — un nstitut  des Hautes          député  à la Chambre française ; —  D  r  Solfier ; —  E. Van de Velde,  archi-
                                                                 I
                      Etudes.                                                                               tecte,  à  Weimar ;  —  Ch.  Diehl,  membre  de  l'Institut  de  France ;  —
                        L'Institut  des Hautes Etudes est  l'organisme le plus  original  par lequel        E.  Bertaux,  professeur  à  l'Université  de  Paris ;  —  Salomon  Reinach,






















                                               —  6  —

                     membre de 'Institut  de France ; —  Henri  Marcel,  directeur du Musée du
                               l
                     Louvre ;  —  G.  Milhaud,  professeur  à  l'Université  de  Paris ;  —  Lalande,
                     professeur  à  l'Université  de  Paris ;  —  Andler,  professeur  à  l'Université
                     de  Paris  ;  —  Luchaire,  professeur  à  l'Université  de  Grenoble,  directeur
                     de  l'Institut  français  de  Florence ;  —  Augagneur,  ancien  ministre  ;  —
                     Professeur  Rathgen,  directeur  de  l'Institut  Colonial  de  Hambourg  ;  —
                     Francis  de  Pressense  ;  —  Sembat,  député  à  la  Chambre  française  ;  —
                     Charles  Dumont,  ministre ;  —  Pierre  Paris,  professeur  à  l'Université  de
                     Bordeaux  ; —  Enlart,  directeur du  Musée du Trocadéro  ; —  L. Benedite,
                     conservateur  du  Musée  du  Luxembourg  ;  —  Jessen,  directeur  au  Musée
                     des  Arts  décoratifs,  à  Berlin  ;  —  Berlage, architecte, à  Amsterdam  ;  —
                     Réau, directeur de l'École française de Saint-Pétersbourg  ; —  Mabel Bode,
                     professeur à l'Université de Londres ; —  Foucher, professeur à l'Université
                     de Paris ; —  d'Ardenne de Tizac, conservateur du Musée Czernuschi.

                                                 * * *

                       Les  conférences  de  l'année  universitaire qui vient  de  finir  (1912-1913),
                     ont  été consacrées, notamment : —  aux Sciences naturelles  (les  méthodes
                     d'investigation  en  mathématique,  astronomie,  physique,  chimie,  bio-
                     logie,  etc.)  ; —  à la Psychologie  ; —  à des  Questions sociales  et  politiques
                      (l'évolution  capitaliste  et  la démocratie  en  France) ; —  et  des  questions
                     d'Art  (l'art  espagnol ;  —  l'art  décoratif  contemporain  ;  —  les  arts  en
                      Extrême-Orient  ; —  le Bouddhisme dans la littérature  et dans  l'art)  ;  —
                      à  la  Pédotechnie  (la  culture  de  l'enfant  au  point  de  vue  psycholo-
                      gique)  etc.,  etc.

