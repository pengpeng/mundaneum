





















                                 I N S T I T U T  I N T E R N A T I O N A L
                                      D E   B I B L I O G R A P H I E           PUBLI-
                                                                                CATION
                             B U T  DB L'INSTITUT  :' Perfectionner  et unifier les méthodes  bibliographiques  et
                              documentaires.  Organise r  l a  coopération  scientifique  internationale  dans  N" 76
                              les travaux  île  bibliographie  et de documentation.  Préparer  un Répertoire
                              Bibliographique  Universe l et e n délivrer des extraits  et  duplicata.









                    Bibliothèque                Collective



                    des     Associations                et    Institutions


                    Scientifiques               et    Corporatives











                                            B R U X E L L E S .  —  S A L L E  GOTHIQUE
                                            27 ,  MONTAGNE   D E  L A  COUR
                                              a
                                            OUVERTE    TOUS  L E S JOURS  NON  FÉRIÉS
                                            D E  9  A  12  E T  D E  2  A  6  H E U R E S













                     I n d i c e  b i b l i o g r a p h i q u e  :       .  _  _
                      [027   (493.2)]       B R U X E L L E S              1906

























                       Institut  International  de  Bibliographie.      [027  (493-2)]
                         1906.  —  Bibliothèque  collective  des  Associations  et  Institutions                                                 L a  Bibliothèque  Collective  des  associa-
                                                                                                                                               tions  e t  des  institutions  scientifiques  et
                           scientifiques  et  corporatives. —  [Programme.  —  Règlement.  —                                                   corporatives  a  été  fondée,  en  A v r i l  1906,
                           Services d'information  et  de  documentation.  —  Local.]                                                          pa r  l'Institut  International  de  Bibliogra -
                         Bruxelles,  au  siège  de  la  Bibliothèque  collective.  27a, Montagne  de  la                                       phie  et  diverses  Sociétés  savantes  d e
                                                                                                                                               Bruxelles.
                           Cour. In-S°,  12 p.





                                                                                                                                    P R O G R A M M E

                                                                                                                           D E  L A  BIBLIOTHÈQUE    C O L L E C T I V E
                              ;                     O
                        X.  B.  Notice  bibliographique  de  l a  présente  publication, destinée  à  servir  de  texte  pour l a prépa-
                               ration  des répertoires bibliographiques  et des catalogues  de  bibliothèque.

                                                                                                                _  Grouper  les  bibliothèques  éparses  des  institutions  et  associations
                                                                                                                scientifiques  et  corporatives,  ainsi  que  celles des  directions  de  pério-
                                                                                                                diques.
                                                                                                                  Assumer,  d'une manière  à  déterminer  dans  chaque  cas,  la  gestion
                                                                                                                administrative  de  ces  bibliothèques.
                                                                                                                  Assurer  à  leur  service  des  locaux  appropriés,  chauffés,  éclairés  et
                                                                                                                accessibles pendant  la  plus grande partie  de  la  journée.
                                                                            rrriTTTT                              Placerles  collections  de  chaque institution  adhérente  sous la garde
                                                                             l i l i i ' l
                                                                                                                d'une  administration  responsable,  chargée  de  les  conserver,  de  les
                                                                                                                cataloguer, deles  classer,  de  les  communiquer  sur  place  et  de  les
                                                                                                                                                 la
                                                                                                                                                   pleine
                                     "TI       1                                  71                            prêter  au  dehors,  tout  en  laissant l'institution  propriété  et  la  libre
                                                                                                                          des
                                                                                                                              ouvrages déposés
                                                                                                                                             à
                                                                                                                                                          adhérente.
                                                                                                                disposition
                                                                    V T                                           Constituer  ainsi,  par  la  réunion  des  diverses  bibliothèques  spé-
                                                     DP                                                         ciales,  une  bibliothèque  collective  embrassant  progressivement  les
                                                                                                                diverses  branches  du  savoir  encyclopédique,  et  qui  soit  l'auxiliaire
                                                      TZ1  Ol  I  I        -Cafcrn-U                            des  bibliothèques  publiques  existantes, dont  le  caractère  est  général.
                                                                                                                  Mettre  par  suite  à  la  disposition  de  l'Institut  International  de
                                                          D                                                     Bibliographie,  en  échange  des  soins  de  sa  gestion,  des  collections
                              >¿ditum¿                                         ffl
                                                                                                                                   travaux
                    D                 •                                                                         étendues,  utiles  à  ses permettre  de  documentation. et  institutions  scien-
                                                                                                                           temps
                                                                                                                     même
                                                                                                                  En
                                                                                                                                               associations
                                                                                                                                           aux
                                                                                                                tifiques  d'assurer  à  leurs membres l'usage  des  services  d'information
                                                                                                                et  de  documentation  de  l'Institut.
                                 LA  B I B L I O T H È Q U E  COLLECTIVE  DES  S O C I É T É S  SAVANTES
























                                                                                                                 les  questions  se  rattachant  au  développement  de  la  Bibliothèque
                                              R E G L E M E N T                                                  ainsi  que  les  mesures.et démarches  nécessaires  en  vue  d'accroître  le
                                                                                                                 nombre  des  institutions affiliées  et d'obtenir  les subsides des  pouvoirs
                                                                                                                 publics en  faveur  de  la  Bibliothèque  Collective.
                                                I .  —  AFFILIATION.

                            ARTICLE  PREMIER.  —  Admission,  —  Sont  admis  à  participer  aux                             I I I .  —  INVENTAIRE  E T  ACCROISSEMENT.
                          services de  la Bibliothèque  Collective, tous  les  groupements  et  insti-
                          tutions  ayant  un  caractère  scientifique  ou  corporatif,  et  dans  la              ART.  7. — Inventaire.  —  Les  collections déposées  sont  inventoriées
                          mesure où l'Institut  de  Bibliographie disposera  lui-même  des  locaux               sur  fiches.  U n numéro  d'inventaire  est  attribué  à  chaque  ouvrage.
                          nécessaires.  Les  publications  périodiques  indépendantes  sont  éga-                Il  est  porté  sur  Yex-libris propre  à chaque institution  affiliée  et  qui  est
                          lement  admises à  participer  à  ces  services.                                       appliqué  sur  chacun  des  ouvrages qu'elle  dépose.
                                                                                                                  Les  fiches  de  l'inventaire  sont  paraphées  par  le bibliothécaire  de
                            ART.  2. —  Effets  de  l'affiliation. —  Les  bibliothèques  affiliées  con-       l'Institut  et  par  le  délégué  de  l'institution  affiliée.  Elles  portent  la
                          servent  la  pleine  propriété  et  la  disposition  de  leurs  collections.           date du  dépôt.
                          L'objet  de  l'affiliation  est  limité  à l'administration  des  collections  et       Le  Bibliothécaire  de  l'Institut  remet  mensuellement  à  l'institution
                          à  leur  usage en commun, dans une  mesure  à déterminer  dans chaque                 adhérente  un  état  constatant  le  nombre  total  d'ouvrages  portés  à
                          cas  en particulier, mais impliquant toujours  la faculté  pour  l'Institut           l'inventaire.
                          international  de  Bibliographie  de  les  consulter  pour  ses  propres
                          besoins.                                                                                ART.  8.  —  Accroissements. —  Les  accroissements  sont  portés  à
                                                                                                                 l'inventaire  à mesure  de leur réception.  Ces  inscriptions sont  contrô-
                            ART.  3. —  Conditions de  l'affiliation. —  L'Institut  de  Bibliographie          lées  et  paraphées  périodiquement  par  le  délégué  de  l'institution
                          reçoit  en  dépôt  les  collections  inventoriées  qui  lui  sont  confiées.  I l     intéressée  et par  le  bibliothécaire.
                          assume l'organisation  des  divers  services énumérés  ci-après.                        Sur  demande  et moyennant  cotisation spéciale, une  liste bibliogra-
                            Les  institutions  adhérentes  interviennent  dans  les  frais  d'admi-             phique  des  accroissements pourra  être  fournie  à  l'institution  affiliée
                          nistration pour  l'occupation  des  rayons  et  pour  l'usage  des  services          et lui servir  de  manuscrit  en  vue  de  la publication dans son  Bulletin
                          auxquels  elles  déclarent  vouloir  participer.  Ces  services  sont  facul-         ou  autrement,  du  catalogue  de  ses  accroissements.
                          tatifs.  Ils  donnent  lieu  à  des  cotisations  proportionnelles  à  l'usage          Les  institutions  adhérentes  sont  invitées  à  donner  à  leur  biblio-
                          que  devra  en faire chaque  bibliothèque.  Les  cotisations  sont  déter-            thèque  tout  le  développement  dont  celles-ci sont  susceptibles.  I l  est
                          minées  au  tableau  ci-après  (page 8).                                              de  l'intérêt  de  chaque  affilié  de  compléter  ses  collections  dans  le
                            Les bibliothèques  affiliées  devront participer  aux  frais  d'une  assu-          domaine  de  ses  travaux,  comme l est  de  l'intérêt  de  la  Bibliothèque
                                                                                                                                             i
                          rance  collective,  chacune  au  prorata  du  nombre  et  de  la  valeur  de          collective  d'être  composée  de  bibliothèques  spéciales  très  riches  afin
                          ses  ouvrages.                                                                        de  former  ainsi  un  ensemble  encyclopédique  aussi  complet  que
                            ART.  4. — Durée.  —  L'affiliation  a  une  durée  minimum  d'un  an.              possible.  Les  institutions adhérentes  sont aussi invitées  à faire  relier
                          Elle  est  prorogée  par  tacite  reconduction.  Toute  résiliation  est              leurs  collections  pour  les  mettre  à  l'abri  des  pertes  et  détériora-
                          notifiée  trois mois  avant  l'échéance  du  terme.                                   tions.
                                                                                                                  Les  publications  périodiques  des  institutions  affiliées  (bulletins,
                                                                                                                actes,  mémoires,  rapports)  constituent  des  éléments  précieux  qui
                                           I L  —  COMITÉ  ADMINISTRATIF.
                                                                                                                permettent  un  accroissement  des  collections presque sans frais,  soit
                                                                                                                par  voie d'échange  avec les  publications  similaires, soit par  la  récep-
                            ART,  5. — Composition. — Les  institutions affiliées  sont  représentées           tion  d'ouvrages  nouveaux  pour  comptes  rendus  et  analyses.  Le
                          au  Comité  central  de  la Bibliothèque  Collective par  un  délégué.
                                                                                                                service  central  de  la  Bibliothèque  collective  se  charge,  pour  compte
                            ART.  6.  —  Attributions.  —  Ce  Comité  a  dans  ses  attributions  lè           des  institutions  qui  le  désirent,  du  service  des  échanges  et  des
                          contrôle  de la gestion  et,  d'une manière  générale,  l'examen  de  toutes          demandes d'ouvrages  nouveaux.





















                                                     —  6  —

                                        I V .  —  SERVICES  DIVERS  ORGANISÉS.                                    Les  membres  et  les délégués  peuvent  recevoir en communication
                                                                                                                sur  place tous  les ouvrages  déposés  à la Bibliothèque  collective, quel
                            ART.  g. — Set-vices  divers.  —  Les  services  organisés  comportent la           que  soit le fonds auquel  ces  ouvrages  appartiennent.
                          garde  des  ouvrages,  leur  classement,  leur  cataloguage,  le  dépouille-            Les  prêts  des  ouvrages  au  dehors  n'est  fait  que.dans  les  limites
                          ment  des  périodiques,  l'organisation  des  menus  imprimés  en" réper-             prescrites  par  le  règlement  de  chacune  des  institutions  dépo-
                          toire documentaire, l'accroissement  des  collections, notamment  par                 santes.
                          l'échange  des  périodiques,  la communication des  ouvrages  sur  place                ART.  I3.  — Service  des échanges. — Les échanges  comportent  l'envoi
                          et au  dehors.                                                                        de la publication  échangée  ainsi que la réception  et l'inventaire de la
                            Les institutions adhérentes  déclarent,  lors de leur affiliation  ou par           publication  avec laquelle l'échange  est  établi.
                          la  suite,  quels  sont  ceux  des  services  organisés  auxquels  elles                Le  service  central  se  charge  de  remettre  gratuitement au  Bureau
                          désirent  recourir et paient les  cotisations y  afférentes.
                                                                                                                belge  du  service  international  des  échanges  les  envois  destinés  à
                            ART.  IO.  —  Conservation  des ouvrages.  —  La  simple garde  et  la con-         l'étranger  en franchise  de port et de retirer  les  publications arrivant
                          servation  des  ouvrages  sont gratuites, à la condition  que  le  mobilier           de l'étranger  à  ce bureau  en destination de l'institution  affiliée.
                          soit fourni  par  l'institution  affiliée  et que  l'usage des ouvrages  déposés        ART.  14.  —  Demandes  pour  compte  rendu.  —  Les  demandes  sont
                          soit  déclaré  commun  aux  membres  et  aux  délégués  de  toutes  les               faites  d'après  les  indications  données  par  les  institutions  intéres-
                          institutions  affiliées.                                                              sées.
                            Le  matériel  nécessaire  à  la  conservation  des  ouvrages  tels  que
                                                                                                                                              nouveaux
                                                                                                                                                        à
                                                                                                                                      ouvrages
                                                                                                                  Pour
                                                                                                                                  des
                          rayons,  cartons  à brochures,  etc.,  peut  être  procuré  par  l'Institut  de       toires  de l'indication  de  Bibliographie  sont  mis  demander,  les  réper-
                                                                                                                                                                         des
                                                                                                                                                              disposition
                                                                                                                         l'Institut
                                                                                                                                                          à
                                                                                                                                                            la
                          Bibliographie  à titre  locatif  ou autre.
                                                                                                                affiliés;
                            ART.  I I .  —  Catalogue.  — l est  établi  sur  fiches,  et  conformément
                                                  I
                                                                                                                          — Service
                                                                                                                                                                    central, l
                                                                                                                                                                           i
                                                                                                                                           — Pour
                                                                                                                                                   l'usage
                                                                                                                                  des envois.
                          aux méthodes  de  l'Institut  de  Bibliographie,  un  catalogue  en double            est ART.  I5. sous forme d'étiquettes  imprimées  et  du  service  des  séries
                                                                                                                   établi,
                                                                                                                                                          gommées,
                          exemplaire, l'un  classé  par  ordre alphabétique  des  noms  d'auteurs,
                          l'autre par  ordre méthodique  des  matières.  Ce catalogue  est  collectif           d'adresses de tous  les membres  des institutions adhérentes.  Ces séries
                          et comprend, en  une  seule série,  l'ensemble  des ouvrages  déposés  par            d'adresses  peuvent  être  cédées  aux  institutions  adhérentes  pour
                          toutes les institutions adhérentes.                                                   leur propre usage.
                            Chaque  fiche  du  catalogue  porte  la  mention de  l'institution  pro-
                          priétaire  de l'ouvrage  catalogué.                                                                        V .  —  COTISATIONS.
                            Les institutions affiliées  peuvent  obtenir  pour leur usage  privé,  et
                          moyennant  rétribution,  un  duplicata  du  catalogue  des  ouvrages                    ART.  16. —  L'affiliation  est  gratuite  mais  l'utilisation  des  divers
                          qu'elles ont  déposés.                                                                services  organisés,  autres  que la simple garde  des  ouvrages,  implique
                                                                                                                le  paiement  des  cotisations indiquées  au tableau  ci-après.
                            ART.  12. —  Consultation, lecture,  prêt.  —  La Bibliothèque  collective
                          comporte  une  salle  de  lecture  qui est  accessible  de  9 à  12 et  de  2 à         Des  subsides  spéciaux  seront  sollicités  des  pouvoirs  publics  à
                          6  heures,  tous  les jours non fériés.  L'entrée  de la salle  de lecture  est       l'effet  de  voir  développer  les  services  communs  aux  bibliothèques
                          située  Montagne de la Cour,  27a.                                                    affilées.  Ces subsides  viendront annuellement  en  déduction  des  coti-
                            Chaque  institution  adhérente  remet  au  service  central une  copie              sations  dues  par  les bibliothèques  affiliées,  et  ce au prorata du mon-
                          tenue  à jour  de  la  liste  de  ses  membres  ou délégués  afin  de  faciliter      tant  des  cotisations qu'elles  se seront  engagées  à  payer.
                          le  service  et le  contrôle.  Notification  est  donnée  des  changements,             Il  est  éventuellement  fait  un  forfait  de  gestion avec  les  institu-
                          radiations,  inscriptions nouvelles,  etc.                                            tions  qui  désirent  faire usage  de  l'ensemble  des  services  organisés.
                            Sont seuls admis à fréquenter  la salle  de lecture les  membres  et  les           Ce  forfait  prend  pour  base  le  nombre  des  membres  de  l'institu-
                          délégués  des  diverses institutions adhérentes,  sur  production de leur             tion  adhérente  et  celui  des  volumes  de  la  bibliothèque  qu'elle
                          carte d'identité,  et  les personnes  auxquelles l'Institut  accordera  cette         dépose.
                          faculté  sous sa propre  responsabilité.                                                Le règlement  des  cotisations se  fait  semestriellement.
























                                            TABLEAU  DES  COTISATIONS.

                          1.  Confection  de  l'inventaire,  par  ouvrage    fr.  o.io                                                  L O C A L
                          2.  Catalogue  et  duplicata  du  catalogue, par fiche  o.o5
                          3.  Prêt  au  dehors  (les frais  éventuels  d'envoi  sont  facturés  à                         D E  L A  BIBLIOTHÈQUE    C O L L E C T I V E
                               part),  par volume                                 o.o5
                          4.  Demandes  d'ouvrages  pour  compte  rendu  (rédaction  et
                               port),  par ouvrage  demandé                       o.i5
                          5.  Echanges,  comprenant  l'envoi  et  la  réception  des  pério-
                               diques  échangés,  par  périodique,  annuellement,  ports                         La  Chapelle  Saint-Georges,  en  laquelle  sont  installées  les  collections
                               non  compris                                       0.20                         de  la  « Bibliothèque  Collective»,  fait  partie  intégrante  des  bâtiments  du
                          6.  Séries  d'adresses  des  membres  :  cent  adresses  différentes                 Musée  et  des  Archives.  Elle  est  située,  273,  Montagne  de  la  Cour,  en
                               imprimées  à  cent  exemplaires  chacune          i5.oo                         plein  centre  de  Bruxelles.
                          7.  Dépouillement  des  périodiques  (conditions  à  convenir).                        Elle  constitue vraiment  un  bijou  d'architecture.  Malgré  sa  destination
                          8.  Fourniture  ou  location  de  .mobilier  et  accessoires  (conditions  à         nouvelle, la  construction  conserve  tout  son  caractère,  et  l'on  se  croirait
                               convenir).                                                                      dans la  library  de  quelque vieux collège d'Oxford  ou  de  Cambridge.
                          9.  Assurance  :  1  franc  par 1,000  francs  de  valeur  déclarée.                   Son  aménagement  en  bibliothèque,  répond  aux  exigences  les  plus
                                                                                                               modernes:  casiers'à  livres,  luminaire électrique,  système  de  classement
                                                                                                               des  livres  et  revues, catalogue,  etc.
                                                                                                                 C'est  à  I5I6  que  remonte  la  reconstruction  actuelle  de  la  Chapelle
                                                                                                               Saint-Georges. Elle  apparaissait déjà  remarquable à Albert  Diirer,  qui  la
                                                                                                               visita  et  y  goûta  fort  un  tableau  d'Hugo  van  der  Goes.  En  style  ogi-
                                                S E R V I C E S                                                val  flamboyant,  avec  ses  trois  longues  et  minces  colonnes  cylindriques
                                                                                                               sans chapitaux,  sa  voûte surbaissée  à  fines  nervures  croisées,  sa  tribune
                                  D'INFORMATION     E T  D E  D O C U M E N T A T I O N
                                                                                                               ornée  d'une élégante balustrade,  elle constitue tout  ce  qui reste  aujour-
                                                                                                               d'hui,  hors une  aile  de la cour  du Musée,  de la  somptueuse demeure  où  le
                                                                                                               Taciturne menait grand train  de  maison.
                                                                                                                 La  Révolution  détruisit  l'autel  de  la  Chapelle, puis un brasseur  y  ins--
                            Par  leur  union avec  l'Institut  International  de  Bibliographie,  les          talla  ses  tonneaux. En  i838,  la  ville la  fit  restaurer  et  on  y  déposa  les
                          associations  et  institutions  scientifiques  peuvent  utiliser,  au  béné-         sculptures achetées  à  Kessels. Plus tard,  le  Musée  d'Histoire  naturelle
                          fice  de  leurs membres, un  service  complet  d'information  et  de  docu-          l'utilisa  pour  ses  empaillages,  et  Depauw  s'y  illustra  en  y  montant  les
                          mentation  pour les  matières qui  font  l'objet  de  leurs études  :                iguanodons.
                            i°  Cabinet des  ouvrages périodiques  spéciaux belges  et étrangers  (1) ;          En  ces  dernières  années,  on  en  avait fait  un  refuge  à  matériaux  divers.
                            2 °  Bibliothèque  d'ouvrages spéciaux  (livres  et brochures) ;                   La  voici retournée  à  sa  plus intellectuelle  destination.  Le  Mont-des-Arts
                            3°  Répertoire  Bibliographique  Universel  renseignant  les  travaux  pu-         exigera  son  enveloppement  extérieur  par  des  bâtiments  nouveaux,  mais
                                bliés  dans toutes  les  branches  du  savoir  encyclopédique,  (notices       l'intérieur  demeurera intact et pourra être  éclairé par  la cour du  Musée.
                                classées  par  noms  d'auteurs  et  par  matières)  comprenant  à  ce
                                jour  sept millions  de  notices  (en  formation) ;
                            4  0  Catalogue Collectif  des  Bibliothèques  de  Belgique, renseignant  les
                                Bibliothèques  du pays, où  les  ouvrages désirés  peu vent être obte-
                                nus  en  consultation  (en  formation) ;
                            5°  Répertoire  Iconographique  Universel,  comportant  les  documents
                                photographiques qui représentent  les  objets,  les  idées  et les  opéra-
                                tions  de  toute  nature  (en  formation).

                           (1)  L e Cabinet des périodiques  de l a Bibliothèque  Royal e  a  principalement  pour  objet  l a  collec-
                          tion  des  grands  périodiques  d'intérêt  général.





















                                  BIBIIOGRAPHIA             UNIVERSALIS

                           Recueils  périodiques  de  bibliographie,  édités  ou  patronnés  par  l'Institut
                              International  de  Bibliographie  et  formant  contributions  imprimées  au
                              Répertoire  Bibliographique  Universel.

                              Edition A  =  en volume ordinaire.  -N
                                »  B =  en volume imprimé  recto, blanc au verso.   Nombr e
                                >  C =  sur  fiches  imprimées.                approximatif
                                                                                de
                                                                                  notices
                                »  î) =  sur fiches collées.                     parues
                                                                               au  i " janvie r
                                                                                  1906
                           Contribution n° 2. Bibliograßhia Zoologien  Universalis;  depuis  1896.
                                Edition A, par fascicules hebdomadaires, fr. 18.75 par an.
                                   »   B,      »          »         25.00  »    117,242
                                   »   C, sur fich.es (fr.  10.00 les  1,000 fiches).
                                         Expédition  pa r séries de 5o  fiches  environ.
                           Contribution n° 3. Bibliograßhia Philosophien  Universalis  ; dep.  i8g5.  18,592
                                Edition B, par fascicules trimestriels, fr. 5.oo par an.
                           Contribution n° 4. Bibliograßhia Physiologien  Universalis;  dep. i8g3.
                                 Edition B,  3 ou 4 fascicules  par an, fr. o.5o par fascicule.   9,007
                                   »   C, sur fiches, prix variable.
                           Contribution n° 6. Bibliograßhia Anatomien  Universalis;  dep. 1897.
                                 Edition A, 24 fascicules par  an, fr.  10.00.   12,139
                                   »   B, 24   »       »   »  14.50.
                                   »   C, sur fiches, prix variable.
                            Contribution n°8. BibliograßhieminsuelledesChemins  de fer;  dep. 1897.  I  24,248
                                 Edition B,  12 fascicules par  an, fr.  10.00.
                            Contribution n° i5. Bibliograßhie  de Belgique;  depuis  1897.
                                 Edition A, fascicules bi-mensuels.            J "4,949
                            Contribution  n° 16. Bibliograßhia Geologien Universalis;  dep. 1896.   J  44,128
                                 Edition A, par volumes annuels,  prix variable.
                            Contribution  n°3o.  Bibliograßhia Medka  Universalis  ; depuis  1900.  \ 108,000
                                 Edition A, environ 36,ooo notices par an, fr.  120.00.
                            Contribution n° Zi.Bibliograßhia Bibliograßhica  Universalis;dep.i8gS.
                                 Edition B, en brochure annuelle, fr. 4.00 par an.
                                   »   C, en fiches imprimées, fr.  12.00 par an.   2,960
                                   »   D,    »   collées,  fr. 7.00 par  an.
                            Contribution n° 3g. Bibliogmßhin Economicn  Universalis;  dep.  1902.
                                 Edition B, en volume annuel, fr. 6.00.         \  9,020
                            Contribution n°40. Bibliograßhia Agronomien  Universnlis  ; dep. igo3.
                                 Edition B, par fascicules trimestriels,  12 francs par  an.  J  4,932
                            Contribution n° 41. Bibliogmßhin  Teehnica Universalis;  depuis igo3.
                                 Edition  B, par  fascicules  mensuels,  par  an  : Belgique,
                                   fr.  10.00; étranger, fr. i3.25.             J  43,564
                            Contribution n° 49.  Bibliograßhie du Droit  beige.                                         LA  B I B L I O T H È Q U E  COLLECTIVE  DES  SOCIÉTÉS  SAVANTES
                                                                                  3,275
                                                    Total des notices, environ.  .  .  5i2,o56























































































                                BRÜX -  —  IMP.  OSCAR  LAM BERTYj  70 .  RU E  VEYOT .  —  T É L .  « 8 4 9

