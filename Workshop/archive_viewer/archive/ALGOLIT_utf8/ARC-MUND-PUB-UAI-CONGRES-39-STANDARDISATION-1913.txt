






















                              C O I V G M . È S  M Q l Y D I A I i
                                        D M
               A S S O C I A T I O N S  I N T E R N A T I O N A L E S  N° 39
                          Deuxième  session  :  GANO-BRUXELLES,  15-18  juin  1913
                   Organisé  par 'Union  des Associations  Internationales
                              l
                         Office  central  :  Bruxelles,  3bis,  rue  de  la  Régence
                                                                       5«  Section
               Actes  du  Congrès.  —  Documents    préliminaires.







                                   The    Standardisation
                   and   the   Work      of  the   British     Engineering


                                  Standards       Committee




                                             [62  (017)]





                     The  British  Engineering  Standards  Committee  was  appointed
                                          I
                   in  1901  and  1902  by  the nstitution  of Civil  Engineers  the  Insti-
                   tution  of  Mechanical  Engineers,  the  Institution  of  Naval
                   Architects,  the  Iron  and  steel  Institute,  the  Institution  of
                   Electrical  Engineers.  The  work  done  by  the  Committee  was
                   described  in  a  full  report  presented  to  the  first  World  Congress
                  of  International  Associations  (1).
                     The  purpose  of  the  following,  is  to  give  some  complementary
                   information  on  the  progress  made  by  the  committee  since
                   1910  (2).



                    (1)  Actes  du  Congrès  Mondial,  1910,  p.  349.
                    (2)  F o r complete  information  see  the  seventh  and  eight  report  on
                  work  accomplished.  August  1912.  December  1912,  b y  order  of  the  C o m -
                  mittee,  J .  H .  T .  Tudsbery,  H o n . Secretary.  Leslie  S.  Roberstson,  Secre-
                  tary.























                                                                                                                                      —  3  —

                                                                                                              Electric  Tramway  Motors and Material.
                              S U B J E C T S  U N D E R  C O N S I D E R A T I O N                           Tubular  Tramway  Poles.
                                                                                                              Ammeters  and  Voltmeters.
                        The  work  undertaken  by  the  Committee  has  thus,  from                           Steel  Conduits  for  Electrical  Wiring.
                      time  to  time,  been  enlarged,  and  the  following  subjects  have                   Consumers'  Electric  Supply  Meters.
                      been,  or  are  now,  under  consideration  :—
                                                                                                              General  Grading  of  Voltages.
                        Rolled  Sections.                                                                     Screw Threads  for Automobile Work.
                        Railway  and Tramway  Rails  and  Fishplates.                                         Pipe  Union Joints.
                        Light  Flat  Bottom  and  Bridge  Rails.                                              Copper  Tubes  for  Screwed  Connections.
                        Railway  Rail  Fish  Bolts.                                                           Definitions  of  Yield  Point  and  Elastic  Limit.
                                        I
                        Locomotives  for ndian  Railways.                                                     Road  Material.
                        Pipe  Flanges.                                                                        Automobile  Parts.
                        Screw  Threads  and  Systems  of  Limit  Gauges  for  same.                           The  subjects  referred  to  above  are  mostly  completed,  but  i t
                        Pipe  Threads.                                                                      has  always  been  recognised,  from  the  inception  of  the  move-
                        Systems of  Limit  Gauges for Running Fits.                                         ment,  that,  to  avoid  stereotyping  present  practice,  revisions  will
                        Nuts,  Bolt  Heads  and  Spanners.                                                  from  time  to  time  be  necessary.
                        Bright  and  Black  Washers.                                                          To  meet  this  the  Main  Committee  has  made  a  ruling  that
                        Rolled  and  Drawn Bars  for use n Automatic  Machines.                             each  Sectional  Committee  shall  be  afforded  an  opportunity of
                                                     i
                        Small  Screws  and  Screw  Heads.                                                   meeting  at  least  once  a  year o  consider  whether  any  revisions
                                                                                                                                        t
                        Keys  and  Key  ways.                                                               in  the  Specifications  and  Reports  drafted  by t  are  desirable.
                                                                                                                                                      i
                        Railway  Rolling Stock Material.
                        Tyre  Profiles.
                        Steel  Castings  and  Forgings  for  Marine  Work.                                                 G O V E R N M E N T  S U P P O R T
                        Steel  for  Shipbuilding, and  Marine Boilers.
                        Steel  for Bridges  and  General  Building Construction.                              The  funds  necessary  for the  carrying  out  of  the  work  of  the
                                                                                                                                                                   I
                                             i
                        Wrought  Iron  for  use n Shipbuilding.                                             Committee  were  at  the  outset  supplied  by  the  supporting nsti-
                                                                                                                       i
                        Wrought  Iron  for Railway  Rolling  Stock.                                         tutions,  but t  was  recognised  at  anearly  date  that  the  support
                                 I
                        Steel  and ron  Boiler  Tubes.                                                      and  countenance  of  H .  M. Government  would  be  invaluable  to
                        Portland  Cement.                                                                   the  movement,  and  also  that  the  various  Government  Departe-
                        Vitrified  Ware  Pipes.                                                             ments,  who  were  large  users of the material?  for which  Standards
                        Cast-Iron  Pipes for various  purposes.                                             were  being  drawn  up,  should  be  asked  to  support  the  Com-
                        Generators,  Motors  and  Transformers.                                             mittee  :  —
                        Primes  Movers  for  Electrical  Purposes.                                            (1)  B y  nominating  representatives  to  assist  in  its  delibera-
                        Carbon  and  Metal Filament  Glow  Lamps.                                           tions,  and
                        Bayonet  Socket  Lamp  Holders  and  Caps.                                            (2)  B y  the  adoption  of  the  Committee's  Standards  when
                        High  Tension  Dielectrics.                                                         issued.
                        Insulating  Materials.                                                                                   C O M M I T T E E S
                        Telegraph  Material.
                        Electric  Cables.                                                                     The  Committee  composed  of  the  official  representatives  of  the
                        Hard  Drawn  Copper  and  Bronze  Wire.                                             five  supporting Institutions  is now known as the Main  Committee.























                                                —  4  —                                                                               —  5  —

                        To  this  Committee  falls  the  whole  of  the  organisation  of  the              of  Naval  work.  The  Board  of  Trade  has  issued  amended  Rules
                     work,  the  raising  of  the  necessary  funds,  the  controlling  of                  in  accordance  with  the  British  Standard  Specifications  for Boiler
                     the expenditure,  the  arranging  of  the  subjects  to  be  dealt  with               Steel,  Ingot  Steel  Forgings  and  Steel  Castings.  Lloyds  Register
                     by  the  various  Sectional  and  Sub-Committees,  and  the  passing                   has  adopted  the  British  Standard  Sections,  and  the  Standard
                     of  all  the  Reports  prior  to  publication.  After  deciding  to  deal              Specifications  for  Steel  for  Shipbuilding  and  Marine  Boilers
                     with  a  subject  the  Main  Committee  delegates  the  preparation of                 and  for  Steel  Castings  and  Forgings  for  Marine  purposes,  and
                                                                                                                                  i
                     the  Reports  and  Specifications  to  a  Sectional  Committee,  thè                   has  incorporated  these n  the  revised  edition of  its  Rules.  The
                     Chairman  of  which  is  appointed  by  the  Main  Committee.                          British  Corporation  for  the  Survey  and  Registry  of  Shipping,
                       Under  the Main  Committee  there  are  16  Sectional  Committees,                   and  the Bureau  Veritas  have likewise  given  effect  to the Commit-
                     appointed  by  the  Main  Committe,  and  29  Sub-Committees,                          tee's  recommandations  in  their  Rules.
                     appointed  by  the  Sectional  Committees,  with  a  total  member-                      The  British  Standard  Specification  for  Portland  Cement  con-
                     ship  of  363.                                                                         tinues  to  find  ever  increasing  use,  and  the  Cement  used  for  the
                       The  Sectional  Committees  consist  of  representatives  of  the                   Naval  Base  at  Rosyth  is  required  to  be  in  accordance  with  the
                     various  Government  Departments,  of  Consulting  Engineers,                         British  Standard  Specification.
                     Manufacturers,  Contractors  and  Users,  and  representatives  of                       It  appears  also hat  more  and  more  material  is  being  ordered
                                                                                                                            t
                     the  Technical  Societies  and  Trade  Associations  interested  in                   in  accordance  with  the  British  Standard  Specifications  for
                     or  affected  by  the  subjects  under  consideration.  The  Sectional                 Railway Material.
                     Committee  decides  the  broad  lines  upon which the  Specifications                    The  use  of  the  British  Standard  Tramway  Rails  by  Tramway
                                                                                                                                                                   r
                     shall  be  drawn  up,  leaving  the  detailed  work  of  drafting  to  a              Undertakings  is  maintained,  the  British  Standard  Sections ol
                     Sub-Committee  consisting  of  a  few  of  its  number.  Informa-                     led  during  the  year  ending  March,  1912,  amounting  to  over
                                                                                                                             t
                     tion  is  collected  by  means  of  Lists  of  Questions  addressed  to               73  per  cent,  of  the otal  tonnage  rolled.
                     those  particularly  interested,  and  if  necessary,  conferences  are                 The  British  Standard  Specifications  for  material  used  for
                     from  time  to  time  arranged.  When  the  draft  Specification  is                  Railway  Rolling  Stock  and  for  Structural  Steel  for  Bridges  and
                     prepared  it  is  submitted  to  the  Sectional  Committee  for  its                  General  Building  Construction,  in  common  with  other  of  the
                     detailed  consideration  and  when  approved  is  sent  on to  the  Main              British  Standard  Specifications,  have  been  adopted  by  the  De-
                     Committee  for  final  approval  and publication.                                     partment  of  the  Inspector  of  Iron  Structures  at  the  War  Office.
                                                                                                             The  London  County  Council, n  its  General  Powers  Act,  1909
                                                                                                                                         i
                                                                                                           (Part  4),  amending  the  London  Building Acts  so  as  to  provide
                                  A D O P T I O N  O F  S T A N D A R D S                                  for  the  construction  of  Steel  Frame  Buildings,  has  required  that
                                                                                                                             i
                                                                                                           all  rolled steel  used n the  construction of  skeleton  frameworks  of
                       The  Committee is  glad to  be  able  to  report  that  its  recommen-
                    dations  for  sizes  and  for  tests  of  materials  continue to  find  their          buildings  shall  comply  with  the  British  Standard  Specification
                     way  gradually  but  surely  into  very  general  use.                                for  Structural  Steel  for  Bridges  and  General  Building  Construc-
                       The  Admiralty has  adopted  the  British  Standard  Specification                  tion,  and  that  all  brickwork,  concrete,  stone, and  other  similar
                     for  Steel  for  Marine Boilers,  where  steel  of  the  tensile  strength            materials  used  in  conjunction  with  the  metal  frameworks  shall
                     dealt  with  therein is required, and  has  also adopted  the  Standard               be  executed  in  cement  conforming  with  the  British  Standard
                     Specification  for  Structural  Steel  for  Shipbuilding.  The  British               Specification  for  Portland  Cement.  The  Act  also  provides  that
                    Standard   Specifications  for  Steel  Castings  and  Forgings  for                    any  Structural  Metal  hereafter  standardised  by  the  Committee
                     Marine  purposes  have  also  been  adopted,  subject  to  certain                    may  be  used  in  the  erection  of  buildings, subject  to  such  terms
                                                                                                                                            t
                     additional  requirements  madenecessary  by  the  special  character                  and  conditions  as  the  Council  may hink  fit  to  attach.























                                                                                                                     \
                                                                                                                                      —  7  —
                       A  large  number  of  Indian  Standard  Locomotives  have  been                      neers,  and  is  the  British  Section  of  the  International  Electro-
                     built  to  the  Standard  types  and  despatched  to  that  country.                   technical  Commission)  a  Sectional  Committee,  entitled  as
                       Many   of  the  Colonial  Governments  are  finding  the  British                    above,  has  now  been  formed  with  Col.  R.  E.  B.  Crompton,  C.  B.,
                     Standard  Specifications  of  utility  and  are  increasingly  employing               as  Chairman.  Meetings  have  been  held  at  which  proposals
                     the  same,  the  Committee's  Specifications  being  substituted  for                  relating  to  an  International  Standard  for  High  Conductivity
                                   i
                     those  hitherto n  use.                                                                Copper,  and  the  Rating  of  Electical  Machinery,  have  been  con-
                                                                                                            sidered  in  conjunction  with  the  Sub-  Committees  of  the  Engi-
                                                                                                            neering  Standards  Committee  interested  therein.  The  proposals
                         P U B L I C A T I O N  A N D  R E V I S I O N  O F  R E P O R T S                  on  these  subjects  are, t  is  understood,  to  be  submitted  for  rati-
                                                                                                                                 i
                       From   the  inauguration  of  the  Committee  in  1901  down  to                     fication  at  the  next  meeting  of  the  International Electrotechni-
                                                                                                                                                                i
                                                                                                                                                       i
                     the  31st  July  1912,  57  Reports  and  Specifications  have  been                   cal  Commission  ( I . E.  C ) ,  which  is  to  be  held n Berlin n  1913.
                     issued.  They  deal  with  the  subjects  enumerated  on  pages  6                     I n  regard  to  the  suggestions  relating to  the  Rating  of  Electrical
                     to  8 and  a complete  list  is  given  on pages 73  to  84  of  the  Eight            Machinery,  the  views  of  the  Electrical  Manufacturers,  as  voiced
                     Report  of  the  Standards  Committee.                                                 by  the  British  Electrical  and  Allied  Manufacturers'  Association,
                                                                                                            will  receive  due  consideration.  The  suggestions  put  forward  by
                                                                                                            a  Special  Committee  of  the  I . E.  C.  are  to  be  re-considered  at
                     I N T E R N A T I O N A L  S T A N D A R D S  F O R  P I P E  T H R E A D S            Zurich  early  in  1913,  and  the  Sectional  Committee  will  have  a
                                                                                                            further  opportunity  of  considering  these  prior  to  their  being
                       No  definite  recommendations  have  yat  been  made  by  the                        submitted  for  ratification at  the  Berlin  meeting.
                     International  Congress  on  Pipe  Threads,  organised  by  the  So-
                     ciété  Technique  de  l'Industrie  du  Gaz  en  France,  and  in  the
                     meantime  the  Engineering  Standards  Committee  is  using  every                      I N T E R N A T I O N A L  S T A N D A R D S  F O R  E L E C T R I C A L
                     endeavour  to  impress  upon  the  Congress  the  superiority  of  the                                        M A C H I N E R Y
                     British  Standard  Pipe  Theards.  W i t h  the  co-operation  and
                     assistance  of  Sir  William  Lindley,  the  German  delegates  have                     As  mantioned  above,  the  Engineering  Standards  Committee
                     arranged  for  a  series  of  comparative  tests  to  be  carried  out n               is  co-operating  with  the  Institution  of  Electrical  Engineers  and
                                                                               i
                                                 i
                     Germany,  the  results  of  which t  is  hoped  to  place before  the  Con-            the  British  Electrical  and  Allied  Manufectureis  Association  in
                                                                                                                 i
                     gress  at  their  next  meeting.                                                       this mportant work,  and  the  result  of  this  co-operation  should
                       In  the  meantime,  at  the  suggestion  of  the  Committee,  the                    prove,  of  considerable  value  to  the  Electrical  Industry  at  large.
                     representatives  of  the  Pipe  Trade  are  having  a  similar  set  of
                     experiments  carried  out n this  country,  and t  is  proposed  that,
                                                               i
                                            i
                     in  addition  to  presenting  the  results  of  these  tests  to  the  next                  I N T E R N A T I O N A L  S T A N D A R D  F O R  H I G H
                     meeting  of  the  Congress,  a  duplicate  series  shall  be  carried  out                     C O N D U C T I V I T Y .  A N N E A L E D  C O P P E R
                                           i
                     in  a testing  Laboratory n Paris, n the  presence of  the  delegates.
                                                    i
                                                                                                                   i
                                                                                                              This mportant subject  relates  purely  to  the  electrical  conduc-
                                                                                                            tivity  and  resistance  of  Copper,  and  forms  the!basis  of  the  Bri-
                                                                                                            tish  Standard  Tables  for  Copper  Conductors  (Publication  N°  7,
                     C O N F E R E N C E  O N  I N T E R N A T I O N A L  E L E C T R I C A L               see  page  74).  As  far,  however,  as  these  Tables  are  concerned,
                                        S T A N D A R D I S A T I O N                                       the  fact  that  the  adoption  of  the  proposed  International  Stan-
                                                                                                                                        i
                       At  the  request  of  the  British  Electrotcchnical  Committee                      dard  implies  a  modification n  the  present  value,  of  onlyone-
                      (which  was  appointed  by  the  Institution  of  Electrical  Engi-                   eighth  of  one  per  cent.,  makes  the  alteration  one  that  can  be






















                                                 -  8   -                                                                      —  9  —

                                                                                                                    i
                                                                                                                                i
                       accepted  with  very  little  disturbance  to  the  present  values.          rials  to  be  held n New  York n  the  Autumn  of  1912,  the  names
                                 i
                       Moreover, n view  of  the importance of  reaching  an  international          of  Dr.  W.  C.  Unwin,  Dr  Archibald Denny,  members  of  the  Main
                       agreement,  it  is  hoped  that  this  proposal  which, emanated  from        Committee,  and  Mr. Bertram  Blount, a member  of  the  Sectional
                       the  Bureau  of  Standards  of  America  in  conjunction  with  the           Committee  on  Cement,  were  put  forward,  and  these  gentlemen
                       National  Physical  Laboratory  and  the  Laboratoires  of  other             proceeded  to  the  United  States  to  represent  the  British  Govern-
                       Nations, may  be  accepted  at  Berlin n  1913,  at  the  plenary  mee-       ment  at  the  Congress.  The  Committee  was  represented  by  the
                                                        i
                                                                                                                                          i
                       ting  of the  International  Electrotechnical Commission.                     Secretary,  who  attended  the  Congress n  an  advisory  capacity
                                                                                                     to  the  British  Delegates.  One  salient  feature  which  was  evi-
                                                                                                     dent at  the Congresses at  Copenhagen,  Brussels and  Paris  was  the
                               C O N T I N U A N C E  O F  O R G A N I S A T I O N                   advantage  which his  country  enjoys  over  most  of  the  Conti-
                                                                                                                      t
                                                                                                                     i
                         I t  will  be  seen from  the  general  tenor of  the above  report  that,  nental  countries n the  possession  of  a  central  organisation on
                      though  much  time  has  been  occupied n preparing new  Standard              which  all  the  interests  concerned  are  represented  and  to  which
                                                         i
                      Specifications  for  various  manufactured  articles,  a  very  large           reference  can  be  made  and  information  obtained  as  to  the  rea-
                      amount  of  the  Committee's  work  has  consisted  in  issuing  addi-          sons which led  to  the  determination of  the  present  British  Stan-
                      tions  and  emendations  of  the  Specifications  already  issued  bX           dards.
                      them,  which were  demanded  by  experience  and  by  new  develop-
                      ments.   I t  is now  quite clear,  as  was  anticipated when  the work
                                                                  i
                                                     i
                      of  the  Committee  was  originated n  1901,  that t  is  necessary  to
                      review  from  time  to  time  the  original  Standard  Specifications,
                      and  that,  unless  this be  a  recognised  part of  the work of the  Com-
                      mittee,  the  process  of  standardisation might  lead  to  undesirable
                      stereotyping  of  procedure,  checking  invention  and  impeding
                      improvements.   Experience  has  made  it  abundantly  evident
                      that  the  work  of  the  Committee  should  not  be  regarded  as  final
                      in  any  subject,  but  that  the  organisation of  the Committee  and
                      Sub-Committees  should  be  maintained  as  at  present,  so  that
                      revisions  may  be  promptly  and  efficiently  dealt  with.  Unless
                      provision  is  made  for  carrying  out  such  necessary  revisions  by
                      Sectional  Committees  already  familiar  with  the  various  aspects
                      of  the  questions  dealt  with,  the great  practical value  of the work
                      already  performed  by  the  Committee  would  be  most  seriously
                                                                                                                                                               >
                      jeopardised.
                        A  comparatively  new  branch  of  the  Committee's  activities
                                               i
                      is  the  co-operation  which t  has  been  asked  to  extend  to  the
                      subject  of  International  Standardisation, and the  Secretary  has
                      attended n an advisory  capacity  Congresses held at  Copenhagen,
                              i
                      Brussels  and  Paris.  The  Board  of  Trade  having  asked  the
                      Main  Committee  to  suggest  the  names  of  three  delegates  to  the
                      6th  Congress  of  the  International  Associations  for Testing  Mate-

