Office International de   Bibliographie
PUBLIC?ION No II
r
CLASSIFICATION llECIMALE
.
DES
Sciences  Astronomiques·
INTRODUCTION
TABLES & INDEX    ALPHABETIQUE
J,
[025'1 ? z../
BRUXELLE?
Office International de  GEORGES BALATI Editeur
,Biblio.graphie I
RUE DU MUSEE, I  RUE POTAGERE, 57
INTRODUC·TION
La Bibliographia Astronomica et la Bibliographia Universalis,
.          .. )
.
La Bibliogl'aphia asironomica a pour objet de publier regulierement
la bibliographie des travaux scientifiques paraissantau jour le jour
·dans tous les pays et relatifs a l'Astl'onomie; la Meteol'ologie, la Geodesi« et
-Ia Physique dlt Globe, que ces ouvrnges.aient pam so us forme de Iivres
-ou qu'ils aient ete publies dans des revues ou des memo ires de
societes savantes.
La Bibliographia Asironomica fait partie integrante du Repertoire
bibliographique universel Oil Bibliog1'aphia Universalis, dont l'Office
international de Bibliographie poursuit l'elaboration. Ce repertoire,
qui comprendra la Bibliographie de l'ensemble des sciences, est orga­
nise sur les bases suivantes :
1° Division du repertoire en publications distinctes .correspondant a
chaque branche de science. Preparation de ces bibliographies par des
groupes speciaux SOllS la direction generale de l'Office international
de Bibliographie;
2° Publication des titres bibliographiques sous une forme telle qu'ils
puissent servir directement a la creation de repertoires sur fiches. De
tels repertoires sont seuls susceptibles d'etre tenus constamment a
jour grace a l'intercalation continue de nouvelles fiches, et toutes les
erreurs ou omissions peuvent y etre aisernent reparees, En outre, les
notes personnelles. peuvent completer celles fournies par les
bibliographies;
30 Classement des titres bibliographiques selon l'ordre methcdique
des matieres a l'aide de nombres classificateurs conventionnels, La
signification de ces nombres est deterrninee par des tables traduites
dans toutes les langues et tenues constamment au courant des neces­
sites scientifiques par l'Office international.
Ces tables ont pour base la Classification decimal» de M. Dewey qui
-5-
a ete adoptee com me classement international par la Conference
bibliographiq ue internationale de 1895 et a ete depuis developpee
en plusieurs de ses parties.
La classification decimale des Sciences astronomiques.
Le principe de la classification par matiere qui sert de base a la
Bibliographia Asironomica est l'attribution a chaque sujet d'un nombre
classificateur toujours identique. Ainsi les travaux relatifs aux teles­
copes auront toujours Ie nombre 52.23, C(;UX relatifs a la lune 52.33.
Un index alphabetique consulte au 1110t Telescope ou Lune referera v
aces nombres. En procedant ainsi, les memes recueils bibliogra­
phiques ont ete rendus egalement accessibles aux personnes qui'?
parlent des langues differentes. Les mots de classement etant rem­
places par un simple numerotage, il a suffi de traduire l'index alpha- ,
betique en plusieurs langues. Ainsi les Anglais, en cherchant au mot
Moon, sont renvoyes a 52.33, et les Allemands aussi en consultant le
mot Moud, Ces nombres ont aussi l'avantage de centraliser au merne
siege toute la bibliographie d'une matiere, quelque nombreux que
soient les synonymes et les equivalents d'un meme mot, et cela parce
qu'ils expriment non pas des mots, mais des idees. Ces nombres
sont concis et cela est d'autant plus precieux que beaucoup
de questions bibliQgraphiques sont trop complexes pour etre
exprimees autrement qu'en une periphrase, Enfin les nombres classi­
ficateurs de la classification decimale ont encore un autre avantage.
ete attribues arbitrairement aux divers sujets, mais bien
Ils n'ont pas
suivant certaines regles dont l'observation a eu pour resultat heureux
de substituer, a I'aparpil lement des sujets et des questions de meme
famille, selon tous les hasards de l'ordre alphabetique, un classement
grace auquel les rnatieres connexes se trouvent groupees ensemble,
dans le voisinage les unes des autres. En effet, sous sa simplicite
-
apparente, 52.33 ne signifie pas autre chose que
5e classe.  Sciences pures.
2e division.  Astronomie.
3e section.   Astronomie pratique.
3e groupe   Lune.
52.33
-6-
-6-
C'est pourquoi cette classification est dite decimale et chacun de ses
nornbres est traite comme un nornbre decimal. Les nouveaux chiffres
qu'on ajoute a la droite de ceux qui forment deja Ie nornbre 52.33 ne
changent rien a la valeur de ceux-ci mais marquent simplernent
des divisions nouvelles du sujet. En effet, la table methodiq ue est
eta blie ainsi :
52.33 Lune.
52.33r Constantes de la lune. Distance et parallaxe.
52.332 Chaleur et lumiere, Phases.
52.333 Orbites et mouvements, etc., etc.
En formant les tables de la classification bibliographique, on a
done etabli une repartition conventionnelle et purement bibliogra­
phique des connaissances hurnaines en dix classes, puis on a divise
chaque classe en dix divisions et chaque division en dix sections et
ainsi de suite selon le degre de specialisation jugee necessaire. Le
principe de la decirnalisation a en pour consequence de laisser la
porte toujours ouverte a de nouvelles subdivisions que le develcppe­
ment ulterieur des sciences rendrait necessaire, D'autre part, ce prin­
cipe n'a pas ete aussi tyrannique qu'on pourrait le croire a premiere
vue: lorsqu'il y avait lieu de diviser un sujet en moins de dix parties,
on a employe que quelques-uns des dix chiffres dont on pouvait
disposer; au contraire, quand il y avait plus de dix parties, on a reuni
sous une denomination collective deux ou plusieurs de ces parties et
on a subdivise a nouveau la rubrique ainsi formee.
En resume, la classification decirnale ne constitue qu'un simple
numerotage des diverses idees ou sujets bibliographiques. Ce nurne­
rotage s'irnpose a raison des diversites de langages, des diversites de
synonymes, et d'equivalents d'une meme idee dans la merne langue, a
raison aussi de la necessite de grouper ensemble des sujets de merne
famille qui demeureraient autrement eparpilles au hasard de l'ordre
alphabetique des mots souches. Ainsi les nombres c1assificateurs sont
de veritables diitominatczH'S conimuns des formes uerbales dioerses d'une mem«
idee et, comme tels, assurent des classements bases sur les idees et non
sur les mots.
Grace aces nombres c1assificateurs, consulter une bibliographie
qui en fait application, exige une operation aussi simple que si, dans
un traite d'astronomie par exemple, on consultait l'index au mot Mars
et qu'on y trouvait indiquee la page 325 comme siege du sujet. Les
repertoires bibliographiqu€s universels ne sont eux-rnemes que des
grands livres, dont les feuillets sont des fiches mobiles paginees dans
l'ordre des nombres classificateurs Les tables alphabetiques de la
..
classification decimale, veritable index de ce livre, consultees au
meme mot Mars, renvoient a 52.34.3 com me a une page ordinaire.
Inversement, pour connaitre Ie sens du nombre 52.343, il suffit de se
reporter a la table methodique ou tous les nombres sont ranges dans
leur ordre naturel avec, en regard, les idees et sujets qui y
correspondent.
Dans les tables et dans l'index qui suivent, la partie de la Classifi­
cation decirnale qui concerne les Sciences astronomiques et meteoro­
logiques a seule ete imprirnee. On y a ajoute toutefois un tableau
resume qui montre la place occupee par ces sciences dans j'ensemble
de la classification, ainsi que les nornbres relatifs a certaines ques­
tions qui ont des rapports avec elle.
De ce que la Classification decimale ernbrasse I'ensemble des
sciences et de leurs divisions et que les bibliographies speciales sont
con?ues comme des parties integrantes du Repertoire bibliographique
universel, il suit que tout titre bibliographique est classe au siege
principal de chaque matiere. La division des sciences, en sciences
principales et en sciences auxiliaires, n'a donc plus ici de raison
d'etre et la Bibliographia A strouomica se sert, 'pour classer ses mate­
riaux, des nombres propres a J'astronomie et de tous les autre's
nombres de la classification. Des references nombreuses etablies
entre toutes les parties de la classification facilitent et coordonnent les
recherches.
Les subdivisions des diverses parties d'une science sont souvent
identiques les unes aux autres. Dans la classification decirnale on a
utilise cette particularite pour etablir autant que possible des develop­
pements symetriques entre ces parties. On remarquera cette syrnetrie
a l'Astronomie descriptive, par exemple, OU les subdivisions de la
Lune, du Soleil et des Etoiles sont similaires :
52.33 Lune .   52.37 Soleil.
. 33 I Constantes.  .371 Constantes.
.332 Chaleur et lurniere. .372 Chaleur tit lumiere.
.337 Spectre.   .377 Spectre.
De telles concordances et syrnetries existent parfois entre les parties
les plus differentes de la classification decimale. C'est ainsi que les
8-
generalites de chaque science sont toujours marquees.par 0 d que ce 0
se divise presque toujours de la meme maniere en
.. OI Theories generales, philosophic de
·      ...
.. 02 Manuels, traites  etc
·   generaux, ...
. 05 Periodiques, revues de
..    .
· .. 09 Histoire de telle Science .
La plupart de ces concordances, ne sont qu'approximatives. Cepen­
dant un grand nombre de sujets etant divisibles a certains points de vue,
toujours les memes, ces divisions-I a ont ete exprimees regulierernent
d'une maniere identique. Il en est ainsi des divisions geographiques qui
trouvent . plusieurs applications en astronomic .. Un nom bre, suivi
imrnediatement d'un autre nombre inscrit entre parentheses, repre­
sujet
sente la division de ce a un point de vue geographique. Ainsi
52.09 Histoire de l'Astronomie.
52.09 (4)    en Europe.
52.09 (44)   en France.
52.09 (5)    en Asie.
Les nornbres inscrits entre parentheses cgalement, decimaux, sont
ernpruntes aux divisions de la geographic et conservent le sens qu'ils
ont dans cette  de la classification. Dans l'ordre de
re<;:u  partie
sequence des chiffres qu'il faut suivre pour avoir un classernent stricte­
ment decimal, et conforme aux tables, chaque moitie de la parenthese
doit etre traitee comme s'il s'agissait d'lI11 onzierne chiffre qui aurait
place dans Ja serie numerique entre le 0 et le I, de la maniere sui vante :
0, (, ), I, 2, 3, 4.
Les nombres classificateurs des sciences astronomiques ont en
moyenne six a sept chiffres. Cette longueur s'ex plique, d'abord parce
les tables ne comportent pas moins de six cents divisions differentes rela­
tives aces seules sciences, ensuite parce que chacune de ces divisions
est exprirnee en fonction de toutes Jes autres divisions et subdivisions
de la classification decimale qui embrasse, elle, toutes Jes connais­
sances. Ainsi, les nombres classificateurs de l'Astronomie se com­
posent en realite de deux parties. La premiere est invariable et traduit
la notion Astronomic, tout en marquant la place attribuee a cette science
dans l'ensemble de la classification (52 : Astronomie, placee entre 5I
Mathematiques. et 53 : Physique). La seconde partie de chaque
nombre marque les divisions propres a l'Astronomie :
52.0 Astronomie en general.
52.1 Astronomie theorique.
52.2 Astronomie pratique.
52.3 Astronomie descriptive.
Pour concilier les exigences d'une classification generale avec celles
d'une classification pai ticuliere, on a isole par un point de ponctuation
les deux premiers chiffres 52 communs a toute l'Astronomie, et les
quatre premiers chiffres 5515 communs a toute la Meteorologie, de
maniere a faire lire d'un seul coup d'ceil ces groupes de chiffres COI1-
stants et mieux attirer l'attention sur les chiffres differencies,
52.1 ; 52.133; 5515·47
Modes divers d'utilisation de la Bihliographia Astronomica
Par le mode de publication, la disposition typographique et l'emploi
des nombres classificateurs, on a cherche a faire de la Bibliogl,aphia
Astronomica tout a la fois :
a) Une bibliographie complete en elle-rneme et dont on puisse se
servir directement et sans difficulte sous la forme de fascicules q u'elle
a revetu,
b) Une partie integrante du Repertoire Bibliographique Universe!'
c) Un recueil de materiaux bibliographiques utilisables presque
directement pour enrichir et completer les repertoires et les catalogues
de formes tres diveres qui existent deja.
La Bibliographia Astronomica parait par fascicules periodiques qui
peuvent etre reunis en volumes ou servir a la formation de fiches
bibliographiques. A cet effet, l'impression en est faite seulement sur
Ie recto des pages, et le verso demeure blanc. Les titres de chaque
travail ont des mentions completes et abregees au minimum, lIs
peuvent done etre decoupes et colles separement sur fiches.
La justification, assez etroite, qui a ete adoptee (9 centimetres de
largeur) permet de se servir a cet effet de fiches de formats tres divers,
selon les usages et les besoins particuliers et d'ajouter les fiches, ainsi
confectionnees, a celles des catalogues manuscrits existants.
Au titre de chaque travail est ajoute le nombre classificateur corrcs­
pondant au sujet auquelle travail se rapporte. Reunis en fascicules,
10-
-
